"""mspanel URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import include, url, patterns
from django.contrib import admin

from main import views

from adminplus.sites import AdminSitePlus
admin.site = AdminSitePlus()
admin.autodiscover()

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^geo/$', 'main.views.target_geo_search'),
    url(r'^request/log/$', 'request.views.log'),
    url(r'^proxy/unactivate/(?P<port>[0-9]+)/', 'main.views.proxy_port_unactivate'),
    url(r'^proxy/activate/(?P<port>[0-9]+)/', 'main.views.proxy_port_activate'),
    url(r'^proxy/stat/', 'main.views.proxy_stat'),
    url(r'^mailing/(?P<mid>[0-9]+)/pic/', 'main.views.mailing_add_url', {'mode': 'pic'}),
    url(r'^mailing/(?P<mid>[0-9]+)/link/', 'main.views.mailing_add_url', {'mode': 'link'}),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )