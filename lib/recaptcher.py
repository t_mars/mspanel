#coding=utf8
import random
import sys
import os
from antigate import AntiGate, AntiGateError
from main.models import Config

class ZeroBalanceException(Exception):
    pass

class ReCaptcher():
    def __init__(self, names={'antigate': {}, 'rucaptcha': {}}, debug=False, try_count=3):
        self.idents = {}
        self.counts = {}
        self.configs = {}
        self.debug = debug
        self.try_count = try_count

        for name, config in names.iteritems():
            k = 'account_reg.%s_key' % name
            keyval = Config.GET(k)
            if not keyval:
                raise Exception('captcha identifiers not setted in Config: %s' % k)
            self.idents[name] = keyval
            self.configs[name] = config
            self.counts[name] = {'try_count': 0}

    def get(self, filename):
        for _ in xrange(self.try_count):
            try:
                return self._get(filename)

            except ZeroBalanceException as e:
                raise e

            except Exception as e:
                # другое какое то исключение
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print 'captcha exception', exc_type, fname, exc_tb.tb_lineno, str(e)
                continue

    def _get(self, filename):
        idents = self.idents.items()
        random.shuffle(idents)
        res = None
        for name, key in idents:
            self.inc_count(name, 'try_count')
            
            aname = 'get_%s' % name
            if not hasattr(self, aname):
                print 'WARNING: has no %s method' % name
                continue

            if self.debug:
                print name

            flag, res = getattr(self, aname)(filename)
            if self.debug:
                print flag, res

            if flag:
                return str(res)
            else:
                self.inc_count(name, res)
        
        if res == 'ERROR_ZERO_BALANCE':
            raise ZeroBalanceException()

        return False

    def get_antigate(self, filename):
        a = 'antigate'
        try:
            val = AntiGate(self.idents[a], filename, **self.configs[a])
            return True, val
        except AntiGateError as e:
            return False, str(e)

    def get_rucaptcha(self, filename):
        a = 'rucaptcha'
        try:
            val = AntiGate(self.idents[a], filename, domain='rucaptcha.com', **self.configs[a])
            return True, val
        except AntiGateError as e:
            return False, str(e)

    def inc_count(self, name, key):
        if key not in self.counts[name]:
            self.counts[name][key] = 0
        self.counts[name][key] += 1