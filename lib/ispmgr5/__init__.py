from lib.mygrab import MyGrab
import demjson

class ISP_API:
    
    def __init__(self, url, username, password):
        self.url = url
        self.grab = MyGrab()
        self.auth(username, password)
        self.last_error = ''

    def auth(self, username, password):
        params = {
            'func': 'auth',
            'lang': 'ru',
            'username': username,
            'password': password,
        }
        
        self.grab.go(
            "%s?func=logon" % (self.url),
            post=params    
        )
        if not self.grab.get_cookie('ispmgrses5'):
            RuntimeError('Authorization error.')

    def parse_result(self):
        try:
            data = demjson.decode(self.grab.doc.body)
        except:
            return False
        
        # print data.keys()
        # print data['msg']

        if 'ok' in data:
            return True
        else:
            self.last_error = data['ermsg']
            return False

    def add_domain(self, domain, owner, docroot):
        params = {
            'func':'webdomain.edit',
            'name':domain,
            'currname':domain,
            'php_enable':'on',
            'userexperience':'expert',
            'aliases':'www.%s' % domain,
            'zoom-aliases':'www.%s' % domain,
            'home':docroot,
            'ipsrc':'auto',
            'email':'admin@%s' % domain,
            'emailcreate':'off',
            'charset':'off',
            'dirindex':'index.html index.php',
            'secure':'off',
            'redirect_http':'off',
            'strict_ssl':'on',
            'ssl_port':'443',
            'ssl_cert':'selfsigned',
            'ssi':'on',
            'autosubdomain':'off',
            'php':'on',
            'php_mode':'php_mode_mod',
            'php_cgi_version':'native',
            'php_native_version':'5.4.42',
            'cgi':'off',
            'log_access':'on',
            'log_error':'on',
            'show_params':'yes',
            'rotation_period':'every_day',
            'rotation_size':'100',
            'rotation_count':'10',
            'analyzer':'off',
            'analyzer_period':'rotate',
            'analyzer_secure':'on',
            'srv_gzip':'off',
            'gzip_level':'1',
            'show_cache':'on',
            'srv_cache':'off',
            'expire_times':'expire_times_s',
            'clicked_button':'ok',
            'sok':'ok',
            'sfrom':'ajax',
        }
        self.grab.go(
            self.url,
            post=params 
        )
        return self.parse_result()

    def delete_domain(self, domain):
        self.grab.go(
            '%s?func=webdomain.delete&tconvert=punycode&elid=%s&plid=&sfrom=ajax' % (self.url, domain)
        )
        return self.parse_result()