#coding=utf8
import uuid
from grab import Grab
from antigate import AntiGate, AntiGateError

from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
def ident_captcha_antigate(filename):
    try:
        val = AntiGate('1fe011ef5fb13b6ccde3116800cd6c6d', filename)
        return True, str(val)
    except AntiGateError as e:
        return False, str(e)

class DreamHostClient():
    INDEX_URL = 'https://panel.dreamhost.com/index.cgi'
    PANEL_URL = 'https://panel.dreamhost.com/'

    def __init__(self, login, password, debug=False):
        from lib.mygrab import MyGrab
        self.login = login
        self.password = password
        
        self.grab = MyGrab()

        self.log_ind = 0
        self.debug = debug
        if self.debug:
            print 'DEBUG ON'

        # для запроса куки
        self.grab.go(self.PANEL_URL)

        # авторизация
        post = {
            'username':login,
            'password':password,
            'Nscmd':'Nlogin',
        }
        self.grab.go(self.INDEX_URL, post=post)

        # авторизируем браузер
        self.display = None
        self.browser = None
        
    def init_browser(self):
        if self.browser:
            return        
        
        self.display = Display(visible=0, size=(1024, 768))
        self.display.start()

        self.browser = webdriver.Firefox()
        self.browser.get(self.INDEX_URL)
        self.browser.find_element_by_id('username').send_keys(self.login)
        self.browser.find_element_by_id('password').send_keys(self.password)
        self.browser.find_element_by_xpath('//button[@type="submit"]').click()

    def close(self):
        if self.display:
            self.display.stop()
        if self.browser:
            self.browser.quit()

    def parse(self):
        error = self.grab.doc.select('//div[@class="errorbox"]//div[@class="body"]')
        if error:
            if self.debug:
                print 'errror =', error.text()
            return False, error.text()
        
        success = self.grab.doc.select('//div[@class="successbox"]//div[@class="body"]')
        if success:
            if self.debug:
                print 'success =', success.text()
            return True, success.text()
        
        return None, None

    def parse_browser(self):
        try:
            error = self.browser.find_element_by_xpath('//div[@class="errorbox"]//div[@class="body"]')
            # print 'errror =', error.text
            return False, error.text
        except:
            pass
        
        try:
            success = self.browser.find_element_by_xpath('//div[@class="successbox"]//div[@class="body"]')
            # print 'success ='#, success.text
            return True, success.text
        except:
            pass

        return None, None

    def get_security_key(self, url):
        self.grab.go(self.INDEX_URL+url)
        inp = self.grab.doc.select('//input[@name="security_key"]')
        if inp:
            return inp.attr('value')
    
    def add_domain(self, domain, docroot):
        self.init_browser()

        self.browser.get(self.INDEX_URL + '?tree=domain.manage&current_step=Index&next_step=ShowAddhttp&domain=')
        
        # 'domain'
        self.browser.find_element_by_id('cgi-domain').send_keys(domain)

        # 'www'
        self.browser.find_element_by_xpath('//input[@name="www_redir" and @value="0"]').click()

        # 'user'
        self.browser.find_element_by_xpath('//select[@id="usid"]/option[last()]').click()

        # 'docroot'
        e = self.browser.find_element_by_id('docroot')
        e.send_keys(Keys.CONTROL, 'a')
        e.send_keys(docroot)
        
        # 'mod_security'
        # self.browser.find_element_by_id('mod_security').get_attribute('checked')
        self.browser.find_element_by_id('mod_security').click()
        # self.browser.find_element_by_id('mod_security').get_attribute('checked')

        try:
            image = self.browser.find_element_by_id('recaptcha_challenge_image').get_attribute('src')
        except:
            image = None
        
        if image:
            image_file = '/tmp/%s.png' % uuid.uuid1()
            self.log_ind += 1
            self.grab.go(image, log_file=image_file)
            self.grab.setup(log_file='1')

            flag, value = ident_captcha_antigate(image_file)
            print 'image =', image
            print 'value =', value
            self.browser.find_element_by_id('recaptcha_response_field').send_keys(value)
            
        self.browser.find_element_by_id('submit_fully_hosted').click()

        flag, msg = self.parse_browser()
        if flag == False:
            if "already in our system" in msg.lower():
                return True
            print 'error =', msg
            return False
        return True

    def add_domain_old(self, domain, docroot):
        # получаем ключи
        security_key = self.get_security_key('?tree=domain.manage&current_step=Index&next_step=ShowAddhttp')
        usid = self.grab.doc.select('//select[@id="usid"]/option')[-1].attr('value')

        add_post = {
            'current_step':'ShowAddhttp',
            'next_step':'AddHttp',
            'security_key':security_key,
            'tree':'domain.manage',
            'domain_type':'cgi',
            'domain':domain,
            'www_redir':'0',
            'usid':usid,
            'newuser_username':'',
            'newuser_machine':'benjy',
            'docroot':docroot,
            'php_mode':'fcgi56',
            'php_upgrade':'1',
            'cfuid':'new',
        }
        
        iframe = self.grab.doc.select('//iframe[starts-with(@src,"https://www.google.com/recaptcha/api/noscript")]')
        if iframe:
            iframe = iframe.attr('src')
            print 'iframe =',iframe
            headers = {
                # 'cookie':'SID=DQAAABoBAADO7z0uNoC4QajxfmugB39oRV7kdQJ5CPMoJeAtEoPV7dHERXZmzXFxumelEPrcH3bvLKCtDiDveQ9jq7P9AjQePv5Ki6hgpl9nEBtO8O-9AZKrxFexF6xtXfcH-GCkcyaifV7Jn0dXworT6iqJ8cyNMkLwi6Q8kwFYYUjJNehd5iU0P53UWjPk1bxNFoZ7Ha9a5zcwj4YL9Q150KbMeH-lkelgRqJ5p3kodDzR5p27MXRfIWREZS577PaQeLiHUTmPqcMpIEYoBLjuQfJMroAyS2q7oHgorQNrOIAa5IA1ZfJ8n4pcgi1WdgpqS0-BqEHPCUeA3U07iQzHKn8DSPenIlGwaYyLL85JWYx9YVqtB7b3c19ARo3s_dTUNt6JhFM; HSID=AYz51XniHyZUtShpa; SSID=A848d7nZdtbU6IH5y; APISID=K9wvr7qa3JgEJCIE/AF8EOJKeKs9_HDZBC; SAPISID=P72mMFWEpG6kns5v/AxKP5J-TTyUrLHixz; PREF=ID=1111111111111111:FF=0:LD=ru:TM=1442060871:LM=1442090597:GM=1:V=1:S=jSCcJ4bLo-M8Fspb; NID=71=nUwg7Rl1tWlArmlQAWmDxU53D1cPUub0YRGaXSDcS_e07JOQayyYZXnQ7kBHdgdPWJnjpEJ_0iVMORsZF8bKR8J5vMZ8YKejVXztkdySnp3X1AcFOIo9ZX3VTcKVtBg9PXe6dK9jAn2xa2GB0I-EI0N9zwFEYuh_OOHG2U4',
                # 'x-client-data': 'CJC2yQEIorbJAQiptskBCMG2yQEI74jKAQj9lcoB',
                # 'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36',
            }
            self.grab.go(iframe, headers=headers)
            image = 'https://www.google.com/recaptcha/api/' + self.grab.doc.select('//img').attr('src')
            field = self.grab.doc.select('//input[@name="recaptcha_challenge_field"]').attr('value')
            
            # field, image = self.get_captcha()
            
            print 'field =',field
            print 'proxy =',self.grab.proxy
            print 'image =',image

            image_file = '/var/www/mspanel/private/image%d.png' % self.log_ind
            self.log_ind += 1
            self.grab.reset_proxy()
            self.grab.go(image, log_file=image_file)
            self.grab.setup(log_file='1')

            flag, value = ident_captcha_antigate(image_file)
            if self.debug:
                print 'value =', value

            post = {
                'recaptcha_challenge_field': field,
                'recaptcha_response_field': value,
                'submit': '1',
            }
            self.grab.go(iframe, post=post)
            try:
                textarea = self.grab.doc.select('//textarea').text()
            except:
                return False
            
            add_post['recaptcha_challenge_field'] = textarea
            add_post['recaptcha_response_field'] = 'manual_challenge'
    
        # add domain
        self.grab.go(self.INDEX_URL, post=add_post)
        if self.debug:
            print 'add domain'
        flag, msg = self.parse()
        if not flag:
            return False

        self.refresh_ns(domain)
        return True

    def refresh_ns(self, domain):
        # получаем ключи
        security_key = self.get_security_key('?tree=domain.manage&current_step=Index&next_step=ShowZone&domain='+domain)
        if not security_key:
            return False
        
        ip = self.grab.doc.select('//td[@title="Comment: main ip (from http service)"]')
        if not ip:
            return False

        # refresh ns
        post = {
            'current_step':'ShowZone',
            'next_step':'RefreshNS',
            'security_key':security_key,
            'tree':'domain.manage',
        }
        self.grab.go(self.INDEX_URL, post=post)
        if self.debug:
            print 'refresh ns'
        flag, msg = self.parse()
        if not flag:
            return False
        return ip.text()

    def delete_domain(self, domain):
        security_key = self.get_security_key('?tree=domain.manage&current_step=Index&next_step=ShowDestroyDomain&domain=' + domain)

        post = {
            'current_step':'ShowDestroyDomain',
            'security_key':security_key,
            'next_step':'DestroyDomain',
            'tree':'domain.manage',
            'confirm':'on',
        }
        self.grab.go(self.INDEX_URL, post=post)
        if self.debug:
            print 'delete domain'
        flag, msg = self.parse()
        if not flag:
            return False
        return True

if __name__ == '__main__':
    api = DreamHostClient('cantros.com@gmail.com', 'Mgu565826', debug=True)

    grab = Grab()
    grab.setup(debug=True)
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:41.0) Gecko/20100101 Firefox/41.0',
        'Host': 'www.google.com',
        'Connection': 'keep-alive',
        'Cache-Control': 'max-age=0',
        'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
        'Accept-Encoding': 'gzip, deflate',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Referer': '',
    }
    headers = {
        'Host': 'www.google.com:443',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, sdch',
        'Accept-Language': 'ru,en;q=0.8,az;q=0.6',
        # 'Cookie': 'NID=71=i3PirmLlmVIptiXFeB2guxbbP6zPz62fciI6uD-LUQ8f2CclebWQvAkDw31Q3Tp_b4UcpL1lDDQtwUBWx1RqFDG5AkDob3a2LLyuEvYigFDHOLWz5nX2oh4ANDtBG-hP; PREF=ID=1111111111111111:TM=1442944172:LM=1442944172:V=1:S=fRTeGozbEccZyiyL',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36',
        # 'X-Client-Data': 'CJC2yQEIorbJAQiptskBCMG2yQEI74jKAQj9lcoB',
        'Keep-Alive': '',
        'Accept-Charset': '',
    }
    grab.go('https://www.google.com/recaptcha/api/noscript?k=6LfFNAIAAAAAAKcHjSK6GNsmZJZe9nBlVmxNwDkD', 
        headers=headers)
    print grab.request_headers

    image = 'https://www.google.com/recaptcha/api/' + grab.doc.select('//img').attr('src')
    image = 'https://www.google.com/recaptcha/api/' + 'image?c=03AHJ_Vuu0oMeODNzLuSW5wC1TKHy7vP7YKMUmjcktaRQfUfoHHORWCJT49knlJsa921Ois12CUp39npQlk4qV9QPUCaQbSYd119FHSRl3-UwzvJ4qCHPNrQKdJJkEW2AWtUB8HkzMb57aVIMRJF0NvCTxZsfMNMqpC0iTWyqJS0rVgRptlvvIn-vcNwF8N18XhUIYl5cVorqP'
    print 'image =',image
    
    grab.go(image, log_file='i.png')
    print grab.request_headers
            

    # urls = [
    #     'https://panel.dreamhost.com/index.cgi?tree=domain.manage&',
    #     'https://panel.dreamhost.com/index.cgi?tab=domain.manage&start=klammer%2Eus',
    #     'https://panel.dreamhost.com/index.cgi?tab=domain.manage&start=martialartsmayhem%2Eus',
    #     'https://panel.dreamhost.com/index.cgi?tab=domain.manage&start=noodlemamma%2Eus',
    #     'https://panel.dreamhost.com/index.cgi?tab=domain.manage&start=premierauctions%2Eus',
    #     'https://panel.dreamhost.com/index.cgi?tab=domain.manage&start=scottwillsey%2Eus',
    #     'https://panel.dreamhost.com/index.cgi?tab=domain.manage&start=tacmedicalschool%2Eus',
    #     'https://panel.dreamhost.com/index.cgi?tab=domain.manage&start=underbear%2Eus',
    #     'https://panel.dreamhost.com/index.cgi?tab=domain.manage&start=whoville%2Eus'
    # ]
    # domains = set()
    # for url in urls:
    #     print 'domains =',len(domains)
    #     client.grab.go(url)
    #     for e in client.grab.doc.select('//td[@class="left"]/strong'):
    #         domains.add(e.text())

    # ds = Domain.objects.filter(name__in=domains, virtual_server__isnull=True)
    # print len(domains)
    # print ds.count()

    # server = VirtualServer.objects.get(pk=2)
    
    # ind = 0
    # for d in ds:
    #     client.grab.go(client.index_url + '?tree=domain.manage&current_step=Index&next_step=ShowZone&domain='+d.name)
    #     ip = client.grab.doc.select('//td[@title="Comment: main ip (from http service)"]').text()
    #     print ind, d.name, ip
    #     ind += 1 
    #     d.action_add(True, server, ip)