
class AuthException(Exception):
    pass

class ParseException(Exception):
    pass