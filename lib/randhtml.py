#coding=utf8
import random
import string

class RandomHtml:

    tags = ['div', 'span', 'p', 'b', 'font']

    styles = {
        'align': ['center', 'middle', 'top', 'left', 'right'],
        'font-size': ['8pt', '10pt', '12px'],
        'color': ['black', 'red', 'green', 'grey'],
        'font-weight': ['bold'],
    }

    s_nouns = [
        u"Чувак",
        u"Моя мама",
        u"Король",
        u"Какой-то парень",
        u"Кошка с бешенством",
        u"Ленивца",
        u"Ваш братан",
        u"Это классный парень мой садовник вчера встретился",
        u"Супермен"
    ]
    p_nouns = [
        u"Эти парни",
        u"Оба моих мам",
        u"Все цари мира",
        u"Некоторые парни",
        u"Все кошки питомника в",
        u"Множество ленивцев, живущих под кроватью",
        u"Ваши корешей",
        u"Как, это, вроде бы, все эти люди",
        u"Супермен"
    ]
    s_verbs = [
        u"съедает",
        u"пинки",
        u"дает",
        u"лечит",
        u"встречается с",
        u"создает",
        u"хаки",
        u"настраивает",
        u"Шпионы на",
        u"тормозит",
        u"мяукает на",
        u"бежит от",
        u"пытается автоматизировать",
        u"взрывается"
    ]
    p_verbs = [
        u"съесть",
        u"удар",
        u"дать",
        u"рассматривать",
        u"встречаться с",
        u"создать",
        u"взломать",
        u"настроить",
        u"шпионить",
        u"тормозить",
        u"мяу на",
        u"бегут от",
        u"стараются автоматизировать",
        u"взорваться"
    ]
    infinitives = [
        u"сделать пирог.",
        u"без всякой видимой причины.",
        u"потому что небо зеленое.",
        u"для лечения заболевания.",
        u"чтобы быть в состоянии сделать тост взорваться.",
        u", чтобы узнать больше об археологии."
    ]

    def sentence(self):
        return ''
        s = ''
        s += random.choice(self.s_nouns) + ' '
        s += random.choice(self.s_verbs) + ' '
        s += random.choice(self.s_nouns).lower() or random.choice(self.p_nouns).lower() + ' '
        s += random.choice(self.infinitives) + ' '
        
        return '<!--%s-->' % s
    
    def style(self):
        count = random.randrange(3)
        props = {}
        for _ in xrange(count):
            k = random.choice(self.styles.keys())
            v = random.choice(self.styles[k])
            props[k] = v
        
        if props:
            return ' style="%s"' % ';'.join(
                ['%s:%s' % (k,v) for k,v in props.items()]
            ) 
        else:
            return ''

    def output(self, generator):
        if isinstance(generator, str) or isinstance(generator, unicode):
            return generator
        else:
            s = ''
            for g in generator: 
                s += self.output(g)
            return s

    def html(self):
        while self.pic > 0 or self.tag_count > 0:
            yield self.section()

    def tag(self):
        tag = random.choice(self.tags)
        style = self.style()
        
        return '<%s%s>' % (tag, style), '</%s>' % tag
    
    def section(self, pic=False):
        if pic and self.link == 0 and self.pic > 0:
            if random.randrange(self.pic) == 0:
                self.pic = 0
                yield self.section()
                yield '<img src="{%pic%}">'
            else:
                self.pic -= 1
                yield self.section(pic=True)

        if self.link > 0:
            if random.randrange(self.link) == 0:
                self.link = 0
                yield self.section()
                # yield '<a href="{%link%}">'
                yield self.section(pic=True)
                # yield '</a>'
            else:
                self.link -= 1

        if random.randrange(2) == 0:
            self.tag_count -= 1
            begin, end = self.tag()
        
            yield begin   
            
            if random.randrange(2) == 0:
                yield self.sentence()
            
            yield end

        if random.randrange(2) == 0:
            yield self.section()

    def get(self):
        self.pic = 5
        self.link = 5
        
        self.tag_count = 10

        return self.output(self.html())
