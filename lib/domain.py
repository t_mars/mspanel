from lib.mwhois.whosearch import WhoisSearch
import lib.mwhois.const as CONST
import os,datetime
import random
import re
from grab import Grab
from django.conf import settings

ip_re = re.compile(r'[0-9]+(?:\.[0-9]+){3}')

def check_domain(domain, proxy):
    print 'domain =',domain,'has_no_dns_records'
    flag = has_no_dns_records(domain)

    if flag == None:
        raise Exception('DNS check error')

    if flag:
        print 'domain =',domain,'has_no_blocked_google'
        flag = has_no_blocked_google(domain, proxy)
    
    if flag == None:
        raise Exception('Google check error')

    ns_servers,expiry_date = None,None
    if flag:
        try:
            print 'domain =',domain,'get_whois_info'
            ns_servers,expiry_date = get_whois_info(domain, proxy)
        except:
            raise Exception('WHOIS check error')

    return flag, ns_servers, expiry_date

def get_whois_info(domain, proxy, try_count=3):
    s = WhoisSearch(dname=domain, debug=False)
    
    if proxy:
        s.connection.proxy = True
        s.connection.proxy_host = proxy['host']
        s.connection.proxy_port = proxy['port']
        if proxy['type'] == 'http':
            s.connection.proxy_type = CONST.PROXY_TYPE_HTTP
        elif proxy['type'] == 'socks4':
            s.connection.proxy_type = CONST.PROXY_TYPE_SOCKS4
        elif proxy['type'] == 'socks5':
            s.connection.proxy_type = CONST.PROXY_TYPE_SOCKS5

    for i in xrange(try_count):
        try:
            s.whois_search()
            date = s.expiry_date()
            y,m,d = date.split('-')
            expiry_date = datetime.datetime(int(y),int(m),int(d))
            
            servers = []
            for n in s.nameservers():
                n = n.lower()
                n = n[:-1] if n.endswith('.') else n
                servers.append(n)
            
            return servers,expiry_date
        
        except Exception as e:
            print 'domain =',domain,'except =',str(e),'ind =',i
            pass

    raise Exception()

def has_no_dns_records(domain):
    res = os.popen("host -t any %s" % domain).read().strip()
    if 'connection timed out' in res:
        return None
    return True if 'has no ANY record' in res else False

def get_dig_ip(domain, debug=False):
    # cmd = 'dig -t A %s +short' % domain
    cmd = 'host -t A %s' % domain
    res = os.popen(cmd).read().strip()
    
    if debug:
        print 'cmd=', cmd, 'res =', res
    
    if not res:
        return None
    
    if 'not found' in res:
        return False
    if 'has no A record' in res:
        return False
    return ip_re.findall(res)

def has_no_blocked_google(domain, proxy=None, grab=None, try_count=3):
    if not grab:
        if proxy:
            grab = Grab(proxy=proxy['address'], proxy_type=proxy['type'])
        else:
            grab = Grab()
    for i in xrange(try_count):
        try:
            grab.go('https://www.google.com/safebrowsing/diagnostic?site=%s&hl=en' % domain)
            return 'Site is listed as suspicious' not in grab.doc.body
        except:
            pass

    return None