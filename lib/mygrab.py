#coding=utf8
from grab import Grab
from main.models import Config

class MyGrab(Grab):
    def __init__(self, proxy_config_key=None, log_name=None, debug=False, *args, **kwargs):
        super(MyGrab, self).__init__(*args, **kwargs)
        
        self.download_size = 0
        self.request_count = 0
        self.log_name = log_name
        self.proxy_config_key = proxy_config_key
        self.debug = debug
        self.log_ind = 0

        self.proxy = None
        self.proxy_dict = None
        self.reset_proxy()
        
    def get_cookie(self, key, default=None):
        for i in self.cookies.get_dict():
            if i.get('name') == key:
                return i.get('value', default)
        return default

    def reset_proxy(self):
        if self.proxy_config_key:
            p = Config.GET(self.proxy_config_key, rand=True, asdict=True, waiting=True)
            self.proxy = p['address'], p['type']
            self.proxy_dict = p
            if self.debug:
                print 'proxy =', self.proxy
            self.setup(proxy=self.proxy[0], proxy_type=self.proxy[1])

    def go(self, url, *args, **kwargs):
        exc = None
        for i in xrange(3):
            try:
                exc = None
                super(MyGrab, self).go(url, *args, **kwargs)
                self.request_count += 1
                self.download_size += self.response.download_size
                break
            except Exception as e:
                exc = e
                if self.debug:
                    print 'go error:', str(e)
                self.reset_proxy()

        if exc:
            raise exc

        if self.log_name:
            fn = '%s_%02d.html' % (self.log_name, self.log_ind)
            self.log_ind += 1
        
            f = open(fn, 'w+')
            f.write('url=%s\n' % url)
            f.write('args=%s\n' % str(args))
            f.write('kwargs=%s\n' % str(kwargs))
            f.write(self.doc.body)
            f.close()