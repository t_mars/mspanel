#coding=utf8
import json
import datetime
import os
import random
import time
import string
import pprint

from lib.mygrab import MyGrab
from lib.recaptcher import ReCaptcher
from lib.mailer.exceptions import AuthException
from main.models import MailRuAccount

from HTMLParser import HTMLParser

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

class Rambler(object):
    METHODS = {
        'GET_PROFILE': 'Rambler::Id::load_profile',
        'LOGIN_RAMBLER': 'Rambler::Id::create_web_session',
        'LOGIN_EXTERNAL': 'Rambler::Id::X::create_session_by_external_account',
        'LOGIN_PHONE': 'Rambler::Id::X::create_session_by_external_phone_account',
        'LOGIN_TOKEN': 'Rambler::Id::create_session_from_auth_token',
        'CHECK_RAMBLER_LOGIN': 'Rambler::Id::login_available',
        'CHECK_EXTERNAL_LOGIN': 'Rambler::Id::X::check_external_service_account_existence',
        'GET_PHONE_ORDER': 'Rambler::Common::create_rpc_phone_order',
        'REGISTER_USER': 'Rambler::Id::register_user',
        'REGISTER_USER_BY_PHONE': 'Rambler::Id::register_user_by_phone',
        'MOUNT_USER_EXTERNAL': 'Rambler::Id::X::create_external_service_account',
        
        'GET_PROFILE': 'Rambler::Id::load_profile',
        'REGISTER_USER': 'Rambler::Id::register_user',
        'REGISTER_USER_BY_PHONE': 'Rambler::Id::register_user_by_phone',
        'GET_CAPTCHA': 'Rambler::Common::create_rpc_order',
        'GET_PHONE_ORDER': 'Rambler::Common::create_rpc_phone_order',
        'GET_RESTORE_PHONE_ORDER': 'Rambler::Common::create_rpc_user_phone_order',
        'GET_PHONE': 'Rambler::Id::get_phone',
        'LOGIN_USER': 'Rambler::Id::create_web_session',
        'LOGIN_EXTERNAL_USER': 'Rambler::Id::X::create_session_by_external_account',
        'ATTACH_PHONE': 'Rambler::Id::attach_phone',
        'DETACH_PHONE': 'Rambler::Id::detach_phone_by_password',
        'CHANGE_PHONE': 'Rambler::Id::change_phone',
        'CHANGE_PROFILE': 'Rambler::Id::change_profile',
        'CHANGE_PASSWORD': 'Rambler::Id::change_password',
        'CHANGE_QUESTION': 'Rambler::Id::change_secret_question',
        'SUGGEST': 'Rambler::Id::suggest_email',
        'CHECK_USER_EXISTENCE': 'Rambler::Id::login_available',
        'GET_RESTORE_INFO': 'Rambler::Id::get_restore_info',
        'RESTORE_PASSWORD': 'Rambler::Id::restore_password',
        'RESTORE_PASSWORD_BY_PHONE': 'Rambler::Id::restore_password_by_phone',
        'OAUTH_ATTACH': 'Rambler::Id::attach_external_account',
        'OAUTH_DETACH': 'Rambler::Id::detach_external_account',
        'GET_UNBAN_INFO': 'Rambler::Id::unban_user_by_passwor',

        'GET_MESSAGES': 'Rambler::Mail::get_folder_messages',
        'GET_MESSAGE': 'Rambler::Mail::render_message',
    }

    URLS = {
        'HOME': 'http://rambler.ru/',
        'REG': 'https://id.rambler.ru/account/?rname=head&utm_source=head&utm_medium=topline&utm_content=id&back=http%3A%2F%2Fwww.rambler.ru%2F#registration',
        'COMPOSE_AJAX': 'https://mail.rambler.ru/ajax/compose/',
        'COMPOSE': 'https://mail.rambler.ru/#/compose/',
    }

    RPCS = {
        'ID': 'https://id.rambler.ru/jsonrpc',
        'MAIL': 'https://mail.rambler.ru/rpc',
        'COMMON': 'https://id.rambler.ru/jsonrpc',
    }

    def __init__(self, grab_config={}):
        self.grab = MyGrab(**grab_config)

    def json(self, method, params, url='ID_RPC'):
        method = self.METHODS[method]
        params = [params]
        _,rpc,_ = method.split('::')
        url = self.RPCS[rpc.upper()]
        print 'url =',url
        print 'method =',method
        print 'params =',params
        print '---'
        self.grab.go(
            url, 
            post=json.dumps({
                'method': method,
                'params': params,
                'rpc': '2.0',    
            }), 
            headers={
                "Content-Type": "application/json",
            }
        )
        return json.loads(self.grab.doc.body)

class WebClient(Rambler):
    def __init__(self, account, *args, **kwargs):
        super(WebClient, self).__init__(*args, **kwargs)
        self.last_error = None
        self.account = account
        self.login, self.domain = self.account.username.split('@')
        
        self.debug = True

        auth_data = account.get_auth_data()
        if auth_data.get('cookies'):
            self.authorized = True
            self.grab.setup(cookies=auth_data.get('cookies'))
        else:
            self.authorized = False
        self.entered = False
    
    def enter(self):
        if self.debug:
            print 'create new session', self.login

        data = self.json('LOGIN_USER', {
            'expire': 0,
            'password': self.account.password.strip(),
            'login': self.login,
        })
        try:
            if data['result']['status'] == 'OK':
                self.authorized = True
                self.entered = True
                return True
        except:
            pass
        raise AuthException('Not entered.')

    def get_auth_cookies(self):
        return {
            'rdomain': self.grab.get_cookie('rdomain'),
            'rlogin':  self.grab.get_cookie('rlogin'),
            'rsid':    self.grab.get_cookie('rsid'),
        }

    def get_auth_data(self):
        if not self.authorized:
            return None
        return {
            'cookies': {
                'rdomain': self.grab.get_cookie('rdomain'),
                'rlogin':  self.grab.get_cookie('rlogin'),
                'rsid':    self.grab.get_cookie('rsid'),
            }
        }

    def get_inbox_list(self, *args, **kwargs):
        return self._get_messages('INBOX', *args, **kwargs)

    def get_sent_list(self, *args, **kwargs):
        return self._get_messages('SentBox', *args, **kwargs)

    def get_drafts_list(self, *args, **kwargs):
        return self._get_messages('DraftBox', *args, **kwargs)

    def get_spam_list(self, *args, **kwargs):
        return self._get_messages('Spam', *args, **kwargs)

    def get_trash_list(self, *args, **kwargs):
        return self._get_messages('Trash', *args, **kwargs) 

    def _get_messages(self, folder, _next=None):
        if not self.authorized:
            self.enter()

        if _next is None:
            _next = 0
        data = self.json('GET_MESSAGES', {
            'folder.name': folder, 
            'folder.sortorder': 'D', 
            'folder.offset': _next, 
            'folder.elements': 50    
        })
        # pprint.pprint(data)
        total = int(data['result']['folder']['total_elements'])
        elements = int(data['result']['folder']['elements'])
        offset = int(data['result']['folder']['offset'])
        last = offset + elements

        msgs = []
        if not data['result']['folder']['messages']:
            return msgs, None

        for m in data['result']['folder']['messages']:
            msgs.append({
                'id': '%s.%s' % (folder, m['uid']),
                'size': m['size'],
                'subject': m['subject'],
                'date': datetime.datetime.fromtimestamp(int(m['rdate'])),
                'from': ['%s@%s' % (f[1],f[2]) for f in m['from']] if m['from'] else [],
                'to': ['%s@%s' % (f[1],f[2]) for f in m['to']] if m['to'] else [],
                'cc': [],
            })
        return msgs, last if last < total else None

    def get_message(self, id):
        if not self.authorized:
            self.enter()
    
        folder, id = id.split('.')
        data = self.json('GET_MESSAGE', {
            'folder.name': folder.upper(),
            'message.uid':id,    
        })
        m = data['result']['message']
        # pprint.pprint(m['data']['subparts'])
        res = {
            'headers': m['mimestructure']['headers'],
            'subject': m['mimestructure']['subject'],
            'date': datetime.datetime.fromtimestamp(int(m['mimestructure']['rdate'])),
            'from': ['%s@%s' % (f[1],f[2]) for f in m['mimestructure']['from']],
            'to': ['%s@%s' % (f[1],f[2]) for f in m['mimestructure']['recipients']],
            'cc': [],
            'html': '',
            'text': '',
        }

        if 'subpart' in m['data']:
            res['html'] = m['data']['subpart']['html']
        elif 'subparts' in m['data']:
            res['html'] = m['data']['subparts'][0]['text']
        elif 'text' in m['data']:
            res['html'] = m['data']['text']
            print '!!!',res['html']
        res['text'] = strip_tags(res['html'])

        return res

    def send(self, emails, subject, body):
        if not self.authorized:
            self.enter()
    
        self.grab.go(self.URLS['COMPOSE'])
        self.grab.go(
            self.URLS['COMPOSE_AJAX'], 
            post={
                'action': 'send',
                'params': json.dumps({
                    'from': {self.account.username: self.account.firstname + ' '+ self.account.lastname},
                    'to':{e:'' for e in emails},
                    'cc':{},
                    'bcc':{},
                    'subject':subject,
                    'body':body,
                    'is_html':1,
                    'uploads':[],
                    'draft_uid':'',
                    'autodraft_uid':'',
                    'mbox':'',
                    'uid':'',
                    'attached_messages':[],
                    'attached_parts':[],
                    'action':''
                })
            },
            headers={
                'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8',
                'X-Requested-With':'XMLHttpRequest',
            }
        )
        print 'entered =',self.entered
        print self.grab.doc.body
        try:
            data = json.loads(self.grab.doc.body)
            if data['status'] == 'OK':
                return {e:True for e in emails}
        except:
            self.last_error = self.grab.doc.body
            pass
        return False

class Reg(Rambler):
    def __init__(self, names, *args, **kwargs):
        super(Reg, self).__init__(*args, **kwargs)
        c = {}
        if 'rucaptcha' in names:
            c['rucaptcha'] = {'send_config': {'language': '1'}, 'binary': False}
        if 'antigate' in names:
            c['antigate'] = {'send_config': {'is_russian': '1'}, 'binary': False}
        self.recaptcher = ReCaptcher(c)

    def get_stat(self):
        return {
            'download_size': self.grab.download_size,
        }

    def reg(self, user):
        firstname = user['firstname']
        lastname = user['lastname'] 
        password = user['password']
        birthdate = user['birthdate']
        sex = user['sex']
        
        # получаем логин
        data = self.json('SUGGEST', {
            'firstname': firstname.decode('utf8'),
            'lastname': lastname.decode('utf8'),
        })
        username, domain = data['result']['email'][0]

        # получаем капчу
        data = self.json('GET_CAPTCHA', {
            'method':self.METHODS['REGISTER_USER'],
            'useBase64':1,
            'captchaConfig':{'width':300,'height':53,'ptsize':35}
        })
        orderId = data['result']['orderId']
        captcha_base64 = data['result']['orderValue.b64']

        captcha = '/tmp/rambler_captcha_%d.png' % (time.time()*1000)
        f = open(captcha, 'wb')
        f.write(captcha_base64.decode('base64'))
        f.close()

        print 'captcha =', captcha
        value = self.recaptcher.get(captcha)
        if not value:
            print 'no  value'
            return
        else:
            # print 'value0 =', value
            value = value.decode('utf-8').upper()
            # print 'value1 =', value

        data = self.json('REGISTER_USER', {
            '__rpcOrderId': orderId,
            '__rpcOrderValue': value,
            'question': "Почтовый индекс ваших родителей",
            'answer': ''.join(random.choice(string.digits) for _ in range(6)),
            'birthday': int(time.mktime(birthdate.timetuple())),
            'domain': domain,
            'firstname': firstname,
            'lastname': lastname,
            'gender': 'm' if user['sex'] == 'male' else 'f',
            'password': password,
            'username': username,
        })
        print data
        try:
            if data['result']['status'] == 'ERROR':
                if data['result']['error']['errno'] == -5:
                    print 'captcha error'
                    return
            if data['result']['status'] != 'OK':
                return
        except:
            return

        a = MailRuAccount()
        a.username = '%s@%s' % (username, domain)
        a.password = user['password']
        a.birthdate = user['birthdate']
        a.firstname = user['firstname']
        a.lastname = user['lastname']
        a.sex = 1 if sex == 'male' else 2
        return a