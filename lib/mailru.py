#coding=utf8

import time
import string
import random
import os

import HTMLParser

from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from lib.mygrab import MyGrab 
from lib.mailer.exceptions import AuthException, ParseException
from main.models import Config


class BrowserClient(object):
    def __init__(self, account, proxy_config_key=None, debug=False):
        self.debug = debug
        self.account = account
        self.proxy_config_key = proxy_config_key
        self.proxy = None
        self.snapshot_id = '%s_%d' % (self.account.username, time.time() * 1000)
        self.snapshot_ind = 1
        self.last_error = None
        
        self.display = Display(visible=0, size=(1024, 768))
        self.display.start()

        profile = webdriver.FirefoxProfile()

        if self.proxy_config_key:
            self.proxy = Config.GET(self.proxy_config_key, rand=True, asdict=True, waiting=True)
            if self.debug:
                print 'proxy =', self.proxy
            
            if self.proxy['type'] in ['socks5', 'socks4']:
                prefix = 'socks'
            elif self.proxy['type'] == 'http':
                prefix = 'http'
            
            profile.set_preference("network.proxy.type", 1)
            profile.set_preference("network.proxy.%s" % prefix, self.proxy['host'])
            profile.set_preference("network.proxy.%s_port" % prefix, self.proxy['port'])
            profile.update_preferences()
        
        self.browser = webdriver.Firefox(firefox_profile=profile)
        
        self.enter()

    def enter(self):
        if self.debug:
            print 'ENTER'
        self.browser.get('https://e.mail.ru/login?from=main&rf=auth.mail.ru')
        self.select('//input[@name="Login"]').send_keys(self.account.username)
        self.select('//input[@name="Password"]').send_keys(self.account.password)
        self.select('//form[@id="LoginExternal"]//button[@type="submit"]').click()

    def get_auth_cookies(self):
        res = {}
        for c in self.browser.get_cookies():
            if c['name'] in ['Mpop', 'sdcs']:
                res[ c['name'] ] = c['value']
        return res

    def snapshot(self, label=None):
        fn = '%s_%2d' % (self.snapshot_id, self.snapshot_ind)
        self.snapshot_ind += 1
        if label:
            fn += '_%s' % label
        self.browser.get_screenshot_as_file('%s.png' % fn)

    def select(self, xpath, wait=True):
        if wait:
            for _ in xrange(3):
                try:
                    return self.browser.find_element_by_xpath(xpath)
                except Exception as e:
                    if self.debug:
                        print 'wait:', xpath,
                        print 'exception:', str(e)
                    time.sleep(0.5)
            raise Exception('not loaded')

        else:
            return self.browser.find_element_by_xpath(xpath)

    def send(self, emails, subject, body):
        self.browser.get('https://e.mail.ru/compose/')
        self.snapshot('compose1')
        
        # emails
        el = self.select('//textarea[@data-original-name="To"]')
        for e in emails:
            el.send_keys(e)
            el.send_keys(Keys.ENTER)
        
        # subject
        self.select('//input[@name="Subject"]').send_keys(subject)

        # body
        self.browser.execute_script("$('tr.mceFirst iframe').contents().find('#tinymce').html('%s');" % body);

        self.snapshot('compose2')

        # send
        self.select('//div[@data-name="send"]').click()
        time.sleep(1)
        self.snapshot('send1')
    
        try:
            error = self.select('//span[@class="js-txt _js-title notify-message__title__text notify-message__title__text_error"]', wait=False)
            incorrect_emails = [e for e in emails if e in error.text]
        except:
            incorrect_emails = []

        if incorrect_emails:
            # correct emails
            incorrect_emails = [e for e in emails if e in error.text]

            self.select('//span[@data-text="asdvwev23fczxc24tefv@mail.ru"]//i').click()
            self.snapshot('correct')

            # send
            self.snapshot('send2')
            self.select('//div[@data-name="send"]').click()
            time.sleep(1)
    
        # pause
        self.snapshot('sended')

        # check sended
        try:
            self.select('//div[contains(@class,"message-sent")]', wait=False)
            return {e:e not in incorrect_emails for e in emails}
        except:
            self.last_error = 'SEND ERROR SELENIUM'
            return False

class WebClient:
    URL_MSGS_COMPOSE = 'https://e.mail.ru/compose'
    URL_MSGS_INBOX = 'https://e.mail.ru/messages/inbox/'
    URL_MSGS_SENT = 'https://e.mail.ru/messages/sent/'
    URL_MSGS_DRAFTS = 'https://e.mail.ru/messages/drafts/'
    URL_MSGS_SPAM = 'https://e.mail.ru/messages/spam/'
    URL_MSGS_TRASH = 'https://e.mail.ru/messages/trash/'
    URL_MSGS_MOVE = 'https://e.mail.ru/api/v1/messages/move'
    URL_MSG = 'https://e.mail.ru/message/%s/'
    URL_LOGIN = 'https://auth.mail.ru/cgi-bin/auth?from=splash'

    def __init__(self, account, grab_config={}, debug=False):
        self.account = account
        self.html_parser = HTMLParser.HTMLParser()
    
        self.email, self.password = self.account.username, self.account.password
        self.login, self.domain = self.email.split('@')
        self.last_error = None

        self.debug = debug
        grab_config['debug'] = debug
        self.grab = MyGrab(**grab_config)

        s = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(4))
        self.grab.log_name = os.path.join('/var/www/mspanel/private/workers/mailing/logs', user + s)

        # авторизация
        auth_data = account.get_auth_data()
        if auth_data.get('cookies'):
            self.grab.setup(cookies=auth_data.get('cookies'))
            self._authorized = True
        else:    
            self.enter()
            self._authorized = False

        if 'tokens' in auth_data:
            self._send_tokens = auth_data['tokens']
        else:
            self._send_tokens = None
        self._token = None
                
        self._folders = {}
        # if account.site == MailRuAccount.MAIL_RU:
        # try:
        #     res['auth_data'] = {
        #         'tokens': s.prepare(),
        #         'cookies': {
        #             'Mpop': s.grab.get_cookie('Mpop'),
        #             'sdcs': s.grab.get_cookie('sdcs'),
        #         }
        #     }
        # except:
        #     pass

    def enter(self):
        if self.debug:
            print 'LOGIN', self.email
        post = {
            'Domain': self.domain,
            'Login': self.login,
            'Password': self.password,
            'new_auth_form': '1',
            'saveauth': '1'
        }
        self.grab.go(self.URL_LOGIN, post=post)

    def prepare(self):
        if not self._send_tokens:
            r = self._get_send_tokens()
            
            if not r:
                self.enter()
                r = self._get_send_tokens()

            if not r:
                raise AuthException
            
        return self._send_tokens

    def _get_send_tokens(self):
        self.grab_go(self.URL_MSGS_COMPOSE)
        if not self._token:
            return False

        self.grab.go('https://e.mail.ru/api/v1/messages/status?ajax_call=1&x-email=' + self.email +
            '&email=' + self.email +
            '&sort=%7B%22type%22%3A%22date%22%2C%22order%22%3A%22desc%22%7D'
            '&offset=0&limit=26&folder=0&htmlencoded=false'
            '&last_modified=1436870240'
            '&letters=false'
            '&nolog=1'
            '&sortby=D'
            '&api=1'
            '&token='+self._token
        )
        try:
            data = json.loads(self.grab.doc.body)
            tokens = data['body']['tokens']['sentmsg']
        except:
            return False

        self._send_tokens = tokens['form_sign'], tokens['form_token']
        return True

    def send(self, emails, subject, body):
        # отправляем всем сразу
        emails = [e.lower() for e in emails]
        
        # заглушка
        # return  {e:True for e in emails}
        
        res,mes = self._send(','.join(emails), subject, body)

        # успешная отправка пачки
        if res:
            return {e:True for e in emails}

        # выявляем ошибку
        else:
            # если есть @ значит жалуется на почту, иначе на аккаунт
            if '@' not in mes:
                return False
            
            # ошибка, убираем тех, кому не отправилось
            correct_emails = [e for e in emails if e not in mes]
            
            # если все почты неверные
            if not len(correct_emails):
                return {e:False for e in emails}

            res,mes = self._send(','.join(correct_emails), subject, body)
            if res:
                return {e:e in correct_emails for e in emails}
            
            # если второй раз возвращается ошибка, значит дело не в адресатах
            else:
                return False

    def _send(self, to, subject, body):
        form_sign, form_token = self.prepare()
        self.grab.go('https://e.mail.ru/compose/?func_name=send&send=1', post={
            'ajax_call':'1',
            'x-email':self.email,
            'text':'',
            'direction':'re',
            'orfo':'rus',
            'form_sign':form_sign,
            'form_token':form_token,
            'old_charset':'utf-8',
            'HTMLMessage':'1',
            'RealName':'0',
            'To':to,
            'Subject':subject,
            'Body':body,
            'EditorFlags':'0',
            'SocialBitmask':'0',
        })

        try:
            data = json.loads(self.grab.doc.body)
            # print 'from =',self.email,'to =', to, 'data =', data
            if data[0] == 'AjaxResponse' and data[1] == 'OK' and data[2]['Ok']:
                # print 'from =',self.email,'to =', to, 'TRUE'
                return True,data[2]['message_id']
            else:
                error = data[2]['Error'].lower()
                # print 'error=',error
                error = self.html_parser.unescape(error)
                # print 'from =',self.email,'to =', to, 'FALSE'
                self.last_error = error
                return False,error
                
        except Exception as e:
            raise AuthException

    def _parse_folder_codes(self):
        try:
            self._folders = {
                'inbox': re.findall(r"case\s*(\d+)\s*:\s*url\s*\+\=\s*\'\/inbox\'", self.grab.doc.body)[0],
                'spam': re.findall(r"case\s*(\d+)\s*:\s*url\s*\+\=\s*\'\/spam\'", self.grab.doc.body)[0],
                'sent': re.findall(r"case\s*(\d+)\s*:\s*url\s*\+\=\s*\'\/sent\'", self.grab.doc.body)[0],
                'drafts': re.findall(r"case\s*(\d+)\s*:\s*url\s*\+\=\s*\'\/drafts\'", self.grab.doc.body)[0],
                'trash': re.findall(r"case\s*(\d+)\s*:\s*url\s*\+\=\s*\'\/trash\'", self.grab.doc.body)[0],
            }
        except:
            pass
        if self.debug:
            print 'FOLDERS =', self._folders
        return True if self._folders else False

    def _get_folder_code(self, key):
        if key not in self._folders:
            self._grab_go(self.URL_MSGS_INBOX)
        return self._folders.get(key)

    def _parse_token(self):
        try:
            self._token = re.findall('patron\.updateToken\(\"[^"]+\"\)', self.grab.doc.body)[0].split('"')[1]
        except:
            pass
        if self.debug:
            print 'TOKEN =', self._token
        return True if self._token else False

    def _get_token(self):
        if not self._token:
            self._grab_go(self.URL_MSGS_INBOX)
        return self._token

    def _grab_go(self, *args, **kwargs):
        self.grab.go(*args, **kwargs)

        p = re.compile(r"useremail:\s*'([^']+)'", flags=re.U|re.I)
        
        # проверяем авторизацию
        if self.email in p.findall(self.grab.doc.body):
            self._parse_folder_codes()
            self._parse_token()
            self._authorized = True
            return

        self.enter()
        self.grab.go(*args, **kwargs)
        
        # проверяем авторизацию
        if p.findall(self.grab.doc.body) == [self.email]:
            self._parse_folder_codes()
            self._parse_token()
            self._authorized = True
            return

        raise AuthException('not find useremail') 
        
    def get_auth_cookies(self):
        if not self._authorized:
            return False
        return {
            'Mpop': self.grab.get_cookie('Mpop'),
            'sdcs': self.grab.get_cookie('sdcs'),
        }

    def get_inbox_list(self, *args, **kwargs):
        return self._get_messages(self.URL_MSGS_INBOX, *args, **kwargs)

    def get_sent_list(self, *args, **kwargs):
        return self._get_messages(self.URL_MSGS_SENT, *args, **kwargs)

    def get_drafts_list(self, *args, **kwargs):
        return self._get_messages(self.URL_MSGS_DRAFTS, *args, **kwargs)

    def get_spam_list(self, *args, **kwargs):
        return self._get_messages(self.URL_MSGS_SPAM, *args, **kwargs)

    def get_trash_list(self, *args, **kwargs):
        return self._get_messages(self.URL_MSGS_TRASH, *args, **kwargs)

    def _get_messages(self, url, page=None):
        if page is None:
            page = 1
        if page > 1:
            url += '?page=%d' % page
        
        self._grab_go(url)
        body = self.grab.doc.body

        # проверяем есть ли нужные данные
        ps = re.findall(r'arMailRuMessages', body, flags=re.U|re.I)
        if not(ps):
            raise ParseException('not parsed email list')

        # парсим данные
        try: 
            a = re.findall(r'return\s*\[\s*{\s*id', body, flags=re.U|re.I)[0]
            b = re.findall(r'\]\;\s*\}\)\(\)\;', body, flags=re.U|re.I)[0]
        except:
            return [], None    

        part = body[body.index(a):body.index(b)]

        data = '[%s]' % part[part.index('{'):part.rindex('}')+1]
        data = re.sub(r'\"(\d+)\"\|0', r'\1', data)

        rs = re.findall(r'\u\(\"[^"]*\"\)', data)
        for r in rs:
            data = data.replace(r, r[2:-1])

        data = re.sub(r'snippet[^\n]+\n', '', data)

        with open('inbox.json', 'w') as f:
            f.write(data)


        ds = demjson.decode(data)
        messages = []
        for d in ds:
            msg = {
                'id': d['id'],
                'size': d['size'],
                'subject': d['subject'],
                'date': datetime.datetime.fromtimestamp(int(d['date'])),
                'from': [f['email'] for f in d['correspondents']['from']],
                'to': [f['email'] for f in d['correspondents']['to']],
                'cc': [f['email'] for f in d['correspondents']['cc']],
            }
            messages.append(msg)
        return messages, page+1 if ds[-1]['next'] != "" else None

    def get_message(self, id):
        self._grab_go(self.URL_MSG % id)
        
        body = self.grab.doc.body
        try:
            a = re.findall(r'var\s*json\s*=\s*\{', body)[0]
            b = re.findall(r'if\s*\(json', body)[0]
        except:
            return None

        part = body[body.index(a):body.index(b)]
        data = part[part.index('{'):part.rindex('}')+1]
        with open('data.json', 'w') as f:
            f.write(data)

        m = demjson.decode(data)
        return {
            'headers': m['body']['headers'],
            'subject': m['body']['subject'],
            'date': datetime.datetime.fromtimestamp(int(m['body']['date'])),
            'from': [f['email'] for f in m['body']['correspondents']['from']],
            'to': [f['email'] for f in m['body']['correspondents']['to']],
            'cc': [f['email'] for f in m['body']['correspondents']['cc']],
            'html': m['body']['body']['html'],
            'text': m['body']['body']['text'],
        }

    def del_message(self, id):
        post = {
            'ajax_call':'1',
            'x-email':self.email,
            'email':self.email,
            'remove':'1',
            'folder':self._get_folder_code('trash'),
            'id':'["%s"]' % id,
            'ids':'["%s"]' % id,
            'move':'1',
            'ajaxmode':'1',
            'noredir':'1',
            'api':'1',
            'token':self._get_token(),
        }
        self.grab.go(self.URL_MSGS_MOVE, post=post)
        res = demjson.decode(self.grab.doc.body)
        if res['status'] == 200:
            return True
        else:
            if self.debug:
                print res
            return False