#coding=utf8
from grab import Grab
from antigate import AntiGate, AntiGateError
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

def ident_captcha_antigate(filename):
    try:
        val = AntiGate('1fe011ef5fb13b6ccde3116800cd6c6d', filename)
        return True, val
    except AntiGateError as e:
        return False, str(e)

class DreamHostClient():
    INDEX_URL = 'https://panel.dreamhost.com/index.cgi'
    PANEL_URL = 'https://panel.dreamhost.com/'

    def __init__(self, login, password, debug=False):
        from lib.mygrab import MyGrab
        self.grab = MyGrab()
        self.log_ind = 0
        
        display = Display(visible=0, size=(1024, 768))
        display.start()

        browser = webdriver.Firefox()
        self.browser = browser
        self.browser.get(self.INDEX_URL)
        self.browser.find_element_by_id('username').send_keys(login)
        self.browser.find_element_by_id('password').send_keys(password)
        self.browser.find_element_by_xpath('//button[@type="submit"]').click()

    def parse(self):
        try:
            error = self.browser.find_element_by_xpath('//div[@class="errorbox"]//div[@class="body"]')
            # print 'errror =', error.text
            return False, error.text
        except:
            pass
        
        try:
            success = self.browser.find_element_by_xpath('//div[@class="successbox"]//div[@class="body"]')
            # print 'success ='#, success.text
            return True, success.text
        except:
            pass

        return None, None
        
    def add_domain(self, domain, docroot):
        self.browser.get(self.INDEX_URL + '?tree=domain.manage&current_step=Index&next_step=ShowAddhttp&domain=')
        
        # 'domain'
        self.browser.find_element_by_id('cgi-domain').send_keys(domain)

        # 'www'
        self.browser.find_element_by_xpath('//input[@name="www_redir" and @value="0"]').click()

        # 'user'
        self.browser.find_element_by_xpath('//select[@id="usid"]/option[last()]').click()

        # 'docroot'
        e = self.browser.find_element_by_id('docroot')
        e.send_keys(Keys.CONTROL, 'a')
        e.send_keys(docroot)
        
        # 'mod_security'
        # self.browser.find_element_by_id('mod_security').get_attribute('checked')
        self.browser.find_element_by_id('mod_security').click()
        # self.browser.find_element_by_id('mod_security').get_attribute('checked')

        try:
            image = self.browser.find_element_by_id('recaptcha_challenge_image').get_attribute('src')
        except:
            image = None
        
        if image:
            image_file = '/tmp/image%d.png' % self.log_ind
            self.log_ind += 1
            self.grab.go(image, log_file=image_file)
            self.grab.setup(log_file='1')

            flag, value = ident_captcha_antigate(image_file)
            value = str(value)
            print 'image =', image
            print 'value =', value
            self.browser.find_element_by_id('recaptcha_response_field').send_keys(value)
            
        self.browser.find_element_by_id('submit_fully_hosted').click()

        flag, msg = self.parse()
        if not flag:
            print 'error =', msg
            return False

        self.refresh_ns(domain)
        return True

    def refresh_ns(self, domain):
        # получаем ключи
        self.browser.get(self.INDEX_URL + '?tree=domain.manage.manage&current_step=Index&next_step=ShowZone&domain='+domain)
        
        try:
            self.browser.find_element_by_xpath('//td[@title="Comment: main ip (from http service)"]')
        except:
            return False

        try:
            self.browser.find_element_by_xpath('//input[@value="Go!"]').click()
        except:
            pass

        flag, msg = self.parse()
        if not flag:
            print 'error =', msg
            return False
        ip = self.browser.find_element_by_xpath('//td[@title="Comment: main ip (from http service)"]')
        return ip.text