import urllib
import urllib2
import ssl, json

class API(object):

	def __init__(self, isp_api):
		self.isp_api = isp_api
		self.url = isp_api.auth.url
		self.sessid = isp_api.auth.sessid
		self.clear_params()

	def clear_params(self, func=None):
		self.params = {
			'auth' : self.sessid,
			'out'  : 'json',
		}
		if func:
			self.params['func'] = func
	
	def pass_params(self, kwargs):
		self.params.update(kwargs)

	def process_api(self, url, params):
		kwargs = {}
		if hasattr(ssl, '_create_unverified_context'):
			kwargs['context'] = ssl._create_unverified_context()
		request = "%s?%s" % (url, urllib.urlencode(params))
		
		print 'request =',request
		self.isp_api.data = json.load(urllib2.urlopen(request, **kwargs))
		self.isp_api.last_error = None
		
		if 'ok' in  self.isp_api.data:
			return True
		elif 'error' in self.isp_api.data:
			self.isp_api.last_error = self.isp_api.data['error']
			return False