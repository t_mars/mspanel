import api

class Domain(api.API):

    def add(self, domain, name, ip, **kwargs):
        self.clear_params('domain.sublist.edit')
        self.params['name'] = name
        self.params['sdtype'] = 'A'
        self.params['addr'] = ip
        self.params['plid'] = domain
        self.params['sok'] = 'ok'
        self.pass_params(kwargs)

        return self.process_api(self.url, self.params)

    def list(self, domain=None):
        if domain:
            self.clear_params('domain.sublist')
            self.params['elid'] = domain
        else:
            self.clear_params('domain')
        self.process_api(self.url, self.params)
        
        try:
            return self.data['elem']
        except KeyError:
            return self.data

    def delete(self, domain):
        self.clear_params('domain.delete')
        self.params['webdomain'] = 'on'
        self.params['extop'] = 'on'
        self.params['elid'] = domain
        self.params['sok'] = 'ok'

        return self.process_api(self.url, self.params)