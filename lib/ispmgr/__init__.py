class ISP_API:
	
	def __init__(self, url, username, password):
		from ispmgr.auth import Auth
		self.auth = Auth(url, username, password)
		self.apis = {}
		self.last_error = None
		self.data = None

	def get_api(self, name, cls):
		if name not in self.apis: 
			self.apis[name] = cls(self)
		return self.apis[name]
	
	@property
	def www(self):
		from ispmgr.wwwdomain import WWWDomain
		return self.get_api('www', WWWDomain)

	@property
	def domain(self):
		from ispmgr.domain import Domain
		return self.get_api('domain', Domain)