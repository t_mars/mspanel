import urllib, urllib2
import json, ssl

class Auth(object):
	def __init__(self, url, username, password,):
		self.url = url
		self.sessid = self.authorize(username, password)

	def authorize(self, username, password):
		params = urllib.urlencode({
			'func': 'auth',
			'out': 'json',
			'username': username,
			'password': password,
		})
		kwargs = {}
		if hasattr(ssl, '_create_unverified_context'):
			kwargs['context'] = ssl._create_unverified_context()
		
		data = urllib2.urlopen("%s?%s" % (self.url, params), **kwargs)
		out = json.load(data)

		if out.has_key('authfail'):
			raise RuntimeError('Authorization error, check your credentials')

		print out
		return out['auth']

	def logout(self):
		params = urllib.urlencode({
			'auth' : self.sessid,
			'func' : 'session.delete',
			'out' : 'json',
			'elid' : self.sessid,
		})

		context = ssl._create_unverified_context()
		data = urllib2.urlopen('%s?%s' % (self.url, params), context=context)
		out = json.load(data)

		if out["result"] == "OK":
			return True
		else:
			raise RuntimeError('Logout failed!')
