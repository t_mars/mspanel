import api

class WWWDomain(api.API):

    def list(self, domain=None):
        if domain:
            self.clear_params('wwwdomain.edit')
            self.params['elid'] = domain
        else:
            self.clear_params('wwwdomain')
        data = self.process_api(self.url, self.params)
        out = json.load(data)
        try:
            return out['elem']
        except KeyError:
            return out

    def add(self, domain, owner, admin, ip, **kwargs):
        self.clear_params('wwwdomain.edit')
        self.params['sok']    = 'yes'
        self.params['domain'] = domain
        self.params['owner']  = owner
        self.params['admin']  = admin
        self.params['ip']     = ip
        self.pass_params(kwargs)

        return self.process_api(self.url, self.params)

    def delete(self, domain):
        self.clear_params('wwwdomain.delete')

        if type(domain) is list:
            self.params['elid'] = ', '.join(domain)
        else:
            self.params['elid'] = domain

        return self.process_api(self.url, self.params)

    def plain(self, domain, **kwargs):
        self.clear_params('wwwdomain.plain')
        self.params['elid'] = domain

        return self.process_api(self.url, self.params)
