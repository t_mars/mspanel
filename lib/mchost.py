import base64
from lib.mygrab import MyGrab

class MCHostClient(object):
    def __init__(self, username, password):
        self.grab = MyGrab()
        
        self.grab.go('https://cp.mchost.ru/login.php')
        if not self.grab.get_cookie('MCHOSTID'):
            raise Exception('MCHOST auth error')
        
        h = base64.encodestring('%s:%s' % (username, password)).replace('\n', '')
        self.grab.go('https://cp.mchost.ru/login.php', 
            headers={"Authorization": "Basic %s" % h})

    def add_mass(self, domains):
        ns = '\n'.join(domains)
        print 'add_mass',len(domains)
        print ns
        try:
            self.grab.go('https://cp.mchost.ru/services_add_mass.php?pagetype=frame&service_type_id=5', 
                post={'save':'1', 'pre':'1', 'service_type_id':'5', 'domain_names':ns})
        except:
            return False

        return True
        
    def delete_domains(self, domains):
        if not hasattr(self, 'domain_map'):
            self.grab.go('https://cp.mchost.ru/domains.php')
            self.domain_map = {}
            for row in self.grab.doc.select('//tr[starts-with(@id,"dom_")]'):
                k = row.attr('id')[4:]
                v = row.select('./td/input[@name="els[]"]').attr('value')
                self.domain_map[k] = v

        domain_ids = []
        for d in domains:
            if d in self.domain_map:
                domain_ids.append(self.domain_map[d])

        data = [('action','delete_multi')]        
        for did in domain_ids:
            data.append(('els[]',did))
        
        try:
            self.grab.go('https://cp.mchost.ru/domains.php', post=data)
        except:
            return False

        return True