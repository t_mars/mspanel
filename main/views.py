#coding=utf8
import datetime
import json
import time
import random
import string

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.core.cache import cache
from django.utils import timezone
from django import forms
from django.db import connection

from datetimewidget.widgets import DateTimeWidget

from sender.models import *
from main.models import *

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def pack(qs, items, sitems, prefix=''):
    if prefix:
        prefix += ' > '
    for i in qs:
        items.append({
            'id': i.get_id(), 
            'text': '%s%s' % (prefix, i.get_name())
        })
        sitems.append(i)

def filt(root):
    class_name = root.__class__.__name__

    if class_name == 'Continent':
        return Country.objects.filter(continent=root)
        
    if class_name == 'Country':
        # return Region.objects.filter(country=root)
        return City.objects.filter(country=root)
    
    if class_name == 'Region':
        return City.objects.filter(region=root)

def target_geo_search(request):
    items = []
    sitems = []
    reset = False
    root = None
    limit = 10
    flag = False

    q = request.GET.get('q').lower().strip()
    r = request.GET.get('r')

    if q.endswith('.'):
        flag = True
        q = q[:-1]

    if r:
        root = Geo.get(r)

        qs = filt(root)
        if q:
            qs = qs.filter(name__istartswith=q)
        
        # raise Exception(qs.count(), str(qs.query))
        items=[];sitems=[]
        pack(qs, items, sitems, root.get_name())
    else:
        for model in Geo.models():
            qs = model.objects.filter(name__istartswith=q)
            pack(qs, items, sitems)

    if flag:
        root = sitems[0]
        reset = True
        
        qs = filt(root)

        items=[];sitems=[]
        pack(qs, items, sitems, root.get_name())

    data = {
        'items': items,
        'total_count': len(items),
        'reset': reset,
        'root': root.get_id() if root else '',
    }
    return HttpResponse(json.dumps(data))

def handle_uploaded_file(f, filename):
    with open(filename, 'wb+') as dest:
        for chunk in f.chunks():
            dest.write(chunk)

def load_accounts(request):
    class UploadForm(forms.Form):
        _file = forms.FileField(label='Файл с аккаунтами', max_length=100)

    if request.method == 'POST':
        form = UploadForm(request.POST, request.FILES)
        if form.is_valid():
            filename = '/tmp/accounts' + ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(4))
            handle_uploaded_file(request.FILES['_file'], filename)
            t = Task()
            t.load_accounts(filename)
            t.run_now()
            return HttpResponseRedirect('/admin/account/load/')
    else:
        form = UploadForm()

    task = Task.GET('accounts_heatup')
    if 'start' in request.GET:
        task.restart()
        task.run_now()
        time.sleep(1)
        return HttpResponseRedirect('/admin/account/load/')

    if 'stop' in request.GET:
        task.kill()
        time.sleep(1)
        return HttpResponseRedirect('/admin/account/load/')
    ms = MailRuAccount.objects
    
    total = ms.count()
    active = ms.filter(is_active=True).count()
    unactive = ms.filter(is_active=False).count()
    loaded = ms.filter(is_active=True,auth_data__isnull=False).count()

    s = None
    t = Task.GET('load_accounts')
    if t:
        s = t.info

    return render(request, 'load_accounts.html', {
        'form': form,
        's': s,
        'task': task,
        'total': total,
        'active': active,
        'unactive': unactive,
        'loaded': loaded,
    })


def reg_accounts(request):
    from django import forms
    import time

    task = Task.GET('accounts_reg')
    
    idents = [('antigate', 'AntiGate.com'), ('rucaptcha', 'RuCaptcha.com')]
    
    now = timezone.now()
    start = now + datetime.timedelta(minutes=2)
    stop = now + datetime.timedelta(minutes=7)

    class TaskForm(forms.Form):
        thread_count = forms.IntegerField(
            label='Количество потоков', initial=task.get_argument('thread_count'))
        limit = forms.IntegerField(
            label='Количество успехов', initial=task.get_argument('limit'), help_text='0-бесконечно')
        start_now = forms.BooleanField(
            label='Начать сейчас', required=False, initial=task.start_at is None)
        start_at = forms.DateTimeField(
            label='Время начала', required=False, initial=task.start_at or start)
        stop_at = forms.DateTimeField(
            label='Время конца', required=False, initial=task.stop_at or stop)
        captcha_idents = forms.MultipleChoiceField(
            label='Распознавание капчи', choices=idents, initial=task.get_argument('captcha_idents'))
        new_script = forms.BooleanField(
            label='Новый алгоритм (Рамблер)', required=False)

        def clean(self):
            data = super(TaskForm, self).clean()
            
            if not data.get('start_now', False):
                if not data.get('start_at'):
                    self.add_error('start_at', forms.ValidationError('Укажите время начала.'))
            
                if not data.get('stop_at'):
                    self.add_error('stop_at', forms.ValidationError('Укажите время конца.'))
            
            return data

    if task.is_active_proc():
        form = None
    else:
        if request.method == 'POST':
            form = TaskForm(request.POST)
            if form.is_valid():
                if form.cleaned_data['new_script']:
                    task.command = 'account/reg_new.py'
                    task.kill_command = 'account/reg_new.py kill'
                else:
                    task.command = 'account/reg.py'
                    task.kill_command = 'account/reg.py kill'
                task.arguments = {
                    'limit': form.cleaned_data['limit'],
                    'thread_count': form.cleaned_data['thread_count'],
                    'captcha_idents': form.cleaned_data['captcha_idents'],
                }
                task.save()

                if form.cleaned_data['start_now']:
                    task.restart()
                    task.start_at = None
                    task.stop_at = None
                    task.save()
                    task.run_now()
                    time.sleep(1)
                else:
                    task.restart()
                    task.start_at = form.cleaned_data['start_at']
                    task.stop_at = form.cleaned_data['stop_at']
                    task.save()
                
                return HttpResponseRedirect('/admin/account/reg/')

        else:
            form = TaskForm()

    ms = MailRuAccount.objects
    
    total = ms.count()
    active = ms.filter(is_active=True).count()
    unactive = ms.filter(is_active=False).count()
    loaded = ms.filter(is_active=True,auth_data__isnull=False).count()
    generated = ms.filter(sex__isnull=False).count()
    
    logs = TaskLog.objects \
        .filter(task=task) \
        .order_by('-id')[:10]

    if 'stop' in request.GET:
        task.kill()
        time.sleep(1)
        return HttpResponseRedirect('/admin/account/reg/')
    
    try:
        log = TaskLog.objects.get(task=task, pk=request.GET['id'])
    except:
        try:
            log = task.log 
        except:
            log = None
    if log:
        # try:
        started_at, stopped_at = log.get_work_range()
        
        try:
            data = json.loads(log.result)
        except:
            data = None
            stat = None
    
        if data:
            try:
                d = data['download_size'] / 1024.0
                
                if data['counts']['user']:
                    p = (stopped_at-started_at).total_seconds() / float(data['counts']['user'])
                    dp = d / float(data['counts']['user'])
                else:
                    p = 0
                    dp = 0

                l1 = '/admin/main/mailruaccount/?task_log_id__exact=%d' % log.id
                l2 = '/admin/main/mailruaccount/?task_log_id__exact=%d&auth_data__isnull=0' % log.id
                l3 = '/admin/main/mailruaccount/?task_log_id__exact=%d&has_my__exact=1' % log.id
                stat = [
                    ('Время начало', started_at),
                    ('Время конца', stopped_at),
                    ('Зарегистрировано', '<a href="%s">%d</a>' % (l1, data['counts']['user'])),
                    (' - авторизировано', '<a href="%s">%d</a>' % (l2, data['counts']['user_auth_data'])),
                    (' - мой мир', '<a href="%s">%s</a>' % (l3, data['counts'].get('has_my','-'))),
                    ('Попыток', data['try_count']),
                    ('В обработке', data['current']),
                    
                    ('Логины', '---'),
                    ('Предложили логин', data['counts'].get('yes_login', '-')),
                    ('Не предложили логин', data['counts'].get('no_login', '-')),
                    
                    ('Ошибки', '---'),
                    ('Ошибок подряд', data.get('error_count','-')),
                    ('Максимум ошибок подряд', data.get('error_limit','-')),
                    
                    ('Ошибки по видам', '---'),
                    ('Пустая форма', data['counts'].get('error_form', '-')),
                    ('Ошибок сети', data['counts'].get('net_error', '-')),
                    ('Не принято', data['counts'].get('no_reg', '-')),
                    ('Забанено IP (телефон)', data['counts'].get('ban_ip', '-')),
                    ('Забанено IP', data['counts'].get('reg_ban_ip', '-')),
                    ('Неправильная капча', data['counts'].get('no_captcha', '-')),
                    ('Недостаток баланса', data['counts'].get('zero_balance', '-')),
                    
                    ('Статистика', '---'),
                    ('Время на регистрацию одного(с)', '%2.2f' % p),
                    ('Траффика всего (кб)', '%2.2f' % d),
                    ('Траффика на регистрацию (кб)', '%2.2f' % dp),
                ]

                for name, counts in data.get('counts_idents', {}).iteritems():
                    if name == 'rucaptcha':
                        link = 'http://pix.academ.org/img/2015/09/04/ed2094f671099d801a8a7e24051361dc.png'
                    elif name == 'antigate':
                        link = 'http://pix.academ.org/img/2015/09/04/f18fefc103d0bf6270d00d8e87792d03.png'
                    stat += [
                        ('Статистика ' + name.encode('utf8'), '<a href="%s" target="_blank">Коды ошибок</a>' % link),
                        ('Обрщений', counts.pop('try_count', '-')),
                        ('Успешно', counts.pop('success', '-')),
                        ('Ошибок', counts.pop('failure', '-')),
                    ]
                    for k, v in counts.iteritems():
                        stat.append( (k, v) )
            except:
                stat = None

        args = json.loads(log.arguments)
        params = [
            ('Параметры', '---'),
            ('Количество потоков', args['thread_count']),
            ('Максимальное количество', args['limit']),
            ('Распознавание капчи', ','.join(args['captcha_idents'])),
        ]
    else:
        stat = None
        params = None

    return render(request, 'reg_accounts.html', {
        'form': form,
        'task': task,
        'stat': stat,
        'params': params,

        'total': total,
        'active': active,
        'unactive': unactive,
        'loaded': loaded,
        'generated': generated,

        'logs': logs,
        'log': log,
    })

def proxy_stat(request):
    from django.core.cache import cache
    import subprocess, os

    host, ports = Config.GET('send_mail.proxy')
    
    stat = []

    if 'set_restart_time' in request.GET:
        try:
            config = Config.objects.get(group='proxy', name='restart_time')
            config.value = float(request.GET['restart_time'])
            config.is_active = True
            config.save()
        except:
            pass
        return HttpResponseRedirect('/admin/proxy/stat/')
        
    if 'reboot' in request.GET:
        port = int(request.GET['reboot'])
        
        proxy = Config.GET('send_mail.proxy', asdict=True)
        ind = port - proxy['port1'] + 1

        cmd = 'bash /home/u1/run%d' % (ind)
        if 'out' in request.GET:
            p = subprocess.Popen(
                cmd.split(' '),
                stdout=subprocess.PIPE, 
                stderr=subprocess.PIPE
            )
            o, e = p.communicate()
        else:
            p = subprocess.Popen(
                cmd.split(' '),
                stdout=subprocess.PIPE, 
                stderr=subprocess.STDOUT
            )
            o, e = '---', '---'
        request.session['result'] = 'cmd:\n%s\nout:\n%s\nerr:\n%s\nreturncode:%s' % (cmd, o, e, p.returncode)
        # raise Exception(cmd)

        return HttpResponseRedirect('/admin/proxy/stat/')

    try:
        proxy = Config.GET('send_mail.proxy', rand=True, asdict=True)
    except:
        proxy = False
    
    for port in ports:
        stat.append({
            'port': port,
            'is_active': cache.get('%s:%d' % (host, port))            
        })

    return render(request, 'proxy_stat.html', {
        'restart_time': Config.GET('proxy.restart_time', 0),
        'proxy': proxy,
        'host': host,
        'stat': stat,    
        'result': request.session.get('result', ''),
    })

def set_proxy_port_status(request, port, value):
    proxy = Config.GET('send_mail.proxy', asdict=True)
    request_ip = get_client_ip(request)
    
    proxy_request_ips = Config.GET('proxy.request_ips', default='').split(',')
    if request_ip in proxy_request_ips:
        cache.set('%s:%s' % (proxy['host'], port), value, timeout=None)
        status = 'ok'
    else:
        status = 'access_denied'

    return HttpResponse(json.dumps({
        'port': port, 
        'status': status, 
        'request_ip': request_ip
    }))

def proxy_port_activate(request, port):
    return set_proxy_port_status(request, port, timezone.now())

def proxy_port_unactivate(request, port):
    return set_proxy_port_status(request, port, False)

def proxy_port_stat(request, port):

    class SelectForm(forms.Form):
        class Meta:
            widgets = {
                'datetime': DateTimeWidget(attrs={'id':"yourdatetimeid"}, usel10n = True, bootstrap_version=3)
            }
        begin = forms.DateTimeField(widget=DateTimeWidget(usel10n = True, bootstrap_version=3), required=True)
        end = forms.DateTimeField(widget=DateTimeWidget(usel10n = True, bootstrap_version=3), required=False)

    begin = None
    end = None
    if 'submit' in request.GET:
        form = SelectForm(request.GET)
        if form.is_valid():
            begin = form.cleaned_data['begin']
            end = form.cleaned_data['end']
    else:
        form = SelectForm()

    res = {'form': form}

    proxy = Config.GET('send_mail.proxy', asdict=True)
    ind = int(port) - proxy['port1'] + 1

    keys = ['tx', 'rx', 'total']
    for k in keys:
        key = 'proxy_%s_trafic_%s' % (ind, k)
        res[k] = []
        for c in Cache.GET_RANGE(key, begin, end):
            res[k].append( (timezone.localtime(c.created_at), c.value) )
    
    return render(request, 'proxy_trafic_stat.html', res)

def domain_stat(request):
    res = {}
    
    class SelectForm(forms.Form):
        class Meta:
            widgets = {
                'datetime': DateTimeWidget(attrs={'id':"yourdatetimeid"}, usel10n = True, bootstrap_version=3)
            }
        begin = forms.DateTimeField(widget=DateTimeWidget(usel10n = True, bootstrap_version=3), required=True)
        end = forms.DateTimeField(widget=DateTimeWidget(usel10n = True, bootstrap_version=3), required=False)
        plot_state = forms.ChoiceField(initial=-1, choices=DOMAIN_STATE_CHOICE.items() + [(-1, 'ALL')])

    rows = None
    cursor = connection.cursor()
    plot_state = -1

    cursor.execute('SELECT MIN(`created_at`) FROM `main_cache`')
    begin, end = cursor.fetchone()[0], timezone.now()
        
    if 'submit' in request.GET:
        form = SelectForm(request.GET)
        if form.is_valid():
            begin = form.cleaned_data['begin'] or begin 
            end = form.cleaned_data['end'] or end
            plot_state = int(form.cleaned_data['plot_state'])
    else:
        form = SelectForm(initial={'begin': begin})

    res['form'] = form

    by_state = {}
    STATES = DOMAIN_STATE_CHOICE
    STATES[-1] = 'ALL'
    for val, name in STATES.items():
        c1 = Cache.GET('domain_state_%s' % name.lower(), created_at=begin)
        c2 = Cache.GET('domain_state_%s' % name.lower(), created_at=end)
        by_state[name] = {
            'current': Cache.GET('domain_state_%s' % name.lower()),
            'begin': c1,
            'end': c2,
            'change': c2 - c1 if None not in [c1, c2] else 0
        }
    
    state = [
        ('Всего', '-', by_state['ALL']['current'], by_state['ALL']['change']),
        ('Активных', 'ACTIVATED', by_state['ACTIVATED']['current'], by_state['ACTIVATED']['change']),
        ('Удаленных', 'DELETED', by_state['DELETED']['current'], by_state['DELETED']['change']),
        ('Проверено', 'CHECKED,NO_CHECKED', by_state['CHECKED']['current'] + by_state['NO_CHECKED']['current'], by_state['CHECKED']['change'] + by_state['NO_CHECKED']['change']),
        ('Положительно', 'CHECKED', by_state['CHECKED']['current'], by_state['CHECKED']['change']),
        ('Отрицательно', 'NO_CHECKED', by_state['NO_CHECKED']['current'], by_state['NO_CHECKED']['change']),
        ('Повторно проверяются', 'RETRY_CHECK', by_state['RETRY_CHECK']['current'], by_state['RETRY_CHECK']['change']),
        ('Ожидают проверки', 'NEW', by_state['NEW']['current'], by_state['NEW']['change']),
    ]
    res['state'] = state

    # check
    cursor.execute("""
        SELECT `key`, SUM(`value_int`)
        FROM `main_cache`
        WHERE `created_at` >= %s
            AND `created_at` <= %s
        GROUP BY `key`
    """, [begin, end])
    rows = cursor.fetchall()
    
    # формируем статистику
    stat = {}
    for r in rows:
        stat[r[0]] = float(r[1])
    check_stat = []
    for key in ['dns', 'google', 'whois']:
        k = 'check_%s_' % key
        if k+'count' not in stat:
            check_stat.append( (key, None) )
            continue
        c = stat[k+'count']
        if c == 0:
            c = 1
        d = {
            'error': int(stat[k+'error']),    
            'error_p': stat[k+'error'] / c * 100,    
            
            'true': int(stat[k+'true']),
            'true_p': stat[k+'true'] / c * 100,
            
            'false': int(stat[k+'false']),
            'false_p': stat[k+'false'] / c * 100,
            
            'none': int(stat[k+'none']),
            'none_p': stat[k+'none'] / c * 100,
            
            'count': int(c),
            'avg_time': stat[k+'time'] / c / 100.0,
        }
        check_stat.append( (key, d) )
    res['check_stat'] = check_stat

    # график
    qs = Cache.objects \
        .filter(key='domain_state_%s' % STATES[plot_state].lower()) \
        .filter(created_at__gte=begin, created_at__lte=end) \
        .order_by('created_at')

    plot = []
    for c in qs:
        plot.append( (timezone.localtime(c.created_at), c.value) )
    res['plot'] = plot
    res['plot_title'] = STATES[plot_state]
    
    return render(request, 'domain_stat.html', res)

def mailing_add_url(request, mid, mode):
    try:
        mailing = Mailing.objects.get(pk=mid)
    except:
        return HttpResponse(json.dumps({'status':'error', 'msg':'mailing not found.'}))

    if mode not in ['pic', 'link']:
        return HttpResponse(json.dumps({'status':'error', 'msg':'mode [%s] not found.' % mode}))
    
    if mode == 'pic':
        m = 0
    elif mode == 'link':
        m = 1

    urls = request.GET.get('urls', '') or request.POST.get('urls', '')
    urls = [u.strip() for u in urls.split(',') if len(u)]

    count = 0    
    for url in urls:
        if MailingUrl.create(mailing=mailing, url=url, mode=m):
            count += 1

    return HttpResponse(json.dumps({'status':'ok', 'msg': 'added %d urls' % count}))