# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0089_mailruaccount_unactivated_at'),
    ]

    operations = [
        migrations.AddField(
            model_name='parcel',
            name='is_ban_checked',
            field=models.NullBooleanField(default=None),
        ),
    ]
