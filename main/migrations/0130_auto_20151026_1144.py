# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0129_mailing_image_gif'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailing',
            name='link_list',
            field=models.TextField(default=None, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='mailing',
            name='pic_list',
            field=models.TextField(default=None, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='mailing',
            name='use_link_list',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='mailing',
            name='use_pic_list',
            field=models.BooleanField(default=False),
        ),
    ]
