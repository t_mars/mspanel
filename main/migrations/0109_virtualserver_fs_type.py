# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0108_auto_20150915_1448'),
    ]

    operations = [
        migrations.AddField(
            model_name='virtualserver',
            name='fs_type',
            field=models.CharField(default='ssh', max_length=10, choices=[(b'ftp', b'FTP'), (b'ssh', b'SSH')]),
            preserve_default=False,
        ),
    ]
