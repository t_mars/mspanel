# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0118_auto_20151002_1444'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='parcel',
            index_together=set([('status',)]),
        ),
    ]
