# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0076_auto_20150811_0649'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailingtask',
            name='thread_count',
            field=models.PositiveIntegerField(default=50),
        ),
    ]
