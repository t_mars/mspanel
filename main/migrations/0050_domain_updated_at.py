# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0049_auto_20150805_1510'),
    ]

    operations = [
        migrations.AddField(
            model_name='domain',
            name='updated_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 8, 5, 16, 44, 25, 485712, tzinfo=utc)),
        ),
    ]
