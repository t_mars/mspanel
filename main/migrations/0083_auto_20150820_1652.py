# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0082_mailing_body_type'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mailingtask',
            name='use_card',
        ),
        migrations.AlterField(
            model_name='mailing',
            name='body_type',
            field=models.PositiveSmallIntegerField(default=0, choices=[(0, b'\xd0\x92\xd1\x80\xd1\x83\xd1\x87\xd0\xbd\xd1\x83\xd1\x8e'), (1, b'\xd0\x9e\xd1\x82\xd0\xba\xd1\x80\xd1\x8b\xd1\x82\xd0\xba\xd0\xb0'), (2, b'\xd0\xa1\xd0\xbb\xd1\x83\xd1\x87\xd0\xb0\xd0\xbd\xd0\xbe\xd0\xb5')]),
        ),
    ]
