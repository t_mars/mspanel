# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0014_auto_20150717_1435'),
    ]

    operations = [
        migrations.RenameField(
            model_name='task',
            old_name='result',
            new_name='err',
        ),
        migrations.AddField(
            model_name='task',
            name='out',
            field=models.TextField(null=True, blank=True),
        ),
    ]
