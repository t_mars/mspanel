# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0109_virtualserver_fs_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='virtualserver',
            name='host',
            field=models.CharField(default='', max_length=30),
            preserve_default=False,
        ),
    ]
