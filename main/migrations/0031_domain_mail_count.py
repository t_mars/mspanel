# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0030_auto_20150728_0801'),
    ]

    operations = [
        migrations.AddField(
            model_name='domain',
            name='mail_count',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
