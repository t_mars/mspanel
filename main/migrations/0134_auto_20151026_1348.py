# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0133_auto_20151026_1347'),
    ]

    operations = [
        migrations.RenameField(
            model_name='mailingurl',
            old_name='site',
            new_name='mode',
        ),
    ]
