# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0048_auto_20150805_1019'),
    ]

    operations = [
        migrations.CreateModel(
            name='DomainStateLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('state', models.IntegerField(default=0, choices=[(0, b'NEW'), (1, b'RETRY_CHECK'), (2, b'NO_CHECKED'), (3, b'CHECKED'), (4, b'ADDED'), (5, b'NO_ADDED'), (6, b'DELETED'), (7, b'VALIDATED'), (8, b'NO_VALIDATED'), (9, b'ACTIVATED'), (10, b'NO_ACITVATED'), (11, b'BANNED')])),
            ],
        ),
        migrations.AddField(
            model_name='domain',
            name='state',
            field=models.IntegerField(default=0, choices=[(0, b'NEW'), (1, b'RETRY_CHECK'), (2, b'NO_CHECKED'), (3, b'CHECKED'), (4, b'ADDED'), (5, b'NO_ADDED'), (6, b'DELETED'), (7, b'VALIDATED'), (8, b'NO_VALIDATED'), (9, b'ACTIVATED'), (10, b'NO_ACITVATED'), (11, b'BANNED')]),
        ),
        migrations.AddField(
            model_name='domainstatelog',
            name='domain',
            field=models.ForeignKey(to='main.Domain'),
        ),
    ]
