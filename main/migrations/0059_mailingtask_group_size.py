# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0058_remove_mailing_task'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailingtask',
            name='group_size',
            field=models.PositiveSmallIntegerField(default=3),
        ),
    ]
