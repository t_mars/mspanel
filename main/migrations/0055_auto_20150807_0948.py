# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0054_auto_20150806_1331'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailing',
            name='unactivated_account_num',
            field=models.PositiveIntegerField(default=0, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='mailing',
            name='used_account_num',
            field=models.PositiveIntegerField(default=0, null=True, blank=True),
        ),
    ]
