# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0041_auto_20150729_1804'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailing',
            name='target_without_geo',
            field=models.BooleanField(default=False),
        ),
    ]
