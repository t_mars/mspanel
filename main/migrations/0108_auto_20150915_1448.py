# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0107_auto_20150915_1306'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='domain',
            index_together=set([('state',), ('name',), ('updated_at',), ('state', 'updated_at')]),
        ),
    ]
