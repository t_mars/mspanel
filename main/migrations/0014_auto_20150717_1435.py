# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0013_domain_main_name_server'),
    ]

    operations = [
        migrations.AlterField(
            model_name='domain',
            name='name',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='nameserver',
            name='name',
            field=models.CharField(max_length=100),
        ),
    ]
