# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0099_auto_20150904_1559'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailruaccount',
            name='task_log',
            field=models.ForeignKey(default=None, blank=True, to='main.TaskLog', null=True),
        ),
    ]
