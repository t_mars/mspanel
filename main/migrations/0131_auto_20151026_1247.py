# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0130_auto_20151026_1144'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailing',
            name='link_list',
            field=models.TextField(default=b''),
        ),
        migrations.AlterField(
            model_name='mailing',
            name='pic_list',
            field=models.TextField(default=b''),
        ),
    ]
