# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0127_auto_20151014_1829'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailingtask',
            name='site',
            field=models.IntegerField(default=0, null=True, blank=True, choices=[(0, b'Mail.ru'), (1, b'Rambler.ru')]),
        ),
    ]
