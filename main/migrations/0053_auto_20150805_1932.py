# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0052_auto_20150805_1932'),
    ]

    operations = [
        migrations.AlterField(
            model_name='domain',
            name='updated_at',
            field=models.DateTimeField(),
        ),
    ]
