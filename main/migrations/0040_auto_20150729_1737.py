# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0039_mailing_is_targeted'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailing',
            name='target_sex',
            field=models.CharField(default=None, max_length=10),
        ),
    ]
