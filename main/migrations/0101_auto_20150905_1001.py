# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0100_mailruaccount_task_log'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailruaccount',
            name='task_log',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, default=None, blank=True, to='main.TaskLog', null=True),
        ),
    ]
