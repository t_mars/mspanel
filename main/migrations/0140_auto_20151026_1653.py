# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0139_mailingurlparcel'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailingurl',
            name='banned_at',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='mailingurl',
            name='is_banned',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='mailingurl',
            name='mail_count',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='mailingurl',
            name='spam_count1',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='mailingurl',
            name='spam_count2',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
