# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0056_auto_20150807_0952'),
    ]

    operations = [
        migrations.CreateModel(
            name='MailingTask',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('used_account_num', models.PositiveIntegerField(default=0, null=True, verbose_name='\u0418\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u043d\u043e \u0430\u043a\u043a\u0430\u0443\u043d\u0442\u043e\u0432', blank=True)),
                ('unactivated_account_num', models.PositiveIntegerField(default=0, null=True, verbose_name='\u0417\u0430\u0431\u0430\u043d\u0435\u043d\u043d\u043e \u0430\u043a\u043a\u0430\u0443\u043d\u0442\u043e\u0432', blank=True)),
                ('started_at', models.DateTimeField(auto_now_add=True)),
                ('finished_at', models.DateTimeField(null=True, blank=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='mailing',
            name='unactivated_account_num',
        ),
        migrations.RemoveField(
            model_name='mailing',
            name='used_account_num',
        ),
        migrations.RemoveField(
            model_name='parcel',
            name='mailing',
        ),
        migrations.RemoveField(
            model_name='parcel',
            name='task',
        ),
        migrations.AddField(
            model_name='mailingtask',
            name='mailing',
            field=models.ForeignKey(to='main.Mailing'),
        ),
        migrations.AddField(
            model_name='mailingtask',
            name='task',
            field=models.ForeignKey(to='main.Task'),
        ),
        migrations.AddField(
            model_name='parcel',
            name='mailing_task',
            field=models.ForeignKey(default=None, blank=True, to='main.MailingTask', null=True),
        ),
    ]
