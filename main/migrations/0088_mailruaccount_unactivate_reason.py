# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0087_auto_20150824_1530'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailruaccount',
            name='unactivate_reason',
            field=models.IntegerField(default=0, null=True, blank=True, choices=[(0, b'SEND'), (1, b'SPAM_CHECK'), (2, b'CONTROL_CHECK')]),
        ),
    ]
