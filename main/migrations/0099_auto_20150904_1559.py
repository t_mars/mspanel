# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0098_auto_20150904_1239'),
    ]

    operations = [
        migrations.AlterField(
            model_name='config',
            name='value_type',
            field=models.IntegerField(default=0, choices=[(0, b'PROXY_RANGE'), (1, b'PERCENT'), (2, b'TEXT'), (3, b'POSITIVE_INT')]),
        ),
    ]
