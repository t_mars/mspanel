# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0025_auto_20150727_0936'),
    ]

    operations = [
        migrations.AddField(
            model_name='domain',
            name='added_at',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='domain',
            name='checked_at',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='domain',
            name='is_valid',
            field=models.NullBooleanField(default=None),
        ),
    ]
