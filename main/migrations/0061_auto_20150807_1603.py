# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0060_mailruaccount_unactivated_mailing_task'),
    ]

    operations = [
        migrations.AlterField(
            model_name='parcel',
            name='is_banned',
            field=models.NullBooleanField(default=None),
        ),
    ]
