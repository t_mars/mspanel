# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0046_auto_20150804_1905'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailruaccount',
            name='checked_at',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='mailruaccount',
            name='need_check',
            field=models.NullBooleanField(default=None),
        ),
        migrations.AddField(
            model_name='mailruaccount',
            name='sended_at',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
    ]
