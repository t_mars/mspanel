# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0125_auto_20151011_1606'),
    ]

    operations = [
        migrations.AddField(
            model_name='parcel',
            name='comment',
            field=models.TextField(default=None, null=True, blank=True),
        ),
    ]
