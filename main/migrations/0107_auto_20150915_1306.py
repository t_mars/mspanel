# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0106_auto_20150915_1035'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cache',
            name='value',
        ),
        migrations.AddField(
            model_name='cache',
            name='typ',
            field=models.PositiveSmallIntegerField(default=0),
        ),
    ]
