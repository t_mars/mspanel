# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0010_auto_20150717_1240'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='arguments',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='name',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='status_string',
            field=models.TextField(null=True, blank=True),
        ),
    ]
