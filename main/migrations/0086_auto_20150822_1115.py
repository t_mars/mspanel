# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0085_mailingtask_recipients'),
    ]

    operations = [
        migrations.AlterField(
            model_name='config',
            name='value_type',
            field=models.IntegerField(default=0, choices=[(0, b'PROXY_RANGE'), (1, b'PERCENT')]),
        ),
        migrations.AlterField(
            model_name='mailing',
            name='body_type',
            field=models.PositiveSmallIntegerField(default=0, choices=[(0, b'\xd0\x92\xd1\x80\xd1\x83\xd1\x87\xd0\xbd\xd1\x83\xd1\x8e'), (1, b'\xd0\x9e\xd1\x82\xd0\xba\xd1\x80\xd1\x8b\xd1\x82\xd0\xba\xd0\xb0'), (2, b'\xd0\xa1\xd0\xbb\xd1\x83\xd1\x87\xd0\xb0\xd0\xb9\xd0\xbd\xd0\xbe\xd0\xb5 \xd1\x81\xd0\xbe\xd0\xb4\xd0\xb5\xd1\x80\xd0\xb6\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5')]),
        ),
        migrations.AlterField(
            model_name='mailingtask',
            name='body_type',
            field=models.PositiveSmallIntegerField(default=0, choices=[(0, b'\xd0\x92\xd1\x80\xd1\x83\xd1\x87\xd0\xbd\xd1\x83\xd1\x8e'), (1, b'\xd0\x9e\xd1\x82\xd0\xba\xd1\x80\xd1\x8b\xd1\x82\xd0\xba\xd0\xb0'), (2, b'\xd0\xa1\xd0\xbb\xd1\x83\xd1\x87\xd0\xb0\xd0\xb9\xd0\xbd\xd0\xbe\xd0\xb5 \xd1\x81\xd0\xbe\xd0\xb4\xd0\xb5\xd1\x80\xd0\xb6\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5')]),
        ),
    ]
