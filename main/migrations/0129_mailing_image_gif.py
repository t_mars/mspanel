# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0128_mailingtask_site'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailing',
            name='image_gif',
            field=models.BooleanField(default=True),
        ),
    ]
