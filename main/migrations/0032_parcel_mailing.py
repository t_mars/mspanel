# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0031_domain_mail_count'),
    ]

    operations = [
        migrations.AddField(
            model_name='parcel',
            name='mailing',
            field=models.ForeignKey(to='main.Mailing'),
            preserve_default=False,
        ),
    ]
