# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0135_auto_20151026_1409'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='mailingurl',
            index_together=set([('url', 'mode')]),
        ),
    ]
