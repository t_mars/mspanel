# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0064_auto_20150808_0909'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailingtask',
            name='download_size',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
