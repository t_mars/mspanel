# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0050_domain_updated_at'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='domain',
            name='activated_at',
        ),
        migrations.RemoveField(
            model_name='domain',
            name='checked_at',
        ),
        migrations.RemoveField(
            model_name='domain',
            name='is_active',
        ),
        migrations.RemoveField(
            model_name='domain',
            name='is_checked',
        ),
        migrations.RemoveField(
            model_name='domain',
            name='is_valid',
        ),
        migrations.RemoveField(
            model_name='domain',
            name='validated_at',
        ),
        migrations.AddField(
            model_name='domain',
            name='spam_count',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='domain',
            name='updated_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 8, 5, 19, 22, 11, 836099, tzinfo=utc)),
        ),
    ]
