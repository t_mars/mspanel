# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0011_auto_20150717_1252'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='result',
            field=models.TextField(null=True, blank=True),
        ),
    ]
