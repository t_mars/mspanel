# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0101_auto_20150905_1001'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailruaccount',
            name='has_my',
            field=models.NullBooleanField(default=None),
        ),
    ]
