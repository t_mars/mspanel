# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0090_parcel_is_ban_checked'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailruaccount',
            name='birthdate',
            field=models.DateField(default=None, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='mailruaccount',
            name='firstname',
            field=models.CharField(default=None, max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='mailruaccount',
            name='lastname',
            field=models.CharField(default=None, max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='mailruaccount',
            name='sex',
            field=models.PositiveSmallIntegerField(default=None, null=True, blank=True),
        ),
    ]
