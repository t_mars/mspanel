# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0123_auto_20151007_1519'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='parcel',
            index_together=set([('is_banned', 'status'), ('status',)]),
        ),
    ]
