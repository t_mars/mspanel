# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0068_config_value_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='config',
            name='value_type',
            field=models.IntegerField(default=0, choices=[(0, b'PROXY_RANGE')]),
        ),
    ]
