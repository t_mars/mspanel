# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20150717_0733'),
    ]

    operations = [
        migrations.CreateModel(
            name='Domain',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('expiry_date', models.DateField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now_add=True)),
                ('alias_to', models.ForeignKey(blank=True, to='main.Domain', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='NameServer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
            ],
        ),
        migrations.AlterField(
            model_name='virtualserver',
            name='expiry_date',
            field=models.DateField(),
        ),
        migrations.AddField(
            model_name='domain',
            name='name_servers',
            field=models.ManyToManyField(to='main.NameServer'),
        ),
        migrations.AddField(
            model_name='domain',
            name='virtual_server',
            field=models.ForeignKey(blank=True, to='main.VirtualServer', null=True),
        ),
    ]
