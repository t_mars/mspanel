# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0038_auto_20150729_1732'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailing',
            name='is_targeted',
            field=models.BooleanField(default=False),
        ),
    ]
