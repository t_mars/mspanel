# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0018_mailruaccount'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailruaccount',
            name='mail_count',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
