# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0036_auto_20150728_1623'),
    ]

    operations = [
        migrations.AlterField(
            model_name='nameserver',
            name='name',
            field=models.CharField(unique=True, max_length=100),
        ),
    ]
