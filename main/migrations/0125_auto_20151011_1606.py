# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0124_auto_20151011_1604'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='parcel',
            index_together=set([('status',), ('id', 'is_banned', 'status')]),
        ),
    ]
