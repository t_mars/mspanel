# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0131_auto_20151026_1247'),
    ]

    operations = [
        migrations.CreateModel(
            name='MailingUrl',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.TextField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('mode', models.CharField(max_length=128)),
                ('mailing', models.ForeignKey(to='main.Mailing')),
            ],
        ),
    ]
