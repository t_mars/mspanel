# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0111_domain_dns_ip'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='virtualserver',
            name='ip',
        ),
        migrations.AlterField(
            model_name='virtualserver',
            name='manager_type',
            field=models.CharField(max_length=10, choices=[(b'isp', b'ISP'), (b'dreamhost', b'DreamHost')]),
        ),
    ]
