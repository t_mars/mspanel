# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0040_auto_20150729_1737'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailing',
            name='target_sex',
            field=models.CharField(default=None, max_length=10, null=True, blank=True),
        ),
    ]
