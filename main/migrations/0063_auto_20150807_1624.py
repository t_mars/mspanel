# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0062_mailruaccount_is_checked'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mailruaccount',
            name='need_check',
        ),
        migrations.AlterField(
            model_name='mailruaccount',
            name='is_checked',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='parcel',
            name='is_banned',
            field=models.BooleanField(default=False),
        ),
    ]
