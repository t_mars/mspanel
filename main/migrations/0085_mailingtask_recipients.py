# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0084_mailingtask_body_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailingtask',
            name='recipients',
            field=models.CharField(default=b'test', max_length=50),
        ),
    ]
