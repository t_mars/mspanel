# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0061_auto_20150807_1603'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailruaccount',
            name='is_checked',
            field=models.NullBooleanField(default=None),
        ),
    ]
