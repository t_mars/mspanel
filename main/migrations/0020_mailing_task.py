# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0019_mailruaccount_mail_count'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailing',
            name='task',
            field=models.ForeignKey(blank=True, to='main.Task', null=True),
        ),
    ]
