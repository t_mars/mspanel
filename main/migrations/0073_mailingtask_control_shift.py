# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0072_auto_20150810_1229'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailingtask',
            name='control_shift',
            field=models.PositiveSmallIntegerField(default=0),
        ),
    ]
