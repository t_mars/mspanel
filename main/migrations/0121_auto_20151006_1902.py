# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0120_auto_20151006_1751'),
    ]

    operations = [
        migrations.AlterField(
            model_name='parcel',
            name='sender_target',
            field=models.ForeignKey(related_name='target', default=None, blank=True, to='sender.Target', null=True),
        ),
    ]
