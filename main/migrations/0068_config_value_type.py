# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0067_config'),
    ]

    operations = [
        migrations.AddField(
            model_name='config',
            name='value_type',
            field=models.IntegerField(default=0, choices=[(0, b'IP:PORT range')]),
        ),
    ]
