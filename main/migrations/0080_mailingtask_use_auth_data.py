# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0079_mailruaccount_auth_data'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailingtask',
            name='use_auth_data',
            field=models.BooleanField(default=False),
        ),
    ]
