# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0105_auto_20150914_1724'),
    ]

    operations = [
        migrations.AddField(
            model_name='cache',
            name='value_int',
            field=models.IntegerField(default=None, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='cache',
            name='value_str',
            field=models.TextField(default=None, null=True, blank=True),
        ),
    ]
