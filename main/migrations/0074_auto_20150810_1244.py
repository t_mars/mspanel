# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0073_mailingtask_control_shift'),
    ]

    operations = [
        migrations.AlterField(
            model_name='controlparcel',
            name='status',
            field=models.IntegerField(default=None, null=True, blank=True, choices=[(0, b'NOT_COME'), (1, b'IN_SPAM'), (2, b'IN_INBOX')]),
        ),
    ]
