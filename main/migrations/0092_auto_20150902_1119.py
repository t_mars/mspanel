# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0091_auto_20150902_1012'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailruaccount',
            name='unactivate_reason',
            field=models.IntegerField(default=None, null=True, blank=True, choices=[(0, b'SEND'), (1, b'SPAM_CHECK'), (2, b'CONTROL_CHECK')]),
        ),
    ]
