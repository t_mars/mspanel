# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0110_virtualserver_host'),
    ]

    operations = [
        migrations.AddField(
            model_name='domain',
            name='dns_ip',
            field=models.GenericIPAddressField(default=None, null=True, blank=True),
        ),
    ]
