# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0047_auto_20150805_1013'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='domain',
            index_together=set([('name',)]),
        ),
    ]
