# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0059_mailingtask_group_size'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailruaccount',
            name='unactivated_mailing_task',
            field=models.ForeignKey(blank=True, to='main.MailingTask', null=True),
        ),
    ]
