# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0009_auto_20150717_0818'),
    ]

    operations = [
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField(null=True)),
                ('command', models.TextField()),
                ('arguments', models.TextField()),
                ('result', models.TextField()),
                ('status', models.IntegerField(default=0, choices=[(0, b'NEW'), (1, b'EXECUTE'), (2, b'SUCCESS'), (3, b'FAILURE'), (4, b'KILL'), (5, b'KILLED')])),
                ('status_string', models.TextField()),
                ('pid', models.PositiveSmallIntegerField(null=True, blank=True)),
                ('try_count', models.PositiveSmallIntegerField(default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.AddField(
            model_name='domain',
            name='is_checked',
            field=models.NullBooleanField(default=None),
        ),
        migrations.AlterField(
            model_name='domain',
            name='name',
            field=models.CharField(max_length=30),
        ),
    ]
