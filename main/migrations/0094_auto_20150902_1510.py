# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0093_auto_20150902_1422'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='started_at',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='task',
            name='stop_at',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='task',
            name='stopped_at',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
    ]
