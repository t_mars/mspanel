# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0134_auto_20151026_1348'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailingurl',
            name='url',
            field=models.CharField(max_length=1024),
        ),
    ]
