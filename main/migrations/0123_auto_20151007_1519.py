# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0011_targetemail'),
        ('main', '0122_auto_20151007_1303'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='parcel',
            name='sender_email',
        ),
        migrations.RemoveField(
            model_name='parcel',
            name='sender_target',
        ),
        migrations.AddField(
            model_name='parcel',
            name='target_email',
            field=models.ForeignKey(default=None, blank=True, to='sender.TargetEmail', null=True),
        ),
    ]
