# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0132_mailingurl'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mailingurl',
            name='mode',
        ),
        migrations.AddField(
            model_name='mailingurl',
            name='site',
            field=models.IntegerField(default=0, choices=[(0, b'PIC'), (1, b'LINK')]),
        ),
    ]
