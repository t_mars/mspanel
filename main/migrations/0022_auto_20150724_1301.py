# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0021_virtualserver_document_root'),
    ]

    operations = [
        migrations.AddField(
            model_name='virtualserver',
            name='manager_login',
            field=models.CharField(default='u1', max_length=30),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='virtualserver',
            name='manager_password',
            field=models.CharField(default='Kfylsi', max_length=30),
            preserve_default=False,
        ),
    ]
