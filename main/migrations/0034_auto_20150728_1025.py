# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0033_auto_20150728_1021'),
    ]

    operations = [
        migrations.AlterField(
            model_name='parcel',
            name='parent',
            field=models.ForeignKey(default=None, blank=True, to='main.Parcel', null=True),
        ),
    ]
