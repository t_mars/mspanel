# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0051_auto_20150805_1922'),
    ]

    operations = [
        migrations.AddField(
            model_name='parcel',
            name='is_banned',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='domain',
            name='updated_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 8, 5, 19, 32, 9, 910703, tzinfo=utc)),
        ),
    ]
