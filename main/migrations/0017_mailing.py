# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0016_auto_20150720_1247'),
    ]

    operations = [
        migrations.CreateModel(
            name='Mailing',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('to', models.TextField()),
                ('subject', models.TextField()),
                ('link', models.CharField(max_length=200)),
                ('body', tinymce.models.HTMLField()),
            ],
        ),
    ]
