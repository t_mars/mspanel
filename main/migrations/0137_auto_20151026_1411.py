# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0136_auto_20151026_1410'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mailing',
            name='link_list',
        ),
        migrations.RemoveField(
            model_name='mailing',
            name='pic_list',
        ),
    ]
