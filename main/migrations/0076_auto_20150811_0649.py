# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0075_auto_20150810_1536'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailruaccount',
            name='unactivated_mailing_task',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='main.MailingTask', null=True),
        ),
    ]
