# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0081_mailingtask_use_card'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailing',
            name='body_type',
            field=models.PositiveSmallIntegerField(default=0, choices=[(0, b'BODY'), (1, b'CARD'), (2, b'RANDOM')]),
        ),
    ]
