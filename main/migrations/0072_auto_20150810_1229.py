# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0071_config_is_active'),
    ]

    operations = [
        migrations.CreateModel(
            name='ControlParcel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.IntegerField(default=None, choices=[(0, b'NOT_COME'), (1, b'IN_SPAM'), (2, b'IN_INBOX')])),
                ('checked_at', models.DateTimeField(default=None, null=True, blank=True)),
                ('account', models.ForeignKey(to='main.MailRuAccount')),
            ],
        ),
        migrations.AddField(
            model_name='parcel',
            name='is_control',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='controlparcel',
            name='parcel',
            field=models.ForeignKey(to='main.Parcel'),
        ),
    ]
