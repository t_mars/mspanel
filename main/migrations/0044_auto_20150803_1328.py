# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0043_remove_mailing_is_targeted'),
    ]

    operations = [
        migrations.AddField(
            model_name='domain',
            name='actived_at',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='domain',
            name='is_active',
            field=models.NullBooleanField(default=None),
        ),
        migrations.AlterField(
            model_name='mailing',
            name='task',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='main.Task', null=True),
        ),
        migrations.AlterField(
            model_name='parcel',
            name='status',
            field=models.IntegerField(default=0, choices=[(0, b'NEW'), (1, b'SENDED'), (2, b'INVALID_EMAIL'), (3, b'ACCOUNT_ERROR'), (4, b'SENDING')]),
        ),
    ]
