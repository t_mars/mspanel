# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0044_auto_20150803_1328'),
    ]

    operations = [
        migrations.RenameField(
            model_name='domain',
            old_name='actived_at',
            new_name='activated_at',
        ),
    ]
