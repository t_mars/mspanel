# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0009_target_only_valid'),
        ('main', '0116_auto_20151002_1438'),
    ]

    operations = [
        migrations.AddField(
            model_name='parcel',
            name='sender_target',
            field=models.ForeignKey(default=None, blank=True, to='sender.Target', null=True),
        ),
    ]
