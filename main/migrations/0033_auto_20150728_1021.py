# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0032_parcel_mailing'),
    ]

    operations = [
        migrations.AddField(
            model_name='parcel',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 7, 28, 10, 21, 15, 265880, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='parcel',
            name='parent',
            field=models.ForeignKey(default=None, to='main.Parcel'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='parcel',
            name='status',
            field=models.IntegerField(default=0, choices=[(0, b'NEW'), (1, b'SENDED'), (2, b'INVALID_EMAIL'), (3, b'SENDING')]),
        ),
        migrations.AddField(
            model_name='parcel',
            name='try_count',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='parcel',
            name='account',
            field=models.ForeignKey(default=None, blank=True, to='main.MailRuAccount', null=True),
        ),
        migrations.AlterField(
            model_name='parcel',
            name='domain',
            field=models.ForeignKey(default=None, blank=True, to='main.Domain', null=True),
        ),
        migrations.AlterField(
            model_name='parcel',
            name='sended_at',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
    ]
