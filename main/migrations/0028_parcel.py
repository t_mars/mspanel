# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0027_task_time_shift'),
    ]

    operations = [
        migrations.CreateModel(
            name='Parcel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.CharField(max_length=200)),
                ('sended_at', models.DateTimeField(auto_now_add=True)),
                ('account', models.ForeignKey(to='main.MailRuAccount')),
                ('domain', models.ForeignKey(to='main.Domain')),
            ],
        ),
    ]
