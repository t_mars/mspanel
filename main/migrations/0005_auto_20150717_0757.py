# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20150717_0753'),
    ]

    operations = [
        migrations.AlterField(
            model_name='domain',
            name='updated_at',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
    ]
