# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0069_auto_20150810_0908'),
    ]

    operations = [
        migrations.AlterField(
            model_name='parcel',
            name='status',
            field=models.IntegerField(default=0, choices=[(0, b'NEW'), (1, b'SENDED'), (2, b'INVALID_EMAIL'), (3, b'ACCOUNT_ERROR'), (4, b'SENDING'), (5, b'CONN_TIMEOUT'), (6, b'NETWORK_ERROR'), (7, b'CONN_BREAK'), (8, b'UNKNOW_ERROR')]),
        ),
    ]
