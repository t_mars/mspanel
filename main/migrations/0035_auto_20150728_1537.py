# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0034_auto_20150728_1025'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='domain',
            name='checked_at',
        ),
        migrations.AddField(
            model_name='domain',
            name='validated_at',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
    ]
