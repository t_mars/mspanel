# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Server',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('ip', models.IPAddressField()),
                ('login', models.CharField(max_length=30)),
                ('password', models.CharField(max_length=30)),
                ('manager_type', models.CharField(max_length=10, choices=[(b'isp', b'ISP')])),
                ('os_type', models.CharField(max_length=10, choices=[(b'debian', b'Debian')])),
                ('name_server', models.CharField(max_length=30)),
                ('expiry_date', models.DateTimeField()),
            ],
        ),
    ]
