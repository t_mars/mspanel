# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0029_auto_20150728_0758'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='mailruaccount',
            index_together=set([('username',)]),
        ),
    ]
