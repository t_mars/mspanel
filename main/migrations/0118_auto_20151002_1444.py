# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0009_target_only_valid'),
        ('main', '0117_parcel_sender_target'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mailing',
            name='target_age_from',
        ),
        migrations.RemoveField(
            model_name='mailing',
            name='target_age_to',
        ),
        migrations.RemoveField(
            model_name='mailing',
            name='target_geo',
        ),
        migrations.RemoveField(
            model_name='mailing',
            name='target_sex',
        ),
        migrations.RemoveField(
            model_name='mailing',
            name='target_without_geo',
        ),
        migrations.AddField(
            model_name='mailing',
            name='target',
            field=models.ForeignKey(default=None, blank=True, to='sender.Target', null=True),
        ),
    ]
