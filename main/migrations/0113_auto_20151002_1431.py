# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0009_target_only_valid'),
        ('main', '0112_auto_20150922_1518'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='parcel',
            name='parent',
        ),
        migrations.AddField(
            model_name='parcel',
            name='email_id',
            field=models.ForeignKey(default=None, blank=True, to='sender.Email', null=True),
        ),
    ]
