# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0020_mailing_task'),
    ]

    operations = [
        migrations.AddField(
            model_name='virtualserver',
            name='document_root',
            field=models.CharField(default='/var/www/u1/data/www/', max_length=200),
            preserve_default=False,
        ),
    ]
