# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0138_auto_20151026_1438'),
    ]

    operations = [
        migrations.CreateModel(
            name='MailingUrlParcel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('mailing_url', models.ForeignKey(to='main.MailingUrl')),
                ('parcel', models.ForeignKey(to='main.Parcel')),
            ],
        ),
    ]
