# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0024_auto_20150727_0844'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='start_at',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
    ]
