# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0070_auto_20150810_0926'),
    ]

    operations = [
        migrations.AddField(
            model_name='config',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
    ]
