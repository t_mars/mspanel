# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0126_parcel_comment'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mailingtask',
            name='use_auth_data',
        ),
        migrations.AddField(
            model_name='mailruaccount',
            name='site',
            field=models.IntegerField(default=0, null=True, blank=True, choices=[(0, b'Mail.ru'), (1, b'Rambler.ru')]),
        ),
    ]
