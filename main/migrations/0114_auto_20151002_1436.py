# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0113_auto_20151002_1431'),
    ]

    operations = [
        migrations.RenameField(
            model_name='parcel',
            old_name='email_id',
            new_name='sender_email',
        ),
    ]
