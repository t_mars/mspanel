# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0077_mailingtask_thread_count'),
    ]

    operations = [
        migrations.AddField(
            model_name='domain',
            name='comment',
            field=models.CharField(default=b'', max_length=50),
        ),
    ]
