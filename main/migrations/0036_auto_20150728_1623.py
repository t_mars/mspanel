# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0035_auto_20150728_1537'),
    ]

    operations = [
        migrations.RenameField(
            model_name='domain',
            old_name='updated_at',
            new_name='checked_at',
        ),
    ]
