# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0097_task_log'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailingtask',
            name='task',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='main.Task', null=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='log',
            field=models.ForeignKey(related_name='log', on_delete=django.db.models.deletion.SET_NULL, default=None, blank=True, to='main.TaskLog', null=True),
        ),
    ]
