# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0088_mailruaccount_unactivate_reason'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailruaccount',
            name='unactivated_at',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
    ]
