# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0094_auto_20150902_1510'),
    ]

    operations = [
        migrations.CreateModel(
            name='TaskLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('arguments', models.TextField(null=True, blank=True)),
                ('result', models.TextField(null=True, blank=True)),
                ('out', models.TextField(null=True, blank=True)),
                ('err', models.TextField(null=True, blank=True)),
                ('started_at', models.DateTimeField(default=None, null=True, blank=True)),
                ('stopped_at', models.DateTimeField(default=None, null=True, blank=True)),
                ('task', models.ForeignKey(to='main.Task')),
            ],
        ),
    ]
