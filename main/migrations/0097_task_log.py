# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0096_auto_20150904_1109'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='log',
            field=models.ForeignKey(related_name='log', default=None, blank=True, to='main.TaskLog', null=True),
        ),
    ]
