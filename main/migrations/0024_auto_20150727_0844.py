# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0023_mailing_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='can_retry',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='task',
            name='kill_command',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='task',
            name='max_try_count',
            field=models.PositiveSmallIntegerField(default=None, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='task',
            name='start_at',
            field=models.DateTimeField(default=None),
        ),
        migrations.AddField(
            model_name='task',
            name='uid',
            field=models.CharField(default='', max_length=30),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='mailing',
            name='body',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='mailruaccount',
            name='updated_at',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
