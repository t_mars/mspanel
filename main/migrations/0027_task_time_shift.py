# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0026_auto_20150727_1147'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='time_shift',
            field=models.PositiveSmallIntegerField(default=None, null=True, blank=True),
        ),
    ]
