# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0095_tasklog'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tasklog',
            name='started_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 4, 8, 9, 59, 888736, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]
