# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0028_parcel'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailruaccount',
            name='username',
            field=models.CharField(unique=True, max_length=100),
        ),
    ]
