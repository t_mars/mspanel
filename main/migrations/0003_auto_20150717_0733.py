# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20150716_1416'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Server',
            new_name='VirtualServer',
        ),
    ]
