# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0102_mailruaccount_has_my'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='typ',
            field=models.IntegerField(default=0, choices=[(0, b'WORKER'), (1, b'COMMAND')]),
        ),
    ]
