# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0140_auto_20151026_1653'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailruaccount',
            name='unactivate_count',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
