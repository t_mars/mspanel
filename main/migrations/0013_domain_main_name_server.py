# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0012_auto_20150717_1252'),
    ]

    operations = [
        migrations.AddField(
            model_name='domain',
            name='main_name_server',
            field=models.ForeignKey(related_name='main_name_server', blank=True, to='main.NameServer', null=True),
        ),
    ]
