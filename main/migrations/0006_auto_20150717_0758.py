# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20150717_0757'),
    ]

    operations = [
        migrations.AlterField(
            model_name='domain',
            name='expiry_date',
            field=models.DateField(null=True, blank=True),
        ),
    ]
