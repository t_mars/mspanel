# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0063_auto_20150807_1624'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mailruaccount',
            name='is_checked',
        ),
        migrations.AlterField(
            model_name='parcel',
            name='is_banned',
            field=models.NullBooleanField(default=None),
        ),
    ]
