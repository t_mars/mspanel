# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0015_auto_20150720_0925'),
    ]

    operations = [
        migrations.AlterField(
            model_name='virtualserver',
            name='name_server',
            field=models.TextField(),
        ),
    ]
