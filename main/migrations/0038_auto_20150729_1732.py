# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0037_auto_20150728_1724'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailing',
            name='target_age_from',
            field=models.PositiveSmallIntegerField(default=None, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='mailing',
            name='target_age_to',
            field=models.PositiveSmallIntegerField(default=None, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='mailing',
            name='target_geo',
            field=models.TextField(default=None, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='mailing',
            name='target_sex',
            field=models.IntegerField(default=None, null=True, blank=True, choices=[(b'1', b'\xd0\xb6\xd0\xb5\xd0\xbd\xd1\x81\xd0\xba\xd0\xb8\xd0\xb9 1'), (b'0', b'\xd0\xbc\xd1\x83\xd0\xb6\xd1\x81\xd0\xba\xd0\xbe\xd0\xb9 0'), (b'3', b'\xd0\xbd\xd0\xb5\xd0\xb8\xd0\xb7\xd0\xb2\xd0\xb5\xd1\x81\xd1\x82\xd0\xbd\xd0\xbe 3'), (b'2', b'\xd0\xbc\xd1\x83\xd0\xb6\xd1\x81\xd0\xba\xd0\xbe\xd0\xb9 2')]),
        ),
    ]
