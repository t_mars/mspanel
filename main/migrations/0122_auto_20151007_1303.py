# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0121_auto_20151006_1902'),
    ]

    operations = [
        migrations.AlterField(
            model_name='virtualserver',
            name='manager_type',
            field=models.CharField(max_length=10, choices=[(b'isp5', b'ISP5'), (b'isp', b'ISP'), (b'dreamhost', b'DreamHost')]),
        ),
    ]
