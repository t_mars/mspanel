# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0119_auto_20151006_1450'),
    ]

    operations = [
        migrations.AlterField(
            model_name='parcel',
            name='sender_email',
            field=models.ForeignKey(related_name='parcel', default=None, blank=True, to='sender.Email', null=True),
        ),
    ]
