# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0055_auto_20150807_0948'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailing',
            name='unactivated_account_num',
            field=models.PositiveIntegerField(default=0, null=True, verbose_name='\u0417\u0430\u0431\u0430\u043d\u0435\u043d\u043d\u043e \u0430\u043a\u043a\u0430\u0443\u043d\u0442\u043e\u0432', blank=True),
        ),
        migrations.AlterField(
            model_name='mailing',
            name='used_account_num',
            field=models.PositiveIntegerField(default=0, null=True, verbose_name='\u0418\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u043d\u043e \u0430\u043a\u043a\u0430\u0443\u043d\u0442\u043e\u0432', blank=True),
        ),
    ]
