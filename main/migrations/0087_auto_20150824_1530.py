# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0086_auto_20150822_1115'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailing',
            name='fake_domain',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='mailing',
            name='fake_domain_pattern',
            field=models.CharField(default=b'', max_length=255),
        ),
        migrations.AddField(
            model_name='mailingtask',
            name='fake_domain',
            field=models.BooleanField(default=False),
        ),
    ]
