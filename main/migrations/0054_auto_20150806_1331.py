# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0053_auto_20150805_1932'),
    ]

    operations = [
        migrations.AddField(
            model_name='parcel',
            name='task',
            field=models.ForeignKey(default=1, to='main.Task'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='domain',
            name='state',
            field=models.IntegerField(default=0, choices=[(0, b'NEW'), (1, b'RETRY_CHECK'), (2, b'NO_CHECKED'), (3, b'CHECKED'), (4, b'ADDED'), (5, b'NO_ADDED'), (6, b'DELETED'), (7, b'VALIDATED'), (8, b'NO_VALIDATED'), (9, b'ACTIVATED'), (10, b'NO_ACTIVATED'), (11, b'BANNED')]),
        ),
        migrations.AlterField(
            model_name='domainstatelog',
            name='state',
            field=models.IntegerField(default=0, choices=[(0, b'NEW'), (1, b'RETRY_CHECK'), (2, b'NO_CHECKED'), (3, b'CHECKED'), (4, b'ADDED'), (5, b'NO_ADDED'), (6, b'DELETED'), (7, b'VALIDATED'), (8, b'NO_VALIDATED'), (9, b'ACTIVATED'), (10, b'NO_ACTIVATED'), (11, b'BANNED')]),
        ),
    ]
