#coding=utf8
import os
import re
from optparse import make_option

from django.core.management.base import BaseCommand

from main.models import *

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--task',
            dest='task_id',
            help=u'Номер таска для запуска',
            metavar="INT"
        ),
    )

    def handle(self, *args, **options):
        self.task = Task.objects.get(pk=options['task_id'])

        filename = self.task.get_argument('filename')

        try:
            f = open(filename)
        except:
            raise Exception('File not found: %s' % filename)

        total = 0
        new = 0
        exist = 0
        for line in f:
            print 'total =', total
            print 'line =', line
            try:
                ps = line.split(';')
                account = MailRuAccount()
                account.username = ps[0]
                account.password = ps[1]
                account.task_log = self.task.log
                print account.username, account.password,
            except:
                print 'error'
                continue

            total += 1

            try:
                account.save()
                print 'new'
                new += 1
            except:
                print 'exist'
                exist += 1

            self.task.save_status_string({
                'total': total, 
                'new': new, 
                'exist': exist, 
            })