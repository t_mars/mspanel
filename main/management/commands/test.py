#coding=utf8
import os

from django.core.management.base import BaseCommand
from django.db import transaction, connection

from main.models import *

class Command(BaseCommand):
    def handle(self, *args, **options):
        from sender.models import *
        c = connection.cursor()
        query = """
            SELECT 
                aminno_member_email.pnum source_id,
                REPLACE(aminno_member_email.email, ' ', '') email,
                am_am_member.first_name as name,
                am_am_member.last_name as surname,
                IF (am_am_member.gender = 2, 0, 1) as sex,
                am_am_member.dob as birthday
            FROM emails.aminno_member_email
            JOIN emails.am_am_member 
                ON (am_am_member.id = aminno_member_email.pnum)
            WHERE aminno_member_email.email != ''
            ORDER BY aminno_member_email.pnum
        """
        step = 1000
        shift = 2262000
        success = 0
        exist = 0
        while True:
            c.execute('%s LIMIT %d,%d' % (query, shift, step))
            shift += step
            rows = c.fetchall()
            emails = []
            for r in rows:
                res = c.execute("""
                    INSERT IGNORE
                    INTO sender_email(source, source_id, email, name, surname, sex, birthday)
                    VALUES (1, %s, %s, %s, %s, %s, %s)
                """, r)
                if res:
                    success += 1
                else:
                    exist += 1
            print 'shift=%d, success=%d, exist=%d' % (shift, success, exist)