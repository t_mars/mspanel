#coding=utf8
import os
import re
from optparse import make_option

from django.core.management.base import BaseCommand

from main.models import *

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--task',
            dest='task_id',
            help=u'Номер таска для запуска',
            metavar="INT"
        ),
    )

    def handle(self, *args, **options):
        self.task = Task.objects.get(pk=options['task_id'])

        pwd = self.task.get_argument('pwd')
        host = self.task.get_argument('host')
        user = self.task.get_argument('user')
        if not pwd or not host or not user:
            print 'no parameters'
            return

        proxy = Config.GET('send_mail.proxy', asdict=True)

        ind = 0
        for port in range(proxy['port1'], proxy['port2']):
            ind += 1
            
            cmd = 'sshpass -p %s ssh %s@%s "vnstat -i tun%d"' % (pwd, user, host, ind-1)
            print cmd
            p = os.popen(cmd)
            res = p.read()

            print ind
            try:
                rx, tx, total = re.findall(r'rx:\s+(\d{1,}\.\d{2})\s+MiB\s+tx:\s+(\d{1,}\.\d{2})\s+MiB\s+total:\s+(\d{1,}\.\d{2})\s+MiB', res)[0]
                rx = float(rx)
                tx = float(tx)
                total = float(total)
                print rx,tx,total
                Cache.SET('proxy_%d_trafic_rx' % ind, float(rx))
                Cache.SET('proxy_%d_trafic_tx' % ind, float(tx))
                Cache.SET('proxy_%d_trafic_total' % ind, float(total))

            except Exception as e:
                print str(e)
                print 'res:'
                print res
                pass