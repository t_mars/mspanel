#coding=utf8
import os
from optparse import make_option

from django.core.management.base import BaseCommand
from django.db import connection
from django.db.models import Count, Max, Min, Q
from django.utils import timezone

from main.models import *
        
class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--task',
            dest='task_id',
            help=u'Номер таска для запуска',
            metavar="INT"
        ),
    )


    def handle(self, *args, **options):
        self.task = Task.objects.get(pk=options['task_id'])
        spam_count1_limit = Config.GET('detect_ban.url_spam_count1', 10)
        spam_count2_limit = Config.GET('detect_ban.url_spam_count2', 10)

        c = connection.cursor()

        # смотрим количество отбившихся mailer-daemon
        c.execute("""
            SELECT mu.id, COUNT(DISTINCT p.account_id)
            FROM main_mailingurl mu
            LEFT OUTER JOIN main_mailingurlparcel mup 
                ON (mup.mailing_url_id=mu.id)
            LEFT OUTER JOIN main_parcel p
                ON (p.id=mup.parcel_id AND p.is_banned=1)
            GROUP BY mu.id
        """)
        for r in c.fetchall():
            url = MailingUrl.objects.get(pk=r[0])
            url.spam_count1 = r[1]
            if r[1] >= spam_count1_limit and not url.is_banned:
                print url.id, url.url, 'banned for spam'
                url.is_banned = True
                url.banned_at = timezone.now()
            url.save()


        # смотрим количество попавших в спам по контрольной рассылке
        c.execute("""
            SELECT mu.id, COUNT(DISTINCT p.account_id)
            FROM main_mailingurl mu
            LEFT OUTER JOIN main_mailingurlparcel mup 
                ON (mup.mailing_url_id=mu.id)
            LEFT OUTER JOIN main_parcel p
                ON (p.id=mup.parcel_id)
            JOIN main_controlparcel cp
                ON (cp.parcel_id=p.id AND cp.status=%s)
            GROUP BY mu.id
        """, [ControlParcel.IN_SPAM])
        for r in c.fetchall():
            url = MailingUrl.objects.get(pk=r[0])
            url.spam_count1 = r[1]
            if r[1] >= spam_count2_limit and not url.is_banned:
                print url.id, url.url, 'banned for control delivery'
                url.is_banned = True
                url.banned_at = timezone.now()
            url.save()