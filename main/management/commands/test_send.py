#coding=utf8
import os

from django.core.management.base import BaseCommand
from django.db import transaction, connection

from main.models import *

class Command(BaseCommand):
    def handle(self, *args, **options):
        from workers.mailing.send import work
        
        account = MailRuAccount.objects.get(pk=34203)
        domain = Domain.objects.get(pk=154961793)

        body = 'test'
        subject = 'test'
        used_domain = False

        task = {
            'id': '',
            'emails': {'t.mars@mail.ru':1},
            'account_id': account.id,
            'body': body,
            'subject': subject,
            'account': account,
        }

        task['auth_data'] = account.get_auth_data()
        
        if used_domain:
            task['domain_id'] = domain.id

        print 'task:', task
        res = work(task)

        print 'res:',res
