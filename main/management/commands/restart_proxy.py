#coding=utf8
from optparse import make_option
from datetime import timedelta
import subprocess
import random
import os

from django.core.management.base import BaseCommand
from django.db import transaction, connection
from django.db.models import Q
from django.core.cache import cache
from django.utils import timezone

from main.models import *

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--task',
            dest='task_id',
            help=u'Номер таска для запуска',
            metavar="INT"
        ),
    )

    def handle(self, *args, **options):
        self.task = Task.objects.get(pk=options['task_id'])

        # проверим есть ли задачи которые используют прокси
        qs = Task.objects \
            .filter(
                Q(uid__startswith='send_mailing:') | \
                Q(uid='accounts_reg')
            ) \
            .filter(status=Task.EXECUTE)
        if qs.count() == 0:
            print 'has not proxy user tasks'
            return

        print 'tasks:'
        for t in qs:
            print t.id, t.uid

        now = timezone.now()
        proxy = Config.GET('send_mail.proxy', asdict=True)
        max_time = Config.GET('proxy.restart_max_time', 2) * 60
        min_time = Config.GET('proxy.restart_min_time', 6) * 60

        print 'id =',os.popen('id').read()        
        ind = 0
        for_reboot = []
        in_reboot = 0
        for port in range(proxy['port1'], proxy['port2']):
            ind += 1
            key = '%s:%d' % (proxy['host'], port)
            val = cache.get(key)

            print '---'
            print key, val

            if val == False:
                in_reboot += 1
                print 'in reboot!'
                continue

            # ребутаем неизвестных сразу
            if val == None:
                for_reboot.append( (port, ind) )
                continue

            delta = (now - val).total_seconds()
            print 'delta =',delta
            
            # НЕ ребутаем - РАНО
            if delta < min_time:
                print 'rano'
                continue
            
            # ребутаем - ПОЗДНО
            if delta > max_time:
                print 'now!'
                for_reboot.append( (port, ind) )
                continue
            
            # случайно
            time = random.randint(min_time, max_time)
            print 'time =',time
            if time < delta:
                for_reboot.append( (port, ind) )

        # перезагружаем так, что половина всегда активна
        limit = len(proxy['ports']) / 2
        for port, ind in for_reboot:
            if in_reboot >= limit:
                print 'limit =', limit
                break
            self.reboot(port, ind)
            in_reboot += 1

    def reboot(self, port, ind):
        print 'reboot'
        import urllib2
        url = "http://136.243.84.194/proxy/stat/?reboot=%s" % port
        print 'url =', url
        res = urllib2.urlopen(url).read()
        # cmd = 'bash /home/u1/run%d' % (ind)
        # print 'cmd =',cmd
        # subprocess.Popen(
        #     cmd.split(' '),
        #     stdout=subprocess.PIPE, 
        #     stderr=subprocess.STDOUT,
        # )
            
