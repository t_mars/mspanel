#coding=utf8
import os
import time
from optparse import make_option

from django.core.management.base import BaseCommand

from main.models import *

class Command(BaseCommand):
    
    option_list = BaseCommand.option_list + (
        make_option('--task',
            dest='task_id',
            help=u'Номер таска для запуска',
            metavar="INT"
        ),
    )

    def handle(self, *args, **options):
        self.task = Task.objects.get(pk=options['task_id'])

    
        # перезаливаем скрипты
        vs = VirtualServer.objects.filter(id__in=self.task.get_argument('server_ids', []))
        for s in vs:
            print 'init server [%s]' % s.name
            self.scripts_init(s)
            for m in Mailing.objects.all():
                self.mailing_init(s, m)

        # перезаливаем рассылки
        ms = Mailing.objects.filter(id__in=self.task.get_argument('mailing_ids', []))
        for m in ms:
            for s in VirtualServer.objects.all():
                self.mailing_init(s, m)

    def command(self, cmd):
        ts = time.time()
        print cmd
        print '---'
        os.popen(cmd)
        print 'time=',time.time() - ts

    def mailing_init(self, s, m):
        print 'INIT SERVER:%d, MAILING:%d' % (s.id, m.id)
        print 'link=',m.gen_key('l')
        print 'image=',m.gen_key('i')
        
        sshpass_cmd = "sshpass -p '%s'" % s.password
        
        cmd = 'ssh-keygen -f "/root/.ssh/known_hosts" -R %s' % s.ip
        print 'Перебиваем ключи'
        self.command(cmd)

        cmd = "%s scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -r %s %s@%s:%s" % \
            (sshpass_cmd, m.image.file.name, s.login, 
                s.ip, os.path.join(s.scripts_root(), "image/source"))
        print 'Копируем изображение'
        self.command(cmd)

        cmd = '%s ssh %s@%s -o StrictHostKeyChecking=no php %s/batch.php %s "%s" "%s" %s' % \
            (sshpass_cmd, s.login, s.ip, s.scripts_root(),
                m.id, 
                m.image.file.name.split('/')[-1].replace('&', '\&'), 
                m.link.replace('&', '\&'),
                ('gif' if m.image_gif else 'static'))
        print 'Фиксируем ссылки и изображение'
        self.command(cmd)

    def scripts_init(self, s):
        print 'INIT SERVER:%d' % (s.id)
        
        if s.fs_type == 'ssh':
            sshpass_cmd = "sshpass -p '%s'" % s.password
            ssh_cmd = 'ssh %s@%s -o StrictHostKeychecking=no' % (s.login, s.ip)

            cmd = 'ssh-keygen -f "/root/.ssh/known_hosts" -R %s' % s.ip
            print 'Перебиваем ключи'
            self.command(cmd)

            cmd = "%s %s rm -rf %s" % \
                (sshpass_cmd, ssh_cmd, s.document_root)
            print 'Удаляем папку'
            self.command(cmd)

            cmd = "%s %s mkdir %s" % \
                (sshpass_cmd, ssh_cmd, s.document_root)
            print 'Создаем папку'
            self.command(cmd)
            
            cmd = "%s scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -r %s/* %s@%s:%s" % \
                (sshpass_cmd, settings.SCRIPTS_ROOT, s.login, s.ip, s.document_root)
            print 'Копируем файлы'
            self.command(cmd)

            cmd = "%s %s chmod 777 -R %s" % \
                (sshpass_cmd, ssh_cmd, os.path.join(s.scripts_root(), 'data'))
            print 'Устанавливаем права на папку с данными'
            self.command(cmd)

            cmd = "%s %s chmod 777 -R %s" % \
                (sshpass_cmd, ssh_cmd, os.path.join(s.scripts_root(), 'logs'))
            print 'Устанавливаем права на папку с логами'
            self.command(cmd)
        
        elif s.fs_type == 'ftp':
            import ftputil
            host = ftputil.FTPHost(s.host, s.login, s.password)

            try:
                host.rmtree(s.document_root)
            except:
                pass
            host.mkdir(s.document_root)
            
            for dirname, dirnames, filenames in os.walk(settings.SCRIPTS_ROOT):
                for subdirname in dirnames:
                    d1 = os.path.join(dirname, subdirname)
                    d2 = d1.replace(settings.SCRIPTS_ROOT, '')
                    d3 = os.path.join(s.document_root, d2[1:])
                    host.mkdir(d3)
                    print d3 

                for filename in filenames:
                    f1 = os.path.join(dirname, filename)
                    f2 = f1.replace(settings.SCRIPTS_ROOT, '')
                    f3 = os.path.join(s.document_root, f2[1:])
                    with open(f1, 'rb') as source:
                        with host.open(f3, 'wb') as target:
                            host.copyfileobj(source, target)
                    print f3
    
                if '.git' in dirnames:
                    dirnames.remove('.git')