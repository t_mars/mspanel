#coding=utf8
from optparse import make_option
from datetime import timedelta
import subprocess
import random
import os

from django.core.management.base import BaseCommand
from django.db import transaction, connection
from django.db.models import Q
from django.core.cache import cache
from django.utils import timezone

from main.models import *

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--task',
            dest='task_id',
            help=u'Номер таска для запуска',
            metavar="INT"
        ),
    )

    def handle(self, *args, **options):
        self.task = Task.objects.get(pk=options['task_id'])

        # проверим есть ли задачи которые используют прокси
        now = timezone.now()
        proxy = Config.GET('send_mail.proxy', asdict=True)
        try:
            time = float(Config.GET('proxy.restart_time', 0.0))
        except:
            time = 0.0
        if time:
            time *= 60
        else:
            print 'not set proxy.restart_time'
            return

        print 'id =',os.popen('id').read()        
        print 'time =',time  
        ind = 0
        for_reboot = []
        for port in range(proxy['port1'], proxy['port2']):
            ind += 1
            key = '%s:%d' % (proxy['host'], port)
            val = cache.get(key)

            print '---'
            print key, val

            if val == False:
                print 'in reboot!'
                continue

            # ребутаем неизвестных сразу
            if val == None:
                for_reboot.append( (port, ind) )
                continue

            delta = (now - val).total_seconds()
            # ребутаем - ПОЗДНО
            if delta >= time:
                print 'now!'
                for_reboot.append( (port, ind) )
                continue
        
        # перезагружаем так, что половина всегда активна
        for port, ind in for_reboot:
            self.reboot(port, ind)

    def reboot(self, port, ind):
        print 'reboot'
        import urllib2
        url = "http://136.243.84.194/proxy/stat/?reboot=%s" % port
        print 'url =', url
        res = urllib2.urlopen(url).read()
            
