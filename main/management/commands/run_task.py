#coding=utf8
import os, logging, pickle, subprocess
from time import sleep
from multiprocessing import Process, Pipe
from StringIO import StringIO

from django.db import transaction
from django.core.management.base import BaseCommand
from django.utils import timezone

from main.models import Task, TaskLog
from optparse import make_option

class Command(BaseCommand):
    
    option_list = BaseCommand.option_list + (
        make_option('--task',
            dest='task_id',
            help=u'Номер таска для запуска',
            metavar="INT"
        ),
        make_option('--kill',
            dest='kill_id',
            help=u'Номер таска для завершения',
            metavar="INT"
        ),
    )

    def handle(self, *args, **options):
        if options['task_id']:
            try:
                task = Task.objects.get(pk=options['task_id'])
            except:
                return 'Задача не найдена'
            
            if not task.can_start():
                print "Задача %d [%s] невозможна для запуска" % (task.id, task.name.encode('utf8'))
                return

            self.run_task(task)
        
        elif options['kill_id']:
            try:
                task = Task.objects.get(pk=options['kill_id'])
            except:
                return 'Задача не найдена'
            
            self.kill_task(task)

        else:
            self.run()
    
    def kill_task(self, task):
        print "Задача %d [%s] УБИТА." % (task.id, task.name.encode('utf8'))
        
        kc = task.get_kill_command()
        print 'kill command =', kc

        os.popen(kc)
        # os.popen("sudo kill -9 %s" % task.pid)
        # os.popen("sudo kill %s" % task.pid)
        p = subprocess.Popen(
            ['sudo', 'kill', '-9', str(task.pid)],
            stdout=subprocess.PIPE, 
            stderr=subprocess.PIPE
        )
        o, e = p.communicate()
        print 'out:', o
        print 'err:', e 

        task.pid = None
        task.status = Task.KILLED
        task.stopped_at = timezone.now()
        task.save()
        print 'killed'

    def run_task(self, task):
        print "Запуск задачи %d [%s]" % (task.id, task.name.encode('utf8'))

        task_log = TaskLog()
        task_log.task = task
        task_log.arguments = task.arguments
        task_log.save()

        task.log = task_log
        task.status = Task.EXECUTE
        task.started_at = timezone.now()
        task.stopped_at = None
        task.status_string = ""
        task.pid = os.getpid()
        task.save()

        print "Запуск %d [%s]" % (task.id, task.name.encode('utf8'))
        
        try:
            sp = subprocess.Popen(task.get_command().split(' '), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            
            # task.pid = sp.pid
            # task.save()

            task = Task.objects.get(pk=task.pk)
            
            o, e = sp.communicate()
            task.out, task.err = o, e
            task.log.out, task.log.err = o, e
            task.log.save()

            print "Завершен"

            if sp.returncode == 0:
                task.status = Task.SUCCESS
            else:
                status = Task.objects.get(pk=task.id).status
                if status == Task.KILL:
                    print '!Task.KILLED'
                    task.status = Task.KILLED
                else:
                    print '!Task.FAILURE'
                    task.status = Task.FAILURE
        
            task.try_count += 1
            task.stopped_at = timezone.now()
            task.pid = None
            task.save()
            task.log.stopped_at = timezone.now()
            task.log.save()
            
        except KeyboardInterrupt:
            task.status = Task.KILLED
            task.pid = None
            task.stopped_at = timezone.now()
            task.save()
            task.log.stopped_at = timezone.now()
            task.log.save()

    def run(self):
        # убиваем задачи
        tasks = Task.objects.raw(
            'SELECT * FROM main_task at '
            'WHERE at.status=%d '
            'ORDER BY at.id FOR UPDATE' % (Task.KILL)
        )
        for task in tasks:
            self.kill_task(task)
        
        # убиваем задачи по расписанию
        now = timezone.now()
        tasks = Task.objects.raw(
            'SELECT * FROM main_task at '
            'WHERE at.status=%d '
                'AND start_at < "%s" '
                'AND stop_at < "%s" '
            'ORDER BY at.id FOR UPDATE' % (Task.EXECUTE, now, now)
        )
        for task in tasks:
            self.kill_task(task)
            
        # проверка 'отвалившихся' задач
        tasks = Task.objects.raw(
            'SELECT * FROM main_task at '
            'WHERE at.status=%d '
            'ORDER BY at.id FOR UPDATE' % (Task.EXECUTE)
        )
        for task in tasks:
            if task.is_active_proc():
                continue
            print "Отвалилась задача %d [%s]" % (task.id, task.name.encode('utf8'))
            task.status = Task.FAILURE
            task.pid = None
            task.save()
            if task.can_start():
                task.restart()
                print "Перезапуск задачи %d [%s]" % (task.id, task.name.encode('utf8'))

        # проверка аварийно завершенных задач
        tasks = Task.objects.raw(
            'SELECT * FROM main_task at '
            'WHERE at.status=%d '
            'ORDER BY at.id FOR UPDATE' % (Task.FAILURE)
        )
        for task in tasks:
            if not task.can_start():
                continue
            task.restart()
            print "Перезапуск задачи %d [%s]" % (task.id, task.name.encode('utf8'))
            
        # запуск новых задач
        tasks = Task.objects \
            .filter(status__in=[Task.NEW,Task.FAILURE,Task.SUCCESS]) \
            .order_by('?')
        for task in tasks:
            if not task.can_start():
                continue
            self.run_task(task)
            return

        print 'Нет задач'