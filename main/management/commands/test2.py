#coding=utf8
import os
import sys

from django.core.management.base import BaseCommand
from django.db import transaction, connection

from main.models import *

class Command(BaseCommand):
    def handle(self, *args, **options):
        from sender.models import *
        # c = connection.cursor()
        # c.execute("""
        #     SELECT 
        #         DISTINCT SUBSTRING_INDEX(LOWER(email), '@', -1) service
        #     FROM sender_email
        # """)
        
        # rows = c.fetchall()
        # for row in rows:
        #     name = row[0]

        f = open('services2.txt')
        for row in f:
            name = row.strip()
            if name == '-' or name == '':
                continue
            try:
                print name
            except:
                try:
                    print name.encode('utf8')
                except:
                    print '-'
            
            s,_ = EmailService.objects.get_or_create(name=name)

            ms = Email.objects \
                .filter(service__isnull=True) \
                .extra(where=["SUBSTRING_INDEX(LOWER(email), '@', -1) = '%s'" % name])
            print 'count =',len(ms),'updating...'
            i = 0
            for m in ms:
                i += 1
                sys.stdout.write('\r%d : %d' % (i, m.id))
                
                m.service_id = s.id
                try:
                    m.save()
                except:
                    sys.stdout.write(' error\n')
                    sys.stdout.flush()
                    continue
                    
                sys.stdout.write(' ok\n')
                sys.stdout.flush()
                
            print 'OK!'

        # for s in EmailService.objects.all():
        #     if not s.name:
        #         continue