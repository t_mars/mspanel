#coding=utf8
from optparse import make_option

from django.core.management.base import BaseCommand
from django.db import transaction, connection
from django.core.cache import cache
from django.utils import timezone

from main.models import *

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--task',
            dest='task_id',
            help=u'Номер таска для запуска',
            metavar="INT"
        ),
    )

    def handle(self, *args, **options):
        domains = Domain.objects.all()

        # сохраняем по каждому состоянию количество
        Cache.SET('domain_state_all', domains.count())
        for val,name in DOMAIN_STATE_CHOICE.items():
            count = domains.filter(state=val).count()
            Cache.SET('domain_state_%s' % name.lower(), count)