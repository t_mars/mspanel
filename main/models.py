#coding=utf8
from django.db import models, connection
from django.conf import settings
from django.utils import timezone
from django.core.cache import cache

from django.core.validators import URLValidator
from django.core.exceptions import ValidationError

import random
import string
import demjson
import subprocess
import sys
import re
import os
import json
import datetime
import time
from datetime import timedelta

from lib.randhtml import RandomHtml

MANAGER_TYPE_CHOICE = {
    'isp': 'ISP',
    'isp5': 'ISP5',
    'dreamhost': 'DreamHost',
}
OS_TYPE_CHOICE = {
    'debian': 'Debian',
}
FS_TYPE_CHOICE = {
    'ftp': 'FTP',
    'ssh': 'SSH',
}
class VirtualServer(models.Model):
    name = models.CharField(max_length=30)
    host = models.CharField(max_length=30)

    login = models.CharField(max_length=30)
    password = models.CharField(max_length=30)
    
    manager_login = models.CharField(max_length=30)
    manager_password = models.CharField(max_length=30)

    manager_type = models.CharField(max_length=10, choices=MANAGER_TYPE_CHOICE.items())
    fs_type = models.CharField(max_length=10, choices=FS_TYPE_CHOICE.items())
    os_type = models.CharField(max_length=10, choices=OS_TYPE_CHOICE.items())

    name_server = models.TextField()
    expiry_date = models.DateField()

    document_root = models.CharField(max_length=200)
    
    MT_ISP = 'isp'
    MT_ISP5 = 'isp5'
    MT_DREAMHOST = 'dreamhost'

    def __str__(self):
        return self.name

    def get_name_servers(self):
        ss = []
        for n in self.name_server.split(','):
            ss.extend(list(NameServer.objects.filter(name__icontains=n)))
        return ss 

    @property
    def ip(self):
        return self.host
    
    def scripts_root(self):
        return os.path.join(self.document_root, "link_encoder")

    def init_api(self):
        if self.manager_type == VirtualServer.MT_ISP:
            from lib.ispmgr import ISP_API
            return ISP_API("https://%s:1500/ispmgr" % self.ip, self.manager_login, self.manager_password)
        
        elif self.manager_type == VirtualServer.MT_ISP5:
            from lib.ispmgr5 import ISP_API
            return ISP_API("https://%s:1500/ispmgr" % self.ip, self.manager_login, self.manager_password)

        elif self.manager_type == VirtualServer.MT_DREAMHOST:
            from lib.dreamhost import DreamHostClient
            return DreamHostClient(self.manager_login, self.manager_password, debug=False)

class NameServer(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name

DOMAIN_STATE_CHOICE = {
    0: 'NEW',
    
    1: 'RETRY_CHECK',
    2: 'NO_CHECKED',
    3: 'CHECKED',
    
    4: 'ADDED',
    5: 'NO_ADDED',
    6: 'DELETED',
    
    7: 'VALIDATED',
    8: 'NO_VALIDATED',
    
    9: 'ACTIVATED',
    10: 'NO_ACTIVATED',
    
    11: 'BANNED',
}
class Domain(models.Model):
    name = models.CharField(max_length=100, unique=True)
    dns_ip = models.GenericIPAddressField(null=True, blank=True, default=None)
    expiry_date = models.DateField(null=True, blank=True)
    name_servers = models.ManyToManyField(NameServer)
    main_name_server = models.ForeignKey(NameServer, null=True, blank=True,related_name='main_name_server')
    
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(null=False, blank=False)
    
    state = models.IntegerField(default=0, choices=DOMAIN_STATE_CHOICE.items(), null=False, blank=False)
    # для комментов
    comment = models.CharField(max_length=50, default='')

    NEW = 0
    RETRY_CHECK = 1
    NO_CHECKED = 2
    CHECKED = 3
    ADDED = 4
    NO_ADDED = 5
    DELETED = 6
    VALIDATED = 7
    NO_VALIDATED = 8
    ACTIVATED = 9
    NO_ACTIVATED = 10
    BANNED = 11

    # добавление к серверу
    alias_to = models.ForeignKey('self', null=True, blank=True)
    virtual_server = models.ForeignKey(VirtualServer, null=True, blank=True)
    added_at = models.DateTimeField(null=True, blank=True)

    mail_count = models.PositiveIntegerField(default=0)
    spam_count = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.name

    def reset_spam_count(self):
        self.spam_count = Parcel.objects \
            .filter(domain=self) \
            .values('account_id') \
            .distinct().count()
        if self.state == Domain.ACTIVATED and self.spam_count >= 15:
            self.set_state(Domain.BANNED)
        self.save()

    def set_state(self, state, created_at=None):
        self.state = state
        self.updated_at = timezone.now()
        
        log = DomainStateLog()
        log.domain = self
        log.state = state
        if created_at:
            log.created_at = created_at
        log.save()

    def action_check(self, flag, ns_servers, expiry_date, comment):
        state = None
        self.comment = comment
        if flag == True:
            if self.state in [Domain.NEW, Domain.RETRY_CHECK]:
                state = Domain.CHECKED
                        
        elif flag == False:
            if self.state in [Domain.NEW, Domain.RETRY_CHECK, Domain.NO_ADDED]:
                state = Domain.NO_CHECKED

        elif flag == None:
            if self.state in [Domain.NEW, Domain.RETRY_CHECK]:
                state = Domain.RETRY_CHECK

        if not state:
            self.updated_at = timezone.now()
            self.save()
            return False

        self.expiry_date = expiry_date
        self.set_state(state)
        
        if ns_servers:
            ns_servers = [s for s in ns_servers if len(s)]
            self.name_servers.clear()
            self.main_name_server = None
            
            for s in sorted(ns_servers):
                try:
                    ns_server,_ = NameServer.objects.get_or_create(name=s)
                except:
                    continue
                if self.main_name_server is None:
                    self.main_name_server = ns_server
                self.name_servers.add(ns_server)

        self.save()
        return True

    def action_add(self, flag, server, ip=None, alias_to=None):
        state = None
            
        if self.state in [Domain.CHECKED,Domain.NO_ADDED]:
    
            if flag == True:
                if ip == None:
                    raise Exception('Domain.action_add: IP is required')
                self.dns_ip = ip
                self.virtual_server = server
                self.added_at = timezone.now()
            
                state = Domain.ADDED
                self.alias_to = alias_to

            elif flag == False:
                state = Domain.NO_ADDED
                
        if not state:
            self.updated_at = timezone.now()
            self.save()
            return False

        self.set_state(state)
        self.save()
        return True

    def action_delete(self, flag):
        state = None
            
        if self.state in [Domain.NO_ACTIVATED,Domain.BANNED,Domain.NO_VALIDATED]:
            self.added_at = timezone.now()
    
            if flag == True:
                self.virtual_server = None
                self.alias_to = None
                self.added_at = None
                state = Domain.DELETED

        if not state:
            self.updated_at = timezone.now()
            self.save()
            return False

        self.set_state(state)
        self.save()
        return True

    def action_validate(self, flag):
        state = None
            
        if self.state == Domain.ADDED:
            if flag == True:
                state = Domain.VALIDATED

            elif flag == False:
                d = timezone.now() - timedelta(days=1)
                if not self.added_at or self.added_at < d: # если прошли сутки отмечаем что не проверился
                    state = Domain.NO_VALIDATED
        
        elif self.state == Domain.ACTIVATED:
            if flag == False:
                state = Domain.NO_VALIDATED
                
        if not state:
            self.updated_at = timezone.now()
            self.save()
            return False
        
        self.set_state(state)
        self.save()
        return True

    def action_activate(self, flag):
        state = None
            
        if self.state in [Domain.VALIDATED, Domain.ACTIVATED]:
            if flag == True:
                state = Domain.ACTIVATED

            elif flag == False:
                state = Domain.NO_ACTIVATED
                
        if not state:
            self.updated_at = timezone.now()
            self.save()
            return False
        
        self.set_state(state)
        self.save()
        return True

    def get_link(self):
        return 'http://' + self.name

    class Meta:
        index_together = [
            ['name'],
            ['state'],
            ['updated_at'],
            ['state', 'updated_at'],
        ]

class DomainStateLog(models.Model):
    domain = models.ForeignKey(Domain, null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    state = models.IntegerField(default=0, choices=DOMAIN_STATE_CHOICE.items(), null=False, blank=False)

    def __str__(self):
        return '%s %s %s' % (self.domain, DOMAIN_STATE_CHOICE[self.state], self.created_at)

TASK_STATUS_CHOICE = {
    0: 'NEW',
    1: 'EXECUTE',
    2: 'SUCCESS',
    3: 'FAILURE',
    4: 'KILL',
    5: 'KILLED',
}

TASK_TYPE_CHOICE = {
    0: 'WORKER',
    1: 'COMMAND',
}
class Task(models.Model):

    name = models.TextField(null=True, blank=True)
    command = models.TextField()
    kill_command = models.TextField(null=True, blank=True)
    arguments = models.TextField(null=True, blank=True)

    log = models.ForeignKey('TaskLog', null=True, blank=True, default=None, related_name='log', on_delete=models.SET_NULL)
    out = models.TextField(null=True, blank=True)
    err = models.TextField(null=True, blank=True)
    status_string = models.TextField(null=True, blank=True)
    uid = models.CharField(max_length=30)
    status = models.IntegerField(default=0, choices=TASK_STATUS_CHOICE.items(), null=False, blank=False)
    typ = models.IntegerField(default=0, choices=TASK_TYPE_CHOICE.items(), null=False, blank=False)

    NEW = 0
    EXECUTE = 1
    SUCCESS = 2
    FAILURE = 3
    KILL = 4
    KILLED = 5

    WORKER_ROOT = '/var/www/mspanel/private/workers'

    TYPE_WORKER = 0
    TYPE_COMMAND = 1

    pid = models.PositiveSmallIntegerField(blank=True,null=True)
    try_count = models.PositiveSmallIntegerField(default=0,blank=False,null=False)
    max_try_count = models.PositiveSmallIntegerField(default=None,blank=True,null=True)
    can_retry = models.BooleanField(default=False)
    time_shift = models.PositiveSmallIntegerField(default=None,blank=True,null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    start_at = models.DateTimeField(default=None, null=True, blank=True)
    stop_at = models.DateTimeField(default=None, null=True, blank=True)

    started_at = models.DateTimeField(default=None, null=True, blank=True)
    stopped_at = models.DateTimeField(default=None, null=True, blank=True)
    
    @staticmethod
    def GET(uid):
        try:
            return Task.objects.filter(uid=uid).order_by('-id')[0]
        except:
            return None

    def can_start(self):
        # если время не указано или время наступило
        if self.start_at == None or timezone.now() > self.start_at:
            # если задача новая
            if self.status == Task.NEW:
                return True
            # если задача неудачно завершилась и может быть перезапущена
            if self.status in [Task.FAILURE,Task.SUCCESS] and self.can_retry:
                from datetime import timedelta
                # if True:
                if self.time_shift == None \
                    or self.updated_at < timezone.now() - timedelta(minutes=self.time_shift):
                    # если указано количество попыток, ограничиваемся
                    if self.max_try_count == None:
                        return True
                    if self.try_count <= self.max_try_count:
                        return True

        return False

    def save_status_string(self, d):
        import json
        self.status_string = json.dumps(d)
        self.save()

        if self.log:
            self.log.result = self.status_string
            self.log.save()

    def get_command(self):
        if self.typ == 0: #WORKER
            return "sudo python %s/%s %d" % (self.WORKER_ROOT, self.command, self.id)
        if self.typ == 1: #COMMAND
            return "sudo python %s/manage.py %s --task %d" % (settings.BASE_DIR, self.command, self.id)

    def get_kill_command(self):
        if self.kill_command:
            if self.typ == 0: #WORKER
                return "sudo python %s/%s %d" % (self.WORKER_ROOT, self.kill_command, self.id)
            if self.typ == 1: #COMMAND
                return "sudo python %s/manage.py %s --task %d" % (settings.BASE_DIR, self.kill_command, self.id)
        return ""

    def get_argument(self, key, default=None):
        try:
            arguments = json.loads(self.arguments)
        except:
            return default
        return arguments.get(key, default)

    def is_active_proc(self):
        if not self.pid:
            return False
        if len(os.popen("ps -ax | grep '^\s*%s'" % self.pid).read().strip()):
            return True
        else:
            return False

    def restart(self):
        self.pid = None
        self.out = None
        self.err = None
        self.status = Task.NEW
        self.status_string = None
        self.save()

    def add_domains(self, serv, count, group):
        self.name = "add domains %s %d" % (serv.name, count)
        self.command = "domain/add.py"
        self.arguments = {
            'server_id': serv.id,
            'count': count,
            'group_size': group
        }
        self.uid = "add_domains:%d" % serv.id
        self.save()

    def repeat_add_domains(self):
        self.name = "repeat add domains"
        self.command = "domain/repeat_add.py"
        self.uid = "repeat_add_domains"
        self.can_retry = True
        self.time_shift = 5
        self.save()

    def check_mailing(self):
        self.name = "check mailing"
        self.command = "mailing/check.py"
        self.uid = "check_mailing"
        self.can_retry = True
        self.time_shift = 5
        self.save()

    def control_check(self):
        self.name = "control check"
        self.command = "mailing/control_check.py"
        self.uid = "control_check_mailing"
        self.can_retry = True
        self.time_shift = 5
        self.save()

    def delete_domains(self):
        self.name = "delete domains"
        self.command = "domain/delete.py"
        self.uid = "delete_domains"
        self.can_retry = True
        self.time_shift = 5
        self.save()

    def send_mailing(self, mailing, date, recipients, limit,
        group_size, control_shift, thread_count, account_log_ids, site,
        selenium):
        self.name = "send mailing %s" % (mailing.name)
        self.command = "mailing/send.py"
        self.kill_command = "mailing/send.py kill"
        self.start_at = date
        self.arguments = {
            'mailing_id': mailing.id, 
            'recipients': recipients, 
            'limit': limit,
            'group_size': group_size,
            'control_shift': control_shift,
            'thread_count': thread_count,
            'account_log_ids': account_log_ids,
            'site': site,
            'selenium': selenium,
        }
        self.uid = "send_mailing:%d" % mailing.id
        self.save()

    def check_domains(self):
        self.name = "check domains"
        self.command = "domain/check.py"
        self.kill_command = "domain/check.py kill"
        self.uid = "check_domains"
        self.can_retry = True
        self.arguments = {'thread_count': 100}
        self.time_shift = 5
        self.save()

    def validate_domains(self):
        self.name = "validate domains"
        self.command = "domain/validate.py"
        self.kill_command = "domain/validate.py kill"
        self.uid = "validate_domains"
        self.can_retry = True
        self.time_shift = 5
        self.save()

    def google_check_domains(self):
        self.name = "google check domains"
        self.command = "domain/google_check.py"
        self.kill_command = "domain/google_check.py kill"
        self.uid = "google_check"
        self.can_retry = True
        self.time_shift = 5
        self.save()

    def load_domains(self):
        self.name = "load domains"
        self.command = "domain/load.py"
        self.uid = "load_domains"
        self.arguments = {'dir': '/home/u1/zone/'}
        # self.can_retry = True
        # self.time_shift = 10
        self.save()

    def init_virtual_servers(self, server_ids=[], mailing_ids=[]):
        self.name = "init virtual servers"
        self.command = "init_virtual_servers"
        self.uid = "init_virtual_servers"
        self.typ = Task.TYPE_COMMAND
        self.arguments = {
            'server_ids': server_ids,
            'mailing_ids': mailing_ids,
        }
        self.save()

    def load_accounts(self, filename):
        self.name = "load accounts %s" % filename
        self.command = "load_accounts"
        self.uid = "load_accounts"
        self.typ = Task.TYPE_COMMAND
        self.arguments = {
            'filename': filename,
        }
        self.save()

    def save(self, *args, **kwargs):
        if isinstance(self.arguments, dict):
            self.arguments = json.dumps(self.arguments)

        return super(Task, self).save(*args, **kwargs)

    def run_now(self):
        if not self.can_start():
            return
        subprocess.Popen(
            [
                'sudo', sys.executable, '%s/private/manage.py' % settings.ROOT_DIR, 
                'run_task', '--task', str(self.id)
            ], 
            stdout=subprocess.PIPE, 
            stderr=subprocess.STDOUT
        )

    def kill(self):
        self.status = Task.KILL
        self.pid = None
        self.save()

        subprocess.Popen(
            [
                'sudo', sys.executable, '%s/private/manage.py' % settings.ROOT_DIR, 
                'run_task', '--kill', str(self.id)
            ] 
        )

    def get_exception(self):
        if not self.err:
            return None

        lines = [l.strip() for l in self.err.split('\n')]
        inds = [i for i,l in enumerate(lines) if l.startswith('File')]
        if not inds:
            return None

        ind = max(inds)
        lines = [l.replace(settings.BASE_DIR, '...') for l in lines[ind:] if not l.startswith('raise')]
        
        return '<br>'.join(lines)

    def __str__(self):
        return '%d %s (%s)' % (self.id, self.name, TASK_STATUS_CHOICE[self.status])

    @property
    def info(self):
        try:
            return json.loads(self.status_string)
        except:
            return {}

    def get_work_range(self):
        if not self.started_at:
            return None

        if not self.stopped_at:
            return self.started_at, timezone.now()

        return self.started_at, self.stopped_at

MAILING_BODY_TYPE = {
    0: 'Вручную',
    1: 'Открытка',
    2: 'Случайное содержание',
}
random_html = RandomHtml()

class Mailing(models.Model):
    name = models.CharField(max_length=100)
    to = models.TextField()
    subject = models.TextField()
    link = models.CharField(max_length=200)
    body = models.TextField()
    image = models.ImageField()
    image_gif = models.BooleanField(default=True)

    target = models.ForeignKey('sender.Target', default=None, null=True, blank=True)
    body_type = models.PositiveSmallIntegerField(default=0, choices=MAILING_BODY_TYPE.items())

    fake_domain = models.BooleanField(default=False)
    fake_domain_pattern = models.CharField(max_length=255, default='')

    use_link_list = models.BooleanField(default=False)
    use_pic_list = models.BooleanField(default=False)

    TYPE_BODY = 0
    TYPE_CARD = 1
    TYPE_RANDOM = 2

    def __str__(self):
        return self.name

    def gen_key(self, mode):
        return os.popen(
            'php %s %s%d' % (settings.GEN_KEY_PATH, mode, self.id)
        ).read().strip()

    def get_subject(self):
        return random.choice([s.strip() for s in self.subject.split('|')])

    def render_macros(self, res, tokens={}):
        d = datetime.datetime.now()
        timestamp = int (time.mktime(d.timetuple())) * 1000 + random.randint(0,1000)
        
        # macros
        result = re.finditer(ur"\{\%([^%]+)\%\}", res)
        for m in result:
            token = m.groups()[0].lower()
            r = None
            # try:
            if token in tokens:
                r = tokens.get(token)
                
            elif token == 'timestamp':
                r = str(timestamp)
                timestamp += 1

            elif token.startswith('rs'):
                ts = token.split(',')[1:]
                length = ts[0]
                typ = ts[1]
                
                # определяем длину строки
                if '-' in length:
                    len_min, len_max = ts[0].split('-')
                    length = random.randint(int(len_min),int(len_max))
                else:
                    length = int(length)

                if typ == 'u':
                    r = ''.join(random.choice(string.ascii_uppercase) \
                        for _ in range(length))
        
                elif typ == 'l':
                    r = ''.join(random.choice(string.ascii_lowercase) \
                        for _ in range(length))
        
                elif typ == 'r':
                    r = ''.join(random.choice(string.ascii_lowercase + string.ascii_uppercase) \
                        for _ in range(length))
        
                elif typ == 'lu':
                    if random.randint(0,1):
                        r = ''.join(random.choice(string.ascii_uppercase) \
                            for _ in range(length))
                    else:
                        r = ''.join(random.choice(string.ascii_lowercase) \
                            for _ in range(length))
        
                elif typ == 'n':
                    r = ''.join(random.choice(string.digits) \
                        for _ in range(length))
            # except:
                # pass

            if r:
                res = res.replace('{%' + token + '%}', r, 1)

        #vars
        var_dict = {}
        for v in re.findall(ur"\$[a-zA-Z]+\=\[[^\]]+\]", res):
            key,val = v[1:-1].split('=[')
            res = res.replace(v, val)
            res = res.replace('{%var,'+key+'%}', val)

        res = res.replace('\n', '')
        res = res.replace('\r', '')

        return res

    def get_body(self, domain_obj):
        if self.fake_domain:
            domain = self.render_macros(self.fake_domain_pattern, None)
        else:        
            domain = domain_obj.get_link()

        return self.render_macros(self.body, domain)

    def get_pic_list(self):
        # определяем значение токенов
        if not hasattr(self, '_pic_list'):
            qs = MailingUrl.objects \
                .filter(mode=MailingUrl.MODE_PIC, mailing=self, is_banned=False)
            self._pic_list = list(qs)
        return self._pic_list

    def get_link_list(self):
        if not hasattr(self, '_link_list'):
            qs = MailingUrl.objects \
                .filter(mode=MailingUrl.MODE_LINK, mailing=self, is_banned=False)
            self._link_list = list(qs)
        return self._link_list

    def get_message(self, domain):
        # определяем содержание сообщения
        if self.body_type == self.TYPE_BODY:
            body = self.body
        elif self.body_type == self.TYPE_CARD:
            body = """<table align="center" cellpadding="10" cellspacing="0" id="{%timestamp%}{%rs,10,l%}"><tbody><tr><td align="center"><font size="5"></font><br><font size="3" id="{%timestamp%}{%rs,10,l%}">&nbsp;</font></td></tr><tr><td align="center"><a href="{%link%}"><img id="{%timestamp%}{%rs,10,l%}" src="{%pic%}" title="" ></a></td></tr></tbody></table>"""
        elif self.body_type == self.TYPE_RANDOM:
            body = random_html.get()

        url_ids = []
        tokens = {}
        if self.use_pic_list:
            try:
                murl = random.choice(self.get_pic_list())
            except:
                raise Exception('No have active url for PIC.')
            url_ids.append(murl.id)
            tokens['pic'] = murl.url
        else:
            tokens['pic'] = os.path.join(domain, self.gen_key('i'))       

        if self.use_link_list:
            try:
                murl = random.choice(self.get_link_list())
            except:
                raise Exception('No have active url for LINK.')
            url_ids.append(murl.id)
            tokens['link'] = murl.url
        else:
            tokens['link'] = os.path.join(domain, self.gen_key('l'))

        body = self.render_macros(body, tokens)
        subject = self.render_macros(self.get_subject(), tokens)

        return body, subject, url_ids

    def get_target_geo(self):
        from sender.models import Geo
        
        geos = []

        if self.target_geo:
            for s in self.target_geo.split(','):
                geos.append(Geo.get(s))

        return geos

    def get_targeted_emails(self):
        from sender.models import Geo,Email
        import operator
        from django.db.models import Q

        fs = []
        
        #geo
        ps = []
        for g in self.get_target_geo():
            ps.append(Q(**{g.__class__.__name__.lower(): g}))

        if self.target_without_geo:
            for m in Geo.models():
                n = m.__name__.lower()
                ps.append(Q(**{'%s__isnull' % n: True}))
                ps.append(Q(**{n: 0}))

        if ps:
            fs.append(reduce(operator.or_, ps))


        #age
        if self.target_age_from:
            d = datetime.datetime.today() - datetime.timedelta(days=self.target_age_from*365.25)
            fs.append(Q(birthday__lte=d))
        if self.target_age_to:
            d = datetime.datetime.today() - datetime.timedelta(days=self.target_age_to*365.25)
            fs.append(Q(birthday__gte=d))

        #sex
        if self.target_sex:
            ps = []
            for g in self.target_sex.split(','):
                ps.append(Q(sex=int(g)))
            fs.append(reduce(operator.or_, ps))
        
        if fs:
            qs = Email.objects.filter(reduce(operator.and_, fs))
        else:
            qs = Email.objects.all()

        return qs

    @property
    def mailing_task(self):
        if not hasattr(self, '_mailing_task'):
            try:
                self._mailing_task = MailingTask.objects.filter(mailing=self).order_by('-started_at')[0]
            except:
                self._mailing_task = None

        return self._mailing_task          

    @property
    def task(self):
        if not hasattr(self, '_task'):
            if self.mailing_task:
                self._task = self.mailing_task.task
            else:
                self._task = None

        return self._task

MAILING_URL_MODE_CHOICE = {
    0: 'PIC',
    1: 'LINK',
}
class MailingUrl(models.Model):
    url = models.CharField(max_length=1024)
    created_at = models.DateTimeField(auto_now_add=True)
    mailing = models.ForeignKey(Mailing)
    mode = models.IntegerField(default=0, choices=MAILING_URL_MODE_CHOICE.items())
    
    mail_count = models.PositiveIntegerField(default=0) # использовалось в письмах
    spam_count1 = models.PositiveIntegerField(default=0) # отбилось mailer-daemon
    spam_count2 = models.PositiveIntegerField(default=0) # пришло в спам (контрольная рассылка)
    
    is_banned = models.BooleanField(default=False)
    banned_at = models.DateTimeField(null=True, blank=True, default=None)

    MODE_PIC = 0
    MODE_LINK = 1

    @staticmethod
    def create(mailing, url, mode):
        url = url.strip()

        val = URLValidator()
        try:
            val(url)
        except ValidationError as e:
            return False

        qs = MailingUrl.objects \
            .filter(mailing=mailing, url=url, mode=mode)
        if qs.count() > 0:
            return False

        obj = MailingUrl()
        obj.mailing = mailing
        obj.url = url
        obj.mode = mode
        obj.save()
        return True
    
    def __str__(self):
        return '%s [%s]' % (self.url, MAILING_URL_MODE_CHOICE[self.mode])

class MailingUrlParcel(models.Model):
    mailing_url = models.ForeignKey(MailingUrl)
    parcel = models.ForeignKey('Parcel')

ACCOUNT_UNACTIVATE_REASON_CHOICE = {
    0: 'SEND',
    1: 'SPAM_CHECK',
    2: 'CONTROL_CHECK',
}
ACCOUNT_SITE_CHOICE = {
    0: 'Mail.ru',
    1: 'Rambler.ru',
}
class MailRuAccount(models.Model):
    username = models.CharField(max_length=100, unique=True)
    password = models.CharField(max_length=100)

    birthdate = models.DateField(default=None, null=True, blank=True)
    firstname = models.CharField(max_length=255, default=None, null=True, blank=True)
    lastname = models.CharField(max_length=255, default=None, null=True, blank=True)
    sex = models.PositiveSmallIntegerField(default=None, null=True, blank=True) # 1-male, 2-female
    has_my = models.NullBooleanField(default=None)
    site = models.IntegerField(default=0, choices=ACCOUNT_SITE_CHOICE.items(), null=True, blank=True)

    MAIL_RU = 0
    RAMBLER_RU = 1

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    mail_count = models.PositiveIntegerField(default=0)

    # последняя отправка письма
    sended_at = models.DateTimeField(default=None, null=True, blank=True)
    # последняя проверка почты
    checked_at = models.DateTimeField(default=None, null=True, blank=True)

    is_active = models.BooleanField(default=True)
    unactivate_count = models.PositiveIntegerField(default=0)
    unactivate_reason = models.IntegerField(default=None, choices=ACCOUNT_UNACTIVATE_REASON_CHOICE.items(), null=True, blank=True)
    unactivated_at = models.DateTimeField(default=None, null=True, blank=True)
    unactivated_mailing_task = models.ForeignKey('MailingTask', null=True, blank=True, on_delete=models.SET_NULL)

    task_log = models.ForeignKey('TaskLog', null=True, blank=True, default=None, on_delete=models.SET_NULL)
    
    UNACTIVATE_SEND = 0
    UNACTIVATE_SPAM_CHECK = 1
    UNACTIVATE_CONTROL_CHECK = 2

    auth_data = models.TextField(null=True, blank=True, default=None)

    def __str__(self):
        return self.username

    def get_auth_data(self):
        try:
            return json.loads(self.auth_data)
        except:
            return {}
    
    def save(self, *args, **kwargs):
        if isinstance(self.auth_data, dict) and self.auth_data:
            self.auth_data = json.dumps(self.auth_data)
        else:
            self.auth_data = None

        return super(MailRuAccount, self).save(*args, **kwargs)

    def unactivate(self, reason, mailing_task):
        self.unactivate_count += 1
        if self.unactivate_count >= 3:
            self.is_active = False
            self.unactivate_reason = reason
            self.unactivated_at = timezone.now()
            self.unactivated_mailing_task = mailing_task

    class Meta:
        index_together = [
            ['username'],
        ]


class MailingTask(models.Model):
    mailing = models.ForeignKey(Mailing)
    task = models.ForeignKey(Task, on_delete=models.SET_NULL, null=True, blank=True)

    recipients = models.CharField(max_length=50, default='test')
    group_size = models.PositiveSmallIntegerField(default=3)
    control_shift = models.PositiveSmallIntegerField(default=0)
    used_account_num = models.PositiveIntegerField(default=0, null=True, blank=True, verbose_name=u'Использовано аккаунтов')
    unactivated_account_num = models.PositiveIntegerField(default=0, null=True, blank=True, verbose_name=u'Забаненно аккаунтов')
    download_size = models.PositiveIntegerField(default=0)
    thread_count = models.PositiveIntegerField(default=50)
    fake_domain = models.BooleanField(default=False)

    started_at = models.DateTimeField(auto_now_add=True)
    finished_at = models.DateTimeField(null=True, blank=True)
    body_type = models.PositiveSmallIntegerField(default=0, choices=MAILING_BODY_TYPE.items())
    site = models.IntegerField(default=0, choices=ACCOUNT_SITE_CHOICE.items(), null=True, blank=True)

    def __str__(self):
        return '%d, %s, Task %d' % (self.id, self.mailing, self.task.id)
    
    @property
    def parcels(self):
        if not hasattr(self, '_parcels'):
            self._parcels = Parcel.objects.filter(mailing_task=self, is_control=False)

        return self._parcels

    def get_cache_key(self, name):
        return 'mt_%d_%s' % (self.id, name)

    def get_cached(self, key, names):
        r = {'updated_at': cache.get(key)}
        
        for n in names:
            v = cache.get('%s_%s' % (key, n), None)
            if v is None:
                return None
            r[n] = v

        return r

    def set_cached(self, key, r):
        data = {key: timezone.now()}
        for k,v in r.iteritems():
            data['%s_%s' % (key, k)] = v
        cache.set_many(data, timeout=None)
    
    def stat(self, name, refresh=False):
        attr = getattr(self, '_stat_%s' % name)
        if not attr:
            return None

        key = self.get_cache_key(name)
        
        r = None
        if not refresh:
            r = self.get_cached(key, attr.names)
        
        if r is None:
            r = attr()
            self.set_cached(key, r)
            r['updated_at'] = cache.get(key)

        return r

    def reset_stat(self, name):
        attr = getattr(self, '_stat_%s' % name)
        if not attr:
            return None
        
        key = self.get_cache_key(name)
        keys = [key] + ['%s_%s' % (key, k) for k in attr.names]
        cache.delete_many(keys)
        
        self.stat(name)

    def _stat_control_delivery(self, refresh=False):
        cps = ControlParcel.objects \
            .filter(parcel__mailing_task=self) \
            .filter(parcel__status=Parcel.SENDED)

        r = {}
        r['total'] = cps.count()

        r['checked'] = cps.filter(status__isnull=False).count()
        r['checked_p'] = r['checked'] / float(r['total']) * 100.0 if r['total'] else 100.0

        r['inbox'] = cps.filter(status=ControlParcel.IN_INBOX).count()
        r['inbox_p'] = r['inbox'] / float(r['total']) * 100.0 if r['total'] else 0.0
        
        r['spam'] = cps.filter(status=ControlParcel.IN_SPAM).count()
        r['spam_p'] = r['spam'] / float(r['total']) * 100.0 if r['total'] else 0.0
        
        r['not_come'] = cps.filter(status=ControlParcel.NOT_COME).count()
        r['not_come_p'] = r['not_come'] / float(r['total']) * 100.0 if r['total'] else 0.0

        r['unknow'] = cps.filter(status=ControlParcel.AUTH_ERROR).count()
        r['unknow_p'] = r['unknow'] / float(r['total']) * 100.0 if r['total'] else 0.0

        return r
    _stat_control_delivery.names = ['total', 
        'checked', 'checked_p',
        'inbox', 'inbox_p',
        'spam', 'spam_p', 
        'not_come', 'not_come_p', 
        'unknow', 'unknow_p'
    ]
    
    def _stat_accounts(self, refresh=False):
        accounts = MailRuAccount.objects.filter(unactivated_mailing_task=self)

        r = {}
        r['total'] = MailRuAccount.objects.filter(parcel__mailing_task=self).distinct().count()
        r['unactivated'] = accounts.count()
        
        r['send'] = accounts.filter(unactivate_reason=MailRuAccount.UNACTIVATE_SEND).count()
        r['send_p'] = r['send'] / float(r['unactivated']) * 100.0 if r['unactivated'] else 0.0
        
        r['spam_check'] = accounts.filter(unactivate_reason=MailRuAccount.UNACTIVATE_SPAM_CHECK).count()
        r['spam_check_p'] = r['spam_check'] / float(r['unactivated']) * 100.0 if r['unactivated'] else 0.0
        
        r['control_check'] = accounts.filter(unactivate_reason=MailRuAccount.UNACTIVATE_CONTROL_CHECK).count()
        r['control_check_p'] = r['control_check'] / float(r['unactivated']) * 100.0 if r['unactivated'] else 0.0

        return r
    _stat_accounts.names = ['total', 'unactivated', 'send', 'send_p', 
        'spam_check', 'spam_check_p',
        'control_check', 'control_check_p'
    ]
    

    def _stat_counts(self, refresh=False):
        r = {}

        r['total'] = self.parcels.count()

        r['new'] = self.parcels.filter(status=Parcel.NEW).count()
        r['sended'] = self.parcels.filter(status=Parcel.SENDED).count()
        
        r['net_error'] = self.parcels.filter(status__in=[Parcel.CONN_TIMEOUT, Parcel.NETWORK_ERROR, Parcel.CONN_BREAK]).count()
        r['net_error_p'] = r['net_error'] / float(r['total']) * 100.0 if r['total'] else 0.0
        
        r['unknow_error'] = self.parcels.filter(status=Parcel.UNKNOW_ERROR).count()
        r['unknow_error_p'] = r['unknow_error'] / float(r['total']) * 100.0 if r['total'] else 0.0
        
        r['invalid'] = self.parcels.filter(status=Parcel.INVALID_EMAIL).count()
        r['invalid_p'] = r['invalid'] / float(r['total']) * 100.0 if r['total'] else 0.0

        r['acc_error'] = self.parcels.filter(status=Parcel.ACCOUNT_ERROR).count()
        r['acc_error_p'] = r['acc_error'] / float(r['total']) * 100.0 if r['total'] else 0.0
        
        return r
    _stat_counts.names = ['total', 'new', 'sended', 
        'net_error', 'net_error_p', 
        'unknow_error', 'unknow_error_p', 
        'invalid', 'invalid_p', 
        'acc_error', 'acc_error_p'
    ]

    def _stat_spam(self, refresh=False):
        r = {}

        parcels = self.parcels.filter(status=Parcel.SENDED)
        ps1 = parcels.filter(is_ban_checked=True)
        ps2 = parcels.filter(is_ban_checked=False)
        
        r['total'] = parcels.count()
        
        r['checked'] = parcels.filter(is_ban_checked__isnull=False).count()
        r['checked_p'] = r['checked'] / float(r['total']) * 100.0 if r['total'] else 100.0
        
        r['sended'] = ps1.filter(is_banned=False).count()
        r['sended_p'] = r['sended'] / float(r['total']) * 100.0 if r['total'] else 0.0
        
        r['spam'] = ps1.filter(is_banned=True).count()
        r['spam_p'] = r['spam'] / float(r['total']) * 100.0 if r['total'] else 0.0
        
        r['unknow'] = ps2.count()
        r['unknow_p'] = r['unknow'] / float(r['total']) * 100.0 if r['total'] else 0.0
        
        return r
    _stat_spam.names = ['total', 'checked', 'checked_p',
        'sended', 'sended_p',
        'spam', 'spam_p',
        'unknow', 'unknow_p'
    ]
    
PARCEL_STATUS_CHOICE = {
    0: 'NEW',
    1: 'SENDED',
    2: 'INVALID_EMAIL',
    3: 'ACCOUNT_ERROR',
    4: 'SENDING',
    5: 'CONN_TIMEOUT',
    6: 'NETWORK_ERROR',
    7: 'CONN_BREAK',
    8: 'UNKNOW_ERROR',
}
class Parcel(models.Model):
    target_email = models.ForeignKey('sender.TargetEmail', default=None, null=True, blank=True)

    domain = models.ForeignKey(Domain, default=None, null=True, blank=True)
    account = models.ForeignKey(MailRuAccount, default=None, null=True, blank=True)
    try_count = models.PositiveSmallIntegerField(default=0,blank=False,null=False)

    mailing_task = models.ForeignKey(MailingTask, default=None, null=True, blank=True)
    email = models.CharField(max_length=200)

    created_at = models.DateTimeField(auto_now_add=True)
    sended_at = models.DateTimeField(default=None, null=True, blank=True)
    comment = models.TextField(default=None, null=True, blank=True)
    
    # None - не проверен 
    # True - проверен 
    # False - акк заблокирован
    is_ban_checked = models.NullBooleanField(default=None)

    # None - не проверен 
    # True - спам 
    # False - не спам
    is_banned = models.NullBooleanField(default=None)
    
    status = models.IntegerField(default=0, choices=PARCEL_STATUS_CHOICE.items(), null=False, blank=False)

    is_control = models.BooleanField(default=False)

    NEW = 0
    SENDED = 1
    INVALID_EMAIL = 2
    ACCOUNT_ERROR = 3
    SENDING = 3
    CONN_TIMEOUT = 5
    NETWORK_ERROR = 6
    CONN_BREAK = 7
    UNKNOW_ERROR = 8

    class Meta:
        index_together = [
            ['status'],
            ['id', 'is_banned', 'status'],
        ]

CONTROL_PARCEL_STATUS_CHOICE = {
    0: 'NOT_COME',
    1: 'IN_SPAM',
    2: 'IN_INBOX',
    3: 'AUTH_ERROR',
}
class ControlParcel(models.Model):
    account = models.ForeignKey(MailRuAccount)
    parcel = models.ForeignKey(Parcel)

    status = models.IntegerField(default=None, choices=CONTROL_PARCEL_STATUS_CHOICE.items(), null=True, blank=True)
    checked_at = models.DateTimeField(default=None, null=True, blank=True)

    NOT_COME = 0
    IN_SPAM = 1
    IN_INBOX = 2
    AUTH_ERROR = 3

    def __str__(self):
        return 'To: %s' % self.account.username

CONFIG_TYPE = {
    0: 'PROXY_RANGE',
    1: 'PERCENT',
    2: 'TEXT',
    3: 'POSITIVE_INT',
}
CONFIGS = {
    
}
class Config(models.Model):
    group = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    value = models.TextField()
    value_type = models.IntegerField(default=0, choices=CONFIG_TYPE.items(), null=False, blank=False)
    comment = models.TextField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    def get_value(self, **kwargs):
        return self._val(**kwargs)
        
        # try:
        #     return self._val(**kwargs)
        
        # except:
        #     return None

    def _val(self, **kwargs):
        typ = CONFIG_TYPE[self.value_type].lower()
        name = 'value_%s' % typ
        if hasattr(self, name):
            return getattr(self, name)(**kwargs)

    @staticmethod
    def GET(name, default=None, **kwargs):
        if name not in CONFIGS:
            try:
                group,name = name.split('.')
                CONFIGS[name] = Config.objects.get(group=group, name=name)
            
            except:
                CONFIGS[name] = None
        
        if not CONFIGS[name]:
            return default

        if CONFIGS[name].is_active:
            return CONFIGS[name].get_value(**kwargs)
        else:
            return default

    def value_positive_int(self):
        if not self.value:
            raise Exception(u'empty value')

        try:
            val = int(self.value)
        except:
            raise Exception('not digit value')

        if val < 0:
            raise Exception('not positive value')

        return val

    def value_text(self):
        if not self.value:
            raise Exception(u'empty value')

        return self.value

    def value_percent(self):
        try:
            percent = int(self.value)
        except:
            raise Exception(u'error val')

        if percent < 0:
            raise Exception(u'less than 0')

        if percent > 100:
            raise Exception(u'more than 100')

        return percent

    def value_proxy_range(self, rand=False, asdict=False, waiting=False):
        typ_ints = {'http': 3, 'socks5': 2, 'socks4': 1}
        
        typ, ip, ports = self.value.split(':')
        
        if typ not in typ_ints.keys():
            raise Exception(u'Неизвестный тип прокси')

        if not re.match('^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$', ip):
            raise Exception(u'IP не найден')

        if '-' in ports:
            port1,port2 = ports.split('-')
            port1 = int(port1)
            port2 = int(port2)+1
        
        else:
            port1 = int(ports)
            port2 = port1+1

        ports = range(port1,port2)

        if rand:
            # выбираем активный порт
            port = None
            for _ in xrange(10 if waiting else 1):
                _ports = ports[:]
                while len(_ports) and port is None:
                    p = random.choice(_ports)
                    _ports.remove(p)
                    if cache.get('%s:%d' % (ip, p)) != False:
                        port = p
            
                if port is None and waiting:
                    print 'waiting...'
                    time.sleep(10)
    
            if port is None:
                raise Exception('Has not active port for proxy range: ' + str(self))

            if asdict:
                return {
                    'address':'%s:%d' % (ip, port),
                    'host': ip,
                    'type': typ,
                    'type_int': typ_ints[typ],
                    'port': port,
                }
            
            return ('%s:%d' % (ip, port), typ)
        
        if asdict:
            return {
                'host': ip,
                'type': typ,
                'type_int': typ_ints[typ],
                'port1': port1,
                'port2': port2,
                'ports': ports
            }
        return ip, ports

    def clean(self):
        try:
            self._val()

        except Exception as e:
            from django import forms
            raise forms.ValidationError(u'Неверное значение: %s' % str(e))

        return True

    def __str__(self):
        return '%s.%s' % (self.group, self.name)

class TaskLog(models.Model):
    task = models.ForeignKey(Task)
    
    arguments = models.TextField(null=True, blank=True)
    result = models.TextField(null=True, blank=True)
    
    out = models.TextField(null=True, blank=True)
    err = models.TextField(null=True, blank=True)
    
    started_at = models.DateTimeField(auto_now_add=True)
    stopped_at = models.DateTimeField(default=None, null=True, blank=True)

    def get_work_range(self):
        if not self.started_at:
            return None

        if not self.stopped_at:
            return self.started_at, timezone.now()

        return self.started_at, self.stopped_at

    def __str__(self):
        return '#%d' % (self.id) 

import pickle
CACHE_TYPE_CHOICE = {
    0: 'INT',
    1: 'FLOAT',
    2: 'DICT',
}
class Cache(models.Model):
    key = models.CharField(max_length=128)
    created_at = models.DateTimeField(auto_now_add=True)
    
    value_int = models.IntegerField(null=True, blank=True, default=None)
    value_str = models.TextField(null=True, blank=True, default=None)
    typ = models.PositiveSmallIntegerField(default=0)

    @property
    def value(self):
        try:
            if self.typ == 0:
                return self.value_int
            elif self.typ == 1:
                return self.value_int / 100.0
            elif self.typ == 2:
                return pickle.loads(self.value_str)
        except:
            return None

    @staticmethod
    def GET(key, default=None, created_at=None, as_obj=False):
        qs = Cache.objects.filter(key=key)
        
        if created_at:
            qs1 = qs.filter(created_at__lte=created_at).order_by('-created_at')
            if qs1.count():
                qs = qs1
            else:
                qs = qs.filter(created_at__gte=created_at).order_by('created_at')
        else:
            qs = qs.order_by('-created_at')

        if qs.count() == 0:
            return default

        if as_obj:
            return qs[0]

        return qs[0].value

    @staticmethod
    def GET_RANGE(key, begin=None, end=None):
        qs = Cache.objects.filter(key=key)

        if begin is None:
            cursor = connection.cursor()
            cursor.execute('SELECT MIN(`created_at`) FROM `main_cache` WHERE `key`=%s', [key])
            begin = cursor.fetchone()[0]

        if end is None:
            end = timezone.now()

        return qs.filter(created_at__gte=begin, created_at__lte=end)


    @staticmethod
    def SET(key, value):
        c = Cache()
        c.key = key
        if isinstance(value, int):
            c.value_int = value
            c.typ = 0
        elif isinstance(value, float):
            c.value_int = value * 100.0
            c.typ = 1
        elif isinstance(value, dict):
            c.value_str = pickle.dumps(value)
            c.typ = 2
        c.save()

    def __str__(self):
        return '%s=%s (%s)' % (self.key, self.value, self.created_at)

    class Meta:
        index_together = [
            ['key']
        ]