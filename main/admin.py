#coding=utf8
import os
import time

from django.contrib import admin
from django.db.models import Count, Max, Min, Q
from django import forms
from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from django.utils import timezone
from django.core.urlresolvers import reverse

from main.models import *
import main.views
import request.views

from django.contrib.admin.sites import AdminSite
class MyAdminSite(AdminSite):
    my_views = [
        {'name': 'Регистрация аккаунтов', 'url': 'account/reg/', 'view': main.views.reg_accounts},
        {'name': 'Загрузка аккаунтов', 'url': 'account/load/', 'view': main.views.load_accounts},
        {'name': 'Статистика доменов', 'url': 'domain/stat/', 'view': main.views.domain_stat},
        {'name': 'Статистика запросов', 'url': 'request/stat/', 'view': request.views.stat},
        {'name': 'Управление прокси', 'url': 'proxy/stat/', 'view': main.views.proxy_stat},
        {'name': '', 'url': 'proxy/unactivate/(?P<port>[0-9]+)/', 'view': main.views.proxy_port_unactivate, 'visible': False},
        {'name': '', 'url': 'proxy/activate/(?P<port>[0-9]+)/', 'view': main.views.proxy_port_activate, 'visible': False},
        {'name': '', 'url': 'proxy/trafic/(?P<port>[0-9]+)/', 'view': main.views.proxy_port_stat, 'visible': False},
    ]
    
    def get_extra_context(self):
        views = []
        for v in self.my_views:
            if not v.get('visible', True):
                continue
            view = {'name': v['name'], 'admin_url': '/admin/' + v['url']}
            views.append(view)
        return {
            'views': views
        }
    
    def index(self, request, extra_context={}):
        return super(MyAdminSite, self).index(request, self.get_extra_context())

    def app_index(self, request, app_label, extra_context={}):
        return super(MyAdminSite, self).app_index(request, app_label, self.get_extra_context())

    def get_urls(self):
        from functools import update_wrapper
        from django.conf.urls import url

        urls = super(MyAdminSite, self).get_urls()

        def wrap(view, cacheable=False):
            def wrapper(*args, **kwargs):
                return self.admin_view(view, cacheable)(*args, **kwargs)
            wrapper.admin_site = self
            return update_wrapper(wrapper, view)

        for v in self.my_views:
            u = url(r'^%s$' % v['url'], wrap(v['view']))
            urls.append(u)
        return urls
admin.site = MyAdminSite()

class AddDomainForm(forms.Form):
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)
    domain_count = forms.IntegerField()
    group_size = forms.IntegerField(initial=400)

class VirtualServerAdmin(admin.ModelAdmin):
    list_display = ('name', 'show_domain_count', 'show_can_domain_count', 'add_task_status')
    actions = ('add_domains',)

    def add_task_status(self, obj):
        task = Task.objects \
            .filter(uid='add_domains:%d' % obj.id) \
            .order_by('-id')
        if not task:
            return '-'
        t = task[0]
        return '<a href="/admin/main/task/%d/">Task %d:%s</a><br>:%s' % \
            (t.id, t.id, TASK_STATUS_CHOICE[t.status], t.status_string)
    add_task_status.allow_tags = True

    def add_domains(self, request, queryset):
        form = None

        if 'apply' in request.POST:
            form = AddDomainForm(request.POST)

            if form.is_valid():
                domain_count = form.cleaned_data['domain_count']
                group_size = form.cleaned_data['group_size']

                for obj in queryset:
                    t = Task()
                    t.add_domains(obj, domain_count, group_size)
                    t.run_now()
                    break

                self.message_user(request, "Добавление %d доменов группами по %d поставлено в очередь." % (domain_count, group_size))
                return HttpResponseRedirect(request.get_full_path())

        if not form:
            form = AddDomainForm(initial={'_selected_action': request.POST.getlist(admin.ACTION_CHECKBOX_NAME)})

        return render(request, 'virtual_server/add_domains.html', {'items': queryset,'form': form, 'title':u'Добавление доменов'})

    def show_domain_count(self, obj):
        qs = Domain.objects \
            .filter(name_servers__in=obj.get_name_servers(), virtual_server=obj) \
            .distinct()
        s = '/admin/main/domain/?name_servers__in=%s&virtual_server_id__exact=%d' % \
            (','.join([str(n.id) for n in obj.get_name_servers()]), obj.id)
        res = '<a href="%s">%d</a><br>' % (s, qs.count())
        res += '<a href="%s&state__exact=%d">не добавились=%d</a><br>' % \
            (s, Domain.NO_ADDED, qs.filter(state=Domain.NO_ADDED).count())
        res += '<a href="%s&state__exact=%d">удалились=%d</a><br>' % \
            (s, Domain.DELETED, qs.filter(state=Domain.DELETED).count())
        res += '<a href="%s&state__exact=%d">активных=%d</a><br>' % \
            (s, Domain.ACTIVATED, qs.filter(state=Domain.ACTIVATED).count())
        res += '<a href="%s&state__in=%d,%d">проверяются=%d</a><br>' % \
            (s, Domain.ADDED,Domain.VALIDATED, qs.filter(state__in=[Domain.ADDED,Domain.VALIDATED]).count())
        res += '<a href="%s&state__in=%d,%d">на удаление=%d</a><br>' % \
            (s, Domain.NO_ACTIVATED, Domain.NO_VALIDATED, qs.filter(state__in=[Domain.NO_ACTIVATED,Domain.NO_VALIDATED]).count())
        return res
    show_domain_count.allow_tags = True
    show_domain_count.short_description = 'Добавленных доменов'
    
    def show_can_domain_count(self, obj):
        qs = Domain.objects \
            .filter(name_servers__in=obj.get_name_servers()) \
            .distinct()
        s = '/admin/main/domain/?name_servers__in=%s' % ','.join([str(n.id) for n in obj.get_name_servers()])
        res = '<a href="%s">%d</a><br>' % (s, qs.count())
        res += '<a href="%s&state__exact=%d">можно добавить=%d</a><br>' % \
            (s, Domain.CHECKED, qs.filter(state=Domain.CHECKED).count())
        res += '<a href="%s&state__exact=%d">не добавились=%d</a><br>' % \
            (s, Domain.NO_ADDED, qs.filter(state=Domain.NO_ADDED).count())
        res += '<a href="%s&state__exact=%d">удалились=%d</a><br>' % \
            (s, Domain.DELETED, qs.filter(state=Domain.DELETED).count())
        res += '<a href="%s&state__exact=%d">активных=%d</a><br>' % \
            (s, Domain.ACTIVATED, qs.filter(state=Domain.ACTIVATED).count())
        res += '<a href="%s&state__in=%d,%d">проверяются=%d</a><br>' % \
            (s, Domain.ADDED,Domain.VALIDATED, qs.filter(state__in=[Domain.ADDED,Domain.VALIDATED]).count())
        res += '<a href="%s&state__in=%d,%d">на удаление=%d</a><br>' % \
            (s, Domain.NO_ACTIVATED, Domain.NO_VALIDATED, qs.filter(state__in=[Domain.NO_ACTIVATED,Domain.NO_VALIDATED]).count())
        return res 
    show_can_domain_count.allow_tags = True
    show_can_domain_count.short_description = 'Всего возможных доменов'

    def save_model(self, request, obj, form, change):
        super(VirtualServerAdmin, self).save_model(request, obj, form, change)
        t = Task()
        t.init_virtual_servers(server_ids=[obj.pk])
        t.run_now()
        
admin.site.register(VirtualServer, VirtualServerAdmin)

class NameServerAdmin(admin.ModelAdmin):
    list_display = ('name', 'show_domain_count')
    search_fields =('name', )

    def get_queryset(self, request):
        return NameServer.objects \
            .annotate(domain_count=Count('domain', distinct=True)) \
            .order_by('-domain_count')
            # .annotate(main_domain_count=Count('main_name_server')) \

    def show_domain_count(self, obj):
        qs = Domain.objects.filter(name_servers=obj)
        s = '/admin/main/domain/?name_servers__in=%s' % obj.id
        res = ''
        res += '<a href="%s">%d</a><br>' % (s, obj.domain_count)
        res += '<a href="%s&state__exact=%d">не добавились=%d</a><br>' % \
            (s, Domain.NO_ADDED, qs.filter(state=Domain.NO_ADDED).count())
        res += '<a href="%s&state__exact=%d">удалились=%d</a><br>' % \
            (s, Domain.DELETED, qs.filter(state=Domain.DELETED).count())
        res += '<a href="%s&state__exact=%d">активных=%d</a><br>' % \
            (s, Domain.ACTIVATED, qs.filter(state=Domain.ACTIVATED).count())
        res += '<a href="%s&state__in=%d,%d">проверяются=%d</a><br>' % \
            (s, Domain.ADDED,Domain.VALIDATED, qs.filter(state__in=[Domain.ADDED,Domain.VALIDATED]).count())
        res += '<a href="%s&state__in=%d,%d">на удаление=%d</a><br>' % \
            (s, Domain.NO_ACTIVATED, Domain.NO_VALIDATED, qs.filter(state__in=[Domain.NO_ACTIVATED,Domain.NO_VALIDATED]).count())
        return res 
    show_domain_count.allow_tags = True
    show_domain_count.admin_order_field = 'domain_count'
admin.site.register(NameServer, NameServerAdmin)

class DomainStateLogInline(admin.TabularInline):
    model = DomainStateLog

class DomainAdmin(admin.ModelAdmin):
    list_display = ('name', 'virtual_server', 'state', 'comment', 'updated_at', 'added_at', 
        'alias_to', 'expiry_date', 
        '_list_servers', 'created_at','spam_count','mail_count')
    list_filter = ('virtual_server', 'state')
    fields = ('name', 'dns_ip', 'state', 'updated_at', 
        'virtual_server', 'added_at',
        'mail_count', 'spam_count')
    search_fields = ('name', )
    inlines = [
        DomainStateLogInline,
    ]

    def _list_servers(self, obj):
        res = []
        for s in obj.name_servers.all():
            r = '<a href="/admin/main/nameserver/%d">%s</a>' % (s.id, s.name)
            if s.id == obj.main_name_server_id:
                r = '<i><b>%s</b></i>' % r
            res.append(r) 
        return ','.join(res)
    _list_servers.allow_tags = True
admin.site.register(Domain, DomainAdmin)

class DomainStateLogAdmin(admin.ModelAdmin):
    pass
admin.site.register(DomainStateLog, DomainStateLogAdmin)

class TaskAdmin(admin.ModelAdmin):
    list_display = ('name', 'typ', 'uid', 'pid', 'try_count', 'is_active_proc',
        'status', 'status_string', 'created_at', 'updated_at', '_logs')
    list_filter = ['can_retry']
    actions = ['startnow', 'restart', 'kill']
    search_fields =('uid', )
    fields = ['name', 'command', 'kill_command', 'arguments', 
    	'out', 'err', 'status_string', 'uid', 'status', 'typ', 
    	'pid', 'try_count', 'max_try_count', 'can_retry',
    	'time_shift', 'start_at', 'stop_at', 'started_at', 'stopped_at']

    def get_queryset(self, request):
        return Task.objects \
            .all() \
            .annotate(log_count=Count('tasklog'))

    def restart(self, request, queryset):
        for task in queryset:
            task.restart()
    restart.short_description = u"Перезапустить"

    def startnow(self, request, queryset):
        for task in queryset:
            task.restart()
            task.run_now()
    startnow.short_description = u"Запуск сейчас"

    def kill(self, request, queryset):
        for task in queryset:
            task.kill()
    kill.short_description = u"Завершить"

    def _logs(self, obj):
        return '<a href="/admin/main/tasklog/?task_id__exact=%d">%d</a>' % (obj.id, obj.log_count)
    _logs.allow_tags = True

    def is_active_proc(self, obj):
        return obj.is_active_proc()
    is_active_proc.boolean = True
admin.site.register(Task, TaskAdmin)

class TaskLogAdmin(admin.ModelAdmin):
    list_display = ['task', '_out_len', '_err_len', 'started_at', 'stopped_at']
    list_filter = ['task']

    def _out_len(self, obj):
        if obj.out:
            return len(obj.out)
    def _err_len(self, obj):
        if obj.err:
            return len(obj.err)

admin.site.register(TaskLog, TaskLogAdmin)

from django.contrib.admin import widgets 

class StartMailingForm(forms.Form):
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)
    recipients = forms.ChoiceField(label=u'Кому отправлять')
    now = forms.BooleanField(label=u'Отпрвить сейчас', required=False, initial=True)
    date = forms.DateTimeField(widget=widgets.AdminSplitDateTime(), required=False, label=u'Отправить по времени')
    limit = forms.IntegerField(required=False, label=u'Максимальное количество')
    group_size = forms.IntegerField(initial=1, label=u'Количество адресатов на одно письмо')
    control_shift = forms.IntegerField(initial=0, label=u'Через сколько писем отправлять контрольное')
    thread_count = forms.IntegerField(initial=100, label=u'Количество потоков')
    account_log_ids = forms.MultipleChoiceField(label=u'Аккаунты', required=False)
    site = forms.ChoiceField(label=u'Сайт', choices=ACCOUNT_SITE_CHOICE.items(), initial=0)
    selenium = forms.BooleanField(label=u'Selenium', required=False, initial=False)
    
    def __init__(self, *args, **kwargs):
        obj = kwargs.pop('obj')
        super(StartMailingForm, self).__init__(*args, **kwargs)
        
        choices = [('test','Тестовая рассылка'), ('target','По целевым почтам')]
        self.fields['recipients'].choices = choices
        self.fields['recipients'].initial = 'test'

        choices = []
        qs = TaskLog.objects \
            .annotate(count=Count('mailruaccount')) \
            .filter(task__uid__in=['accounts_reg', 'load_accounts'], count__gt=0) \
            .order_by('-id')[:50]
        for l in qs:
            choices.append( (l.id, u'#%d %s колво=%d (%s)' % 
                (
                    l.id,
                    l.task.name,
                    l.count,
                    timezone.localtime(l.started_at).strftime("%Y-%m-%d %H:%M"), 
                )) 
            )
        self.fields['account_log_ids'].choices = choices

    def clean(self):
        data = super(StartMailingForm, self).clean()
        
        if data['recipients'] == 'target' and not data.get('limit', False):
            self.add_error('limit', forms.ValidationError('Укажите сколько писем отправить'))
        return data

class MailingForm(forms.ModelForm):
    body = forms.CharField(widget=forms.Textarea(attrs={'class':'tinymce'}))
    # body = forms.CharField(widget=forms.Textarea())
    fake_domain_pattern = forms.CharField(required=False)
    pic_list = forms.CharField(widget=forms.Textarea(),required=False)
    link_list = forms.CharField(widget=forms.Textarea(),required=False)

    def __init__(self, *args, **kwargs):
        super(MailingForm, self).__init__(*args, **kwargs)
        
        instance = kwargs.get('instance')
        self.obj = instance

        self.fields['fake_domain_pattern'].help_text = 'Например: http://{%rs,3-5,l%}.com'
        self.fields['body'].help_text = """
        <hr>
        {%rs,1-8,u%} - случайная строка в верхнем регистре длина от 1 до 8 символов<br>
        {%rs,1-8,l%} - случайная строка в нижнем регистре длина от 1 до 8 символов<br>
        {%rs,1-8,r%} - случайная строка в верхнем И нижнем регистре длина от 1 до 8 символов<br>
        {%rs,5,lu%} - случайная строка в верхнем ИЛИ нижнем регистре длина 5 символов<br>
        {%rs,3,n%} - случайная строка из символов длина 3 символа<br>
        {%timestamp%} - timestamp<br>
        <hr>
        $temp=[{%rs,3,n%}] - сохранение в переменную<br>
        {%var,temp%} - вывод переменной<br>
        <hr>
        {%pic%} - вывод ссылки на изображение<br>
        {%link%} - вывод ссылки на переадресацию<br>
        """

    def clean(self):
        data = super(MailingForm, self).clean()
        
        if data.get('fake_domain', False):
            p = data.get('fake_domain_pattern')
            if p is None or p == '':
                self.add_error('fake_domain_pattern', forms.ValidationError('Укажите шаблон для тестового домена.'))
        
        # сохраняет ссылки
        for u in data.get('pic_list', '').split('\n'):
            MailingUrl.create(mailing=self.obj, url=u, mode=MailingUrl.MODE_PIC)
        
        for u in data.get('link_list', '').split('\n'):
            MailingUrl.create(mailing=self.obj, url=u, mode=MailingUrl.MODE_LINK)
        
        return data

class MailingUrlAdmin(admin.ModelAdmin):
    list_filter = ['mode', 'mailing', 'is_banned']
    list_display = ['url', 'mode', 'is_banned', 'banned_at', 
        '_mail_count', '_spam_count1', '_spam_count2']
    readonly_fields = ['mail_count', 'spam_count1', 'spam_count2']

    def get_queryset(self, request):
        return MailingUrl.objects \
            .annotate(
                parcel_count=Count('mailingurlparcel'),
                banned_parcel_count=Count('mailingurlparcel', parcel__is_banned=True)
            )

    def _mail_count(self, obj):
        return obj.mail_count
    _mail_count.short_description = u'Количество писем'
    
    def _spam_count1(self, obj):
        return obj.spam_count1
    _spam_count1.short_description = u'Вернулось как спам'
    
    def _spam_count2(self, obj):
        return obj.spam_count2
    _spam_count2.short_description = u'Пришло в спам'

admin.site.register(MailingUrl, MailingUrlAdmin)

class MailingAdmin(admin.ModelAdmin):
    list_display = ['name', 'id', 'body_type', 'image_gif', 
        '_last_mailing_task', '_target_count', '_image_count', '_link_count',
        '_link_url_count', '_pic_url_count']
    actions = ['start']
    form = MailingForm

    class Media:
        js = ('%sadmin/jquery.js' % settings.STATIC_URL,  
              '%stinymce/tinymce.min.js' % settings.STATIC_URL,  
              '%stinymce/tinymce_init.js' % settings.STATIC_URL,
              '%sselect2/js/select2.min.js' % settings.STATIC_URL)
        css = {
            'all': ('%sselect2/css/select2.min.css' % settings.STATIC_URL,)
        }

    def _last_mailing_task(self, obj):
        if not obj.mailing_task:
            return '-'

        return '<a href="/admin/main/mailingtask/?mailing__id__exact=%d">Task %d, %s</a>' % \
            (obj.id, obj.task.id, TASK_STATUS_CHOICE[obj.task.status])
    _last_mailing_task.allow_tags = True
    _last_mailing_task.short_description = u'Последняя рассылка'

    def _target_count(self, obj):
        if not obj.target:
            return '-'
        # return '{:,}'.format(obj.target.get_emails().count())
    _target_count.short_description = u'Количество почт'

    def _image_count(self, obj):
        from request.models import Log
        count = Log.objects.filter(mode='image', mailing_id=obj.id).count()
        res = '<a href="">%d</a><br>' % (count)
        try:
            d = Domain.objects.filter(state=Domain.ACTIVATED).order_by('?')[0]
            l = os.path.join(d.get_link(), obj.gen_key('i'))
            res += u'изображение: <a href="%s" target="_blank">%s</a>' % (l, l)
        except:
            res += 'нет доменов'
        return res
    _image_count.allow_tags = True
    _image_count.short_description = u'Изображений'

    def _link_count(self, obj):
        from request.models import Log
        count = Log.objects.filter(mode='link', mailing_id=obj.id).count()
        res = '<a href="">%d</a><br>' % (count)
        try:
            d = Domain.objects.filter(state=Domain.ACTIVATED).order_by('?')[0]
            l = os.path.join(d.get_link(), obj.gen_key('l'))
            res += u'переадресация: <a href="%s" target="_blank">%s</a>' % (l, l)
        except:
            res += 'нет доменов'
        return res
    _link_count.allow_tags = True
    _link_count.short_description = u'Переходов'

    def _link_url_count(self, obj):
        d = MailingUrl.objects.filter(mailing=obj, mode=MailingUrl.MODE_LINK)
        u = '/admin/main/mailingurl/?mailing__id__exact=%d&mode__exact=%s' % (obj.id, MailingUrl.MODE_LINK)
        res = '<a href="%s&is_banned__exact=0">%d</a>' % \
            (u, d.filter(is_banned=False).count())
        res += ' / <a href="%s&is_banned__exact=1">%d</a>' % \
            (u, d.filter(is_banned=True).count())
        return res
    _link_url_count.short_description = u'Ссылок переадресаций (активно/бан)'
    _link_url_count.allow_tags = True
    
    def _pic_url_count(self, obj):
        d = MailingUrl.objects.filter(mailing=obj, mode=MailingUrl.MODE_PIC)
        u = '/admin/main/mailingurl/?mailing__id__exact=%d&mode__exact=%s' % (obj.id, MailingUrl.MODE_PIC)
        res = '<a href="%s&is_banned__exact=0">%d</a>' % \
            (u, d.filter(is_banned=False).count())
        res += ' / <a href="%s&is_banned__exact=1">%d</a>' % \
            (u, d.filter(is_banned=True).count())
        return res 
    _pic_url_count.short_description = u'Ссылок изображений (активно/бан)'
    _pic_url_count.allow_tags = True

    def start(self, request, queryset):
        obj = queryset[0]
        form = None

        if 'apply' in request.POST:
            form = StartMailingForm(request.POST, obj=obj)

            if form.is_valid():
                recipients = form.cleaned_data.get('recipients')
                group_size = form.cleaned_data.get('group_size', 1)
                control_shift = form.cleaned_data.get('control_shift', 1)
                thread_count = form.cleaned_data.get('thread_count', 100)
                limit = form.cleaned_data.get('limit', 0)
                account_log_ids = form.cleaned_data.get('account_log_ids', [])
                site = form.cleaned_data.get('site', 0)
                selenium = form.cleaned_data.get('selenium', False)

                now = form.cleaned_data['now']
                if now:
                    date = timezone.now()
                else:
                    date = form.cleaned_data['date']

                t = Task()
                t.send_mailing(obj, date, recipients, 
                    limit, group_size, control_shift, thread_count, 
                    account_log_ids, site, selenium)
                obj.save()
                
                # запускаем процесс сразу
                if now:
                    t.run_now()

                self.message_user(request, "Рассылка поставлена на время %s." % (date))
                return HttpResponseRedirect(request.get_full_path())

        if not form:
            form = StartMailingForm(obj=obj, initial={'_selected_action': request.POST.getlist(admin.ACTION_CHECKBOX_NAME)})

        return render(request, 'mailing/start.html', {'items': queryset,'form': form, 'title':u'Начать рассылку'})
    start.short_description = u"Запустить"

    def save_model(self, request, obj, form, change):
        super(MailingAdmin, self).save_model(request, obj, form, change)
        t = Task()
        t.init_virtual_servers(mailing_ids=[obj.pk])
        t.run_now()

admin.site.register(Mailing, MailingAdmin)

class MailRuAccountAdmin(admin.ModelAdmin):
    list_display = ('username', 'site', 'task_log', '_auth_data', 'has_my',
        '_is_active', 'mail_count', 'sended_at','checked_at', 'created_at')
    list_filter = ('is_active','unactivated_mailing_task', 'unactivate_reason', 'site')
    search_fields = ('username',)
    fields = ['username', 'password', 'birthdate', 'firstname', 'lastname', 
        'sex', 'has_my', 'mail_count', 'unactivate_count', 'sended_at', 'checked_at', 
        'is_active', 'unactivate_reason', 'unactivated_at',
        'unactivated_mailing_task', 'auth_data']
    readonly_fields = ['mail_count', 'unactivate_count']
    def _auth_data(self, obj):
        if not obj.auth_data:
            return False
        return True
    _auth_data.boolean = True

    def _is_active(self, obj):
        res = ''
        if obj.is_active:
            res += u'<img src="/static/admin/img/icon-yes.gif" alt="True"> <span title="Попыток блокировки">(%d)</span>' % obj.unactivate_count
        else:
            res += u'<img src="/static/admin/img/icon-no.gif" alt="False"><br>Время: %s<br>Причина: %s<br>Таск: %s ' % (
                timezone.localtime(obj.unactivated_at).strftime("%Y-%m-%d %H:%M") if obj.unactivated_at else '-', 
                ACCOUNT_UNACTIVATE_REASON_CHOICE[obj.unactivate_reason] if obj.unactivate_reason else '-', 
                obj.unactivated_mailing_task
            )
        return res
    _is_active.allow_tags = True
    _is_active.short_description = u'Is Active'

admin.site.register(MailRuAccount, MailRuAccountAdmin)

class ParcelAdmin(admin.ModelAdmin):
    list_display = ('email', 'status', '_comment', 'try_count', 'created_at', 'sended_at', '_target_email', 'account_link', 'mailing_task_link', 'domain_link', 'is_banned', 'is_ban_checked', 'is_control')
    list_filter = ('status','is_banned', 'is_control', 'mailing_task')
    fields = ('mailing_task', 'email', 'status', 'is_banned', 'is_ban_checked', 'comment')

    def _comment(self, obj):
        if not hasattr(self, 'html_parser'):
            import HTMLParser
            self.html_parser = HTMLParser.HTMLParser()
        if obj.comment:
            try:
                return json.loads(obj.comment)[2]['Error']
            except:
                return obj.comment
                return self.html_parser.unescape(obj.comment)[:100]
    _comment.short_description = u'Комментарий'
    _comment.allow_tags = True

    def _target_email(self, obj):
        if obj.target_email:
            return '#%d %s [%d]' % (obj.target_email.id, obj.target_email.target, obj.target_email.email_id)
    _target_email.allow_tags = True
    _target_email.short_description = u'Таргет'

    def account_link(self, obj):
        if obj.account:
            return '<a href="/admin/main/mailruaccount/%d">%s</a>' % (obj.account.id, obj.account)
        return '-'
    account_link.allow_tags = True

    def mailing_task_link(self, obj):
        if obj.mailing_task:
            return '<a href="/admin/main/mailing_task/%d">%s</a>' % (obj.mailing_task.id, obj.mailing_task)
        return '-'
    mailing_task_link.allow_tags = True

    def domain_link(self, obj):
        if obj.domain:
            return '<a href="/admin/main/domain/%d">%s</a>' % (obj.domain.id, obj.domain)
        return '-'
    domain_link.allow_tags = True

admin.site.register(Parcel, ParcelAdmin)

def get_plot(rows):
    data = []
    for d, v in rows:
        data.append('[Date.UTC(%d, %d, %d, %d, %d), %s]' % \
            (d.year, d.month, d.day, d.hour, d.minute, v)
        ) 
    return ','.join(data)


class MailingTaskForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(MailingTaskForm, self).__init__(*args, **kwargs)

        instance = kwargs.get('instance')

        from django.db import connection
        cursor = connection.cursor()

        query = """
            SELECT MAX(sended_at),MIN(sended_at) 
            FROM main_parcel 
            WHERE mailing_task_id=%s AND status=%s
        """
        cursor.execute(query, [instance.id, Parcel.SENDED])
        maxd, mind = cursor.fetchone()
        
        res1 = []
        res2 = []
        d = mind
        while d < maxd:
            c = Parcel.objects.filter(mailing_task=instance, status=Parcel.SENDED, sended_at__lte=d).count()
            res1.append( (d, '%d' % c) )
            
            df = (d-mind).total_seconds()
            res2.append( (d, '%2.2f' % (c / float(df) if df else 0.0)) )
            
            d += timedelta(seconds=5)
        
        data1 = get_plot(res1)
        data2 = get_plot(res2)

        self.fields['mailing'].help_text = """
        <script src="http://code.highcharts.com/highcharts.js"></script>
        <script src="http://code.highcharts.com/modules/exporting.js"></script>

        <script type="text/javascript">
        $(function () {
            $('#container1').highcharts({
                chart: {
                    type: 'spline'
                },
                title: {
                    text: 'Количество отправленных писем'
                },
                xAxis: {
                    type: 'datetime',
                    dateTimeLabelFormats: { // don't display the dummy year
                        month: '%e.%b',
                        year: '%y'
                    },
                    title: {
                        text: 'Date'
                    }
                },
                yAxis: {
                    title: {
                        text: 'кол-во писем'
                    },
                },
                tooltip: {
                    headerFormat: '<b>{series.name}</b><br>',
                    pointFormat: '{point.x:%e.%b %H:%M}: {point.y}'
                },

                plotOptions: {
                    spline: {
                        marker: {
                            enabled: true
                        }
                    }
                },

                series: [{
                    data: [
                        """ + data1 + """
                    ]
                }]
            });
            $('#container2').highcharts({
                chart: {
                    type: 'spline'
                },
                title: {
                    text: 'Время на отправку письма (c)'
                },
                xAxis: {
                    type: 'datetime',
                    dateTimeLabelFormats: { // don't display the dummy year
                        month: '%e.%b',
                        year: '%y'
                    },
                    title: {
                        text: 'Date'
                    }
                },
                yAxis: {
                    title: {
                        text: 'На одно письмо (с)'
                    },
                },
                tooltip: {
                    headerFormat: '<b>{series.name}</b><br>',
                    pointFormat: '{point.x:%e.%b %H:%M}: {point.y}'
                },

                plotOptions: {
                    spline: {
                        marker: {
                            enabled: true
                        }
                    }
                },

                series: [{
                    data: [
                        """ + data2 + """
                    ]
                }]
            });
        });
        </script>
        <div id="container1" style="min-width: 310px; height: 600px; margin: 0 auto"></div>
        <div id="container2" style="min-width: 310px; height: 600px; margin: 0 auto"></div>
        """


class MailingTaskAdmin(admin.ModelAdmin):
    form = MailingTaskForm
    list_per_page = 10
    
    list_display = ('id', 'mailing', 'task_link', 'parameters', '_download_size',
        'counts', 'spam_count', 'control_nums', 'accounts', 'times')
    list_filter = ('mailing',)
    actions = ('kill','reset_cache', 'reset_spam', 'reset_control_delivery')

    def reset_spam(self, request, queryset):
        for obj in queryset:
            obj.parcels \
                .filter(status=Parcel.SENDED) \
                .update(is_ban_checked=None)
            # сбрасываем стату
            obj.reset_stat('spam')
        # перезапускаем задачу
        time.sleep(1)
        t = Task.GET('check_mailing')
        if t:
            t.run_now()
    reset_spam.short_description = u'Перепроверить спам'

    def reset_control_delivery(self, request, queryset):
        for obj in queryset:
            ControlParcel.objects \
                .filter(parcel__mailing_task=obj) \
                .filter(parcel__status=Parcel.SENDED) \
                .update(status=None, checked_at=None)
            # сбрасываем стату
            obj.reset_stat('control_delivery')
        # перезапускаем задачу
        time.sleep(1)
        t = Task.GET('control_check_mailing')
        if t:
            t.run_now()
    reset_control_delivery.short_description = u'Перепроверить контрольную рассылку'

    def kill(self, request, queryset):
        for mt in queryset:
            mt.task.kill()
    kill.short_description = u"Завершить"

    def reset_cache(self, request, queryset):
        ks = ['control_delivery', 'spam', 'counts', 'accounts']
        for mt in queryset:
            for k in ks:
                mt.reset_stat(k)
    reset_cache.short_description = u'Пересчитать статистику'

    def parameters(self, obj):
        res = ''
        res += 'BCC: %d<br>' % obj.group_size
        res += 'Потоков: %d<br>' % obj.thread_count
        res += 'Шаг контрольных.писем: %d<br>' % obj.control_shift
        res += 'Тип: %s<br>' % MAILING_BODY_TYPE[obj.body_type]
        res += 'Fake domain: %s<br>' % ('Y' if obj.fake_domain else 'N')
        res += 'SITE: %s<br>' % ACCOUNT_SITE_CHOICE[obj.site]
        
        recipients = obj.recipients.encode('utf8')
        if recipients == 'target':
            recipients = 'По таргету'
        elif recipients == 'test':
            recipients = 'По тестовым получателям'
        elif recipients.startswith('mailingtask'):
            _,id = recipients.split('_')
            recipients = 'перезапуск рассылки #%s' % id
        res += 'Получатели: %s<br>' % recipients

        return res
    parameters.allow_tags = True
    parameters.short_description = u'Параметры'

    def _download_size(self, obj):
        begin = obj.started_at
        end = obj.finished_at

        res = ''
        # if begin and end:
        #     p = Config.GET('send_mail.proxy', asdict=True)
            
        #     stat = {'tx': 0.0, 'rx': 0.0}
        #     for k in stat.keys():
        #         ind = 0
        #         for port in p['ports']:
        #             ind += 1
        #             key = 'proxy_%d_trafic_%s' % (ind, k)
        #             b = Cache.GET(key, default=0.0, created_at=begin)
        #             e = Cache.GET(key, default=0.0, created_at=end)
        #             stat[k] += (e-b)
        #     res += u'vnstat:<br>'
        #     res += u'исходящий(мб):%2.2f<br>входящий(мб):%2.2f<br>---<br>' % (stat['tx'], stat['rx'])

        d = obj.download_size / 1024.0
        t = obj.parcels.filter(status=Parcel.SENDED).count()
        if t:
            m = d / float(t) 
        else:
            m = 0
        res += u'all=%2.2fкб<br>avg=%2.2fкб<br>' % (d, m)
        return res
    _download_size.allow_tags = True
    _download_size.short_description = u'Траффик'

    def spam_count(self, obj):
        t = obj.stat('spam')

        s = '/admin/main/parcel/?mailing_task__id__exact=%d&is_control__exact=0&status__exact=1' % obj.id
        
        res = ''
        res += timezone.localtime(t['updated_at']).strftime("UPD:%Y-%m-%d %H:%M<br>")
        res += '<a href="%s">всего=%d</a><br>' % (s, t['total'])
        res += '<a href="%s&is_ban_checked__isnull=0">проверено=%d (%2.2f%%)</a><br>' % (s, t['checked'], t['checked_p'])
        res += '---<br>'
        res += '<a href="%s&is_ban_checked__exact=1&is_banned__exact=1">спам=%d (%2.2f%%)</a><br>' % (s, t['spam'], t['spam_p'])
        res += '<a href="%s&is_ban_checked__exact=1&is_banned__exact=0">дошло=%d (%2.2f%%)</a><br>' % (s, t['sended'], t['sended_p'])
        res += '<a href="%s&is_ban_checked__exact=0">неизвестно=%d (%2.2f%%)</a><br>' % (s, t['unknow'], t['unknow_p'])
        return res
    spam_count.allow_tags = True
    spam_count.short_description = u'Спам'

    def counts(self, obj):
        res = ''
        t = obj.stat('counts')

        s = '/admin/main/parcel/?&mailing_task__id__exact=%d&is_control__exact=0' % obj.id

        res += timezone.localtime(t['updated_at']).strftime("UPD:%Y-%m-%d %H:%M<br>")
        res += '<a target="_blank" ' \
            'href="%s">всего=%d</a><br>' % \
                (s, t['total'])
        res += '<a target="_blank" ' \
            'href="%s&status__exact=0">отправляются=%d</a><br>' % \
            (s, t['new'])
        res += '<a target="_blank" ' \
            'href="%s&status__exact=1">отправлено=%d</a><br>' % \
                (s, t['sended'])
        res += '---<br>'
        res += '<a target="_blank" ' \
            'href="%s&status__in=5,6,7">сеть=%d (%2.2f%%)</a><br>' % \
                (s, t['net_error'], t['net_error_p'])
        res +=  '<a target="_blank" ' \
            'href="%s&status__exact=8">неизвестно=%d (%2.2f%%)</a><br>' % \
                (s, t['unknow_error'], t['unknow_error_p'])

        res += '<a target="_blank" ' \
            'href="%s&status__exact=2">невалид=%d (%2.2f%%)</a><br>' % \
                (s, t['invalid'], t['invalid_p'])
        res += '<a target="_blank" ' \
            'href="%s&status__exact=3">аккаунт=%d (%2.2f%%)</a><br>' % \
                (s, t['acc_error'], t['acc_error_p'])


        return res
    counts.allow_tags = True
    counts.short_description = u'Количество'

    def task_link(self, obj):
        task = obj.task
        res = '<a href="/admin/main/task/%d">Task %d, %s</a><br>%s' % \
            (task.id, task.id, TASK_STATUS_CHOICE[task.status], task.status_string)
        
        if task.status == Task.FAILURE:
            exc = task.get_exception()
            if exc:
                res += u'<br>Причина:<br>%s' % exc.encode('utf8')
        return res
    task_link.allow_tags = True
    task_link.short_description = u'Задача'

    def times(self, obj):
        res = 'Начало:%s<br>' % timezone.localtime(obj.started_at).strftime("%Y-%m-%d %H:%M:%S")

        if obj.finished_at:
            res += 'Конец:%s<br>' % timezone.localtime(obj.finished_at).strftime("%Y-%m-%d %H:%M:%S")
            
            delta = obj.finished_at - obj.started_at
            
            m, s = int(delta.total_seconds() / 60.0), int(delta.total_seconds() % 60.0)
            res +=  'Затрачено(м:c): %02d:%02d<br>' % (m,s)

            d = obj.parcels \
                .filter(status=Parcel.SENDED) \
                .count()
            t = delta.total_seconds() / float(d) if d else 0
            res +=  'На письмо(с):%2.2f' % t
        else:
            t = obj.stat('counts')
            c1 = t['sended']
            c2 = t['total'] - c1
            d = (timezone.now() - obj.started_at).total_seconds() / float(c1) if c1 else 0
            delta = datetime.timedelta(seconds=d*c2)
            f = obj.started_at + delta
            m, s = int(delta.total_seconds() / 60.0), int(delta.total_seconds() % 60.0)
            res += '---<br>'
            res += 'Прогнозируемый конец:%s<br>' % timezone.localtime(f).strftime("%Y-%m-%d %H:%M:%S")
            res += 'Примерно осталось(м:c): %02d:%02d<br>' % (m,s)
            res += 'На письмо(с):%2.2f<br>' % d
        return res
    times.allow_tags = True
    times.short_description = u'Время'
    
    def accounts(self, obj):
        t = obj.stat('accounts')

        res = ''
        res += timezone.localtime(t['updated_at']).strftime("UPD:%Y-%m-%d %H:%M<br>")
        res += 'менялось=%d<br>' % (obj.used_account_num)
        res += 'использовано=%d<br>' % (t['total'])
        res += '<a target="_blank" href="/admin/main/mailruaccount/?unactivated_mailing_task__id__exact=%d">забаненно=%d</a><br>' % \
            (obj.id, t['unactivated'])
        res += 'отправка=%d (%2.2f%%)<br>' % (t['send'], t['send_p'])
        res += 'проверка спама=%d (%2.2f%%)<br>' % (t['spam_check'], t['spam_check_p'])
        res += 'контрольная рассылка=%d (%2.2f%%)<br>' % (t['control_check'], t['control_check_p'])
        return res
    accounts.allow_tags = True
    accounts.short_description = u'Аккаунты'

    def control_nums(self, obj):
        t = obj.stat('control_delivery')

        s = '/admin/main/controlparcel/?parcel__mailing_task__id__exact=%d' % obj.id
        
        res = ''
        res += timezone.localtime(t['updated_at']).strftime("UPD:%Y-%m-%d %H:%M<br>")
        res += '<a href="%s">всего=%d</a><br>' % (s, t['total'])
        res += '<a href="%s&parcel__status__exact=%d&status__isnull=0&status__in=0,1,2">проверено=%d (%2.2f%%)</a><br>' % \
                (s, Parcel.SENDED, t['checked'], t['checked_p'])
        res += '---<br>'
        res += '<a href="%s&parcel__status__exact=%d&status__exact=%d">входящие=%d (%2.2f%%)</a><br>' % \
                (s, Parcel.SENDED, ControlParcel.IN_INBOX, t['inbox'], t['inbox_p'])
        res += '<a href="%s&parcel__status__exact=%d&status__exact=%d">спам=%d (%2.2f%%)</a><br>' % \
                (s, Parcel.SENDED, ControlParcel.IN_SPAM, t['spam'], t['spam_p'])
        res += '<a href="%s&parcel__status__exact=%d&status__exact=%d">не пришло=%d (%2.2f%%)</a><br>' % \
                (s, Parcel.SENDED, ControlParcel.NOT_COME, t['not_come'], t['not_come_p'])
        res += '<a href="%s&parcel__status__exact=%d&status__exact=%d">неизвестно=%d (%2.2f%%)</a><br>' % \
                (s, Parcel.SENDED, ControlParcel.AUTH_ERROR, t['unknow'], t['unknow_p'])
        return res
    control_nums.allow_tags = True
    control_nums.short_description = u'Контрольная рассылка'

admin.site.register(MailingTask, MailingTaskAdmin)

class ConfigAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'is_active', 'value', 'created_at', 'updated_at', 'comment')
    list_filter = ('group', )

    # def save_model(self, request, obj, form, change):
    #     try:
    #         obj.save()
    #     except:
    #         raise forms.ValidationError(u"You haven't set a valid department. Do you want to continue?")

admin.site.register(Config, ConfigAdmin)

class ControlParcelAdmin(admin.ModelAdmin):
    list_display = ('pk', '_parcel', '_from', '_to', 'status', 'checked_at')
    list_filter = ('status', 'parcel__status', 'parcel__mailing_task')

    def _parcel(self, obj):
        return '<a href="/admin/main/parcel/%d/">%s %d</>' % (obj.parcel.id, PARCEL_STATUS_CHOICE[obj.parcel.status], obj.parcel.id)
    _parcel.allow_tags = True
    
    def _from(self, obj):
        if obj.parcel.account:
            return '<a href="/admin/main/mailruaccount/%d">%s</a>' % (obj.parcel.account.id, str(obj.parcel.account))
    _from.allow_tags = True
    _from.short_description = u'Отправитель'

    def _to(self, obj):
        return '<a href="/admin/main/mailruaccount/%d">%s</a>' % (obj.account.id, str(obj.account))
    _to.allow_tags = True
    _to.short_description = u'Получатель'

admin.site.register(ControlParcel, ControlParcelAdmin)

import pickle
class CacheAdmin(admin.ModelAdmin):
    list_display = ('key', 'created_at', 'value', 'value_int', 'value_str')
    list_filter = ['key']
admin.site.register(Cache, CacheAdmin)