#coding=utf8
import operator
import datetime

from django.utils import timezone
from django.core.cache import cache
from django.db import models
from django.db.models import fields, Q, Count
from main.models import Parcel
# from south.modelsinspector import add_introspection_rules

EMAIL_SOURCE_CHOICE = {
    0: 'СНГ',
    1: 'Бурж',
}

class BigAutoField(fields.AutoField):
    def db_type(self, connection):
        if 'mysql' in connection.__class__.__module__:
            return 'bigint AUTO_INCREMENT'
        return super(BigAutoField, self).db_type(connection)

# add_introspection_rules([], ["^sender\.fields\.BigAutoField"])

class Geo():
    def get_id(self):
        return '%s:%d' % (self.__class__.__name__, self.id)
    def get_name(self):
        return '%s (%s)' % (self.name, self.__class__.__name__)

    @staticmethod
    def models():
        return [Continent, Country, Region, City]

    @staticmethod
    def get(s):
        try:
            prefix,id = s.split(':')
            for m in Geo.models():
                if prefix == m.__name__:
                    return m.objects.get(pk=id)
        except:
            pass

class Target(models.Model):
    name = models.CharField(max_length=255)
    age_from = models.PositiveSmallIntegerField(default=None, null=True, blank=True)
    age_to = models.PositiveSmallIntegerField(default=None, null=True, blank=True)
    sex = models.CharField(default=None, null=True, blank=True, max_length=10)
    geo = models.TextField(default=None, null=True, blank=True)
    without_geo = models.BooleanField(default=False)
    only_valid = models.BooleanField(default=False)
    source = models.TextField(null=True, blank=True, default='0')
    services = models.TextField(null=True, blank=True, default=None)
    
    created_at = models.DateTimeField(auto_now_add=True)

    def get_geo(self):
        geos = []
        if self.geo:
            for s in self.geo.split(','):
                geos.append(Geo.get(s))
        return geos

    def get_source(self):
        sources = []
        if self.source:
            for s in self.source.split(','):
                sources.append(int(s))
        return sources

    def get_services(self):
        services = []
        if self.services:
            for s in self.services.split(','):
                services.append(int(s))
        return services

    def get_filter_emails(self):
        fs = []
        
        #geo
        ps = []
        ss = []
        for g in self.get_geo():
            n = g.__class__.__name__.lower()
            ps.append(Q(**{n: g}))

        if self.without_geo:
            for m in Geo.models():
                n = m.__name__.lower()
                ps.append(Q(**{'%s__isnull' % n: True}))
                ps.append(Q(**{n: 0}))
        if ps:
            fs.append(reduce(operator.or_, ps))

        #age
        if self.age_from:
            d = self.created_at - datetime.timedelta(days=self.age_from*365.25)
            fs.append(Q(birthday__lte=d.strftime('%Y-01-01')))
        
        if self.age_to:
            d = self.created_at - datetime.timedelta(days=self.age_to*365.25)
            fs.append(Q(birthday__gte=d.strftime('%Y-01-01')))

        #sex
        if self.sex:
            ps = []
            for g in self.sex.split(','):
                ps.append(Q(sex=int(g)))
            fs.append(reduce(operator.or_, ps))

        #valid
        if self.only_valid:
            fs.append(Q(is_valid=True))
        else:
            fs.append(Q(is_valid=True) | Q(is_valid=None))

        # source
        for s in self.get_source():
            fs.append(Q(source=s))

        # services
        for s in self.get_services():
            if s == 0:
                # которые не выведены в селект
                fs.append(Q(service__in_select=False))
            else:
                fs.append(Q(service_id=s))

        return fs

    def get_emails(self):
        fs = self.get_filter_emails()

        if fs:
            qs = Email.objects.filter(reduce(operator.and_, fs))
        else:
            qs = Email.objects.all()

        return qs

    def get_lost_emails(self):
        qs = self.get_emails()
        ss = [
            Parcel.NEW, 
            Parcel.ACCOUNT_ERROR, 
            Parcel.CONN_TIMEOUT, 
            Parcel.CONN_BREAK, 
            Parcel.NETWORK_ERROR
        ]
        qs = qs \
            .filter(
                Q(targetemail__isnull=True) | \
                Q(targetemail__last_parcel__is_banned=True) | \
                Q(targetemail__last_parcel__status__in=ss)
            )
        return self.prepare_qs(qs)

    def prepare_qs(self, qs):
        def f(a, b):
            return 'LEFT OUTER JOIN `sender_targetemail` ON (`sender_email`.`id` = `sender_targetemail`.`email_id` AND `sender_targetemail`.`target_id` = %s)', [self.id]
        qs.query.alias_map['sender_targetemail'].as_sql = f
        return qs

    def get_sended_parcels(self):
        return self.prepare_qs(self.get_emails() \
            .filter(targetemail__last_parcel__status=Parcel.SENDED) \
            .distinct())

    def get_came_parcels(self):
        return self.prepare_qs(self.get_emails() \
            .filter(targetemail__last_parcel__status=Parcel.SENDED, 
                targetemail__last_parcel__is_banned=False) \
            .distinct())
    
    def get_not_came_parcels(self):
        return self.prepare_qs(self.get_emails() \
            .filter(targetemail__last_parcel__is_banned=True) \
            .distinct())

    def get_invalid_parcels(self):
        return self.prepare_qs(self.get_emails() \
            .filter(targetemail__last_parcel__status=Parcel.INVALID_EMAIL) \
            .distinct())
    
    def create_parcels(self, mailing_task, count):
        print 'CREATE PARCELS', count
        parcels = []

        if count < 0:
            return parcels
        
        # сначала перезапускаем повторно отказные
        # statuses = [
        #     Parcel.NEW, 
        #     Parcel.ACCOUNT_ERROR, 
        #     Parcel.CONN_TIMEOUT, 
        #     Parcel.CONN_BREAK, 
        #     Parcel.NETWORK_ERROR
        # ]
        # qs = Parcel.objects.filter(mailing_task_id=mailing_task_id) \
        #     .filter(status__in=statuses, try_count__lte=50) \
        #     .order_by('created_at','id')
        # if qs.count():
        #     self.parcels = list(qs)

        emails = self.get_lost_emails()
        for email in emails[:count]:
            t, new = TargetEmail.objects.get_or_create(target=self,email=email)
            # if not new and t.last_parcel.status == Parcel.SENDED and t.last_parcel.is
            p = Parcel()
            p.mailing_task = mailing_task
            p.email = email.email
            p.target_email = t
            if len(email.email) < 5:
                p.status = Parcel.INVALID_EMAIL
            p.save()
            
            t.last_parcel = p
            t.save()
            
            parcels.append(p)
        return parcels

    def __str__(self):
        return '%d %s' % (self.id, self.name)
    def __unicode__(self):
        return self.__str__()

    def reset_stat(self):
        cache.delete('target_%d_stat' % self.id)

    def get_stat(self):
        key = 'target_%d_stat' % self.id
        if not cache.get(key):
            res = timezone.localtime(timezone.now()).strftime("UPD:%Y-%m-%d %H:%M<br>")
            res += 'всего: {:,}<br>'.format(self.get_emails().count())
            res += 'осталось отправить: {:,}<br>'.format(self.get_lost_emails().count())
            res += 'отправилось: {:,}<br>'.format(self.get_sended_parcels().count())
            res += 'дошло: {:,}<br>'.format(self.get_came_parcels().count())
            res += 'НЕдошло: {:,}<br>'.format(self.get_not_came_parcels().count())
            res += 'невалид: {:,}<br>'.format(self.get_invalid_parcels().count())
            # raise Exception(str(qs.filter(parcel__status=Parcel.SENDED).distinct().query))
            # res += 'не отправилось: {:,}<br>'.format(qs.exclude(parcel__status=Parcel.SENDED).count())

            cache.set(key, res, timeout=None)
        return cache.get(key)

    def save(self, *args, **kwargs):
        self.reset_stat()
        return super(Target, self).save(*args, **kwargs)

class Continent(models.Model, Geo):
    name = models.CharField(max_length=64, null=True, default=None)
    is_deleted = models.IntegerField(null=True, default=0)
    delta = models.IntegerField(null=True, default=None)

    def __str__(self):
        return self.name
    def __unicode__(self):
        return self.__str__()

class Country(models.Model, Geo):
    name = models.CharField(max_length=64, null=True, default=None)
    is_deleted = models.IntegerField(null=True, default=0)
    delta = models.IntegerField(null=True, default=None)
    is_moderate = models.IntegerField(null=True, default=0)
    codename = models.CharField(max_length=128, default=None, null=True)
    continent = models.ForeignKey(Continent, db_column='continent')

    def __str__(self):
        return self.name
    def __unicode__(self):
        return self.__str__()


class Region(models.Model, Geo):
    name = models.CharField(max_length=64)
    is_city = models.IntegerField(null=True, default=0)
    country = models.ForeignKey(Country, db_column='country')
    is_deleted = models.IntegerField(null=True, default=0)
    delta = models.IntegerField(null=True, default=None)
    is_moderate = models.IntegerField(null=True, default=0)
    codename = models.CharField(max_length=128, default=None, null=True)
    parent = models.ForeignKey('self', null=True, default=None)

    def __str__(self):
        return self.name
    def __unicode__(self):
        return self.__str__()

class Location(models.Model):
    location_id = models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name
    def __unicode__(self):
        return self.__str__()

class City(models.Model, Geo):
    name = models.CharField(max_length=64, default=None, null=True)
    region = models.ForeignKey(Region, db_column='region')
    country = models.ForeignKey(Country, db_column='country')
    is_deleted = models.IntegerField(null=True, default=0)
    delta = models.IntegerField(null=True, default=None)
    is_moderate = models.IntegerField(null=True, default=0)
    codename = models.CharField(max_length=128, default=None, null=True)

    def __str__(self):
        return self.name
    def __unicode__(self):
        return self.__str__()

class StreetType(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name
    def __unicode__(self):
        return self.__str__()

class Street(models.Model):
    city = models.ForeignKey(City)
    street_type = models.ForeignKey(StreetType, db_column='street_type_id')
    name = models.CharField(max_length=64)

    def __str__(self):
        return self.name
    def __unicode__(self):
        return self.__str__()

EMAIL_SEX_CHOICE = {
    0: 'мужской',
    1: 'женский',
}

class EmailService(models.Model):
    name = models.CharField(max_length=50, default=None, null=True, unique=True)
    in_select = models.BooleanField(default=False)

class Email(models.Model):
    # id = BigAutoField()
    service = models.ForeignKey(EmailService, null=True, blank=True, default=None)
    email = models.CharField(max_length=50, default=None, null=True)
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    location = models.ForeignKey(Location, db_column='location', null=True, default=None)
    city = models.CharField(max_length=255, null=False, default=None)
    sex = models.IntegerField(default=0, choices=EMAIL_SEX_CHOICE.items())
    birthday = models.DateField(null=True, default=None)
    country = models.ForeignKey(Country, null=True, default=None) #
    region = models.ForeignKey(Region, null=True, default=None)  #
    city = models.ForeignKey(City, null=True, default=None) #
    continent = models.ForeignKey(Continent, null=True, default=None) #
    street = models.ForeignKey(Street, null=True, default=None) #
    
    is_valid = models.NullBooleanField(default=None)
    source = models.PositiveSmallIntegerField(default=0, choices=EMAIL_SOURCE_CHOICE.items())
    source_id = models.IntegerField(null=True, blank=True, default=None) # id в источнике

    def __str__(self):
        return self.email
    def __unicode__(self):
        return self.__str__()


    class Meta:
        index_together = [
            ['email'],
            ['birthday', 'is_valid'],
            ['is_valid']
        ]

class TargetEmail(models.Model):
    target = models.ForeignKey(Target)
    email = models.ForeignKey(Email)
    last_parcel = models.ForeignKey('main.Parcel', default=None, null=True)

    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        index_together = [
            ['email', 'target'],
        ]