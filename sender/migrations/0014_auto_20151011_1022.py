# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0013_auto_20151007_1628'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='targetemail',
            index_together=set([('email', 'target')]),
        ),
    ]
