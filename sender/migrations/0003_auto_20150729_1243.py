# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0002_auto_20150729_1219'),
    ]

    operations = [
        migrations.AlterField(
            model_name='email',
            name='birthday',
            field=models.DateField(default=None, null=True),
        ),
    ]
