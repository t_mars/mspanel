# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0017_email_source'),
    ]

    operations = [
        migrations.AddField(
            model_name='email',
            name='source_id',
            field=models.IntegerField(default=None, null=True, blank=True),
        ),
    ]
