# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0021_targetemail_source'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='targetemail',
            name='source',
        ),
        migrations.AddField(
            model_name='target',
            name='source',
            field=models.PositiveSmallIntegerField(default=0, choices=[(0, b'\xd0\xa1\xd0\x9d\xd0\x93'), (1, b'\xd0\x91\xd1\x83\xd1\x80\xd0\xb6')]),
        ),
    ]
