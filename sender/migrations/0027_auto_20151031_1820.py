# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0026_auto_20151031_1815'),
    ]

    operations = [
        migrations.AlterField(
            model_name='target',
            name='services',
            field=models.TextField(default=None, null=True, blank=True),
        ),
    ]
