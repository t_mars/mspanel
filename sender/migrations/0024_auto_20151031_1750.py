# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0023_auto_20151031_1740'),
    ]

    operations = [
        migrations.AddField(
            model_name='target',
            name='services',
            field=models.TextField(default=None, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='target',
            name='source',
            field=models.TextField(default=b'0', null=True, blank=True),
        ),
    ]
