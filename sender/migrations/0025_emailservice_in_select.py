# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0024_auto_20151031_1750'),
    ]

    operations = [
        migrations.AddField(
            model_name='emailservice',
            name='in_select',
            field=models.BooleanField(default=False),
        ),
    ]
