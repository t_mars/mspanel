# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0008_target'),
    ]

    operations = [
        migrations.AddField(
            model_name='target',
            name='only_valid',
            field=models.BooleanField(default=False),
        ),
    ]
