# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0014_auto_20151011_1022'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='email',
            index_together=set([('email',), ('birthday', 'is_valid')]),
        ),
    ]
