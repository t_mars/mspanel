# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0006_auto_20150730_0938'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='email',
            name='is_validate',
        ),
        migrations.RemoveField(
            model_name='email',
            name='isactive',
        ),
        migrations.AddField(
            model_name='email',
            name='is_valid',
            field=models.NullBooleanField(default=None),
        ),
    ]
