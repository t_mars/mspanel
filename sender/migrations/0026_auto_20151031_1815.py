# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0025_emailservice_in_select'),
    ]

    operations = [
        migrations.AlterField(
            model_name='target',
            name='services',
            field=models.TextField(default=b'0', null=True, blank=True),
        ),
    ]
