# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0007_auto_20150819_1438'),
    ]

    operations = [
        migrations.CreateModel(
            name='Target',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('age_from', models.PositiveSmallIntegerField(default=None, null=True, blank=True)),
                ('age_to', models.PositiveSmallIntegerField(default=None, null=True, blank=True)),
                ('sex', models.CharField(default=None, max_length=10, null=True, blank=True)),
                ('geo', models.TextField(default=None, null=True, blank=True)),
                ('without_geo', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
