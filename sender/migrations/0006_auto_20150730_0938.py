# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0005_auto_20150730_0750'),
    ]

    operations = [
        migrations.RenameField(
            model_name='city',
            old_name='city_name',
            new_name='name',
        ),
        migrations.RenameField(
            model_name='continent',
            old_name='continent_name',
            new_name='name',
        ),
        migrations.RenameField(
            model_name='country',
            old_name='country_name',
            new_name='name',
        ),
        migrations.RenameField(
            model_name='location',
            old_name='location',
            new_name='name',
        ),
        migrations.RenameField(
            model_name='region',
            old_name='region_name',
            new_name='name',
        ),
    ]
