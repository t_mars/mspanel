# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0009_target_only_valid'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='email',
            index_together=set([('email',)]),
        ),
    ]
