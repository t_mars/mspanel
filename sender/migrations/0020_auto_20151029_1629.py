# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0019_emailservice'),
    ]

    operations = [
        migrations.AddField(
            model_name='email',
            name='service',
            field=models.ForeignKey(default=None, blank=True, to='sender.EmailService', null=True),
        ),
        migrations.AlterField(
            model_name='emailservice',
            name='name',
            field=models.CharField(default=None, max_length=50, unique=True, null=True),
        ),
    ]
