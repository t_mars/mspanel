# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0003_auto_20150729_1243'),
    ]

    operations = [
        migrations.AlterField(
            model_name='email',
            name='sex',
            field=models.IntegerField(default=0, choices=[(b'1', b'\xd0\xb6\xd0\xb5\xd0\xbd\xd1\x81\xd0\xba\xd0\xb8\xd0\xb9'), (b'0', b'\xd0\xbc\xd1\x83\xd0\xb6\xd1\x81\xd0\xba\xd0\xbe\xd0\xb9')]),
        ),
    ]
