# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0022_auto_20151031_1531'),
    ]

    operations = [
        migrations.AlterField(
            model_name='email',
            name='city',
            field=models.ForeignKey(default=None, to='sender.City', null=True),
        ),
        migrations.AlterField(
            model_name='email',
            name='continent',
            field=models.ForeignKey(default=None, to='sender.Continent', null=True),
        ),
        migrations.AlterField(
            model_name='email',
            name='country',
            field=models.ForeignKey(default=None, to='sender.Country', null=True),
        ),
        migrations.AlterField(
            model_name='email',
            name='location',
            field=models.ForeignKey(db_column=b'location', default=None, to='sender.Location', null=True),
        ),
        migrations.AlterField(
            model_name='email',
            name='region',
            field=models.ForeignKey(default=None, to='sender.Region', null=True),
        ),
        migrations.AlterField(
            model_name='email',
            name='street',
            field=models.ForeignKey(default=None, to='sender.Street', null=True),
        ),
        migrations.AlterField(
            model_name='region',
            name='parent',
            field=models.ForeignKey(default=None, to='sender.Region', null=True),
        ),
    ]
