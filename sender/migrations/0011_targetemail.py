# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0010_auto_20151002_1655'),
    ]

    operations = [
        migrations.CreateModel(
            name='TargetEmail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('email', models.ForeignKey(to='sender.Email')),
                ('target', models.ForeignKey(to='sender.Target')),
            ],
        ),
    ]
