# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0015_auto_20151011_1028'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='email',
            index_together=set([('email',), ('is_valid',), ('birthday', 'is_valid')]),
        ),
    ]
