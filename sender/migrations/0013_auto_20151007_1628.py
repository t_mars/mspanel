# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0012_targetemail_last_parcel'),
    ]

    operations = [
        migrations.AlterField(
            model_name='targetemail',
            name='last_parcel',
            field=models.ForeignKey(default=None, to='main.Parcel', null=True),
        ),
    ]
