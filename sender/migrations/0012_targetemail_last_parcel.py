# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0123_auto_20151007_1519'),
        ('sender', '0011_targetemail'),
    ]

    operations = [
        migrations.AddField(
            model_name='targetemail',
            name='last_parcel',
            field=models.ForeignKey(default=None, to='main.Parcel'),
        ),
    ]
