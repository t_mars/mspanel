# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('city_name', models.CharField(default=None, max_length=64, null=True)),
                ('is_deleted', models.IntegerField(default=0, null=True)),
                ('delta', models.IntegerField(default=None, null=True)),
                ('is_moderate', models.IntegerField(default=0, null=True)),
                ('codename', models.CharField(default=None, max_length=128, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Continent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('continent_name', models.CharField(default=None, max_length=64, null=True)),
                ('is_deleted', models.IntegerField(default=0, null=True)),
                ('delta', models.IntegerField(default=None, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('country_name', models.CharField(default=None, max_length=64, null=True)),
                ('is_deleted', models.IntegerField(default=0, null=True)),
                ('delta', models.IntegerField(default=None, null=True)),
                ('is_moderate', models.IntegerField(default=0, null=True)),
                ('codename', models.CharField(default=None, max_length=128, null=True)),
                ('continent', models.ForeignKey(to='sender.Continent', db_column=b'continent')),
            ],
        ),
        migrations.CreateModel(
            name='Email',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.CharField(default=None, max_length=50, null=True)),
                ('name', models.CharField(max_length=50)),
                ('surname', models.CharField(max_length=50)),
                ('sex', models.IntegerField(default=0)),
                ('birthday', models.DateTimeField(default=None, null=True)),
                ('isactive', models.IntegerField(default=1)),
                ('is_validate', models.IntegerField(default=0, null=True)),
                ('city', models.ForeignKey(default=0, to='sender.City', null=True)),
                ('continent', models.ForeignKey(default=0, to='sender.Continent', null=True)),
                ('country', models.ForeignKey(default=0, to='sender.Country', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('location_id', models.AutoField(verbose_name=b'ID', serialize=False, auto_created=True, primary_key=True)),
                ('location', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('region_name', models.CharField(max_length=64)),
                ('is_city', models.IntegerField(default=0, null=True)),
                ('is_deleted', models.IntegerField(default=0, null=True)),
                ('delta', models.IntegerField(default=None, null=True)),
                ('is_moderate', models.IntegerField(default=0, null=True)),
                ('codename', models.CharField(default=None, max_length=128, null=True)),
                ('country', models.ForeignKey(to='sender.Country', db_column=b'country')),
                ('parent', models.ForeignKey(default=0, to='sender.Region', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Street',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('city', models.ForeignKey(to='sender.City')),
            ],
        ),
        migrations.CreateModel(
            name='StreetType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32)),
            ],
        ),
        migrations.AddField(
            model_name='street',
            name='street_type',
            field=models.ForeignKey(to='sender.StreetType', db_column=b'street_type_id'),
        ),
        migrations.AddField(
            model_name='email',
            name='location',
            field=models.ForeignKey(db_column=b'location', default=0, to='sender.Location', null=True),
        ),
        migrations.AddField(
            model_name='email',
            name='region',
            field=models.ForeignKey(default=0, to='sender.Region', null=True),
        ),
        migrations.AddField(
            model_name='email',
            name='street',
            field=models.ForeignKey(default=0, to='sender.Street', null=True),
        ),
        migrations.AddField(
            model_name='city',
            name='country',
            field=models.ForeignKey(to='sender.Country', db_column=b'country'),
        ),
        migrations.AddField(
            model_name='city',
            name='region',
            field=models.ForeignKey(to='sender.Region', db_column=b'region'),
        ),
    ]
