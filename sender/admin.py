#coding=utf8
from django.contrib import admin
from django import forms
from django.conf import settings
from django.db.models import Count

from sender.models import *
from main.models import *

class TargetForm(forms.ModelForm):
    sex = forms.CharField(widget=forms.Textarea(attrs={'style':'display:none'}), required=False)
    geo = forms.CharField(widget=forms.Textarea(attrs={'style':'display:none'}), required=False)
    source = forms.CharField(widget=forms.Textarea(attrs={'style':'display:none'}), required=False)
    services = forms.CharField(widget=forms.Textarea(attrs={'style':'display:none'}), required=False)
    
    def __init__(self, *args, **kwargs):
        super(TargetForm, self).__init__(*args, **kwargs)
        
        instance = kwargs.get('instance')

        # sex
        if instance:
            sex = kwargs['instance'].sex.split(',')
        else:
            sex = ''
        options = ''.join(
            ['<option value="%s" %s>%s</option>' % (k, 'selected="selected"' if str(k) in sex else '', v) \
                for k,v in EMAIL_SEX_CHOICE.items()]
        )
        self.fields['sex'].help_text = '<style>.select2-container {width: 100%% !important;}</style>' \
            '<select id="sex" width="100%%" multiple="multiple">%s</select>' % options
        
        # geo
        if instance:
            geos = instance.get_geo()
        else:
            geos = []
        options = ''.join(
            ['<option value="%s" selected="selected">%s</option>' % (g.get_id(), g.get_name()) \
                for g in geos]
        )
        self.fields['geo'].help_text = \
            '<select id="geo" width="100%%" multiple="multiple">%s</select>' % options
    
        # source
        if instance:
            sources = instance.get_source()
        else:
            sources = []
        options = ''.join(
            ['<option value="%s" %s>%s</option>' % (k, 'selected="selected"' if k in sources else '', v) \
                for k,v in EMAIL_SOURCE_CHOICE.items()]
        )
        self.fields['source'].help_text = \
            '<select id="source" width="100%%" multiple="multiple">%s</select><span>пустое значение-все базы</span>' % options

        # services
        ss = {int(s.id):s.name for s in EmailService.objects.filter(in_select=True)}
        ss[0] = u'Остальные'
        if instance:
            services = instance.get_services()
        else:
            services = []
        options = ''.join(
            ['<option value="%s" %s>%s</option>' % (k, 'selected="selected"' if k in services else '', v) \
                for k,v in ss.items()]
        )
        self.fields['services'].help_text = \
            u'<select id="services" width="100%%" multiple="multiple">%s</select><span>пустое значение-все сервисы</span>' % options

class TargetAdmin(admin.ModelAdmin):
    form = TargetForm
    list_display = ['__str__', '_count']
    class Media:
        js = [
            '%sadmin/jquery.js' % settings.STATIC_URL,  
            '%sselect2/js/select2.min.js' % settings.STATIC_URL,
        ]
        css = {
            'all': ('%sselect2/css/select2.min.css' % settings.STATIC_URL,)
        }

    def _count(self, obj):
        return obj.get_stat() 
    _count.allow_tags = True
admin.site.register(Target, TargetAdmin)

class ContinentAdmin(admin.ModelAdmin):
    list_display = ('name',)
admin.site.register(Continent, ContinentAdmin)

class CountryAdmin(admin.ModelAdmin):
    list_display = ('name','continent')
    list_filter = ('continent',)
admin.site.register(Country, CountryAdmin)

class RegionAdmin(admin.ModelAdmin):
    list_display = ('name','country','parent')
    list_filter = ('country',)
admin.site.register(Region, RegionAdmin)

class LocationAdmin(admin.ModelAdmin):
    list_display = ('name',)
admin.site.register(Location, LocationAdmin)

class CityAdmin(admin.ModelAdmin):
    list_display = ('name','country','region')
    list_filter = ('country',)
admin.site.register(City, CityAdmin)

class StreetTypeAdmin(admin.ModelAdmin):
    list_display = ('name',)
admin.site.register(StreetType, StreetTypeAdmin)

class StreetAdmin(admin.ModelAdmin):
    list_display = ('name',)
admin.site.register(Street, StreetAdmin)

class EmailServiceAdmin(admin.ModelAdmin):
    list_display = ('name', '_count', 'in_select')
    search_fields =('name', )

    def get_queryset(self, request):
        return EmailService.objects \
            .annotate(count=Count('email')) \
            .order_by('-count')
    def _count(self, obj):
        return '<a href="/admin/sender/email?service_id__exact=%d">%s</a>' % (obj.id, '{:,}'.format(obj.count)) 
    _count.allow_tags = True
    _count.short_description = u'Количество почт'
admin.site.register(EmailService, EmailServiceAdmin)

class EmailAdmin(admin.ModelAdmin):
    list_display = ('email','id', 'source', 'source_id', 'sex', 'is_valid')
    list_filter =('is_valid', )
    search_fields =('email', )
admin.site.register(Email, EmailAdmin)

class TargetEmailAdmin(admin.ModelAdmin):
    list_display = ('target','email','created_at', 'last_parcel')
admin.site.register(TargetEmail, TargetEmailAdmin)

