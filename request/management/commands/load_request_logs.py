#coding=utf8
import os
from optparse import make_option

from django.core.management.base import BaseCommand

from main.models import VirtualServer, Mailing, Domain, Task
from request.models import *

class Command(BaseCommand):
    
    option_list = BaseCommand.option_list + (
        make_option('--task',
            dest='task_id',
            help=u'Номер таска для запуска',
            metavar="INT"
        ),
    )

    def handle(self, *args, **options):
        self.task = Task.objects.get(pk=options['task_id'])

        vs = VirtualServer.objects.all()
        total = vs.count()
        current = 0
        count = 0

        for s in vs:
            current += 1
            
            print 'get logs from server [%s]' % s.name

            sshpass_cmd = "sshpass -p '%s'" % s.password
            ssh_cmd = 'ssh %s@%s -o StrictHostKeychecking=no' % (s.login, s.ip)

            cmd = "%s scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -r %s@%s:%s/link_encoder/requests.log requests.log" % \
                (sshpass_cmd, s.login, s.ip, s.document_root)
            print 'Копируем логи'
            print cmd
            print '---'
            os.popen(cmd)

            cmd = "%s %s rm %s/link_encoder/requests.log" % \
                (sshpass_cmd, ssh_cmd, s.document_root)
            print 'Удаляем логи'
            print cmd
            print '---'
            os.popen(cmd)

            print 'Читаем логи'
            with open('requests.log') as f:
                for line in f:
                    timestamp, ip, useragent, referer, dname, path, mode, is_blocked = line[:-1].split('\t')
                    
                    created_at = get_datetime(timestamp)

                    session = Session.GET(ip, useragent, created_at)

                    log = Log()
                    log.created_at = created_at
                    log.session = session
                    log.referer = referer
                    log.is_blocked = True if is_blocked == 'yes' else False
                    log.path = dname + path

                    dname = '.'.join(dname.split('.')[-2:])
                    try:
                        log.domain = Domain.objects.get(name=dname)
                    except:
                        log.domain = None

                    if mode == 'fail_code':
                        log.mode = 'fail_code'
                    elif mode == 'home':
                        log.mode = 'home'

                    elif mode.startswith('link:'):
                        log.mode = 'link'
                        log.mailing_id = mode.split(':')[1]
                    elif mode.startswith('link_error'):
                        log.mode = 'link_error'

                    elif mode.startswith('image:'):
                        log.mode = 'image'
                        log.mailing_id = mode.split(':')[1]
                    elif mode.startswith('image_error'):
                        log.mode = 'image_error'

                    log.save()
                    count += 1
                    self.task.save_status_string({
                        'ind': '%d/%d' % (current, total),
                        'count': count
                    })

            os.remove('requests.log')