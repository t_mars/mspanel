#coding=utf8
import json

from django.shortcuts import render
from django.db import connection
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from django.http import HttpResponse, HttpResponseRedirect
from request.models import *

def stat(request):
    cursor = connection.cursor()
    
    ss = False
    if 'ip_state' in request.GET:
        ip = request.GET['ip_state']
        comment = request.GET['ip_comment']
        
        if 'approve' in request.GET:
            IPState.SET(ip, IPState.APPROVED, comment)
        elif 'block' in request.GET:
            IPState.SET(ip, IPState.BLOCKED, comment)
        elif 'hide' in request.GET:
            IPState.SET(ip, IPState.HIDDEN, comment)
        elif 'none' in request.GET:
            IPState.SET(ip, IPState.NONE, comment)
        
        return HttpResponseRedirect(request.META.get('HTTP_REFERER','/'))

    if 'ip' in request.GET:
        ip = request.GET['ip']
        i = ip.count('.')+1
        ip_like = ip
        back = '.'.join(ip.split('.')[:i-2])
        if back:
            back = '?ip=%s.' % back
        else:
            back = '?'
    else:
        i = 1
        ip_like = ''
        back = None

    q = """
        SELECT SUBSTRING_INDEX(s.ip, '.', %d), COUNT(*)
        FROM request_session as s
        WHERE s.ip LIKE '%s%%'
        GROUP BY SUBSTRING_INDEX(s.ip, '.', %d)
    """ % (i, ip_like, i)
    cursor.execute(q, None)
    rows = cursor.fetchall()
    res = {}
    for ip, count in rows:
        try:
            o = int(ip.split('.')[-1])
        except:
            continue

        state, comment, parent_comment = IPState.GET(ip, comment=True)
        res[ip] = {
            'part': o,
            'ip': ('%s.*' % ip) if ip.count('.') < 3 else ip,
            'session_count': count,
            'link': ('%s.' % ip) if ip.count('.') < 3 else '',
            'ip_link': ('%s.' % ip) if ip.count('.') < 3 else ip,
            'state': state,
            'comment': comment,
            'parent_comment': parent_comment,
            'log_counts': {},
        }
    
    # кол-во запросов
    q = """
        SELECT SUBSTRING_INDEX(s.ip, '.', %d), COUNT(*)
        FROM request_log as s
        WHERE s.ip LIKE '%s%%'
        GROUP BY SUBSTRING_INDEX(s.ip, '.', %d)
    """ % (i, ip_like, i)
    cursor.execute(q, None)
    rows = cursor.fetchall()
    for ip, count in rows:
        if ip not in res:
            continue
        res[ip]['log_count'] = count
    
    # кол-во запросов по типам
    q = """
        SELECT SUBSTRING_INDEX(s.ip, '.', %d), s.ip_state, COUNT(*)
        FROM request_log as s
        WHERE s.ip LIKE '%s%%'
        GROUP BY SUBSTRING_INDEX(s.ip, '.', %d), s.ip_state
    """ % (i, ip_like, i)
    cursor.execute(q, None)
    rows = cursor.fetchall()
    for ip, state, count in rows:
        if ip not in res:
            continue
        res[ip]['log_counts'][state] = count

    # сортируем по ip
    res = sorted(res.items(), key=lambda r: ip_str2int(r[0]))
    
    return render(request, 'request/session_stat.html', {
        'res': [r[1] for r in res],
        'back': back,
    })

@csrf_exempt
def log(request):
    created_at = timezone.now()

    res = {}
    try:
        ip = request.POST['ip']
        dname = request.POST['domain']
        referer = request.POST['referer']
        mode = request.POST['mode']
        useragent = request.POST['useragent']
        path = request.POST['path']
    except Exception as e:
        res['is_blocked'] = False
        res['exception'] = str(e)
        return HttpResponse(json.dumps(res))
    

    session = Session.GET(ip, useragent, created_at)
    
    log = Log()
    log.ip = session.ip
    log.created_at = created_at
    log.session = session
    log.referer = referer
    log.path = dname + path
    log.is_blocked = False

    dname = '.'.join(dname.split('.')[-2:])
    try:
        log.domain = Domain.objects.get(name=dname)
    except:
        log.domain = None

    if mode == 'fail_code':
        log.mode = 'fail_code'
    elif mode == 'home':
        log.mode = 'home'

    elif mode.startswith('link:'):
        log.mode = 'link'
        log.mailing_id = mode.split(':')[1]
    elif mode.startswith('link_error'):
        log.mode = 'link_error'

    elif mode.startswith('image:'):
        log.mode = 'image'
        log.mailing_id = mode.split(':')[1]
    elif mode.startswith('image_error'):
        log.mode = 'image_error'

    # проверка блокировки по IP
    log.ip_state = IPState.GET(session.ip)
    if log.ip_state == IPState.APPROVED:
        # разрешенный IP
        log.is_blocked = False
    elif log.ip_state == IPState.BLOCKED:
        # Блокированный IP
        log.is_blocked = True
    else:
        # Скрытый или нейтральный IP
        
        # проверка привелегии
        tbl = Privilege.objects.model._meta.db_table
        q = """
            SELECT id
            FROM %s
            WHERE is_active=1 
                AND ip1_int <= %d 
                AND ip2_int >= %d
            """ % (tbl, session.ip_int, session.ip_int)
        
        cursor = connection.cursor()
        cursor.execute(q)
        row = cursor.fetchone()
        if row:
            log.privilege_id = row[0]

        # проверка блокировки по фильтрам
        # картинки не блочим!
        if log.privilege_id == None and log.mode != 'image':
            tbl = Locking.objects.model._meta.db_table

            conds = []

            conds.append(
                """(ip_check=1 AND ip1_int <= %d AND ip2_int >= %d) 
                OR ip_check=0""" % (session.ip_int, session.ip_int))
            conds.append(
                """(device_type_check=1 AND device_type = "%s") 
                OR device_type_check=0""" % (session.device_type))
            conds.append(
                """(geo_country_check=1 AND geo_country LIKE "%%|%s|%%") 
                OR (geo_country_check=0 AND geo_country NOT LIKE "%%|%s|%%")
                OR (geo_country_check IS NULL)""" % (session.geo_country, session.geo_country))
            
            q = """
                SELECT id
                FROM %s
                WHERE is_active=1 AND 
                (%s)
            """ % (tbl, ')\n AND ('.join(conds))
            # res['query'] = q
            
            try:
                cursor = connection.cursor()
                cursor.execute(q)
                row = cursor.fetchone()
                if row:
                    log.is_blocked = True
                    log.locking_id = row[0]
            except Exception as e:
                res['exception'] = str(e)

    log.save()
    
    if ip == '194.15.116.49' and log.mode != 'image':
        res['is_debug'] = True
        res['mmode'] = log.mode

    res['is_blocked'] = log.is_blocked
    res['is_privilege'] = log.privilege_id is not None
    
    return HttpResponse(json.dumps(res))