# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0026_log_ip'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='log',
            index_together=set([('session',), ('ip',)]),
        ),
    ]
