# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0011_auto_20150916_1247'),
    ]

    operations = [
        migrations.AlterField(
            model_name='iplocking',
            name='ip1',
            field=models.BigIntegerField(),
        ),
        migrations.AlterField(
            model_name='iplocking',
            name='ip2',
            field=models.BigIntegerField(),
        ),
    ]
