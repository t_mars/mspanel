# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0029_ipstate_comment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ipstate',
            name='comment',
            field=models.CharField(default=b'', max_length=255),
        ),
    ]
