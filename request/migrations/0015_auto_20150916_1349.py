# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0014_auto_20150916_1347'),
    ]

    operations = [
        migrations.AddField(
            model_name='iplocking',
            name='ip1',
            field=models.CharField(default='', max_length=15),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='iplocking',
            name='ip2',
            field=models.CharField(default='', max_length=15),
            preserve_default=False,
        ),
    ]
