# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0020_auto_20150916_1623'),
    ]

    operations = [
        migrations.AlterField(
            model_name='locking',
            name='geo_country_check',
            field=models.NullBooleanField(),
        ),
    ]
