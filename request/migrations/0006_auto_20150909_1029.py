# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0005_auto_20150909_0850'),
    ]

    operations = [
        migrations.AddField(
            model_name='session',
            name='browser_family',
            field=models.CharField(default=None, max_length=128, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='session',
            name='browser_version',
            field=models.CharField(default=None, max_length=128, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='session',
            name='device_brand',
            field=models.CharField(default=None, max_length=128, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='session',
            name='device_family',
            field=models.CharField(default=None, max_length=128, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='session',
            name='device_model',
            field=models.CharField(default=None, max_length=128, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='session',
            name='device_type',
            field=models.CharField(default=None, max_length=20, null=True, blank=True, choices=[(b'mobile', b'Mobile'), (b'pc', b'PC'), (b'bot', b'Bot'), (b'tablet', b'Tablet')]),
        ),
        migrations.AddField(
            model_name='session',
            name='os_family',
            field=models.CharField(default=None, max_length=128, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='session',
            name='os_version',
            field=models.CharField(default=None, max_length=128, null=True, blank=True),
        ),
    ]
