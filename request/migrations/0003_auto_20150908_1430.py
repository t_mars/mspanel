# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0002_auto_20150908_1429'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='session',
            index_together=set([('uid',), ('ip',)]),
        ),
    ]
