# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0021_auto_20150916_1730'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='log',
            index_together=set([('session',)]),
        ),
    ]
