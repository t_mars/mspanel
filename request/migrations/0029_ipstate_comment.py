# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0028_auto_20151103_1601'),
    ]

    operations = [
        migrations.AddField(
            model_name='ipstate',
            name='comment',
            field=models.CharField(default=None, max_length=255, null=True, blank=True),
        ),
    ]
