# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0009_auto_20150910_0156'),
    ]

    operations = [
        migrations.AddField(
            model_name='session',
            name='whois_descr',
            field=models.CharField(default=None, max_length=128, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='session',
            name='whois_flag',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='session',
            name='whois_inetnum',
            field=models.CharField(default=None, max_length=128, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='session',
            name='whois_netname',
            field=models.CharField(default=None, max_length=128, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='session',
            name='whois_netrange',
            field=models.CharField(default=None, max_length=128, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='session',
            name='whois_org',
            field=models.CharField(default=None, max_length=128, null=True, blank=True),
        ),
    ]
