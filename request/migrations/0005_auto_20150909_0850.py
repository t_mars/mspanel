# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0004_auto_20150908_1453'),
    ]

    operations = [
        migrations.AddField(
            model_name='session',
            name='geo_continent',
            field=models.CharField(default=None, max_length=128, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='session',
            name='geo_country',
            field=models.CharField(default=None, max_length=128, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='session',
            name='geo_latitude',
            field=models.DecimalField(default=None, null=True, max_digits=6, decimal_places=2, blank=True),
        ),
        migrations.AddField(
            model_name='session',
            name='geo_longitude',
            field=models.DecimalField(default=None, null=True, max_digits=6, decimal_places=2, blank=True),
        ),
        migrations.AddField(
            model_name='session',
            name='geo_timezone',
            field=models.CharField(default=None, max_length=128, null=True, blank=True),
        ),
    ]
