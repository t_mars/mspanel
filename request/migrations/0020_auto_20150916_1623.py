# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0019_auto_20150916_1621'),
    ]

    operations = [
        migrations.AddField(
            model_name='log',
            name='privilege',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, default=None, blank=True, to='request.Privilege', null=True),
        ),
        migrations.AlterField(
            model_name='log',
            name='locking',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, default=None, blank=True, to='request.Locking', null=True),
        ),
    ]
