# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0027_auto_20151103_1537'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='log',
            index_together=set([('ip', 'ip_state'), ('session',), ('ip',)]),
        ),
    ]
