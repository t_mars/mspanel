# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0023_ipstate'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ipstate',
            name='ip',
            field=models.CharField(unique=True, max_length=15),
        ),
        migrations.AlterIndexTogether(
            name='ipstate',
            index_together=set([('ip',)]),
        ),
    ]
