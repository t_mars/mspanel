# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0012_auto_20150916_1258'),
    ]

    operations = [
        migrations.AddField(
            model_name='session',
            name='ip_int',
            field=models.BigIntegerField(default=True, null=True, blank=True),
        ),
    ]
