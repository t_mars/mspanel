# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0006_auto_20150909_1029'),
    ]

    operations = [
        migrations.CreateModel(
            name='Locking',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('ip', models.CharField(max_length=15)),
            ],
        ),
    ]
