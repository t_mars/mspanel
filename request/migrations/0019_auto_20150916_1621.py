# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0018_privilege'),
    ]

    operations = [
        migrations.AddField(
            model_name='privilege',
            name='comment',
            field=models.CharField(default='', max_length=1024),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='privilege',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 16, 13, 21, 6, 225544, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='privilege',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
    ]
