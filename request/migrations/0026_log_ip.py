# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0025_auto_20151103_1231'),
    ]

    operations = [
        migrations.AddField(
            model_name='log',
            name='ip',
            field=models.CharField(default='', max_length=15),
            preserve_default=False,
        ),
    ]
