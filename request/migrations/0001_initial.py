# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0102_mailruaccount_has_my'),
    ]

    operations = [
        migrations.CreateModel(
            name='Log',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('referer', models.CharField(max_length=1024)),
                ('path', models.CharField(max_length=1024)),
                ('is_blocked', models.BooleanField()),
                ('created_at', models.DateTimeField()),
                ('mode', models.CharField(max_length=10, choices=[(b'fail_code', b'Fail code'), (b'image', b'Image'), (b'link', b'Link'), (b'home', b'Home')])),
                ('domain', models.ForeignKey(to='main.Domain')),
                ('mailing', models.ForeignKey(blank=True, to='main.Mailing', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Session',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField()),
                ('user_agent', models.CharField(max_length=1024)),
                ('ip', models.CharField(max_length=15)),
            ],
        ),
        migrations.AlterIndexTogether(
            name='session',
            index_together=set([('ip', 'user_agent'), ('user_agent',), ('ip',)]),
        ),
        migrations.AddField(
            model_name='log',
            name='session',
            field=models.ForeignKey(to='request.Session'),
        ),
    ]
