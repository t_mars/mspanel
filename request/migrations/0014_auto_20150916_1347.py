# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0013_session_ip_int'),
    ]

    operations = [
        migrations.RenameField(
            model_name='iplocking',
            old_name='ip1',
            new_name='ip1_int',
        ),
        migrations.RenameField(
            model_name='iplocking',
            old_name='ip2',
            new_name='ip2_int',
        ),
    ]
