# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0008_auto_20150910_0119'),
    ]

    operations = [
        migrations.AlterField(
            model_name='locking',
            name='ip',
            field=models.CharField(unique=True, max_length=15),
        ),
    ]
