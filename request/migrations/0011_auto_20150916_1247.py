# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0010_auto_20150910_1630'),
    ]

    operations = [
        migrations.CreateModel(
            name='IpLocking',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('ip1', models.IntegerField()),
                ('ip2', models.IntegerField()),
                ('is_active', models.BooleanField(default=True)),
                ('comment', models.CharField(max_length=1024)),
            ],
        ),
        migrations.DeleteModel(
            name='Locking',
        ),
    ]
