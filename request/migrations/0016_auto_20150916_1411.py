# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0015_auto_20150916_1349'),
    ]

    operations = [
        migrations.AddField(
            model_name='log',
            name='block_id',
            field=models.IntegerField(default=None, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='log',
            name='block_reason',
            field=models.CharField(default=None, max_length=10, null=True, blank=True, choices=[(b'info', b'Info'), (b'ip', b'IP')]),
        ),
    ]
