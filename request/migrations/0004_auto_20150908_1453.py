# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0003_auto_20150908_1430'),
    ]

    operations = [
        migrations.AlterField(
            model_name='log',
            name='domain',
            field=models.ForeignKey(blank=True, to='main.Domain', null=True),
        ),
        migrations.AlterField(
            model_name='log',
            name='mode',
            field=models.CharField(max_length=10, choices=[(b'image', b'Image'), (b'fail_code', b'Fail code'), (b'link', b'Link'), (b'link_error', b'Link Error'), (b'home', b'Home'), (b'image_error', b'Image Error')]),
        ),
    ]
