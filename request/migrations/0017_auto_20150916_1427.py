# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0016_auto_20150916_1411'),
    ]

    operations = [
        migrations.CreateModel(
            name='Locking',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('comment', models.CharField(max_length=1024)),
                ('ip_check', models.BooleanField()),
                ('ip1', models.CharField(default=None, max_length=15, null=True, blank=True)),
                ('ip2', models.CharField(default=None, max_length=15, null=True, blank=True)),
                ('ip1_int', models.BigIntegerField(default=None, null=True, blank=True)),
                ('ip2_int', models.BigIntegerField(default=None, null=True, blank=True)),
                ('device_type_check', models.BooleanField()),
                ('device_type', models.CharField(default=None, max_length=20, null=True, blank=True, choices=[(b'mobile', b'Mobile'), (b'pc', b'PC'), (b'bot', b'Bot'), (b'tablet', b'Tablet')])),
                ('geo_country_check', models.BooleanField()),
                ('geo_country', models.CharField(default=None, max_length=128, null=True, blank=True)),
            ],
        ),
        migrations.DeleteModel(
            name='IpLocking',
        ),
        migrations.RemoveField(
            model_name='log',
            name='block_id',
        ),
        migrations.RemoveField(
            model_name='log',
            name='block_reason',
        ),
        migrations.AddField(
            model_name='log',
            name='locking',
            field=models.ForeignKey(default=None, blank=True, to='request.Locking', null=True),
        ),
    ]
