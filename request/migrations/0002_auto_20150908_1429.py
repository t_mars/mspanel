# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='session',
            name='uid',
            field=models.CharField(default='', max_length=256),
            preserve_default=False,
        ),
        migrations.AlterIndexTogether(
            name='session',
            index_together=set([('uid',), ('user_agent',), ('ip',)]),
        ),
    ]
