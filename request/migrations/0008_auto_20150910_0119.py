# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0007_locking'),
    ]

    operations = [
        migrations.AddField(
            model_name='locking',
            name='comment',
            field=models.CharField(default='', max_length=1024),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='locking',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
    ]
