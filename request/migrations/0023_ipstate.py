# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0022_auto_20151102_1334'),
    ]

    operations = [
        migrations.CreateModel(
            name='IPState',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ip', models.CharField(max_length=15)),
                ('state', models.PositiveSmallIntegerField(choices=[(0, b'\xd0\x9e\xd0\xb4\xd0\xbe\xd0\xb1\xd1\x80\xd0\xb5\xd0\xbd\xd0\xbe'), (1, b'\xd0\x97\xd0\xb0\xd0\xb1\xd0\xbb\xd0\xbe\xd0\xba\xd0\xb8\xd1\x80\xd0\xbe\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xbe'), (2, b'\xd0\xa1\xd0\xba\xd1\x80\xd1\x8b\xd1\x82\xd0\xbe')])),
            ],
        ),
    ]
