#coding=utf8
import operator

from django.contrib import admin
from django.db.models import Count, Q
from django import forms
from django.contrib.admin import SimpleListFilter

from daterange_filter.filter import DateTimeRangeFilter

from request.models import *

class SessionLockingSetFilter(SimpleListFilter):
    title = 'Блокировка'
    prefix = ''
    parameter_name = 'locking_set'

    def lookups(self, request, model_admin):
        return (
            ('in', 'Заблокированные'),
            ('out', 'НЕ заблокированные'),
        )

    def queryset(self, request, queryset):
        conds = []
        qs = Locking.objects.filter(is_active=True)
        for l in qs:
            cond = []
            
            if l.ip_check:
                cond.append(Q(**{self.prefix + 'ip_int__lte': l.ip2_int}))
                cond.append(Q(**{self.prefix + 'ip_int__gte': l.ip1_int}))
            
            if l.device_type_check:
                cond.append(Q(**{self.prefix + 'device_type': l.device_type}))
            
            if l.geo_country_check == True:
                cond.append(Q(**{self.prefix + 'geo_country__in': l.geo_country_list}))
            elif l.geo_country_check == False:
                cond.append(~Q(**{self.prefix + 'geo_country__in': l.geo_country_list}))

            conds.append(reduce(operator.and_, cond))

        if not conds:
            return queryset
    
        if self.value() == 'in':
            return queryset.filter(reduce(operator.or_, conds))
        if self.value() == 'out':
            return queryset.exclude(reduce(operator.or_, conds))

class LogLockingSetFilter(SessionLockingSetFilter):
    prefix = 'session__'

class SessionPrivilegeSetFilter(SimpleListFilter):
    title = 'Привилегия'
    prefix = ''
    parameter_name = 'privilege_set'

    def lookups(self, request, model_admin):
        return (
            ('in', 'В белом списке'),
            ('out', 'НЕ в белом списке'),
        )

    def queryset(self, request, queryset):
        conds = []
        qs = Privilege.objects.filter(is_active=True)
        for l in qs:
            cond = []
            
            cond.append(Q(**{self.prefix + 'ip_int__lte': l.ip2_int}))
            cond.append(Q(**{self.prefix + 'ip_int__gte': l.ip1_int}))
        
            conds.append(reduce(operator.and_, cond))

        if not conds:
            return queryset

        if self.value() == 'in':
            return queryset.filter(reduce(operator.or_, conds))
        if self.value() == 'out':
            return queryset.exclude(reduce(operator.or_, conds))

class LogPrivilegeSetFilter(SessionPrivilegeSetFilter):
    prefix = 'session__'

session_search_fields = ['user_agent', 'ip', 
    'geo_country', 'geo_continent', 'geo_timezone',
    'device_type', 'device_family', 'device_brand', 'device_model',
    'os_family', 'os_version',
    'browser_family', 'browser_version',
    'whois_descr', 'whois_netname', 'whois_org', 'whois_inetnum', 'whois_netrange']

class SessionShowNewIpFilter(SimpleListFilter):
    title = 'Новые IP?'
    prefix = ''
    parameter_name = 'show_new_ip'

    def lookups(self, request, model_admin):
        return (
            ('1', 'Только новые'),
            ('0', 'Новые и старые'),
        )

    def queryset(self, request, queryset):
        if self.value() is None or self.value() == '1':
            self.used_parameters[self.parameter_name] = '1'
            
            # выбираем IP которые не соответствуют новым
            ips = IPState.objects.exclude(state=IPState.NONE).values_list('ip', flat=True)
            conds = []
            for ip in ips:
                conds.append(Q(ip__startswith=ip+'.' if ip.count('.') < 3 else ip))
            if conds:
                return queryset.exclude(reduce(operator.or_, conds))
        return queryset

class SessionAdmin(admin.ModelAdmin):
    list_display = ['id', 'ip', 'created_at', 'user_agent', '_geo', 'device_type', '_whois', '_request_count']
    list_filter = [
        SessionShowNewIpFilter,
        ('created_at', DateTimeRangeFilter),
        SessionLockingSetFilter,
        SessionPrivilegeSetFilter,
        'geo_country', 'geo_continent', 'geo_timezone', 
        'device_family', 'device_brand', 'device_model', 'device_type',
        'os_family', 'os_version', 'browser_family']
    actions = ['lock', 'unlock']
    exclude = ['ip_int']
    search_fields = session_search_fields
    
    # def get_queryset(self, request):
    #     return Session.objects \
    #         .annotate(request_count=Count('log', distinct=True)) \
    #         .order_by('-created_at')

    def _request_count(self, obj):
        request_count = Log.objects.filter(session=obj).count()
        return '<a href="/admin/request/log/?session_id__exact=%d">%d</a>' % (obj.id, request_count)
    _request_count.allow_tags = True
    _request_count.admin_order_field = 'request_count'

    def _geo(self, obj):
        if not obj.geo_country:
            return '-'    
        res = ''
        res += '%s: %s<br>' % ('country', obj.geo_country or '-')
        res += '%s: %s<br>' % ('continent', obj.geo_continent or '-')
        res += '%s: %s<br>' % ('timezone', obj.geo_timezone or '-')
        return res
    _geo.allow_tags = True
    
    def _whois(self, obj):
        if not obj.whois_flag:
            return '-'
        res = ''
        res += '%s: %s<br>' % ('descr', obj.whois_descr or '-')
        res += '%s: %s<br>' % ('netname', obj.whois_netname or '-')
        res += '%s: %s<br>' % ('org', obj.whois_org or '-')

        comment = obj.whois_descr or obj.whois_netname or obj.whois_org
        s = '%s: %s <a href="/admin/request/locking/add/?ip1=%s&ip2=%s&ip_check=1&comment=%s">[-]</a><br>|<a href="/admin/request/privilege/add/?ip1=%s&ip2=%s&comment=%s">[+]</a><br>'
        ips = get_ips(obj.whois_inetnum)
        if len(ips) == 2:
            res += s % ('inetnum', obj.whois_inetnum, ips[0], ips[1], comment,
                ips[0], ips[1], comment)
        else:
            res += '%s: %s<br>' % ('inetnum', obj.whois_inetnum or '-')
        
        ips = get_ips(obj.whois_netrange)
        if len(ips) == 2:
            res += s % ('netrange', obj.whois_netrange, ips[0], ips[1], comment,
                ips[0], ips[1], comment)
        else:
            res += '%s: %s<br>' % ('netrange', obj.whois_netrange or '-')
        
        return res
    _whois.allow_tags = True

    def lock(self, request, queryset):
        for s in queryset:
            l = Locking()
            l.ip = s.ip
            l.comment = 'session:%d' % s.id
            try:
                l.save()
            except:
                continue
    lock.short_description = u"Заблокировать"

    def unlock(self, request, queryset):
        for s in queryset:
            try:
                l = Locking.objects.get(ip=s.ip)
                l.is_active = False
                l.save()
            except:
                continue
    unlock.short_description = u"Разблокировать"


admin.site.register(Session, SessionAdmin)

class LogShowNewIpFilter(SimpleListFilter):
    title = 'Новые IP?'
    prefix = ''
    parameter_name = 'show_new_ip'

    def lookups(self, request, model_admin):
        return (
            ('1', 'Только новые'),
            ('0', 'Новые и старые'),
        )

    def queryset(self, request, queryset):
        # if (self.value() is None and 'ip_state' not in request.GET) or self.value() == '1':
        #     ips = IPState.objects.exclude(state=IPState.NONE).values_list('ip', flat=True)
        #     conds = []
        #     for ip in ips:
        #         conds.append(Q(ip__startswith=ip+'.' if ip.count('.') < 3 else ip))
        #     if conds:
        #         return queryset.exclude(reduce(operator.or_, conds))

        if self.value() is None and 'ip_state' not in request.GET:
            self.used_parameters[self.parameter_name] = '1'
            return queryset.filter(ip_state=IPState.NONE)
        
        if self.value() == '1':
            return queryset.filter(ip_state=IPState.NONE)

        return queryset

class LogAdmin(admin.ModelAdmin):
    
    list_display = ['id', '_domain', '_session', 'mode', '_mailing', '_path', 'created_at', 'is_blocked', 'ip', '_ip_state', '_locking', '_privilege']
    list_filter = [
        LogShowNewIpFilter,
        ('created_at', DateTimeRangeFilter), 
        LogLockingSetFilter,
        LogPrivilegeSetFilter,
        'mode', 'is_blocked']
    search_fields = ['referer', 'path', 'mode', 'domain__name', 'mailing__name'] + ['session__'+s  for s in session_search_fields]

    # def get_queryset(self, request):
    #     return Log.objects.all() \
    #         .select_related('session')
    #         # .select_related('session', 'locking', 'privilege', 'domain')

    def _ip_state(self, obj):
        if obj.ip_state == 0:
            return '<span class="label label-success">Одобрено</span>'
        elif obj.ip_state == 1:
            return '<span class="label label-danger">Заблокировано</span>'
        elif obj.ip_state == 2:
            return '<span class="label label-info">Скрыто</span>'
        elif obj.ip_state == 3:
            return '<span class="label label-default">Нейтральный</span>'
    _ip_state.allow_tags = True
        
    def _mailing(self, obj):
        return obj.mailing
    _mailing.allow_tags = True

    def _domain(self, obj):
        if not obj.domain:
            return '-'
        return '<a href="/admin/main/domain/?id__exact=%d">%s</a>' % (obj.domain.id, obj.domain.name)
    _domain.allow_tags = True

    def _session(self, obj):
        return '<a href="/admin/request/session/?id__exact=%d">%s</a>' % (obj.session.id, str(obj.session))
    _session.allow_tags = True

    def _path(self, obj):
        if len(obj.path) > 50:
            return '<a href="http://%s" title="%s">%s...</span>' % (obj.path, obj.path, obj.path[:50])
        return '<a href="http://%s">%s</a>' % (obj.path, obj.path)
    _path.allow_tags = True

    def _locking(self, obj):
        if not obj.locking:
            return '-'
        return '<a href="/admin/request/locking/%d">%s</a>' % (obj.locking.id, str(obj.locking))
    _locking.allow_tags = True

    def _privilege(self, obj):
        if not obj.privilege:
            return '-'
        return '<a href="/admin/request/privilege/%d">%s</a>' % (obj.privilege.id, str(obj.privilege))
    _privilege.allow_tags = True

admin.site.register(Log, LogAdmin)

class LockingForm(forms.ModelForm):
    geo_country = forms.CharField(widget=forms.HiddenInput(), required=False)
    geo_country_ = forms.MultipleChoiceField(
        choices=[('-','-')] + [(s['geo_country'],s['geo_country']) for s in Session.objects.values('geo_country').distinct()],
        required=False
    )

    geo_country_check = forms.NullBooleanField(widget=forms.HiddenInput())
    geo_country_check_ = forms.ChoiceField(
        choices=[(1, 'Не проверять'), (2, 'Только указанные'), (3, 'Все, кроме указанных')],
    )
    
    def __init__(self, *args, **kwargs):
        res = super(LockingForm, self).__init__(*args, **kwargs)
        if 'instance' in kwargs and kwargs['instance']:
            obj = kwargs['instance']
            
            self.fields['geo_country_'].initial = obj.geo_country_list
        
            if obj.geo_country_check == None:
                initial = 1
            elif obj.geo_country_check == True:
                initial = 2
            elif obj.geo_country_check == False:
                initial = 3
            self.fields['geo_country_check_'].initial = initial
        
        return res
    
    def clean(self, *args, **kwargs):
        data = super(LockingForm, self).clean(*args, **kwargs)
        if data.get('geo_country_'):
            data['geo_country'] = '|' + '|'.join(data['geo_country_']) + '|'
        
        if data.get('geo_country_check_') == '1':
            data['geo_country_check'] = None
        elif data.get('geo_country_check_') == '2':
            data['geo_country_check'] = True
        elif data.get('geo_country_check_') == '3':
            data['geo_country_check'] = False
        return data

class LockingAdmin(admin.ModelAdmin):
    form = LockingForm
    list_display = ['id', '__str__', 'comment', 'created_at', 'is_active', '_logs']
    list_filter = ['is_active']
    exclude = ['ip1_int', 'ip2_int']
    actions = ['activate', 'unactivate']
    search_fields = ['comment', 'ip1', 'ip2', 'device_type', 'geo_country']

    def activate(self, request, queryset):
        for l in queryset:
            l.is_active = True
            l.save()
    activate.short_description = u"Активировать"

    def unactivate(self, request, queryset):
        for l in queryset:
            l.is_active = False
            l.save()
    unactivate.short_description = u"ДЕ-активировать"

    def _logs(self, obj):
        qs = Log.objects.filter(locking_id=obj.id)
        return '<a href="/admin/request/log/?locking_id=%d">%d</a>' % \
            (obj.id, qs.count())
    _logs.allow_tags = True
admin.site.register(Locking, LockingAdmin)

class PrivilegeAdmin(admin.ModelAdmin):
    list_display = ['id', '__str__', 'comment', 'created_at', 'is_active', '_logs']
    list_filter = ['is_active']
    exclude = ['ip1_int', 'ip2_int']
    actions = ['activate', 'unactivate']
    search_fields = ['comment', 'ip1', 'ip2']

    def activate(self, request, queryset):
        for l in queryset:
            l.is_active = True
            l.save()
    activate.short_description = u"Активировать"

    def unactivate(self, request, queryset):
        for l in queryset:
            l.is_active = False
            l.save()
    unactivate.short_description = u"ДЕ-активировать"

    def _logs(self, obj):
        qs = Log.objects.filter(privilege_id=obj.id)
        return '<a href="/admin/request/log/?privilege_id=%d">%d</a>' % \
            (obj.id, qs.count())
    _logs.allow_tags = True
admin.site.register(Privilege, PrivilegeAdmin)

class IPStateAdmin(admin.ModelAdmin):
    list_display = ['ip', '_state', 'comment']

    def _state(self, obj):
        if obj.state == 0:
            return '<span class="label label-success">Одобрено</span>'
        elif obj.state == 1:
            return '<span class="label label-danger">Заблокировано</span>'
        elif obj.state == 2:
            return '<span class="label label-info">Скрыто</span>'
        elif obj.state == 3:
            return '<span class="label label-default">Нейтральный</span>'
    _state.allow_tags = True

admin.site.register(IPState, IPStateAdmin)