#coding=utf8
import datetime
import re
import subprocess
import socket
import struct

from geoip import geolite2
import user_agents

from django.db import models

from main.models import Domain, Mailing

def get_datetime(timestamp):
    return datetime.datetime.fromtimestamp(int(timestamp))

def ip_str2int(ip):
    try:
        return struct.unpack("!L", socket.inet_aton(ip))[0]
    except:
        pass

def ip_int2str(ip):
    try:
        return socket.inet_ntoa(struct.pack('!L', ip))
    except:
        pass

def get_ips(ip):
    try:
        return re.findall(r'[0-9]+(?:\.[0-9]+){3}', ip)
    except:
        return []

IP_STATE_CHOICE = {
    0: 'Одобрено',
    1: 'Заблокировано',
    2: 'Скрыто',
    3: 'Нейтральный',
}
class IPState(models.Model):
    ip = models.CharField(max_length=15, unique=True)
    state = models.PositiveSmallIntegerField(choices=IP_STATE_CHOICE.items())
    comment = models.CharField(max_length=255, default='')

    APPROVED = 0
    BLOCKED = 1
    HIDDEN = 2
    NONE = 3

    @staticmethod
    def SET(ip, state, comment=''):
        if ip.endswith('.'):
            ip = ip[:-1]
        
        try:
            obj = IPState.objects.get(ip=ip)
        except:
            obj = IPState()
            obj.ip = ip
        
        obj.state = state
        obj.comment = comment
        obj.save()
        
        qs = IPState.objects.filter(ip__startswith=('%s.' % ip))
        qs.delete()
        
    @staticmethod
    def GET(ip, comment=False):
        if ip.endswith('.'):
            ip = ip[:-1]
        
        ips = ip.split('.')
        for i in range(len(ips)):
            try:
                _ip = '.'.join(ips[:len(ips)-i])
                obj = IPState.objects.get(ip=_ip)
                
                if not comment:
                    return obj.state

                if obj.ip == ip:
                    return obj.state, obj.comment, ''
                else:
                    return obj.state, '', obj.comment
            
            except:
                pass

        if not comment:
            return IPState.NONE
        else:
            return (IPState.NONE, '', '')

    class Meta:
        index_together = [
            ['ip'],
        ]

SESSION_DEVICE_TYPE_CHOICE = {
    'mobile': 'Mobile',
    'pc': 'PC',
    'tablet': 'Tablet',
    'bot': 'Bot',
}
class Session(models.Model):
    created_at = models.DateTimeField()
    user_agent = models.CharField(max_length=1024)
    ip = models.CharField(max_length=15)
    ip_int = models.BigIntegerField(null=True, blank=True, default=True)
    uid = models.CharField(max_length=256)
    
    geo_latitude = models.DecimalField(default=None, null=True, blank=True, max_digits=6, decimal_places=2)
    geo_longitude = models.DecimalField(default=None, null=True, blank=True, max_digits=6, decimal_places=2)
    geo_country = models.CharField(default=None, null=True, blank=True, max_length=128)
    geo_continent = models.CharField(default=None, null=True, blank=True, max_length=128)
    geo_timezone = models.CharField(default=None, null=True, blank=True, max_length=128)
    
    device_type = models.CharField(default=None, null=True, blank=True, choices=SESSION_DEVICE_TYPE_CHOICE.items(), max_length=20)
    device_family = models.CharField(default=None, null=True, blank=True, max_length=128)
    device_brand = models.CharField(default=None, null=True, blank=True, max_length=128)
    device_model = models.CharField(default=None, null=True, blank=True, max_length=128)
    
    os_family = models.CharField(default=None, null=True, blank=True, max_length=128)
    os_version = models.CharField(default=None, null=True, blank=True, max_length=128)
    
    browser_family = models.CharField(default=None, null=True, blank=True, max_length=128)
    browser_version = models.CharField(default=None, null=True, blank=True, max_length=128)
    
    whois_flag = models.BooleanField(default=False)
    whois_descr = models.CharField(default=None, null=True, blank=True, max_length=128)
    whois_netname = models.CharField(default=None, null=True, blank=True, max_length=128)
    whois_org = models.CharField(default=None, null=True, blank=True, max_length=128)
    whois_inetnum = models.CharField(default=None, null=True, blank=True, max_length=128)
    whois_netrange = models.CharField(default=None, null=True, blank=True, max_length=128)
    
    class Meta:
        index_together = [
            ['uid'],
            ['ip'],
        ]

    @staticmethod
    def GET(ip, user_agent, created_at):
        from hashlib import md5
        
        uid = '%s:%s' % (ip, user_agent)
        
        m = md5()
        m.update(uid)
        uid = m.hexdigest()

        try:
            session = Session.objects.get(uid=uid)
        
        except:
            session = Session()
            session.ip = ip
            session.user_agent = user_agent
            session.uid = uid
            session.created_at = created_at
            session.save()

        return session

    def __str__(self):
        return '%d ip=%s' % (self.id, self.ip)

    def save(self, *args, **kwargs):
        if self.geo_latitude is None:
            match = None
            try:
                match = geolite2.lookup(self.ip)
            except:
                pass
            
            if match:
                d = match.to_dict()
                if d['location']:
                    self.geo_latitude, self.geo_longitude = d['location'] 
                self.geo_country = d['country']
                self.geo_continent = d['continent']
                self.geo_timezone = d['timezone']

        if self.device_type is None:
            ua = user_agents.parse(self.user_agent)
            if ua.is_mobile:
                self.device_type = 'mobile'
            elif ua.is_bot:
                self.device_type = 'bot'
            elif ua.is_pc:
                self.device_type = 'pc'
            elif ua.is_tablet:
                self.device_type = 'tablet'

            self.device_family = ua.device.family
            self.device_brand = ua.device.brand
            self.device_model = ua.device.model

            self.os_family = ua.os.family
            self.os_version = ua.os.version_string

            self.browser_family = ua.browser.family
            self.browser_version = ua.browser.version_string

        # устанавливаем whois info
        if not self.whois_flag:
            self.whois_flag = True
        
            p = subprocess.Popen(['whois', self.ip], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            out, _ = p.communicate()

            keys = ['descr', 'netname', 'org', 'inetnum', 'netrange']
            for key in keys:
                res = re.findall('%s:\s*[^\n]+' % key, out, flags=re.I|re.U)
                try:
                    value = res[0].split(':')[1].strip()
                except:
                    continue
                setattr(self, 'whois_%s' % key, value)
    
        if self.pk is  None:
            ip = get_ips(self.ip)
            if ip:
                self.ip = ip[0]
                self.ip_int = ip_str2int(ip[0])

        return super(Session, self).save(*args, **kwargs)


LOG_MODE_CHOICE = {
    'fail_code': 'Fail code',
    'image': 'Image',
    'image_error': 'Image Error',
    'link': 'Link',
    'link_error': 'Link Error',
    'home': 'Home',
}
class Log(models.Model):
    session = models.ForeignKey(Session)
    domain = models.ForeignKey(Domain, null=True, blank=True)
    mailing = models.ForeignKey(Mailing, null=True, blank=True)
    
    referer = models.CharField(max_length=1024)
    path = models.CharField(max_length=1024)

    created_at = models.DateTimeField()
    mode = models.CharField(max_length=10, choices=LOG_MODE_CHOICE.items())

    is_blocked = models.BooleanField()
    locking = models.ForeignKey('Locking', null=True, blank=True, default=None, on_delete=models.SET_NULL)
    privilege = models.ForeignKey('Privilege', null=True, blank=True, default=None, on_delete=models.SET_NULL)

    ip_state = models.PositiveSmallIntegerField(default=IPState.NONE, choices=IP_STATE_CHOICE.items())
    ip = models.CharField(max_length=15)

    class Meta:
        index_together = [
            ['session'],
            ['ip'],
            ['ip', 'ip_state'],
        ]

class Locking(models.Model):
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    comment = models.CharField(max_length=1024)

    ip_check = models.BooleanField()
    ip1 = models.CharField(max_length=15, null=True, blank=True, default=None)
    ip2 = models.CharField(max_length=15, null=True, blank=True, default=None)
    ip1_int = models.BigIntegerField(null=True, blank=True, default=None)
    ip2_int = models.BigIntegerField(null=True, blank=True, default=None)

    device_type_check = models.BooleanField()
    device_type = models.CharField(default=None, null=True, blank=True, choices=SESSION_DEVICE_TYPE_CHOICE.items(), max_length=20)

    # None - не проверять
    # True - включение
    # False - исключение
    geo_country_check = models.NullBooleanField()
    geo_country = models.CharField(default=None, null=True, blank=True, max_length=128)

    @property
    def geo_country_list(self):
        if not self.geo_country:
            return []
        return self.geo_country[1:-1].split('|')
    
    def save(self, *args, **kwargs):
        self.ip1_int = ip_str2int(self.ip1)
        if self.ip2:
            self.ip2_int = ip_str2int(self.ip2)
        else:
            self.ip2_int = self.ip1_int

        return super(Locking, self).save(*args, **kwargs)

    def __str__(self):
        res = []
        if self.ip_check:
            if self.ip1_int == self.ip2_int:
                res.append('ip=%s' % self.ip1)
            else:
                res.append('iprange=[%s,%s]' % (self.ip1, self.ip2))

        if self.device_type_check == True:
            res.append('device_type=%s' % self.device_type)

        if self.geo_country_check == True:
            res.append('geo_country_include=%s' % self.geo_country)
        if self.geo_country_check == False:
            res.append('geo_country_exclude=%s' % self.geo_country)

        return '; '.join(res)

class Privilege(models.Model):
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    comment = models.CharField(max_length=1024)
    
    ip1 = models.CharField(max_length=15)
    ip2 = models.CharField(max_length=15)
    ip1_int = models.BigIntegerField()
    ip2_int = models.BigIntegerField()

    def save(self, *args, **kwargs):
        self.ip1_int = ip_str2int(self.ip1)
        if self.ip2:
            self.ip2_int = ip_str2int(self.ip2)
        else:
            self.ip2_int = self.ip1_int

        return super(Privilege, self).save(*args, **kwargs)

    def __str__(self):
        if self.ip1_int == self.ip2_int:
            return 'ip=%s' % self.ip1
        else:
            return 'iprange=[%s,%s]' % (self.ip1, self.ip2)