<?php
/**
 * @example run php flaer.png 115
 *  flaer.png - файл в /var/www/link_encoder/subscribe_images
 *  115 id рассылки
 * допустимо также 
 * run php 115.png
 *  115 - файл в /var/www/link_encoder/subscribe_images
 * 
 * Скрипт получает как аргумент короткое имя файла, файл уже должен существовать в 
 * /var/www/link_encoder/subscribe_images/
 * из файла создается гифка и кладется в ту же папку
 * /var/www/link_encoder/subscribe_images/
*/
require_once dirname(__FILE__) . '/config.php';

if (!isset($argv[1])) {
	throw new Exception('short filename of image required');
}

if (!file_exists( $IMAGE_FOLDER . '/' . $argv[1] )) {
	throw new Exception('file ' . $argv[1] . ' not found in directory "' . $IMAGE_FOLDER . '"');
}
//Проверяю, число ли имя файла и если не число, то есть ли переданное число вторым аргументом
$subscribe_id = get_subscribe_id($argv);
$source = $IMAGE_FOLDER . '/' . $argv[1];

/* Пытается получить id рассылки из переданных скрипту аргументов 
 * Если передано вторым аргументом число, значит оно считается id рассылки
 * Если передан только один аргумент (имя файла) и его часть до первой точки - число - оно считается id рассылки и не передан второй - число
 * В противном случае бросаю исключение
*/
function get_subscribe_id($argv) {
	if (!isset($argv[1])) {
		throw new Exception('short filename of image required');
	}
	$a = explode('.', $argv[1]);
	$id = intval($a[0]);
	if (!$id) {
		if (!isset($argv[2]) || !intval($argv[2])) {
			throw new Exception('subscribe id required, got argv array: ' . print_r($argv, 1));
		} else {
			$id = intval($argv[2]);
		}
	} else if (isset($argv[2]) && intval($argv[2])) {
		$id = $argv[2];
	}
	return $id;
}

// Определяем формат
if ($argv[3] == 'gif') {
	$format = 'gif';
	@unlink($IMAGE_FOLDER . '/' . $subscribe_id . '.static');
} else {
	$format = 'static';
	@unlink($IMAGE_FOLDER . '/' . $subscribe_id . '.gif');
}

// удялаем весь кеш - потому что картинка может измениться
@rmdir($TARGET_FOLDER . '/' . $subscribe_id);

echo 'format = '.$format;
echo 'argv[3] = '.$argv[3];

rename($source, $IMAGE_FOLDER . '/' . $subscribe_id . '.' . $format);