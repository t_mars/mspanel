<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once dirname(__FILE__) . '/config.php';
require_once dirname(__FILE__) . '/../lib/encrypt.php';
require_once dirname(__FILE__) . '/common.php';

if ($_SERVER['REQUEST_URI'] == '/') {
	request_log('home');
	header('HTTP/1.0 403 Forbidden');
	//echo file_get_contents('static.html');
	exit;
}

$hash = preg_replace("#^/#", '', $_SERVER['REQUEST_URI']);
$gen = new HashGen();
$string = $gen->resolve($hash);

$mode = NULL;
if ($string[0] == 'i' or $string[0] == 'l') {
	$id = substr($string, 1);
	if (is_numeric($id)) {
		$mode = $string[0];
	}
}

switch ($mode) {
	case 'l':
		$redirect_js_off_link = $redirect_link = select_link($id);
		if ($redirect_link === false) {
			request_log('link_error');
			$redirect_js_off_link = $redirect_link = '/404';
		} else {
			request_log('link:'.$id);
		}
		?>
		<!DOCTYPE html><html>
		<meta charset="UTF-8">
		<head>
		<style>
		* { color: white; }
		</style> 
		<script type="text/javascript"> 
		var x = 0
	    function go() { location.replace("<?=$redirect_js_off_link?>") }
	    window.setTimeout('go()', 5000)
		</script> 
		</head>
		<body>
		<iframe src="javascript:parent.location='<?=$redirect_js_off_link?>'" style="visibility:hidden">
		<? include dirname(__FILE__) . '/random_content.php';?>
		</body>
		</html><?
		break;
	
	case 'i':
		$source = $IMAGE_FOLDER . '/' . $id;
		if (file_exists($source . '.gif') or file_exists($source . '.static')) {
			request_log('image:'.$id);
			require "image.php";
		} else {
			request_log('image_error');
			header("HTTP/1.1 404 Not Found");
		}
		break;
	
	default:
		request_log('fail_code');
		die('Unable detect link or image path');
}

