<?php
require_once dirname(__FILE__) . '/config.php';

function main($argv) {
	if (!isset($argv[1]) || !isset($argv[2])) {
		throw new Exception('Require two argument: subscribe id and link');
	}
	$subscribe_id = (int)$argv[1];
	if (!$subscribe_id) {
		throw new Exception('SubscribeId is 0!');
	}
	$db_file = dirname(__FILE__) . '/' . LINK_DB;
	create_db($db_file);
	add_relation($db_file, $subscribe_id, $argv[2]);
}
main($argv);
/**
 * @desc Смотрит, есть ли файл $db_file, создает если нет, если нет таблицы, создает таблицу
 * @param string $db_file
*/
function create_db($db_file) {
	if (!file_exists($db_file)) {
		@file_put_contents($db_file, '');
		if (!file_exists($db_file)) {
			throw new Exception('Unable write file ' . $db_file);
		}
	} else {
		/*На вероятное будущее
		 * $res = sqlite_open($db_file);
		$sql_query = 'CREATE TABLE IF NOT EXISTS link_relations(subscribe_id INTEGER, link TEXT)';
		sqlite_exec($sql_query, $res);
		sqlite_close($db_file);
		*/
	}
}
/**
 * @desc Смотрит, есть ли файл $db_file, создает если нет, если нет таблицы, создает таблицу
 * @param string $db_file - путь к базе данных
 * @param int $id - идентификатор рассылки
 * @param string $link - ссылка
*/
function add_relation($db_file, $id, $link, $unique = true) {
	$strings = explode("\n", file_get_contents($db_file));
	$len = count($strings);
	$s = "{$id},{$link}";
	$success = false;
	if ($unique) {
		for ($i = 0; $i < $len; $i++) {
			$pair = explode(',', $strings[$i]);
			if (count($pair) == 2) {
				if ($pair[0] == $id) {
					$success = true;
					$strings[$i] = $s;
				}
			}
		}
	}
	if (!$success) {
		if ($len == 1) {
			$pair = explode(',', $strings[0]);
			if (count($pair) != 2) {
				$strings[0] = $s;
				$success = true;
			}
		}
	}
	if (!$success) {
		$strings[] = $s;
	}
	$clear = array();
	$len = count($strings);
	for ($i = 0; $i < $len; $i++) {
		$str = $strings[$i];
		if (trim($str)) {
			$clear[] = trim($str);
		}
	}
	file_put_contents($db_file, join("\n", $clear));
	/*На вероятное будущее $res = sqlite_open($db_file);
	if ($unique) {
		$sql_query = "DELETE FROM link_relations WHERE subscribe_id = '{$id}';";
		sqlite_exec($sql_query, $res);
	}
	$sql_query = "INSERT INTO link_relations (subscribe_id, link) VALUES('{$id}', '{$link}')";
	sqlite_exec($sql_query, $res);
	$inserted = sqlite_last_insert_rowid ($res);
	if (!$inserted) {
		throw new Exception('Error insert');
	}
	sqlite_close($db_file);*/
}
