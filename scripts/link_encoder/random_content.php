<?php
require_once dirname(__FILE__) . '/../lib/shared.php';
$tags = 'h1,h2,h3,h4,h5,h6,p,span,i,b,u,div,article,section';
$a_tags = explode(',', $tags);
$sz_tags = count($a_tags) - 1;


$sz = 14692;
$dir = dirname(__FILE__);
$a = explode("\n", file_get_contents($dir . '/data/content.txt' ));
$sz_content_lines = rand(2, 25);
$sz = count($a);

$content = '';
for ($i = 0; $i < $sz_content_lines; $i++) {
	$line = rand(0, $sz);
	$s = $a[$line];
	$l_tag = rand(0, $sz_tags);
	$tag = $a_tags[$l_tag];
	$content .= "<{$tag}>{$s}</{$tag}>\n";
}

echo $content;
