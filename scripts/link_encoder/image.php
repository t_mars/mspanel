<?php
require_once dirname(__FILE__) . '/config.php';

// Если статичная картинка то возвращаем
if (file_exists($source . '.static')) {
  header ("Content-Type: image/jpeg");
  header ("Cache-Control: public, max-age=3600");
  echo file_get_contents($source . '.static');
  exit;
}

// в другом случае гиф
$source .= '.gif';
$dir = $TARGET_FOLDER . '/' . $id;
if (!file_exists($dir)) {
    mkdir($dir, 0777, true);
}
$filename = $dir . '/' . $hash;

if (file_exists($filename)) {
  header ("Content-Type: image/gif");
  header ("Cache-Control: public, max-age=3600");
  echo file_get_contents($filename);
  exit;
}

/* Функция очистки папки: */
function removeDirRec($dir) 
{ 
    if ($objs = glob($dir."/*")) { 
        foreach($objs as $obj) { 
            is_dir($obj) ? removeDirRec($obj) : unlink($obj); 
        } 
    } 
} 
$dir = dirname(__FILE__) . "/image/imagego"; 
removeDirRec($dir);  

$dir = dirname(__FILE__) . "/image/gifgo"; 
removeDirRec($dir);

function make_seed()
{
  list($usec, $sec) = explode(' ', microtime());
  return (float) $sec + ((float) $usec * 100000);
}
mt_srand(make_seed());

// Генератор случайного числа

$random_number_color1 = mt_rand(0, 255);
$random_number_color2 = mt_rand(0, 255);
$random_number_color3 = mt_rand(0, 255);
$random_number_color4 = mt_rand(0, 255);
$random_number_color5 = mt_rand(0, 255);
$random_number_color6 = mt_rand(0, 255);
$random_number_color7 = mt_rand(0, 255);
$random_number_color8 = mt_rand(0, 255);
$random_number_color9 = mt_rand(0, 255);
$random_number_xy1 = mt_rand(300, 1000);
$random_number_xy2 = mt_rand(400, 500);
$random_number_xy3 = mt_rand(300, 800);
$random_number_xy4 = mt_rand(100, 1000);
$random_number_xy5 = mt_rand(200, 600);
$random_number_xy6 = mt_rand(200, 600);
$random_number_xy7 = mt_rand(100, 1000);
$random_number_xy5 = mt_rand(200, 600);
$random_number_xy8 = mt_rand(100, 1000);
// Генератор картинки

//Фон
  $i = imagecreate(mt_rand(700, 900), mt_rand(700, 900));// Размер каринки
  $color = imageColorAllocate($i, $random_number_color1, $random_number_color2, $random_number_color3);

  // Кривая незамкнутая
  imageSetThickness($i, $random_number_xy8);
  imageArc($i, $random_number_xy1, $random_number_xy2, $random_number_xy3, $random_number_xy4, $random_number_xy5, $random_number_xy6, $color);
// Кривая замкнутая
  $color = imageColorAllocate($i, $random_number_color1, $random_number_color3, $random_number_color8);
  imagePolygon($i, array($random_number_xy5, $random_number_xy2, $random_number_xy4, $random_number_xy8, $random_number_xy3, $random_number_xy2, $random_number_xy1, $random_number_xy4, $random_number_xy2, $random_number_xy5), 2, $color);

  
// Фигура1
  $color = imageColorAllocate($i, $random_number_color7, $random_number_color8, $random_number_color9);
  imageFilledRectangle($i, $random_number_xy8, $random_number_xy1, $random_number_xy5, $random_number_xy3, $color);
// Фигура2
  $color = imageColorAllocate($i, $random_number_color2, $random_number_color8, $random_number_color5);
  imageFilledRectangle($i, $random_number_xy3, $random_number_xy4, $random_number_xy3, $random_number_xy3, $color);
// Фигура3
  $color = imageColorAllocate($i, $random_number_color7, $random_number_color8, $random_number_color9);
  imageFilledRectangle($i, $random_number_xy4, $random_number_xy8, $random_number_xy2, $random_number_xy4, $color);
// Фигура4
  $color = imageColorAllocate($i, $random_number_color1, $random_number_color4, $random_number_color1);
  imageFilledRectangle($i, $random_number_xy1, $random_number_xy3, $random_number_xy5, $random_number_xy3, $color);
// Фигура5
  $color = imageColorAllocate($i, $random_number_color5, $random_number_color3, $random_number_color2);
  imageFilledRectangle($i, $random_number_xy1, $random_number_xy1, $random_number_xy4,$random_number_xy1, $color);
// Фигура6
  $color = imageColorAllocate($i, $random_number_color9, $random_number_color2, $random_number_color3);
  imageFilledRectangle($i, $random_number_xy2, $random_number_xy2, $random_number_xy8, $random_number_xy5, $color);
// Фигура7
  $color = imageColorAllocate($i, $random_number_color7, $random_number_color1, $random_number_color2);
 imageSetThickness($i, $random_number_color1);
 imageellipse($i, $random_number_xy4, $random_number_xy4, $random_number_xy5, $random_number_xy5, $color);
 
  //Сохранение файла
  imagepng($i, dirname(__FILE__) .  "/image/imagego/1.png");

//Создание GIF
//
include('GIFEncoder.class.php');


$ranprex1 = mt_rand(0, 9999);
$ranprex2 = mt_rand(0, 9999);
$ranprex3 = mt_rand(0, 9999);
$ranprex4 = mt_rand(0, 9999);
$ranprex5 = mt_rand(0, 9999);

//Генератор случайной строки имени файла

// Символы, которые будут использоваться в имени. 

$chars="qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP"; 
$max=28; 
$size=StrLen($chars)-1; 
$name_image_gif=null; 
while($max--) 
  $name_image_gif .= $chars[rand(0,$size)];
//
// подключаем класс для создания gif
$frames = array(); // массив для хранения слайдов
$framed = array(); // массив для храниения интервалов смены слайдов

$size = getimagesize($source);
$format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
$icfunc = "imagecreatefrom".$format;
if(!function_exists($icfunc)) {
  if (file_exists('/var/log') && is_dir('/var/log')) {
    copy($source, '/var/log/' . $argv[1]);
  }
  throw new Exception('Invalid image format ' . $format);
}
$image = $icfunc($source);
ob_start();
imagegif($image);
$frames[] = ob_get_contents();
$framed[] = 0; //999;
ob_end_clean();

$image = imagecreatefrompng(dirname(__FILE__) . "/image/imagego/1.png"); // загружаем картинку
ob_start(); // включаем буферизацию вывода
imagegif($image); // пишем в буфер получившийся слайд
$frames[] = ob_get_contents(); // записываем в хранилище слайд из буфера
$framed[] = 0; // записываем интервал до смены слайда
ob_end_clean(); // отключаем буферизацию и чистим буфер
 
 
// второй слайд
//Генерация

$size = getimagesize($source);
$format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
$icfunc = "imagecreatefrom".$format;
if(!function_exists($icfunc)) {
  if (file_exists('/var/log') && is_dir('/var/log')) {
    copy($source, '/var/log/' . $argv[1]);
  }
  throw new Exception('Invalid image format ' . $format);
}
$image = $icfunc($source);

ob_start();
imagegif($image);
$frames[] = ob_get_contents();
$framed[] = 9999999999999;
ob_end_clean();
 
// создаем gif картинку из
$gif = new GIFEncoder($frames, $framed, 0, 0, 0, 0, 0, 'bin');
file_put_contents($filename, $gif->GetAnimation()); // пишем в него 

header ("Content-Type: image/gif");
header ("Cache-Control: public, max-age=3600");
echo $gif->GetAnimation();
