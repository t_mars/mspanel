<?php
function set_offset($state_file, $v) {
	file_put_contents($state_file, $v);
}

function get_offset($state_file) {
	$n = 0;
	if (file_exists($state_file)) {
		$n = (int)file_get_contents($state_file);
	} else {
		file_put_contents($state_file, '0');
	}
	return $n;
}
function cpoint($msg = '', $cmd = '') {
	global $dbg_start_time, $dbg_total_start_time;
	$ret_obj = new StdClass();
	$ret_obj->time = null;
	$ret_obj->total_time = null;
	if ($cmd == 'set') {
		if (!$dbg_total_start_time) {
			$dbg_total_start_time = microtime(true);
		}
		$dbg_start_time = microtime(true);
		if ($msg) {
			print "{$msg}\n";
		}
	} else {
		$time = microtime(true) - $dbg_start_time;
		print $msg . ': ' . "{$time}\n";
		$ret_obj->time = $time;
		if ($cmd == 'total') {
			$time = microtime(true) - $dbg_total_start_time;
			$ret_obj->total_time = $time;
			print 'Total: ' . "{$time}\n";
		}
	}
	return $ret_obj;
}

if (!function_exists("mb_str_replace")) 
{
    function mb_str_replace($needle, $replacement, $haystack) {
        return implode($replacement, mb_split($needle, $haystack));
    }
}

/*function dump($sql, $__FILE__, $source_db = null) {
	$sql = 'SELECT * FROM '. $source_table .'  LIMIT 1000';
	if ($source_db) {
		setActiveDb($source_db);
	}
	$rows = query($sql);
	$s =json_encode($rows);
	$dir = dirname($__FILE__);
	file_put_contents("$dir/last_std_dump.json", $s);
}
function restore($dumpfile) {
	
}*/


