<?php
/*
 * use:
 * $password = "Passwort";
	$string = "http://test.ru/img.ru";        

	$gen = new HashGen();

	for ($i = 0; $i < 10; $i++) {
		$hash = $gen->generate($string);
		echo "hash: ".$hash."\n";
		$string = $gen->resolve($hash);
		echo "string: ".$string."\n";     
	}*/
class HashGen {
	const spec_chars = '+/:.';

	public function __construct($secretKey = '') 
	{
		$this->secretKey = $secretKey;
	}

	// Разбрасывает массив
	private function shuffle($array, $seed)
	{
	    mt_srand($seed);
	    $order = array();
	    $count = count($array);
	    while($count--)
	        $order[] = mt_rand();

	    array_multisort($order, $array);
	    return $array;
	}

	// Генерирует таблицу отображения симолов
	private function getCharMap($seed)
	{
		$chars = str_split('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.self::spec_chars);
		
		$chars_to = $this->shuffle($chars, $seed);
		$map = array();
		for ($i = 0; $i < count($chars); $i++) {
			$map[$chars[$i]] = $chars_to[$i];
		}
		
		return $map;
	}

	// Отображает строку с помощью таблицы отображения
	private function mapChars($string, $map) 
	{
		for ($i = 0; $i < strlen($string); $i++) {
			$char = $string[$i];
			if (!array_key_exists($char, $map))
				continue;
			$string[$i] = $map[$char];
		}
		
		return $string;
	}

	// Кодирует строку с помощью ключа
	private function encrypt($string, $key)
	{
	    return rtrim(
	        base64_encode(
	            mcrypt_encrypt(
	                MCRYPT_RIJNDAEL_128,
	                $key, $string, 
	                MCRYPT_MODE_ECB, 
	                mcrypt_create_iv(
	                    mcrypt_get_iv_size(
	                        MCRYPT_RIJNDAEL_128, 
	                        MCRYPT_MODE_ECB
	                    ), 
	                    MCRYPT_RAND)
	                )
	            ), "\0"
	        );
	}

	// Раскодирует строку с помощью ключа
	private function decrypt($string, $key)
	{
	    return rtrim(
	        mcrypt_decrypt(
	            MCRYPT_RIJNDAEL_128, 
	            $key, 
	            base64_decode($string), 
	            MCRYPT_MODE_ECB,
	            mcrypt_create_iv(
	                mcrypt_get_iv_size(
	                    MCRYPT_RIJNDAEL_128,
	                    MCRYPT_MODE_ECB
	                ), 
	                MCRYPT_RAND
	            )
	        ), "\0"
	    );
	}

	// Генерирует случайную строку
	private function randString($length = 1, $characters = '') 
	{
	    if (!$characters)
	    	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $string = '';
	    for ($i = 0; $i < $length; $i++) {
	        $string .= $characters[rand(0, $charactersLength - 1)];
	    }
	    
	    return $string;
	}

	// Вставляем подстроку в указанное место
	private function putSubstring(&$string, $pos, $sub) 
	{
		$string = substr($string, 0, $pos).$sub.substr($string, $pos);
		
		return $string;
	}

	// Извлекает подстроку с указанного места
	private function cutSubstring(&$string, $pos, $length=1) 
	{
		$sub = substr($string, $pos, $length);
		$string = substr($string, 0, $pos).substr($string, $pos+$length);
		
		return $sub;
	}

	public function generate($string)
	{
		$hash = $string;

		// Цикл, чтобы исключить спецсимволы из выходной строке
		do {
			// Определяем место где будем хранить кодовый символ
			$char_pos = $this->randString();
			$pos = 1+ (ord($char_pos) % strlen($hash));
			$char_code = $this->randString();
			
			// Отображаем строку
			$map = $this->getCharMap(ord($char_pos));
			$hash2 = $this->mapChars($hash, $map);
			$map = $this->getCharMap(ord($char_code));
			$hash2 = $this->mapChars($hash2, $map);
		} while (array_intersect(str_split($hash2), str_split(self::spec_chars)));
		$hash = $hash2;

		$this->putSubstring($hash, 0, $char_pos);
		$this->putSubstring($hash, $pos, $char_code);

		return $hash; 
	}

	public function resolve($hash) 
	{
		// Выявляем и извлекаем кодовый символ
		$char_pos = $this->cutSubstring($hash, 0);
		$pos = (ord($char_pos) % (strlen($hash)-1));
		$char_code = $this->cutSubstring($hash, $pos);

		// Пере-отображаем строку
		$rmap = array_flip($this->getCharMap(ord($char_code)));
		$hash = $this->mapChars($hash, $rmap);
		$rmap = array_flip($this->getCharMap(ord($char_pos)));
		$hash = $this->mapChars($hash, $rmap);

		$string = $hash;

		return $string;
	}
}
