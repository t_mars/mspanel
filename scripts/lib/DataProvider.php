<?php

require_once dirname(__FILE__) . '/mysql.php';

class DataProvider {
	/** @const validator script id* временная мера, т к id пока неизвестен */
	const VALIDATOR_SCRIPT_ID = 'validator/CEmailValidator.php';
	const VALIDATOR_WEB_AGENT_SCRIPT_ID = 'validator/WebAgent.php';
	
	/** статусы */
	const STATUS_OK = 200;
	const STATUS_OK_TEXT = 'OK';
	const STATUS_404 = 404;
	const STATUS_404_TEXT = 'Not Found';
	const STATUS_403 = 403;
	const STATUS_403_TEXT = 'Forbidden';
	const STATUS_400 = 400;
	const STATUS_400_TEXT = 'Bad Request';
	/** @var _client_id идентификатор скрипта, сейчас пока string, будет int */
	private $_client_id;
	public function __construct($client_id) {
		$this->_client_id = $client_id;
	}
	public function setClientId($client_id) {
		$this->_client_id = $client_id;
	}
	public function getClientId() {
		return $this->_client_id;
	}
	/**
	 * @desc Получить данные от системы
	 * @param string $rest_query запрос в стиле REST, например /emails/page/15
	 * @param $client_id если передан, запрос отправляется от имени скрипта client_id, иначе - от имени скрипта, id которого передано в __construct или setClientId
	*/
	public function get($rest_query, $client_id = null) {
		if ($client_id === null) {
			$client_id = $this->getClientId();
		}
		return $this->_tmp_patch_get($rest_query, $client_id);
	}
	/**
	 * @desc Удалить сущность
	 * @param string $rest_query запрос в стиле REST, например /emails/25 правим email с id 25
	 * @param $client_id если передан, запрос отправляется от имени скрипта client_id, иначе - от имени скрипта, id которого передано в __construct или setClientId
	*/
	public function delete($rest_query, $client_id = null) {
		
	}
	/**
	 * @desc Изменить сущность, перезаписать какие-то поля, кроме ее id
	 * @param string $rest_query запрос в стиле REST, например /emails/25 правим email с id 25
	 * @param array $data ассоциативный массив с данными
	 * @param $client_id если передан, запрос отправляется от имени скрипта client_id, иначе - от имени скрипта, id которого передано в __construct или setClientId
	*/
	public function patch($rest_query, $data, $client_id = null) {
		if ($client_id === null) {
			$client_id = $this->getClientId();
		}
		return $this->_tmp_patch_patch($rest_query, $data, $client_id);
	}
	/**
	 * @desc Добавить сущность
	 * @param string $rest_query запрос в стиле REST, например /emails/25 правим email с id 25
	 * @param array $data ассоциативный массив с данными
	 * @param $client_id если передан, запрос отправляется от имени скрипта client_id, иначе - от имени скрипта, id которого передано в __construct или setClientId
	*/
	public function post($rest_query, $data, $client_id = null) {
		if ($client_id === null) {
			$client_id = $this->getClientId();
		}
		return $this->_tmp_patch_post($rest_query, $data, $client_id);
	}
	
	//==================================================================
	//Эти методы будут удалены, т к запросы будут просто
	//переадресовыватьcя питон скрипту
	private function _tmp_patch_get($rest_query, $client_id) {
		if ($client_id == self::VALIDATOR_SCRIPT_ID) {
			return $this->_get_method_for_validator_script($rest_query);
		}
		if ($client_id == self::VALIDATOR_WEB_AGENT_SCRIPT_ID) {
			return $this->_get_method_for_validator_web_agent_script($rest_query);
		}
		$response = array('status' => self::STATUS_403, 'status_text' => self::STATUS_403_TEXT);
		return $response;
	}
	private function _tmp_patch_patch($rest_query, $data, $client_id) {
		if ($client_id == self::VALIDATOR_SCRIPT_ID) {
			return $this->_patch_method_for_validator_script($rest_query, $data);
		}
		if ($client_id == self::VALIDATOR_WEB_AGENT_SCRIPT_ID) {
			return $this->_patch_method_for_validator_web_agent_script($rest_query, $data);
		}
		$response = array('status' => self::STATUS_403, 'status_text' => self::STATUS_403_TEXT);
		return $response;
	}
	private function _tmp_patch_post($rest_query, $data, $client_id) {
		/*if ($client_id == self::VALIDATOR_SCRIPT_ID) {
			return $this->_patch_method_for_validator_script($rest_query, $data);
		}*/
		if ($client_id == self::VALIDATOR_WEB_AGENT_SCRIPT_ID) {
			return $this->_post_method_for_validator_web_agent_script($rest_query, $data);
		}
		$response = array('status' => self::STATUS_403, 'status_text' => self::STATUS_403_TEXT);
		return $response;
	}
	private function _patch_method_for_validator_script($rest_query, $data) {
		switch ($rest_query) {
			case '/status':
				if (isset($data['done'])) {
					$file = dirname(__FILE__) . '/../validator/output/done.txt';
					if ($data['done'] == false) {
						file_put_contents($file, '');
						@unlink($file);
					} else {
						$msg = (isset($data['info']['message']['text']) ? $data['info']['message']['text'] : 'true');
						file_put_contents($file, $msg);
					}
					$response = array('status' => self::STATUS_OK, 'status_text' => self::STATUS_OK_TEXT);
				}
				return $response;
		}
		$response = array('status' => self::STATUS_404, 'status_text' => self::STATUS_404_TEXT);
		return $response;
	}
	private function _get_method_for_validator_script($rest_query) {
		$status = self::STATUS_OK;
		$status_text = self::STATUS_OK_TEXT;
		switch ($rest_query) {
			case '/emails':
				$result = query("SELECT id, email FROM emails WHERE is_validate = 0 ORDER BY id LIMIT 0, 100"); //10 000 TODO !!
				$response = array('status' => $status, 'status_text' => $status_text, 'data' => $result);
				return $response;
			case '/accounts':
				$result = query("SELECT id, email, password FROM checker_accounts WHERE is_banned  = 0 ORDER BY id LIMIT 0, 1000");
				$response = array('status' => $status, 'status_text' => $status_text, 'data' => $result);
				return $response;
		}
		$response = array('status' => self::STATUS_404, 'status_text' => self::STATUS_404_TEXT);
		return $response;
	}
	
	private function _get_method_for_validator_web_agent_script($rest_query) {
		if (preg_match("#^/accounts/offset/[0-9]+$#", $rest_query, $m)) {
			$a = explode("/", $rest_query);
			$n = intval($a[3]);
			if ($n || $n === 0) {
				$row = dbrow("SELECT id, email, password FROM checker_accounts WHERE is_banned = 0 ORDER BY id LIMIT {$n}, 1;", $num);
				if ($num) {
					$response = array('status' => self::STATUS_OK, 'status_text' => self::STATUS_OK_TEXT, 'data' => $row);
				} else {
					$response = array('status' => self::STATUS_404, 'status_text' => self::STATUS_404_TEXT, 'info' => 'emty_scope');
				}
				return $response;
			}
		}
		
		if (preg_match("#^/accounts/authorize/.+$#", $rest_query, $m)) {
			$a = explode("/", $rest_query);
			if (isset($a[3])) {
				$email = $a[3];
				$list = query("SELECT * FROM webagent_auth_log WHERE account = '{$email}';");
				$response = array('status' => self::STATUS_OK, 'status_text' => self::STATUS_OK_TEXT, 'data' => $list);
				return $response;
			} else {
				$response = array('status' => self::STATUS_400, 'status_text' => self::STATUS_400_TEXT, 'info' => 'email not defined');
				return $response;
			}
		}
		$response = array('status' => self::STATUS_404, 'status_text' => self::STATUS_404_TEXT, 'info' => 'Bad request');
		return $response;
	}
	
	private function _patch_method_for_validator_web_agent_script($rest_query, $data) {
		if (isset($data['is_valid']) && preg_match("#^/emails/.+#", $rest_query, $m)) {
			$a = explode('/', $rest_query);
			$mail_id = isset($a[2])  ? $a[2] : false;
			if ($data['is_valid'] == true) {
				query("UPDATE emails SET is_validate = 1 WHERE email = '{$mail_id}'"); //TODO id
			} else {
				$mail = dbvalue("SELECT email FROM emails WHERE email = '{$mail_id}'"); //TODO id
				query("INSERT INTO blacklist (`email`) VALUES('{$mail}') ON DUPLICATE KEY UPDATE id = id;");
				query("DELETE FROM emails WHERE email = '{$mail_id}';");					//TODO id
			}
			
			$response = array('status' => self::STATUS_OK, 'status_text' => self::STATUS_OK_TEXT);
			return $response;
		}
		
		if (isset($data['is_banned']) && preg_match("#^/accounts/.+#", $rest_query, $m)) {
			$a = explode('/', $rest_query);
			$mail_id = (isset($a[2])) ? $a[2] : false;
			if ($data['is_banned'] == true) {
				query("UPDATE checker_accounts SET is_banned = 1 WHERE email = '{$mail_id}'");//TODO id
			} else {
				query("UPDATE checker_accounts SET is_banned = 0 WHERE email = '{$mail_id}'");//TODO id
			}
			$response = array('status' => self::STATUS_OK, 'status_text' => self::STATUS_OK_TEXT);
			return $response;
		}
		
		$response = array('status' => self::STATUS_404, 'status_text' => self::STATUS_404_TEXT, 'ei' => '180');
		return $response;
	}
	private function _post_method_for_validator_web_agent_script($rest_query, $data) {
		if (isset($data['account']) && isset($data['country']) && isset($data['city']) && $rest_query == '/accounts/authorize' ) {
			$account = $data['account'];
			$country = $data['country'];
			$city = $data['city'];
			query("INSERT INTO webagent_auth_log (`account`, `country`, `city`) VALUES('{$account}', '{$country}', '{$city}') ON DUPLICATE KEY UPDATE account = account");
			$response = array('status' => self::STATUS_OK, 'status_text' => self::STATUS_OK_TEXT);
			return $response;
		}
		$response = array('status' => self::STATUS_404, 'status_text' => self::STATUS_404_TEXT, 'ei' => '180');
		return $response;
	}
}
