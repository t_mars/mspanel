<?php
if (!isset($argv[1])) {
	die('argument required');
}
include dirname(__FILE__) . '/../lib/encrypt.php';
$gen = new HashGen();
$hash = $gen->generate($argv[1]);
echo "$hash";
