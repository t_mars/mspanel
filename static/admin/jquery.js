//var $ = django.jQuery;
function formatRepo (repo) {
	return repo.text;
}

function formatRepoSelection (repo) {
	return repo.text;
}
$(document).ready(function(){
    $("#sex")
        .select2()
        .on('change', function (e) {
            $('#id_sex').val($("#sex").val());
        });

    $("#source")
        .select2()
        .on('change', function (e) {
            $('#id_source').val($("#source").val());
        });

    $("#services")
        .select2()
        .on('change', function (e) {
            $('#id_services').val($("#services").val());
        });

    var geo_root = '';
    $("#geo")
	    .select2({
		  ajax: {
		    url: "/geo/",
		    dataType: 'json',
		    delay: 250,
		    data: function (params) {
		      return {
		        q: params.term, // search term
		        r: geo_root,
		        page: params.page
		      };
		    },
		    processResults: function (data, page) {
		      geo_root = data.root
		      console.log('geo_root',geo_root)
		      if (data.reset) {
		      	$('.select2-search__field').val('')
		      }
		      return {
		        results: data.items
		      };
		    },
		    cache: true
		  },
		  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
		  minimumInputLength: 1,
		  templateResult: formatRepo, // omitted for brevity, see the source of this page
		  templateSelection: formatRepoSelection, // omitted for brevity, see the source of this page
		})
		.on('blur', function (e) {
			geo_root = ''
        })
		.on('change', function (e) {
			geo_root = ''
            $('#id_geo').val($("#geo").val());
        });
})
