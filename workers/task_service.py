from init import django_init
django_init()

import sys,os,json,datetime
from main.models import *
from django.utils import timezone
from datetime import timedelta

def get_service(name='', *args, **kwargs):
    return globals()["%sService" % name](sys.argv[1], *args, **kwargs)

class Service():
    def __init__(self, id):
        self.id = id
        try:
            self.task = Task.objects.get(pk=self.id) 
        except:
            self.task = Task() 
        self.pid = os.getpid()
        
        self.init()

    def save_status_string(self,d):
        self.task.save_status_string(d)

    def get_argument(self, *args, **kwargs):
        return self.task.get_argument(*args, **kwargs)

    def init(self):
        pass

from collections import Iterator
class DataSet(Iterator):
    def __init__(self, query_set, step=1000, limit=None):
        self.query_set = query_set
        self.datas = []
        self.shift = 0
        self.limit = limit
        self.step = step

        if self.limit:
            self.count = min(self.limit, query_set.count())
        else:
            self.count = query_set.count()

        print 'total count data set =',self.count
    
    def pop(self):
        if not len(self.datas):
            self._init_datas()
        
        if not len(self.datas):
            return None
    
        self.count -= 1

        return self.datas.pop()

    def next(self):
        if not len(self.datas):
            self._init_datas()

        if not len(self.datas):
            raise StopIteration
        
        self.count -= 1
    
        return self.datas.pop()

    def _init_datas(self):
        new_shift = self.shift+self.step
        if self.limit:
            new_shift = min(new_shift, self.limit)
        qs = self.query_set[self.shift:new_shift]
        self.datas = list(qs)
        self.shift = new_shift


    def __len__(self):
        return self.count