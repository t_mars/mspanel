#coding=utf8
import sys, os, json
sys.path.append('/var/www/mspanel/private/workers/')

from multi import Worker,freeze_support

from task_service import *
from main.models import *
from mailru import Sender

def work(task):
    id, username, password, proxy = task

    s = Sender(username, password, proxy=proxy)
    try:
        data = {
            'tokens': s.prepare(),
            'cookies': {
                'Mpop': s.get_cookie('Mpop'),
                'sdcs': s.get_cookie('sdcs'),
            }
        }
        
    except:
        data = None

    return id, data

class MyWorker(Worker):
    def init(self, accounts):
        self.accounts = accounts
        self.total = self.accounts.count
        self.success = 0

    def task(self):
        account = self.accounts.pop()
        if account:
            return account.id, account.username, account.password, Config.GET('send_mail.proxy', rand=True)
        
    def push(self, results):
        for id, data in results:
            print '%d/%d' % (self.success, self.total)

            account = MailRuAccount.objects.get(pk=id)
            account.auth_data = json.dumps(data)
            account.save()        
            self.success += 1

        self.service.save_status_string(
            {'success': self.success, 'total': self.total}
        )

if __name__ == '__main__':
    freeze_support()
    
    accounts = MailRuAccount.objects\
        .filter(is_active=True, auth_data=None) \
        
    w = MyWorker(100, work, 
        step=500, 
        service=get_service(),
        accounts=DataSet(accounts)
    )
    w.start()