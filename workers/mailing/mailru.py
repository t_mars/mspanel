#coding=utf8
from lib.mygrab import MyGrab  
from grab import Grab
import json
import os
import re
import sys
import hashlib
import datetime
import HTMLParser
import demjson

import string
import random

from lib.mailer.exceptions import AuthException, ParseException

class Sender:

    def __init__(self, account, grab_config={}, debug=False):
        self.html_parser = HTMLParser.HTMLParser()
    
        self.account = account
        self.email, self.password = self.account.username, self.account.password
        self.login, self.domain = self.email.split('@')
        self.last_error = None

        grab_config['debug'] = debug
        self.grab = MyGrab(**grab_config)

        s = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(4))
        self.grab.log_name = os.path.join('/var/www/mspanel/private/workers/mailing/logs', self.login + s)
        self.debug = debug

        auth_data = account.get_auth_data()
        if auth_data.get('cookies'):
            self.authorized = True
            self.grab.setup(cookies=auth_data.get('cookies'))
        else:
            self.authorized = False

        self.tokens = auth_data.get('tokens')
        self.entered = False

    def enter(self):
        if self.debug:
            print 'create new session', self.email
        self.grab.go('https://auth.mail.ru/cgi-bin/auth?from=splash', post={
            'Domain': self.domain,
            'Login': self.login,
            'Password': self.password,
            'new_auth_form': '1',
            'saveauth': '1'
        })
        p = 'window.location.replace("https://e.mail.ru/messages/inbox/?back=1");'
        if len(self.grab.doc.body) > 500 or p not in self.grab.doc.body:
            raise AuthException('Not entered.')
        self.authorized = True
        self.entered = True

    def prepare(self):
        if not self.tokens:
            if not self.authorized:
                self.enter()
            
            r = self.get_tokens()
            
            if not r:
                self.enter()
                r = self.get_tokens()

            if not r:
                raise AuthException
            
        return self.tokens

    def set_tokens(self, tokens):
        self.tokens = tokens

    def get_tokens(self):
        self.grab.go('https://e.mail.ru/compose')
        try:
            token = re.findall('patron\.updateToken\(\"[^"]+\"\)', self.grab.doc.body)[0].split('"')[1]
        except:
            return False

        self.grab.go('https://e.mail.ru/api/v1/messages/status?ajax_call=1&x-email=' + self.email +
            '&email=' + self.email +
            '&sort=%7B%22type%22%3A%22date%22%2C%22order%22%3A%22desc%22%7D'
            '&offset=0&limit=26&folder=0&htmlencoded=false'
            '&last_modified=1436870240'
            '&letters=false'
            '&nolog=1'
            '&sortby=D'
            '&api=1'
            '&token='+token
        )
        try:
            data = json.loads(self.grab.doc.body)
            tokens = data['body']['tokens']['sentmsg']
        except:
            return False

        self.tokens = tokens['form_sign'], tokens['form_token']
        return True

    def send(self, emails, subject, body):
        # отправляем всем сразу
        emails = [e.lower() for e in emails]
        
        # заглушка
        # return  {e:True for e in emails}
        
        # успешная отправка пачки
        if self._send(','.join(emails), subject, body):
            return {e:True for e in emails}

        # выявляем ошибку
        else:
            # если есть @ значит жалуется на почту, иначе на аккаунт
            if '@' not in self.last_error:
                return False
            
            # ошибка, убираем тех, кому не отправилось
            correct_emails = [e for e in emails if e not in self.last_error]
            
            # если все почты неверные
            if not len(correct_emails):
                return {e:False for e in emails}

            if self._send(','.join(correct_emails), subject, body):
                return {e:e in correct_emails for e in emails}
            
            # если второй раз возвращается ошибка, значит дело не в адресатах
            else:
                return False

    def _send_request(self, to, subject, body):
        form_sign, form_token = self.prepare()
        self.grab.go('https://e.mail.ru/compose/?func_name=send&send=1', post={
            'ajax_call':'1',
            'x-email':self.email,
            'text':'',
            'direction':'re',
            'orfo':'rus',
            'form_sign':form_sign,
            'form_token':form_token,
            'old_charset':'utf-8',
            'HTMLMessage':'1',
            'RealName':'0',
            'To':to,
            'Subject':subject,
            'Body':body,
            'EditorFlags':'0',
            'SocialBitmask':'0',
        })

        try:
            return json.loads(self.grab.doc.body)
        except:
            raise ParseException('No parsed answer from mail.ru.')

        if data[0] == 'AjaxResponse' and data[1] == 'OK' and data[2]['Ok']:
            return True
        elif data[0] == 'AjaxResponse' and data[1] == 'Redirect':
            raise AuthException('On send not authorized #1.')
        else:
            error = data[2]['Error'].lower()
            error = self.html_parser.unescape(error)
            self.last_error = error
            return False

    def _send(self, to, subject, body):
        try:
            res = self._send_request(to, subject, body)
        except AuthException:
            # если входа не было (использовался токен) авторизируемся заново
            if not self.entered:
                self.enter()
            else:
                raise AuthException('On send not authorized #2.')
            res = self._send_request(to, subject, body)
        return res

    def get_auth_data(self):
        if not self.authorized:
            return None
        return {
            'tokens': self.prepare(),
            'cookies': {
                'Mpop': self.grab.get_cookie('Mpop'),
                'sdcs': self.grab.get_cookie('sdcs'),
            }
        }

class WebClient:
    URL_MSGS_INBOX = 'https://e.mail.ru/messages/inbox/'
    URL_MSGS_SENT = 'https://e.mail.ru/messages/sent/'
    URL_MSGS_DRAFTS = 'https://e.mail.ru/messages/drafts/'
    URL_MSGS_SPAM = 'https://e.mail.ru/messages/spam/'
    URL_MSGS_TRASH = 'https://e.mail.ru/messages/trash/'
    URL_MSGS_MOVE = 'https://e.mail.ru/api/v1/messages/move'
    URL_MSG = 'https://e.mail.ru/message/%s/'
    
    URL_LOGIN = 'https://auth.mail.ru/cgi-bin/auth?from=splash'

    def __init__(self, user, password, grab=None, cookies=None, debug=False):
        if grab:
            self.grab = grab
        else:
            self.grab = MyGrab()
        
        self._folders = {}
        self.debug = debug

        self.email, self.password = user, password
        self.login, self.domain = self.email.split('@')

        # авторизируемся
        if cookies:
            if self.debug:
                print 'use cookie'

            self.grab.setup(cookies=cookies)

        self._token = None
        self._authorized = False

    def _login(self):
        if self.debug:
            print 'LOGIN', self.email
        post = {
            'Domain': self.domain,
            'Login': self.login,
            'Password': self.password,
            'new_auth_form': '1',
            'saveauth': '1'
        }
        self.grab.go(self.URL_LOGIN, post=post)

    def _parse_folder_codes(self):
        try:
            self._folders = {
                'inbox': re.findall(r"case\s*(\d+)\s*:\s*url\s*\+\=\s*\'\/inbox\'", self.grab.doc.body)[0],
                'spam': re.findall(r"case\s*(\d+)\s*:\s*url\s*\+\=\s*\'\/spam\'", self.grab.doc.body)[0],
                'sent': re.findall(r"case\s*(\d+)\s*:\s*url\s*\+\=\s*\'\/sent\'", self.grab.doc.body)[0],
                'drafts': re.findall(r"case\s*(\d+)\s*:\s*url\s*\+\=\s*\'\/drafts\'", self.grab.doc.body)[0],
                'trash': re.findall(r"case\s*(\d+)\s*:\s*url\s*\+\=\s*\'\/trash\'", self.grab.doc.body)[0],
            }
        except:
            pass
        if self.debug:
            print 'FOLDERS =', self._folders
        return True if self._folders else False

    def _get_folder_code(self, key):
        if key not in self._folders:
            self._grab_go(self.URL_MSGS_INBOX)
        return self._folders.get(key)

    def _parse_token(self):
        try:
            self._token = re.findall('patron\.updateToken\(\"[^"]+\"\)', self.grab.doc.body)[0].split('"')[1]
        except:
            pass
        if self.debug:
            print 'TOKEN =', self._token
        return True if self._token else False

    def _get_token(self):
        if not self._token:
            self._grab_go(self.URL_MSGS_INBOX)
        return self._token

    def _grab_go(self, *args, **kwargs):
        self.grab.go(*args, **kwargs)

        p = re.compile(r"useremail:\s*'([^']+)'", flags=re.U|re.I)
        
        # проверяем авторизацию
        if p.findall(self.grab.doc.body) == [self.email]:
            self._parse_folder_codes()
            self._parse_token()
            self._authorized = True
            return

        self._login()
        self.grab.go(*args, **kwargs)
        
        # проверяем авторизацию
        if p.findall(self.grab.doc.body) == [self.email]:
            self._parse_folder_codes()
            self._parse_token()
            self._authorized = True
            return

        raise AuthException('not find useremail') 
        
    def get_auth_cookies(self):
        if not self._authorized:
            return False
        return {
            'Mpop': self.grab.get_cookie('Mpop'),
            'sdcs': self.grab.get_cookie('sdcs'),
        }

    def get_inbox_list(self, *args, **kwargs):
        return self._get_messages(self.URL_MSGS_INBOX, *args, **kwargs)

    def get_sent_list(self, *args, **kwargs):
        return self._get_messages(self.URL_MSGS_SENT, *args, **kwargs)

    def get_drafts_list(self, *args, **kwargs):
        return self._get_messages(self.URL_MSGS_DRAFTS, *args, **kwargs)

    def get_spam_list(self, *args, **kwargs):
        return self._get_messages(self.URL_MSGS_SPAM, *args, **kwargs)

    def get_trash_list(self, *args, **kwargs):
        return self._get_messages(self.URL_MSGS_TRASH, *args, **kwargs)

    def _get_messages(self, url, page=None):
        if page is None:
            page = 1
        if page > 1:
            url += '?page=%d' % page
        
        self._grab_go(url)
        body = self.grab.doc.body

        # проверяем есть ли нужные данные
        ps = re.findall(r'arMailRuMessages', body, flags=re.U|re.I)
        if not(ps):
            raise ParseException('not parsed email list')

        # парсим данные
        try: 
            a = re.findall(r'return\s*\[\s*{\s*id', body, flags=re.U|re.I)[0]
            b = re.findall(r'\]\;\s*\}\)\(\)\;', body, flags=re.U|re.I)[0]
        except:
            return [], None    

        part = body[body.index(a):body.index(b)]

        data = '[%s]' % part[part.index('{'):part.rindex('}')+1]
        data = re.sub(r'\"(\d+)\"\|0', r'\1', data)

        rs = re.findall(r'\u\(\"[^"]*\"\)', data)
        for r in rs:
            data = data.replace(r, r[2:-1])

        data = re.sub(r'snippet[^\n]+\n', '', data)

        with open('inbox.json', 'w') as f:
            f.write(data)


        ds = demjson.decode(data)
        messages = []
        for d in ds:
            msg = {
                'id': d['id'],
                'size': d['size'],
                'subject': d['subject'],
                'date': datetime.datetime.fromtimestamp(int(d['date'])),
                'from': [f['email'] for f in d['correspondents']['from']],
                'to': [f['email'] for f in d['correspondents']['to']],
                'cc': [f['email'] for f in d['correspondents']['cc']],
            }
            messages.append(msg)
        return messages, page+1 if ds[-1]['next'] != "" else None

    def get_message(self, id):
        self._grab_go(self.URL_MSG % id)
        
        body = self.grab.doc.body
        try:
            a = re.findall(r'var\s*json\s*=\s*\{', body)[0]
            b = re.findall(r'if\s*\(json', body)[0]
        except:
            return None

        part = body[body.index(a):body.index(b)]
        data = part[part.index('{'):part.rindex('}')+1]
        with open('data.json', 'w') as f:
            f.write(data)

        m = demjson.decode(data)
        return {
            'headers': m['body']['headers'],
            'subject': m['body']['subject'],
            'date': datetime.datetime.fromtimestamp(int(m['body']['date'])),
            'from': [f['email'] for f in m['body']['correspondents']['from']],
            'to': [f['email'] for f in m['body']['correspondents']['to']],
            'cc': [f['email'] for f in m['body']['correspondents']['cc']],
            'html': m['body']['body']['html'],
            'text': m['body']['body']['text'],
        }

    def del_message(self, id):
        post = {
            'ajax_call':'1',
            'x-email':self.email,
            'email':self.email,
            'remove':'1',
            'folder':self._get_folder_code('trash'),
            'id':'["%s"]' % id,
            'ids':'["%s"]' % id,
            'move':'1',
            'ajaxmode':'1',
            'noredir':'1',
            'api':'1',
            'token':self._get_token(),
        }
        self.grab.go(self.URL_MSGS_MOVE, post=post)
        res = demjson.decode(self.grab.doc.body)
        if res['status'] == 200:
            return True
        else:
            if self.debug:
                print res
            return False