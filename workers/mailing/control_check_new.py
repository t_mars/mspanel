#coding=utf8
import sys, os
sys.path.append('/var/www/mspanel/private/workers/')

from multi import Worker,freeze_support
from task_service import *
from main.models import *
from django.utils import timezone
from datetime import timedelta


def find_email(client, func, from_email, subject=None, delete=False):
    _next = None
    while True:
        messages, _next = func(_next)
        for m in messages:
            
            print 'from_email =',from_email,'from =', m['from']
            if from_email.lower() not in m['from']:
                continue

            if subject and subject != m['subject']:
                continue

            if delete:
                print 'delete =', client.del_message(m['id'])

            return m['date']
        
        if _next == None:
            break

    return None

def control_check(client, from_email, delete):
    if find_email(client, client.get_inbox_list, from_email, delete=delete):
        print 'inbox'
        return 'inbox'
    
    if find_email(client, client.get_spam_list, from_email, delete=delete):
        print 'spam'
        return 'spam'

    print 'none'
    return 'none'
    
def work(task):
    id, account, from_email = task
    
    print task

    auth_data = account.get_auth_data()
    
    from lib.mygrab import MyGrab
    from mailru import *
    grab = MyGrab(proxy_config_key='check_mail.proxy', debug=False)
    client = WebClient(account.username, account.password, 
        grab=grab, cookies=auth_data.get('cookies'), debug=True)
    delete = False

    try:
        res = control_check(client, from_email, delete)
        account.auth_data = {
            'tokens': auth_data.get('tokens'),
            'cookies': client.get_auth_cookies()
        }
        account.save()
        return id, res 
    
    except AuthException:
        print 'sd2', 'AuthException'
        return id, 'auth_error'
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print 'sd2', (exc_type, fname, exc_tb.tb_lineno, str(e))
        return id, 'error'

class MyWorker(Worker):
    def init(self, sended_at):
        self.current = 0
        self.cparcels = []
        self.sended_at = sended_at

        qs = ControlParcel.objects \
            .select_related('account', 'parcel') \
            .filter(
                account__is_active=True,
                status__isnull=True, 
                parcel__status=Parcel.SENDED, 
                parcel__sended_at__lte=self.sended_at
            )

        print 'total =', qs.count()
        self.total = qs.count()
        self.cparcels = list(qs)

    def task(self):
        if self.cparcels:
            cp = self.cparcels.pop()
            return cp.id, cp.account, cp.parcel.account.username

    def push(self, results):
        mailing_task_ids = set()
        for id, res in results:
            self.current += 1

            try:
                cp = ControlParcel.objects.get(pk=id)
            except:
                continue

            mailing_task_ids.add(cp.parcel.mailing_task_id)

            if res == 'inbox':
                cp.status = ControlParcel.IN_INBOX
            
            elif res == 'spam':
                cp.status = ControlParcel.IN_SPAM
                # urls = MailingUrl.objects.filter(mailingurlparcel__parcel_id=cp.parcel_id)
                # print 'url =',urls.count(),str(urls.query)
                # for url in urls:
                #     url.spam_count2 += 1
                #     url.save()

            elif res == 'none':
                cp.status = ControlParcel.NOT_COME

            elif res == 'error':
                continue

            elif res == 'auth_error':
                cp.status = ControlParcel.AUTH_ERROR
                cp.account.unactivate(MailRuAccount.UNACTIVATE_CONTROL_CHECK, cp.parcel.mailing_task)
                cp.account.save()

            cp.checked_at = timezone.now()
            cp.save()

            self.service.save_status_string(
                {'current': self.current, 'total': self.total}
            )

        # удалем статистику
        for m in MailingTask.objects.filter(pk__in=mailing_task_ids):
            m.reset_stat('control_delivery')
            m.reset_stat('accounts')
            try:
                if m.mailing.target:
                    m.mailing.target.reset_stat()
            except:
                print 'NO RESET TARGET STAT'
    
if __name__ == '__main__':
    freeze_support()

    time = timezone.now() - timedelta(seconds=Config.GET('check_mail.control_check_time', 300))
    cps = ControlParcel.objects \
        .filter(
            account__is_active=False,
            status__isnull=True, 
            parcel__status=Parcel.SENDED, 
            parcel__sended_at__lte=time,
        )
    cps.update(status=ControlParcel.AUTH_ERROR, checked_at=timezone.now())
    print 'can`t check %d control parcels' % cps.count()

    w = MyWorker(
        10,
        work, 
        step=50, 
        service=get_service(),
        sended_at=time
    )
    w.start()