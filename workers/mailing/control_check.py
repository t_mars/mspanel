#coding=utf8
import sys, os
sys.path.append('/var/www/mspanel/private/workers/')

import imaplib
import getpass
import email
import datetime
import base64
import socks
import socket

from multi import Worker,freeze_support
from task_service import *
from main.models import *
from django.utils import timezone
from datetime import timedelta

IMAP_SERVER = 'imap.mail.ru'

class QueryException(Exception):
    pass

class AuthException(Exception):
    pass

def find_email(box, from_email, subject=None, delete=False):
    code, data = box.search(None, "ALL")
    if code != 'OK':
        raise QueryException

    for num in data[0].split():
        code, data = box.fetch(num, '(RFC822)')
        if code != 'OK':
            raise QueryException
            
        msg = email.message_from_string(data[0][1])
        
        # проверяем откуда
        if from_email.lower() not in msg['From'].lower():
            continue

        # проверяем тему
        if subject and subject != decode(msg['Subject']):
            continue

        date = True
        date_tuple = email.utils.parsedate_tz(msg['Date'])
        if date_tuple:
            date = datetime.datetime.fromtimestamp(email.utils.mktime_tz(date_tuple))
        
        # print 'Message %s: |%s|' % (num, decode(msg['Subject']).decode('utf8'))
        # print 'From:', decode(msg['From'])
        # print 'Raw Date:', date
        
        # удаляем письмо
        # if delete:
            # box.store(num, '+FLAGS', '\\Deleted')

        return date

    return None

def control_check(username, password, from_email):
    box = imaplib.IMAP4_SSL(IMAP_SERVER)
    
    print 'cc1'
    try:
        box.login(username, password)
    except:
        raise AuthException
        
    print 'cc2'
    code, mailboxes = box.list()
    if code != 'OK':
        raise QueryException
    print 'cc3'

    # проверяем входящие
    code,_ = box.select('INBOX')
    if code != 'OK':
        raise QueryException
    res = find_email(box, from_email, delete=True)
    if res:
        return 'inbox'
    print 'cc4'

    # проверяем спам
    spam_code = None
    for s in mailboxes:
        ps = s.split(' ')
        if ps[0] == '(\\Spam)':
            spam_code = ps[2][1:-1]
    
    print 'cc5'
    if not spam_code:
        raise QueryException

    code,_ = box.select(spam_code)
    if code != 'OK':
        raise QueryException
    print 'cc6'

    res = find_email(box, from_email, delete=True)
    if res:
        return 'spam'
    print 'cc7'

    return 'none'

def work(task):
    id, username, password, from_email = task
    print task
    try:
        print 'sd1'
        
        p = Config.GET('check_mail.proxy', rand=True, waiting=True, asdict=True)
        print 'proxy =', p
        if p:
            socks.setdefaultproxy(p['type_int'],p['host'],p['port'])
            socket.socket = socks.socksocket
        
        return id, control_check(username, password, from_email)
    except AuthException:
        print 'sd2', 'AuthException'
    except QueryException:
        print 'sd2', 'QueryException'
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print 'sd2', (exc_type, fname, exc_tb.tb_lineno, str(e))
        return id, 'error'

class MyWorker(Worker):
    def init(self):
        self.current = 0
        self.cparcels = []

    def get_cparcel(self):
        if not self.cparcels:
            qs = ControlParcel.objects \
                .select_related('account', 'parcel') \
                .filter(
                    account__is_active=True,
                    status__isnull=True, 
                    parcel__status=Parcel.SENDED, 
                    parcel__sended_at__lte=time
                )

            print 'total =', qs.count()
            self.total = qs.count()
            self.cparcels = list(qs[:1000])

        if self.cparcels:
            return self.cparcels.pop()

    def task(self):
        cp = self.get_cparcel()
        if cp:
            return cp.id, cp.account.username, cp.account.password, cp.parcel.account.username

    def push(self, results):
        mailing_task_ids = set()
        for id, res in results:
            self.current += 1

            try:
                cp = ControlParcel.objects.get(pk=id)
            except:
                continue

            mailing_task_ids.add(cp.parcel.mailing_task_id)

            if res == 'inbox':
                cp.status = ControlParcel.IN_INBOX
            
            elif res == 'spam':
                cp.status = ControlParcel.IN_SPAM

            elif res == 'none':
                cp.status = ControlParcel.NOT_COME

            elif res == 'error':
                cp.status = ControlParcel.AUTH_ERROR
                cp.account.unactivate(MailRuAccount.UNACTIVATE_CONTROL_CHECK, cp.parcel.mailing_task)
                cp.account.save()

            cp.checked_at = timezone.now()
            cp.save()

            self.service.save_status_string(
                {'current': self.current, 'total': self.total}
            )

        # удалем статистику
        for m in MailingTask.objects.filter(pk__in=mailing_task_ids):
            m.reset_stat('control_delivery')
            m.reset_stat('accounts')

if __name__ == '__main__':
    freeze_support()
    
    time = timezone.now() - timedelta(minutes=15)
    cps = ControlParcel.objects \
        .filter(
            account__is_active=False,
            status__isnull=True, 
            parcel__status=Parcel.SENDED, 
            parcel__sended_at__lte=time,
        )
    cps.update(status=ControlParcel.AUTH_ERROR, checked_at=timezone.now())
    print 'can`t check %d control parcels' % cps.count()

    w = MyWorker(
        1, 
        work, 
        step=10, 
        service=get_service(),
    )
    w.start()