#coding=utf8
import sys, os
sys.path.append('/var/www/mspanel/private/workers/')

from multi import Worker,freeze_support

import poplib, email, string
import re
import socks
import socket

from task_service import *
from main.models import *
from django.utils import timezone
from django.db.models import Q
from datetime import timedelta

from common import find_content

def check_mailier_daemon(_from):
    p = re.compile('MAILER-DAEMON@saddam\d+\.rambler\.ru', flags=re.I)
    for e in _from:
        if 'mailer-daemon@corp.mail.ru' == e.lower():
            return True
        if p.match(e):
            return True
    return False

def work(task):
    account = task

    delete = False
    
    flag = None
    failed_emails = []
    spam_emails = []

    auth_data = account.get_auth_data()

    try:
        if account.site == 0:
            from lib.mygrab import MyGrab
            from mailru import WebClient, AuthException  
            grab = MyGrab(proxy_config_key='check_mail.proxy', debug=False)
            client = WebClient(account.username, account.password, 
                grab=grab, cookies=auth_data.get('cookies'))
        elif account.site == 1:
            from lib.rambler import WebClient, AuthException
            client = WebClient(account, grab_config={'proxy_config_key':'check_mail.proxy'})
            client.enter()

        # читаем сообщения
        _next = None
        while True:
            messages, _next = client.get_inbox_list(_next)
            for m in messages:
        
                if not check_mailier_daemon(m['from']):
                    continue
        
                print 'mailer_mail =',m['from']
                msg = client.get_message(m['id'])
                
                emails = find_content(msg['text'], '>\nTo:', 'Subject')
                print 'emails string=',emails
                if emails:
                    emails = [e.strip().lower() for e in emails.split(',')]
                else:
                    emails =re.findall(r'[\w\.-]+@[\w\.-]+', msg['text'])
                    
                if emails:
                    print 'emails =', emails
                    # неправильные адреса
                    if 'smtp error' in msg['text'].lower():
                        print 'failed'
                        failed_emails.extend(emails)        

                        if delete:
                            client.del_message(m['id'])
                    
                    # спам
                    if 'spam message rejected' in msg['text'].lower():
                        print 'spam'
                        spam_emails.extend(emails)        

                        if delete:
                            client.del_message(m['id'])

            if _next == None:
                break
        flag = True
    except AuthException:
        flag = False
    except Exception as e:
        flag = None
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print (exc_type, fname, exc_tb.tb_lineno, str(e))

    account.auth_data = {
        'tokens': auth_data.get('tokens'),
        'cookies': client.get_auth_cookies()
    }
    account.save()

    print account.id, failed_emails, spam_emails, flag
    return account.id, failed_emails, spam_emails, flag

class MyWorker(Worker):
    def init(self):
        self.count = 0
        self.success = 0
        self.t1 = timezone.now() - timedelta(seconds=Config.GET('check_mail.mailru_spam_check_time', 300))
        self.t2 = timezone.now() - timedelta(seconds=Config.GET('check_mail.rambler_spam_check_time', 300))
        
        qs = MailRuAccount.objects\
            .filter(
                Q(sended_at__lte=self.t1, site=MailRuAccount.MAIL_RU) | Q(sended_at__lte=self.t2, site=MailRuAccount.RAMBLER_RU),
                parcel__is_ban_checked=None,
                parcel__status=Parcel.SENDED
            ) \
            .distinct()
        self.total = qs.count()
        print 'total =', self.total
        self.accounts = list(qs)

    def task(self):
            
        if len(self.accounts):
            account = self.accounts.pop()
            print account.id, account.username, account.password
            return account
        
    def push(self, results):
        mailing_task_ids = set()
        for id, failed_emails, spam_emails, flag in results:
            self.success += 1

            if flag == None:
                continue

            try:
                account = MailRuAccount.objects.get(pk=id)
            except:
                print 'no account'
                continue

            if account.site == MailRuAccount.MAIL_RU:
                sended_at = self.t1
            elif account.site == MailRuAccount.RAMBLER_RU:
                sended_at = self.t2

            parcels = Parcel.objects \
                .filter(
                    sended_at__lte=sended_at, 
                    account=account, 
                    is_ban_checked=None,
                    status=Parcel.SENDED
                )

            for p in parcels:
                mailing_task_ids.add(p.mailing_task_id)
            
            # если не авторизировались помеяаем и идем дальше
            account.checked_at = timezone.now()
            
            if flag == False:
                if parcels.count():
                    p = parcels.order_by('-sended_at')[0]
                    account.unactivate(MailRuAccount.UNACTIVATE_SPAM_CHECK, p.mailing_task)
                    account.save()
                    parcels.update(is_ban_checked=False)
                continue
            
            account.save()        
            
            print account.username
            
            # неправильная почта
            qs = parcels.filter(email__in=failed_emails)
            print u'failed', qs.count()
            qs.update(status=Parcel.INVALID_EMAIL,is_ban_checked=True)

            # не спам
            qs = parcels.exclude(email__in=spam_emails)
            print u'no spam', qs.count()
            qs.update(is_banned=False,is_ban_checked=True)

            # спам
            qs = parcels.filter(email__in=spam_emails)
            # urls = MailingUrl.objects.filter(mailingurlparcel__parcel_id__in=[p.id for q in qs])
            # for url in urls:
            #     url.spam_count1 += 1
            #     url.save()
            print u'spam', qs.count()
            qs.update(is_banned=True,is_ban_checked=True)

            self.service.save_status_string(
                {'success': self.success, 'total': self.total}
            )

        # удалем статистику
        for m in MailingTask.objects.filter(pk__in=mailing_task_ids):
            m.reset_stat('spam')
            m.reset_stat('accounts')
            try:
                if m.mailing.target:
                    m.mailing.target.reset_stat()
            except:
                print 'NO RESET TARGET STAT'
    

if __name__ == '__main__':
    freeze_support()
    time = timezone.now() - timedelta(minutes=5)
    
    # помечаем парсели которые не сможем проверить
    ps = Parcel.objects \
        .filter(
            is_ban_checked=None,
            status=Parcel.SENDED,
            account__sended_at__lte=time,
            account__is_active=False
        )
    ps.update(is_ban_checked=False)
    print 'can`t check %d parcels' % ps.count()

    w = MyWorker(50, work, 
        step=50, 
        service=get_service()
    )
    w.start()