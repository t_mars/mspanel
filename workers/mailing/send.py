#coding=utf8
import sys, os, json
sys.path.append('/var/www/mspanel/private/workers/')
import time
import random

from task_service import get_service, DataSet
from multi import Worker, freeze_support

from django.utils import timezone
from django.conf import settings
from django.db.models import Q
from django.db import transaction, connection

from validate_email import validate_email

from grab.error import GrabTimeoutError, GrabNetworkError, GrabConnectionError

from main.models import *
from sender.models import Email
from lib.mailer.exceptions import AuthException

def work(task):
    proxy_config_key = 'send_mail.proxy'
    try:
        task_id = task['id']
        emails = task['emails']
        account = task['account']
        subject = task['subject']
        body = task['body']
        selenium = task.get('selenium', False)
        
        res = {
            'download_size': 0,
            'request_count': 0,
            'id': task_id,
            'success': 0,
            'failure': 0,
            'pids': {},
            'account_is_active': True,
        }

        print 'task =',task_id, emails.keys()
        print 'task =',task_id, 'account =', (account.username, account.password)
        
        try:
            # авторизируемся
            if account.site == MailRuAccount.MAIL_RU:
                if selenium:
                    from lib.mailru import BrowserClient as Sender
                    s = Sender(account, debug=True, proxy_config_key=proxy_config_key)
                else:
                    from mailru import Sender
                    s = Sender(account, grab_config={'proxy_config_key':proxy_config_key})

            elif account.site == MailRuAccount.RAMBLER_RU:
                from lib.rambler import WebClient as Sender
                s = Sender(account, grab_config={'proxy_config_key':proxy_config_key})
            
            print 'task =',task_id, 'sender inited'
            # отправляем
            r = s.send(emails.keys(), subject, body)
            if hasattr(s, 'grab'):
                res['download_size'] = s.grab.download_size
                res['request_count'] = s.grab.request_count
            print 'task =',task_id, 'sended!'

        except AuthException as e:
            print 'task =',task_id, 'AuthException', str(e)
            res['account_is_active'] = False
            for email, pid in emails.items():
                res['pids'][pid] = Parcel.ACCOUNT_ERROR
            return res

        except GrabTimeoutError:
            for email, pid in emails.items():
                res['pids'][pid] = Parcel.CONN_TIMEOUT
            return res

        except GrabNetworkError:
            for email, pid in emails.items():
                res['pids'][pid] = Parcel.NETWORK_ERROR
            return res

        except GrabConnectionError:
            for email, pid in emails.items():
                res['pids'][pid] = Parcel.CONN_BREAK
            return res

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print 'task =',task_id, 'OtherException', e.__class__.__name__, e, exc_type, fname, exc_tb.tb_lineno
            for email, pid in emails.items():
                res['pids'][pid] = Parcel.UNKNOW_ERROR
            return res

        if hasattr(s, 'get_auth_data'):
            res['auth_data'] = s.get_auth_data()

        # обрабатываем результаты
        if r == False:
            print 'task =',task_id, 'Sending Error'
            # res['account_is_active'] = False
            
            for email, pid in emails.items():
                res['pids'][pid] = Parcel.ACCOUNT_ERROR
            res['comment'] = s.last_error
        else:
            print 'task =',task_id, 'sended 2.'
            print 'task =',task_id, 'r =',r
            res['sended_at'] = timezone.now()
            for email,flag in r.items():
                pid = emails[email]
                if flag:
                    res['pids'][pid] = Parcel.SENDED
                    res['success'] += 1
                else:
                    res['pids'][pid] = Parcel.INVALID_EMAIL
                    res['failure'] += 1
            print 'task =',task_id, 'pids =',res['pids']
        print 'task =',task_id, 'return'
        return res

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        raise Exception(exc_type, fname, exc_tb.tb_lineno, str(e))

class MyWorker(Worker):

    def init(self):
        self.mailing_id = self.service.get_argument('mailing_id')
        self.recipients = self.service.get_argument('recipients')
        self.limit = self.service.get_argument('limit', None)
        self.group_size = self.service.get_argument('group_size', 3)
        self.control_shift = self.service.get_argument('control_shift', 10)
        self.account_log_ids = self.service.get_argument('account_log_ids', [])
        self.site = self.service.get_argument('site', 0)
        self.mailing = Mailing.objects.get(pk=self.mailing_id)
        self.selenium = self.service.get_argument('selenium', False)
        
        mts = MailingTask.objects.filter(task=self.service.task)
        if mts.count() > 0:
            raise Exception(u'Нельзя перезапускать задачу с рассылкой')
        
        mailing_task = MailingTask()
        mailing_task.fake_domain = self.mailing.fake_domain
        mailing_task.recipients = self.recipients
        mailing_task.group_size = self.group_size
        mailing_task.control_shift = self.control_shift
        mailing_task.task = self.service.task
        mailing_task.mailing = self.mailing
        mailing_task.body_type = self.mailing.body_type
        mailing_task.used_account_num = 0
        mailing_task.unactivated_account_num = 0
        mailing_task.thread_count = self.size
        mailing_task.site = self.site
        mailing_task.save()

        print 'mailing_task=',mailing_task.id,'pid=',os.getpid()
        self.mailing_task = mailing_task

        print 'limit=',self.limit
        print 'group_size=',self.group_size
        
        self.domains = []
        self.control_accounts = []
        self.accounts = []
        self.parcels = []

        self.tasks = {}
        self.success = 0
        self.failure = 0
        self.download_size = 0
        self.parcel_ind = 0
        self.email_ind = 0
        self.last_control_ind = None

        if self.recipients == 'target':
            if not self.mailing.target:
                raise Exception('Have no target!!!')
            qs = self.mailing.target.get_emails()
            self.total = qs.count()
        elif self.recipients == 'test':
            emails = self.mailing.to.split(',')
            self.total = len(emails)
        else:
            raise Exception('Unknow recipients value: %s' % self.recipients)

    def get_domain(self):
        if not len(self.domains):
            self.service.save_status_string(
                {'stage': 'get domains'}
            )
            qs = Domain.objects \
                .filter(state=Domain.ACTIVATED) \
                .order_by('mail_count','?')[:1000]
            self.service.save_status_string(
                {'stage': 'getted domains'}
            )
            self.domains = list(qs)[::-1]

            if not len(self.domains):
                raise Exception('no have any domains.')

        return self.domains.pop()

    def get_control_account(self):
        if not len(self.control_accounts):
            qs = MailRuAccount.objects \
                .filter(is_active=True,site=MailRuAccount.MAIL_RU)
            
            qs = qs.order_by('?')[:1000]
            
            self.control_accounts = list(qs)[::-1]

            if not len(self.control_accounts):
                raise Exception('no have any control accounts.')

        return self.control_accounts.pop()

    def get_account(self):
        if not len(self.accounts):
            self.service.save_status_string(
                {'stage': 'get accounts'}
            )
            qs = MailRuAccount.objects \
                .filter(is_active=True,site=self.site)
            
            if self.account_log_ids:
                qs = qs.filter(task_log_id__in=self.account_log_ids)

            print 'accounts count=', qs.count()
            qs = qs.order_by('mail_count','?')[:1000]
            
            self.service.save_status_string(
                {'stage': 'getted accounts'}
            )
            self.accounts = list(qs)[::-1]

            if not len(self.accounts):
                raise Exception('no have any accounts.')

        return self.accounts.pop()

    def get_parcel(self):
        # контрольное письмо
        if self.control_shift and (self.email_ind + 1) % self.control_shift == 0 and self.last_control_ind != self.email_ind:
            self.last_control_ind = self.email_ind
            a = self.get_control_account()

            p = Parcel()
            p.mailing_task = self.mailing_task
            p.email = a.username
            p.is_control = True
            p.save()

            cp = ControlParcel()
            cp.parcel = p
            cp.account = a
            cp.save()
            
            p.email = cp.account.username
            p.save()
            
            self.parcel_ind += 1
            return p

        if not len(self.parcels):
            # если таких нет созадем новые
            if self.recipients == 'target':
                if not self.mailing.target:
                    raise Exception('Have no target!!!')
                count = min(self.limit - self.email_ind, self.step)
                if count > 0:
                    self.parcels = self.mailing.target.create_parcels(self.mailing_task, count)
            
            # тестовая рассылка
            elif self.recipients == 'test':
                if self.email_ind == 0:
                    emails = self.mailing.to.split(',')
                    for email in emails:
                        p = Parcel()
                        p.mailing_task = self.mailing_task
                        p.email = email
                        p.save()
                        self.parcels.append(p)
            else:
                raise Exception('Unknow recipients value: %s' % self.recipients)

        if len(self.parcels):
            self.parcel_ind += 1
            self.email_ind += 1
            return self.parcels.pop()

    def finish(self):
        self.mailing_task.finished_at = timezone.now()
        self.mailing_task.save()

        # помечаем невалидные почты
        qs = Parcel.objects \
            .filter(mailing_task=self.mailing_task) \
            .filter(status=Parcel.INVALID_EMAIL)
        parcels = DataSet(qs)

        total = len(parcels)
        current = 0
        for p in parcels:
            email = Email.objects \
                .filter(email=p.email) \
                .update(is_valid=False)

            current += 1
            self.service.save_status_string(
                {'stage': 'make invalid emails', 'total': total, 'current': current}
            )

        # помечаем валидные почты
        qs = Parcel.objects \
            .filter(mailing_task=self.mailing_task) \
            .filter(status=Parcel.SENDED)
        parcels = DataSet(qs)

        total = len(parcels)
        current = 0
        for p in parcels:
            email = Email.objects \
                .filter(email=p.email) \
                .update(is_valid=True)

            current += 1
            self.service.save_status_string(
                {'stage': 'make valid emails', 'total': total, 'current': current}
            )

        # сброс статистики
        self.mailing_task.reset_stat('counts')
        self.mailing_task.reset_stat('spam')
        self.mailing_task.reset_stat('control_delivery')
        self.mailing_task.reset_stat('accounts')
        if self.recipients == 'target':
            self.mailing.target.reset_stat()

    def check_spam_limit(self):

        # проверяем спам
        parcels = self.mailing_task.parcels.filter(status=Parcel.SENDED)
        total = parcels.count()
        
        # Остановка должна происходить только если отправлено больше 1000 писем
        if total < 1000:
            return

        max_spam_percent = Config.GET('send_mail.max_spam_percent', 35)

        spam = parcels.filter(is_banned=True).count()
        spam_percent = spam / float(total) * 100.0 if total else 0.0
        if spam_percent > max_spam_percent:
            raise Exception('SPAM LIMIT ERROR:MAILCORP return spam percent is %d more than %d' % (spam_percent, max_spam_percent))
        
        # проверяем контрольную рассылку
        cps = ControlParcel.objects \
            .filter(parcel__mailing_task=self.mailing_task) \
            .filter(parcel__status=Parcel.SENDED) \
            .exclude(status=ControlParcel.AUTH_ERROR)
        
        total = cps.count()
        spam = cps.filter(status=ControlParcel.IN_SPAM).count()
        spam_percent = spam / float(total) * 100.0 if total else 0.0
        if spam_percent > max_spam_percent:
            raise Exception('SPAM LIMIT ERROR:Control spam percent is %d more than %d' % (spam_percent, max_spam_percent))

    def task(self):
        group = []
        while len(group) < self.group_size:
            p = self.get_parcel()
            self.service.save_status_string({
                'parcel_ind': self.parcel_ind, 
                'email_ind': self.email_ind,
            })
            
            if not p:
                break

            if not validate_email(p.email):
                p.status = Parcel.INVALID_EMAIL
                p.save()
                continue

            group.append(p)

        if not len(group):
            return None

        domain = self.get_domain()
        account = self.get_account()
        
        # определяем домен
        if self.mailing.fake_domain:
            domain_name = self.mailing.render_macros(self.mailing.fake_domain_pattern)
        else:        
            domain_name = domain.get_link()

        body, subject, url_ids = self.mailing.get_message(domain_name)
        
        task = {
            'id': self.parcel_ind,
            'emails': {p.email:p.id for p in group},
            'account_id': account.id,
            'body': body,
            'subject': subject,
            'account': account,
            'selenium': self.selenium,
            'url_ids': url_ids,
        }
        
        if not self.mailing.fake_domain:
            task['domain_id'] = domain.id

        self.tasks[self.parcel_ind] = task

        return task

    def push(self, results):
        diff = self.time_click()

        for res in results:
            id = res['id']
            task = self.tasks[id]
            self.download_size += res['download_size']
            self.mailing_task.download_size = self.download_size
            self.mailing_task.save()

            account = MailRuAccount.objects.get(pk=task['account_id'])
            if res['account_is_active'] == False:
                account.unactivate(MailRuAccount.UNACTIVATE_SEND, self.mailing_task)
                self.mailing_task.unactivated_account_num += 1
                self.mailing_task.used_account_num += 1
                self.mailing_task.save()

            elif 'sended_at' in res:
                account.mail_count += 1
                account.sended_at = res['sended_at']
                self.mailing_task.used_account_num += 1
                self.mailing_task.save()

            if 'auth_data' in res:
                account.auth_data = res['auth_data']
            account.save()
            
            # использовался домен
            if 'domain_id' in task:
                domain = Domain.objects.get(pk=task['domain_id'])
                if 'sended_at' in res:
                    domain.mail_count += 1
                    domain.save()
            else:
                domain = None
            
            self.success += res['success']
            self.failure += res['failure']
        
            for pid,status in res['pids'].items():
                print 'parcel_id =', pid
                p = Parcel.objects.get(pk=pid)

                p.comment = res.get('comment', '')
                p.status = status
                p.try_count += 1
                if domain:
                    p.domain = domain
                p.account = account
                
                if 'sended_at' in res:
                    p.sended_at = res['sended_at']

                    # сохраняем данные об использовании URL
                    for uid in task['url_ids']:
                        mu = MailingUrl.objects.get(pk=uid)
                        mu.mail_count += 1
                        mu.save()
                        mp = MailingUrlParcel()
                        mp.parcel = p
                        mp.mailing_url_id = uid
                        mp.save()
                p.save()

            self.service.save_status_string(
                {'stage': 'sended', 'total': self.total, 'time': diff,
                'success': self.success, 'failure': self.failure}
            )

        self.tasks = {}
        self.check_spam_limit()
        self.mailing_task.reset_stat('counts')
        self.mailing_task.reset_stat('spam')
        self.mailing_task.reset_stat('accounts')

if __name__ == '__main__':
    freeze_support()
    service = get_service()
    thread_count = service.get_argument('thread_count', 100)
    w = MyWorker(
        thread_count, 
        work, 
        step=thread_count, 
        service=service
    )
    w.start()