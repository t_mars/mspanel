#coding=utf8
import sys, os
sys.path.append('/var/www/mspanel/private/workers/')

from multi import Worker,freeze_support

import poplib, email, string
import re
import socks
import socket

from task_service import *
from main.models import *
from django.utils import timezone
from datetime import timedelta

from common import find_content

server = 'pop.mail.ru'
mailer_mail = 'mailer-daemon@corp.mail.ru'
url_pattern = 'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'

def work(task):
    id, username, password = task
    print task
    p = Config.GET('check_mail.proxy', rand=True, waiting=True, asdict=True)
    # подключаемся к серверу
    flag = True
    # urls = []
    failed_emails = []
    spam_emails = []

    try:
        print 'proxy =', p
        if p:
            socks.setdefaultproxy(p['type_int'],p['host'],p['port'],True)
            socket.socket = socks.socksocket
        
        mailserver = poplib.POP3_SSL(server)
        mailserver.user(username)
        mailserver.pass_(password)
    except:
        flag = False

    if flag:    
        # читаем сообщения
        num = len(mailserver.list()[1])
        for i in reversed(range(num)):
            print 'i =', i, 'num =', num, 0
            message = ""
            msg = mailserver.retr(i+1)
            print 'i =', i, 'num =', num, 0.1, len(msg[1])
            content = string.join(msg[1], "\n")
            
            print 'i =', i, 'num =', num, 1, len(content)
            if 'From: %s' % mailer_mail not in content:
                print 'i =', i, 'num =', num, 1.2
                continue
            print 'i =', i, 'num =', num, 2

            text = find_content(content, 'X-Failed-Recipients:', 'Auto-Submitted: auto-replied')
            if text:
                failed_emails.extend([e.strip().lower() for e in text.split(',')])
            print 'i =', i, 'num =', num, 3
                
            if 'spam message rejected' in content:
                text = find_content(content, '>\nTo:', 'Subject')
                if text:
                    spam_emails.extend([e.strip().lower() for e in text.split(',')])
            print 'i =', i, 'num =', num, 4

            # удаляем письмо
            # mailserver.dele(i+1)
            
            # отковыриваем base64 возвратившееся письмо
            # text = find_content(content, 'Content-Type: text/html; charset=utf-8\nContent-Transfer-Encoding: base64', '----ALT--')
            # if text:
            #     text = text.strip().replace('\n', '')
            #     text = base64.b64decode(text)
            #     urls.extend(re.findall(url_pattern, text))
            #     if urls:
            #         print username, 'urls =', urls
        print 'quit1'
        mailserver.quit()
        print 'quit2'

    return id, failed_emails, spam_emails, flag

class MyWorker(Worker):
    def init(self, sended_at):
        self.sended_at = sended_at
        self.count = 0
        self.success = 0
        self.accounts = []

    def get_account(self):
        if not self.accounts:
            qs = MailRuAccount.objects\
                .filter(
                    sended_at__lte=time,
                    parcel__is_ban_checked=None,
                    parcel__status=Parcel.SENDED
                ) \
                .distinct()
            self.total = qs.count()
            self.accounts = list(qs[:1000])

        if self.accounts:
            return self.accounts.pop()

    def task(self):
        account = self.get_account()
            
        if account:
            print account.id, account.username, account.password
            return account.id, account.username, account.password
        
    def push(self, results):
        mailing_task_ids = set()
        for id, failed_emails, spam_emails, flag in results:
            self.success += 1

            try:
                account = MailRuAccount.objects.get(pk=id)
            except:
                print 'no account'
                continue

            parcels = Parcel.objects \
                .filter(
                    account=account, 
                    sended_at__lte=self.sended_at, 
                    is_ban_checked=None,
                    status=Parcel.SENDED
                )

            for p in parcels:
                mailing_task_ids.add(p.mailing_task_id)
            
            # если не авторизировались помеяаем и идем дальше
            account.checked_at = timezone.now()
            
            if not flag:
                parcels.update(is_ban_checked=False)
                if parcels.count():
                    p = parcels.order_by('-sended_at')[0]
                    account.unactivate(MailRuAccount.UNACTIVATE_SPAM_CHECK, p.mailing_task)
                    account.save()
                continue
            
            account.save()        
            
            print account.username
            print u'failed', parcels.filter(email__in=failed_emails).count()
            print u'no spam', parcels.exclude(email__in=spam_emails).count()
            print u'spam', parcels.filter(email__in=spam_emails).count()

            # неправильная почта
            parcels.filter(email__in=failed_emails) \
                .update(status=Parcel.INVALID_EMAIL,is_ban_checked=True)
            
            # не спам
            parcels.exclude(email__in=spam_emails,is_banned=None) \
                .update(is_banned=False,is_ban_checked=True)

            # спам
            parcels.filter(email__in=spam_emails,is_banned=None) \
                .update(is_banned=True,is_ban_checked=True)

            self.service.save_status_string(
                {'success': self.success, 'total': self.total}
            )

        # удалем статистику
        for m in MailingTask.objects.filter(pk__in=mailing_task_ids):
            m.reset_stat('spam')
            m.reset_stat('accounts')

if __name__ == '__main__':
    freeze_support()
    time = timezone.now() - timedelta(minutes=15)
    
    # помечаем парсели которые не сможем проверить
    ps = Parcel.objects \
        .filter(
            is_ban_checked=None,
            status=Parcel.SENDED,
            account__sended_at__lte=time,
            account__is_active=False
        )
    ps.update(is_ban_checked=False)
    print 'can`t check %d parcels' % ps.count()

    w = MyWorker(10, work, 
        step=10, 
        service=get_service(),
        sended_at=time,
    )
    w.start()