import base64

def find_content(content, begin, end):
    begin_pos = content.find(begin)
    if begin_pos >= 0:
        end_pos = content.find(end, begin_pos+len(begin))
        return content[begin_pos+len(begin):end_pos]

    return None

def decode(s):
    r = ''
    for p in s.split(' '):
        if p.startswith('=?UTF-8?B?'):
            s = s.replace(p, base64.b64decode(p[10:]))

    return s