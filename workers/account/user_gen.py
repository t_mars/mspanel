import random
import string
import datetime
import os

class UserGenerator:
    def __init__(self):
        self.root_dir = os.path.dirname(os.path.abspath(__file__))
        d = os.path.join(self.root_dir, 'data')

        self.data = {
            'male': {
                'lastname': [l.strip() for l in open(os.path.join(d, 'fm.txt'))],
                'firstname': [l.strip() for l in open(os.path.join(d, 'im.txt'))],
                'middlename': [l.strip() for l in open(os.path.join(d, 'om.txt'))],
            },
            'female': {
                'lastname': [l.strip() for l in open(os.path.join(d, 'ff.txt'))],
                'firstname': [l.strip() for l in open(os.path.join(d, 'if.txt'))],
                'middlename': [l.strip() for l in open(os.path.join(d, 'of.txt'))],
            }
        }

        now = datetime.date.today()
        self.min_bdate = now - datetime.timedelta(days=55*365.25)
        self.max_bdate = now - datetime.timedelta(days=18*365.25)

    def _password(self):
        l = random.randrange(8,12)
        ss = string.ascii_uppercase + \
            string.ascii_lowercase + \
            string.digits + \
            '!@#$%^&*()-_+=;:,./?\|`~[]{}'
        return ''.join(random.choice(ss) for _ in range(l))

    def _birthdate(self):
        shift = (self.max_bdate - self.min_bdate).total_seconds() * random.random()
        return self.min_bdate + datetime.timedelta(seconds=shift)
        
    def get(self):
        sex = random.choice(self.data.keys())
        birthdate = self._birthdate()

        return {
            'sex': sex,
            'firstname': random.choice(self.data[sex]['firstname']),
            'middlename': random.choice(self.data[sex]['middlename']),
            'lastname': random.choice(self.data[sex]['lastname']),
            'birthdate': birthdate,
            'birthdate_day': '{d.day}'.format(d=birthdate),
            'birthdate_month': '{d.month}'.format(d=birthdate),
            'birthdate_year': '{d.year}'.format(d=birthdate),
            'password': self._password(),
        }