#coding=utf8
import sys, os
sys.path.append('/var/www/mspanel/private/workers/')

from datetime import timedelta
import json

from django.utils import timezone
from django.db.models import Q

from multi import Worker,freeze_support

from task_service import *
from mailing.mailru import *

from main.models import *

from grab.error import GrabTimeoutError, GrabNetworkError, GrabConnectionError

def work(task):
    account = task
    s = Sender(account, grab_config={'proxy_config_key':'send_mail.proxy'})
    try:
        data = s.get_auth_data()
        return account.id,data
            
    except AuthException as e:
        return account.id, False

    except GrabConnectionError:
        return account.id, None
        
    except GrabNetworkError:
        return account.id, None
        
    except GrabConnectionError:
        return account.id, None

class MyWorker(Worker):
    def init(self):
        self.total = 0
        self.success = 0
        self.error = 0
        self.current = 0
        self.retry = 0

        qs = MailRuAccount.objects \
                .filter(site=0, auth_data=None) \
                .filter(is_active=True)
        self.total = qs.count()
        print 'total =',self.total
        self.accounts = list(qs[:10000])

    def get_account(self):
        if self.accounts:
            return self.accounts.pop()

    def put_status(self):
        self.service.save_status_string(
            {'current': self.current, 'total': self.total,
            'success': self.success, 'error': self.error, 'retry': self.retry}
        )

    def task(self):
        a = self.get_account()
        if a:
            self.current += 1
            self.put_status()    
            return a
        
    def push(self, results):
        for id,res in results:
            self.put_status()    
            print 'success=%d, error=%d, total=%d' % (self.success, self.error, self.total)
            print id, res
            try:
                account = MailRuAccount.objects.get(pk=id)
            except:
                continue

            if res == False:
                self.error += 1
                account.is_active = False
                account.save()
                continue
            
            if res == None:
                self.retry += 1
                continue
            
            self.success += 1

            account.auth_data = res
            account.save()

            

if __name__ == '__main__':
    freeze_support()
    
    w = MyWorker(
        50, 
        work,
        service=get_service(),
        step=100
    )
    w.start()