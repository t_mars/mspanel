#coding=utf8
import sys
sys.path.append('/var/www/mspanel/private/workers/')

from task_service import get_service
from multi import Worker, freeze_support

from user_gen import UserGenerator
from main.models import *
from lib.rambler import Reg

def work(task):
    try:
        user, kwargs = task
        reg = Reg(**kwargs)
        return reg.reg(user), reg.get_stat()
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print (exc_type, fname, exc_tb.tb_lineno, str(e))
        return None, None

class MyWorker(Worker):

    def init(self):
        self.user_gen = UserGenerator()
        
        self.limit = self.service.get_argument('limit')
        self.lost = self.limit
        self.current = 0
        self.try_count = 0

        self.error_count = 0
        self.error_limit = Config.GET('account_reg.error_limit', 100)
        
        self.min_pause = Config.GET('account_reg.min_pause', None)
        self.max_pause = Config.GET('account_reg.max_pause', None)
        self.download_size = 0

        self.captcha_idents = self.service.get_argument('captcha_idents')
        if not self.captcha_idents:
            raise Exception('captcha identifiers not selected')
        
        self.counts = {
            'user_auth_data': 0
        }

    def put_status(self):
        self.service.save_status_string({
            'limit': self.limit,
            'current': self.current,
            'lost': self.lost,
            'try_count': self.try_count,
            'counts': self.counts,
            'error_count': self.error_count,
            'error_limit': self.error_limit,
            'download_size': self.download_size,
        })

    def task(self):
        if self.limit:
            if self.current >= self.lost:
                return

            if self.lost <= 0:
                return

        self.current += 1
        self.try_count += 1
        self.put_status()

        user = self.user_gen.get()

        kwargs = {
            'names': self.captcha_idents,
            'grab_config': {
                'proxy_config_key': 'account_reg.proxy',    
            },
        }
        return user, kwargs

    def push(self, results):
        captcha_zero_balance = False
        for account, stat in results:
            
            if account:
                self.error_count = 0
                self.lost -= 1

                self.inc_count('user')
                if account.auth_data:
                    self.inc_count('user_auth_data')
                if account.has_my:
                    self.inc_count('has_my')
                
                account.site = 1
                account.task_log = self.service.task.log
                account.save()
            
            if stat:
                self.download_size += stat['download_size']
            
            self.put_status()

        self.current = 0
        if captcha_zero_balance:
            raise Exception('Captcha Zero Balance.')

        if self.error_count >= self.error_limit:
            raise Exception('Error limit.')

    def inc_count(self, k, v=1):
        if k not in self.counts:
            self.counts[k] = 0
        self.counts[k] += v
        print 'inc_count', k, self.counts[k] 

    def finish(self):
        self.put_status()

if __name__ == '__main__':
    freeze_support()
    
    service = get_service()
    thread_count = service.get_argument('thread_count', 100)
    
    w = MyWorker(
        thread_count,
        work,
        step=thread_count,
        service=service,
        auto_status=False
    )
    w.start()