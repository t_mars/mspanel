import sys
sys.path.append('/var/www/mspanel/private/workers/')

from task_service import get_service
from multi import Worker, freeze_support

from lib.rambler import Reg
from user_gen import UserGenerator

if __name__ == '__main__':
    g = UserGenerator()
    u = g.get()

    r = Reg()
    
    print r.reg(u['firstname'], u['lastname'], u['password'], u['birthdate'], u['sex'])