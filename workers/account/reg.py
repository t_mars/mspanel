#coding=utf8
import random
import os
import datetime
import time
import string
import urllib
import json
import sys
import logging
import re
from unidecode import unidecode
import urllib2

sys.path.append('/var/www/mspanel/private/workers/')

from task_service import get_service
from multi import Worker, freeze_support

from mailing.mailru import *

from main.models import *
from sender.models import Email

from antigate import AntiGate, AntiGateError

from grab import Grab
from grab.spider import Task, Spider
from grab.error import GrabTimeoutError, GrabNetworkError, GrabConnectionError

class ConnException(Exception):
    pass

class ZeroBalanceException(Exception):
    pass

def randstr():
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))

def ident_captcha_antigate(filename, key):
    try:
        val = AntiGate(key, filename)
        return True, val
    except AntiGateError as e:
        return False, str(e)

def ident_captcha_rucaptcha(filename, key):
    try:
        val = AntiGate(key, filename, domain='rucaptcha.com')
        return True, val
    except AntiGateError as e:
        return False, str(e)

class Reg:
    domains = ['mail.ru', 'inbox.ru', 'bk.ru', 'list.ru']

    def __init__(self, user, log_dir, mail, captcha_idents, pause, reg_my_mode):
        self.user = user
        self.log_dir = log_dir
        self.mail = mail
        self.captcha_idents = captcha_idents
        self.pause = pause
        self.reg_my_mode = reg_my_mode

    def inc_count(self, key):
        if key not in self.res['counts']:
            self.res['counts'][key] = 0
        self.res['counts'][key] += 1

    def ident_inc_count(self, name, key):
        if key not in self.res['idents'][name]:
            self.res['idents'][name][key] = 0
        self.res['idents'][name][key] += 1

    def prepare(self):
        self.res = {
            'ds': 0, # download size
            'counts': {}, # общая статистика
            'idents': {k:{} for k in self.captcha_idents.keys()}, # статистика по распознованиям капчи
        }

        self.grab = MyGrab(proxy_config_key='account_reg.proxy')
        
    def reg(self):
        self.prepare()
        return self.work(), self.res

    def get_field_names(self):
        try:
            fn = {
                'firstname': self.grab.doc.select('//div[contains(@class,"qc-firstname-row")]/label').attr('for'),
                'lastname': self.grab.doc.select('//div[contains(@class,"qc-lastname-row")]/label').attr('for'),
                'sex': self.grab.doc.select('//div[contains(@class,"qc-sex-row")]//input[@id="man1"]').attr('name'),
                'login': self.grab.doc.select('//div[contains(@class,"qc-login-row")]/label').attr('for'),
                'password': self.grab.doc.select('//div[contains(@class,"qc-pass-row")]/label').attr('for'),
                'retype_password': self.grab.doc.select('//div[contains(@class,"qc-passverify-row")]/label').attr('for'),
                'mail': self.grab.doc.select('//div[contains(@class,"qc-mail-row")]/label').attr('for'),
                'captcha': self.grab.doc.select('//div[contains(@class,"qc-captcha-answer-row")]//input').attr('name'),
            }

            bd_day = self.grab .doc.select('//select[contains(@class,"qc-select-day")]')
            if not bd_day:
                bd_day = self.grab .doc.select('//input[contains(@class,"days")]')

            bd_month = self.grab .doc.select('//select[contains(@class,"qc-select-month")]')
            if not bd_month:
                bd_month = self.grab .doc.select('//select[contains(@class,"months")]')

            bd_year = self.grab .doc.select('//select[contains(@class,"qc-select-year")]')
            if not bd_year:
                bd_year = self.grab .doc.select('//input[contains(@class,"years")]')

            fn['bd_day'] = bd_day.attr('name')
            fn['bd_month'] = bd_month.attr('name')
            fn['bd_year'] = bd_year.attr('name')
            self.fn = fn
            return True

        except Exception as e:
            print 'ggg'
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print (exc_type, fname, exc_tb.tb_lineno, str(e))

            return False

    def get_login(self, reg_id):
        domain = random.choice(self.domains)
        login = unidecode(self.user['firstname'].decode('utf8') + '.' \
            + self.user['lastname'].decode('utf8')).lower()
        
        data = {
            'RegistrationDomain': domain,
            'Signup_utf8': '1',
            'LANG': 'ru_RU',
            'x_reg_id': reg_id,
            self.fn['firstname']: self.user['firstname'],
            self.fn['lastname']: self.user['lastname'],
            self.fn['bd_day']: self.user['birthdate_day'],
            self.fn['bd_month']: self.user['birthdate_month'],
            self.fn['bd_year']: self.user['birthdate_year'],
            self.fn['sex']: self.user['sex'],
            self.fn['login']: login,
        }

        # берем логин и домен которые предлагает mail.ru
        self.grab.go('https://e.mail.ru/cgi-bin/checklogin', post=data)

        logins = [l for l in self.grab.doc.body.split('\n') if '@' in l]
        if logins:
            self.inc_count('yes_login')
            return logins[0].split('@')
        self.inc_count('no_login')
        return login, domain

    def identify_captcha(self, filename):
        idents = self.captcha_idents.items()
        random.shuffle(idents)
        res = None
        for name, key in idents:
            self.ident_inc_count(name, 'try_count')
            
            flag, res = globals()["ident_captcha_" + name](filename, key)
            if flag:
                return name, res
            else:
                self.ident_inc_count(name, res)
        
        if res == 'ERROR_ZERO_BALANCE':
            raise ZeroBalanceException()

        return name, False

    def get_captcha(self, reg_id, captcha_url):
        # распознаем капчу
        captcha_filename = os.path.join(self.log_dir, '%s.jpg' % randstr())
        print 'captcha_filename =',captcha_filename
        for i in xrange(3):
            self.grab.go(captcha_url, log_file=captcha_filename)

            try:
                ident, captcha_value = self.identify_captcha(captcha_filename)
            
            except ZeroBalanceException:
                # закончился баланс на всех
                self.inc_count('zero_balance')
                return False
            
            except Exception as e:
                # другое какое то исключение
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print 'captcha exception', exc_type, fname, exc_tb.tb_lineno, str(e)
                continue
            
            if not captcha_value:
                continue

            data = {
                'ajax_call': '1',    
                'func_name': 'ajax_check_answer_dual',    
                'data': '["%s","%s"]' % (reg_id, captcha_value),    
                'x-email': '',    
            }
            print 'captcha_value =',captcha_value

            try:
                self.grab.go('https://e.mail.ru/cgi-bin/mail_ajax_img', post=data)
                response = json.loads(self.grab.doc.body)
                print 'response =', response
            except:
                continue

            if response[2] == 0:
                self.ident_inc_count(ident, 'success')
                return captcha_value
            
            self.ident_inc_count(ident, 'failure')
        
        self.inc_count('no_captcha')
        return False

    def send(self, reg_id, captcha_value, login, domain):
        # посылаем запрос регистрации пользователя
        data = {
            'x_reg_id': reg_id,
            'new_captcha':'1',
            'RegistrationDomain': domain,
            self.fn['firstname']: self.user['firstname'],
            self.fn['lastname']: self.user['lastname'],
            self.fn['bd_day']: self.user['birthdate_day'],
            self.fn['bd_month']: self.user['birthdate_month'],
            self.fn['bd_year']: self.user['birthdate_year'],
            self.fn['sex']: self.user['sex'],
            self.fn['login']: login,
            self.fn['password']: self.user['password'],
            self.fn['retype_password']: self.user['password'],
            self.fn['mail']: self.mail,
            self.fn['captcha']: captcha_value,
        }

        # несколько раз пушаем форму, меняя ip
        self.grab.go('https://e.mail.ru/reg?from=main', post=data, log_file=os.path.join(self.log_dir, login+'_out.html'))
        
        # f = open('/var/www/mspanel/private/reg_%s@%s' % (login, domain), 'w+')
        # f.write(self.grab.doc.body)
        # f.close()

        # успешно зареганы    
        url = 'https://e.mail.ru/messages/inbox?newreg=1&sms_reg=1'
        if url in self.grab.doc.body:
            return True

        # ошибка
        error = self.grab.doc.select('//table[@class="oranzhe"]')
        if error and 'IP' in error[0].text():
            if self.grab.proxy:
                urllib2.urlopen("http://136.243.84.194/proxy/stat/?reboot=%s" % self.grab.proxy_dict['port']).read()
            self.inc_count('reg_ban_ip')
        else:
            self.inc_count('no_reg')
        return False

    def reg_my(self):
        try:
            url = self.grab.doc.select('//a[@id="ph_my"]').attr('href')
            
            self.grab.go(url)
        
            mnb = re.findall('"mnb": "\d+"', self.grab.doc.body)[0].split(':')[1][2:-1]
            mna = re.findall('"mna": "\d+"', self.grab.doc.body)[0].split(':')[1][2:-1]

            data = {
                'ajax_call':'1',
                'func_name':'welcome.create_my',
                'mna':mna,
                'mnb':mnb,
            }
            headers = {
                'Referer':'https://my.mail.ru/my/welcome', 
                'X-Requested-With':'XMLHttpRequest'
            }
            self.grab.go('https://my.mail.ru/cgi-bin/my/ajax', post=data, headers=headers)
            
            self.grab.go('https://my.mail.ru/')
            return True
        except:
            return False
    
    def work(self):
        try:
            self.grab.go('https://e.mail.ru/signup?from=main&lang=ru_RU')

            # проверяем обязателен ли телефон
            if not self.grab.doc.select('//a[@id="noPhoneLink"]'):
                self.inc_count('ban_ip')
                return False

            # определяем соответствие полей
            if not self.get_field_names():
                self.inc_count('error_form')
                return False
            reg_id = self.grab.doc.select('//input[@name="x_reg_id"]')[0].attr('value')
            captcha_url = 'https://' + self.grab.doc.select('//div[contains(@class,"qc-captcha-swa-row")]//img')[0].attr('src')[2:]

            # ждем некоторое время
            print 'pause =', self.pause
            if self.pause is not None:
                time.sleep(self.pause)
            
            login, domain = self.get_login(reg_id)

            captcha_value = self.get_captcha(reg_id, captcha_url)
            self.grab.setup(log_file='out.html')
            if not captcha_value:
                return False

            if not self.send(reg_id, captcha_value, login, domain):
                return False

            self.res['user'] = {
                'username': '%s@%s' % (login, domain),
                'password': self.user['password'],
                'birthdate': self.user['birthdate'],
                'firstname': self.user['firstname'],
                'lastname': self.user['lastname'],
                'sex': self.user['sex'],
                'auth_data': None,
                'has_my': False,
            }

            # сохраняем данные авторизации
            try:
                self.grab.go('https://e.mail.ru/messages/inbox?newreg=1&sms_reg=1')
                auth_flag = True
            except:
                auth_flag = False
            
            # регистрация мой мир   
            if auth_flag:
                if self.reg_my_mode == 1 or self.reg_my_mode == 2 and random.randrange(2):
                    self.res['user']['has_my'] = self.reg_my()
                    
            if auth_flag:
                cookies = {
                    'Mpop': self.grab.get_cookie('Mpop'),
                    'sdcs': self.grab.get_cookie('sdcs'),
                }
                s = Sender(self.res['user']['username'], self.res['user']['password'], auth_cookies=cookies, grab=self.grab)
                try:
                    self.res['user']['auth_data'] = {
                        'tokens': s.prepare(),
                        'cookies': cookies
                    } 
                except:
                    pass

            return True

        except GrabTimeoutError:
            self.inc_count('net_error')
            return False
            
        except GrabNetworkError:
            self.inc_count('net_error')
            return False
            
        except GrabConnectionError:
            self.inc_count('net_error')
            return False

        except ConnException:
            self.inc_count('net_error')
            return False

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            raise Exception(exc_type, fname, exc_tb.tb_lineno, str(e))

def work(task):
    try:
        # генерируем пользователя
        user, log_dir, mail, captcha_idents, pause, reg_my_mode = task
        r = Reg(user, log_dir, mail, captcha_idents, pause, reg_my_mode)
        return r.reg()

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        raise Exception(exc_type, fname, exc_tb.tb_lineno, str(e))

class MyWorker(Worker):

    def init(self):
        self.limit = self.service.get_argument('limit')
        self.try_count = 0
        self.current = 0
        self.lost = self.limit

        self.root_dir = os.path.dirname(os.path.abspath(__file__))
        self.log_dir = os.path.join(self.root_dir, 'log2')
        d = os.path.join(self.root_dir, 'data')
        
        self.error_count = 0
        self.error_limit = Config.GET('account_reg.error_limit', 100)
        
        self.min_pause = Config.GET('account_reg.min_pause', None)
        self.max_pause = Config.GET('account_reg.max_pause', None)
        
        self.download_size = 0
        self.counts = {
            'user': 0,
            'user_auth_data': 0,
        }
        self.mails = []
        self.captcha_try_limit = 3
        self.data = {
            'male': {
                'lastname': [l.strip() for l in open(os.path.join(d, 'fm.txt'))],
                'firstname': [l.strip() for l in open(os.path.join(d, 'im.txt'))],
                'middlename': [l.strip() for l in open(os.path.join(d, 'om.txt'))],
            },
            'female': {
                'lastname': [l.strip() for l in open(os.path.join(d, 'ff.txt'))],
                'firstname': [l.strip() for l in open(os.path.join(d, 'if.txt'))],
                'middlename': [l.strip() for l in open(os.path.join(d, 'of.txt'))],
            }
        }

        now = datetime.date.today()
        self.min_bdate = now - datetime.timedelta(days=55*365.25)
        self.max_bdate = now - datetime.timedelta(days=18*365.25)

        names = self.service.get_argument('captcha_idents')
        if not names:
            raise Exception('captcha identifiers not selected')
        
        
        self.counts_idents = {}
        self.captcha_idents = {}
        for name in names:
            k = 'account_reg.%s_key' % name
            keyval = Config.GET(k)
            if not keyval:
                raise Exception('captcha identifiers not setted in Config: %s' % k)
            self.captcha_idents[name] = keyval
            self.counts_idents[name] = {'try_count': 0}
    
    def _password(self):
        l = random.randrange(8,12)
        ss = string.ascii_uppercase + \
            string.ascii_lowercase + \
            string.digits + \
            '!@#$%^&*()-_+=;:,./?\|`~[]{}'
        return ''.join(random.choice(ss) for _ in range(l))

    def _birthdate(self):
        shift = (self.max_bdate - self.min_bdate).total_seconds() * random.random()
        return self.min_bdate + datetime.timedelta(seconds=shift)
        
    def _user(self):
        sex = random.choice(self.data.keys())
        birthdate = self._birthdate()

        return {
            'sex': '1' if sex == 'male' else '2',
            'firstname': random.choice(self.data[sex]['firstname']),
            'middlename': random.choice(self.data[sex]['middlename']),
            'lastname': random.choice(self.data[sex]['lastname']),
            'birthdate': birthdate,
            'birthdate_day': '{d.day}'.format(d=birthdate),
            'birthdate_month': '{d.month}'.format(d=birthdate),
            'birthdate_year': '{d.year}'.format(d=birthdate),
            'password': self._password(),
        }

    def _mail(self):
        if not self.mails:
            qs = Email.objects \
                .filter() \
                .order_by('?') \
                .values_list('email', flat=True)[:100]

            if not qs.count():
                raise Exception('Not have email.')

            self.mails = list(qs)

        m = self.mails.pop().split('@')[0]
        d = random.choice(['gmail.com', 'ya.ru', 'yandex.ru', 'rambler.ru'])
        
        return '%s@%s' % (m, d)
    
    def put_status(self):
        self.service.save_status_string({
            'limit': self.limit,
            'try_count': self.try_count,
            'current': self.current,
            'lost': self.lost,
            'counts': self.counts,
            'counts_idents': self.counts_idents,
            'download_size': self.download_size,
            'error_count': self.error_count,
            'error_limit': self.error_limit,
        })

    def task(self):
        if self.limit:
            if self.current >= self.lost:
                return

            if self.lost <= 0:
                return

        self.current += 1
        self.try_count += 1
        self.put_status()

        reg_my_mode = Config.GET('account_reg.reg_my_mode', 0)

        if self.min_pause and self.max_pause:
            pause = random.randint(self.min_pause, self.max_pause)
        else:
            pause = None

        return self._user(), self.log_dir, self._mail(), self.captcha_idents, pause, reg_my_mode

    def push(self, results):
        captcha_zero_balance = False
        for flag, res in results:
            self.download_size += res['ds']
            
            if flag:
                self.error_count = 0

                user = res['user']
                print 'user', user['username']
                
                self.lost -= 1

                ac = MailRuAccount()
                ac.username = user['username']
                ac.password = user['password']
                ac.birthdate = user['birthdate']
                ac.firstname = user['firstname']
                ac.lastname = user['lastname']
                ac.sex = user['sex']
                ac.auth_data = user['auth_data']
                ac.task_log = self.service.task.log
                ac.has_my = user['has_my']
                ac.save()

                self.counts['user'] += 1
                if user['auth_data']:
                    self.inc_count('user_auth_data')
                if user['has_my']:
                    self.inc_count('has_my')

            for k,v in res['counts'].iteritems():
                if k not in ['no_login', 'yes_login']:
                    self.error_count += 1
                if k == 'zero_balance':
                    captcha_zero_balance = True
                print 'error', k
                self.inc_count(k, v)
            
            for name,counts in res['idents'].iteritems():
                for k,v in counts.iteritems():
                    print 'idents error', name, k
                    if k not in self.counts_idents[name]:
                        self.counts_idents[name][k] = 0
                    self.counts_idents[name][k] += v

            self.put_status()

        self.current = 0
        if captcha_zero_balance:
            raise Exception('Captcha Zero Balance.')

        if self.error_count >= self.error_limit:
            raise Exception('Error limit.')

    def inc_count(self, k, v=1):
        if k not in self.counts:
            self.counts[k] = 0
        self.counts[k] += v
        print 'inc_count', k, self.counts[k] 

    def finish(self):
        self.put_status()

if __name__ == '__main__':
    freeze_support()

    # logging.basicConfig(level=logging.DEBUG)

    service = get_service()
    thread_count = service.get_argument('thread_count', 100)
    
    w = MyWorker(
        thread_count,
        work,
        step=thread_count,
        service=service,
        auto_status=False
    )
    w.start()
    
    print 'current =',w.current
    print 'lost =',w.lost
    print 'limit =',w.limit
    for k,v in  w.counts.iteritems():
        print 'count %s = %d' % (k, v)