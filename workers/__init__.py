def django_init():
	sys.path.append('/var/www/mspanel/private/')
	os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mspanel.settings")
	import django
	django.setup()
