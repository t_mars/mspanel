def django_init():
	import django
	import sys
	import os
	sys.path.append('/var/www/mspanel/private/')
	os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mspanel.settings")
	django.setup()
