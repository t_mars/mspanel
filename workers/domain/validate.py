#coding=utf8
import sys
sys.path.append('/var/www/mspanel/private/workers/')

from task_service import get_service, DataSet

from lib.domain import get_dig_ip
from multi import Worker,freeze_support

from django.db.models import Q
from django.utils import timezone

from main.models import Domain
from datetime import timedelta

def work(task):
    id, ip, domain = task
    ips = get_dig_ip(domain, True)
    
    if ips is None:
        res = None
    elif ips == False:
        res = False
    elif len(ips) == 1 and ips[0] == ip:
        res = True
    else:
        res = False
    
    print res, id, domain, ip, ips
    return id, res 

if __name__ == '__main__':

    freeze_support()
    
    class MyWorker(Worker):
        
        def init(self, domains):
            self.domains = domains

        def push(self, results):
            for id, flag in results:
                if flag is None:
                    continue
                d = Domain.objects.get(pk=id)
                d.action_validate(flag)

        def task(self):
            domain = self.domains.pop()

            if domain:
                print domain.id, domain.dns_ip, domain.name
                return domain.id, domain.dns_ip, domain.name

    domains = Domain.objects \
        .filter(state=Domain.ADDED, updated_at__lte=timezone.now() - timedelta(minutes=10)) \
        .select_related('virtual_server') \
        .order_by('updated_at')
    

    w = MyWorker(
        100, 
        work, 
        step=100, 
        service=get_service(), 
        domains=DataSet(domains)
    )
    w.start()