#coding=utf8
import operator
import sys
import time
sys.path.append('/var/www/mspanel/private/workers/')

from task_service import *

def add_domains(server, domains, service):
    if server.manager_type == VirtualServer.MT_ISP:
        isp(server, domains, service)

    elif server.manager_type == VirtualServer.MT_ISP5:
        isp5(server, domains, service)

    elif server.manager_type == VirtualServer.MT_DREAMHOST:
        dreamhost(server, domains, service)

def dreamhost(server, domains, service):
    DOCROOT = 'www/link_encoder'
    api = server.init_api()
    api.debug = True

    total = len(domains)
    added = []
    try_counts = {}
    current = 0

    limit = 25

    # добавляем домены
    for current, domain in enumerate(domains):
        service.save_status_string({'domain': domain.name, 'added': len(added), 'current': current, 'total': total})
        
        print 'domain=%s' % domain.name
        
        domain = Domain.objects.get(pk=domain.id)
        if domain.state != Domain.CHECKED:
            print 'was_added'
            continue
        
        try:
            res = api.add_domain(domain.name, DOCROOT)
        except Exception as e:
            print 'add domain exception: ', str(e)
            continue

        if res:
            added.append(domain)
            try_counts[domain.id] = 0
            print 'ok'
        else:
            domain.action_add(False, server)
            print 'error'

        # пушаем порциями
        if len(added) == limit or current >= len(domains)-1:
            # проверяем добавились ли
            added_count = len(added)
            refreshed = 0
            ind = 0        
            while len(added):
                domain = added.pop()
                print domain.id, domain.name, try_counts[domain.id],
                try_counts[domain.id] += 1 
                ind += 1

                service.save_status_string({'domain': domain.name, 'added': added_count, 
                    'refreshed': refreshed, 'lost': len(added), 'total': total})
                
                ip = api.refresh_ns(domain.name)
                print 'ip =',ip,
                if ip == False:
                    if try_counts[domain.id] <= 10:
                        # снова ставим в очередь
                        added.append(domain)
                        print 'retry'
                    else:

                        print 'no_added'
                        api.delete_domain(domain.name)
                        # помечаем что не удалось добавить
                        domain.action_add(False, server)
                else:
                    print 'yes_added'
                    refreshed += 1
                    domain.action_add(True, server, ip)

                # на последнем задержка
                if ind % added_count == 0:
                    ind = 0
                    service.save_status_string({'stage': 'sleep before refresh', 'added': added_count, 
                        'refreshed': refreshed, 'total': total})
                    time.sleep(10)
    api.close()

def isp5(server, domains, service):
    DOCROOT = '/www/link_encoder'

    api = server.init_api()
    owner = server.manager_login

    total = len(domains)
    added = 0
    current = 0
    added_domains = []
    limit = 5
    
    from lib.mchost import MCHostClient
    mchost = MCHostClient('a151874', 'sm26Mu3A2n71')

    while len(domains):
        current += 1
        domain = domains.pop()
        service.save_status_string({'domain': domain.name, 'added': added, 'current': current, 'total': total})
        
        # создаем основной домен
        res = api.add_domain(domain=domain.name, owner=owner, docroot=DOCROOT)
            
        print 'domain=%s' % domain.name,
        
        if res:
            added_domains.append(domain)
            print 'ok'
            added += 1
        else:
            print 'error', 
            try:
                print api.last_error,
            except:
                try:
                    print api.last_error.encode('utf8'),
                except:
                    pass
            print ''

        if len(added_domains) == limit:
            if mchost.add_mass([d.name for d in added_domains]):
                for d in added_domains:
                    d.action_add(True, server, server.ip)
            added_domains = []

    if len(added_domains):
        if mchost.add_mass([d.name for d in added_domains]):
            for d in added_domains:
                d.action_add(True, server, server.ip)

def isp(server, domains, service):
    ADMIN_EMAIL = 'webmaster@reg.ru'
    DOCROOT = '/www/link_encoder'

    group_size = service.get_argument('group_size', 400)
    api = server.init_api()
    owner = server.manager_login
    ip = server.ip
    
    total = len(domains)
    added = 0
    current = 0
    while len(domains):
        while len(domains):
            current += 1
            domain = domains.pop()
            service.save_status_string({'domain': domain.name, 'added': added, 'current': current, 'total': total})
            
            # создаем основной домен
            kwargs = {
                'domain':domain.name,
                'owner':owner,
                'admin':ADMIN_EMAIL,
                'ip':ip,
                'php':'phpmod',
                'docroot':DOCROOT,
            }
            res = api.www.add(**kwargs)
                
            print 'domain=%s' % domain.name,
            
            domain.action_add(res, server, server.ip)
            if res:
                print 'ok'
                added += 1
                # api.domain.add(domain=domain.name,ip=ip,name='*')
                break
            else:
                print 'error', api.last_error
                
        # создаем поддомены
        # kwargs['elid'] = domain.name
        # aliases_added = []
        # for i in range(group_size):
        #     if not len(domains):
        #         break
        #     current += 1
        #     alias = domains.pop()
        #     aliases_added.append(alias.name)
            
        #     kwargs['alias'] =' '.join(aliases_added)
        #     res = api.www.add(**kwargs)
            
        #     print 'alias=%s' % alias.name,
        #     alias.action_add(res, server, domain)
            
        #     if res:
        #         print 'ok'
        #         added += 1
        #         api.domain.add(domain=alias.name,ip=ip,name='*')
        #     else:
        #         print 'error', api.last_error
        #         aliases_added.remove(alias.name)
        #     service.save_status_string({'alias': alias.name, 'added': added, 'current': current, 'total': total})

if __name__ == '__main__':
    service = get_service()

    server_id = service.get_argument('server_id')
    count = service.get_argument('count')
    
    server = VirtualServer.objects.get(pk=server_id)
    
    domains = Domain.objects.filter(
        name_servers__in=server.get_name_servers(),
        state=Domain.CHECKED
    ).distinct().order_by('?')
    
    print 'all count =',domains.count()
    
    domains = list(domains[:count])
    print 'total count =',len(domains)
    
    if not len(domains):
        print ('Not found domains')
        sys.exit(1)
    
    add_domains(server, domains, service)