#coding=utf8
import sys
sys.path.append('/var/www/mspanel/private/workers/')

from datetime import timedelta
import random
import time
import os

from grab import Grab

from django.utils import timezone
from django.db.models import Q
from django.conf import settings

from task_service import get_service, DataSet
from multi import Worker,freeze_support
from lib.domain import has_no_dns_records, has_no_blocked_google, get_whois_info
from main.models import *

def check_domain(domain, stat, proxy):
    # print 'domain =',domain,'has_no_dns_records'

    # DNS
    t = time.time()
    flag = has_no_dns_records(domain)
    stat['dns'] = (time.time() - t, flag)

    if flag == None:
        return 'DNS check error'

    # Google
    if flag:
        # print 'domain =',domain,'has_no_blocked_google'
        t = time.time()
        flag = has_no_blocked_google(domain, proxy)
        stat['google'] = (time.time() - t, flag)
        
    if flag == None:
        return 'Google check error'

    # WHOIS
    ns_servers,expiry_date = None,None
    if flag:
        try:
            # print 'domain =',domain,'get_whois_info'
            t = time.time()
            ns_servers, expiry_date = get_whois_info(domain, proxy)
            stat['whois'] = (time.time() - t, True)
        except:
            return 'WHOIS check error'

    return flag, ns_servers, expiry_date

def work(task):
    pid = os.getpid()
    print 'pid =', pid, 'start'
    id, domain, proxy = task
    
    flag, ns_servers, expiry_date, stat = None, None, None, {}
    comment = None

    stat = {}
    res = check_domain(domain, stat, proxy)
    
    if isinstance(res, str):
        comment = res
    else:
        flag, ns_servers, expiry_date = res 

    print 'pid =', pid, 'stop'
    return id, flag, ns_servers, expiry_date, stat, comment

if __name__ == '__main__':

    freeze_support()
    
    class MyWorker(Worker):
        def init(self):
            self.count = 0
            self.success = 0
            self.failure = 0
            self.errors = {}
            self.domains = []

        def domain(self):
            if not self.domains:
                qs = Domain.objects \
                    .filter(
                        Q(state=Domain.NEW) | \
                        Q(
                            Q(state=Domain.RETRY_CHECK) & \
                            Q(updated_at__lte=timezone.now() - timedelta(minutes=10))
                        )
                    ) \
                    .order_by('-id')
                    # .filter(created_at__gte=timezone.now()-timedelta(days=1)) \
                    # .order_by('-created_at')
                self.total = qs.count()
                print 'total =',self.total
                self.domains = list(qs[:self.step])

            if self.domains:
                return self.domains.pop()

        def push(self, results):
            cache = {}
            for k in ['dns', 'whois', 'google']:
                cache.update({
                    k+'_time': 0.0,
                    k+'_count': 0,
                    k+'_error': 0,
                    k+'_true': 0,
                    k+'_false': 0,
                    k+'_none': 0,
                })

            for id, flag, ns_servers, expiry_date, stat, comment in results:
                for k,(time, flag) in stat.items():
                    cache[k+'_time'] += time
                    cache[k+'_count'] += 1
                    if flag == True:
                        cache[k+'_true'] += 1
                    elif flag == False:
                        cache[k+'_false'] += 1
                    elif flag == None:
                        cache[k+'_none'] += 1

                if comment == 'DNS check error':
                    cache['dns_error'] += 1
                elif comment == 'Google check error':
                    cache['google_error'] += 1
                elif comment == 'WHOIS check error':
                    cache['whois_error'] += 1

                d = Domain.objects.get(pk=id)
                # print d.name,d.id,comment
                d.action_check(flag, ns_servers, expiry_date, comment)
                self.count += 1
                
                if flag is not None:
                    self.success += 1
                    
                else:
                    self.failure += 1
            
                if not comment:
                    continue
                
                if comment not in self.errors:
                    self.errors[comment] = 0
                self.errors[comment] += 1
            
            for k,v in cache.items():
                Cache.SET('check_'+k, v)
            self.put_status()

        def put_status(self):
            log = self.service.task.log
            started_at, stopped_at = log.get_work_range()
            avg = (stopped_at - started_at).total_seconds() / float(self.count / self.step)
            
            self.service.save_status_string({
                'total': self.total, 
                'count': self.count, 
                'time': self.time_click(), 
                'avg': avg,
                'step': self.step,
                'pool_size': self.size,
                'errors': self.errors,
                'success': '%2.2f' % (self.success / float(self.count) * 100),
                'failure': '%2.2f' % (self.failure / float(self.count) * 100),
            })

        def task(self):
            domain = self.domain()
            
            if domain:
                return domain.id, domain.name, Config.GET('domain_check.proxy', rand=True, asdict=True)

    service = get_service()
    thread_count = service.get_argument('thread_count', 100)
    
    if thread_count < 1000:
        step = 1000
    else:
        step = thread_count * 10

    w = MyWorker(
        thread_count,
        work,
        step=step,
        service=service,
        auto_status=False
    )
    w.start()