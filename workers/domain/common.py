#coding=utf8

def group_domains_by_alias(domains):
    domains = {}
    aliases = {}
    for d in ds:
        if d.alias_to:
            if d.alias_to.id not in aliases:
                aliases[d.alias_to.id] = []
            aliases[d.alias_to.id].append(d)
        else:
            domains[d.id] = d

    res = []
    for did, als in aliases.items():
        res.append( (domains[did], als) )

    return res

# группитруем домены по серверам
def group_domains_by_server(domains):
    domains_by_server = {}
    servers = {}
    for domain in domains:
        if domain.virtual_server.id not in domains_by_server:
            domains_by_server[domain.virtual_server.id] = []
            servers[domain.virtual_server.id] = domain.virtual_server

        domains_by_server[domain.virtual_server.id].append(domain)

    res = []
    for sid, ds in domains_by_server.items():
        res.append( (servers[sid], ds) )
    
    return res