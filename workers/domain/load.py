#coding=utf8
import sys
sys.path.append('/var/www/mspanel/private/workers/')

import os

from task_service import get_service
from main.models import Domain

from django.utils import timezone
from django.db.utils import IntegrityError

def load_domains(filename, service, counts):
    print 'load', filename
    
    with open(filename) as f:
        ind = 0
        for domain in f:
            domain = domain.strip().lower()
            
            # print 'domain =', domain
            counts['total'] += 1

            d = Domain()
            d.name = domain
            d.updated_at = timezone.now()            
            
            try:
                d.save()
                counts['success'] += 1
            except IntegrityError as e:
                _, mes = e
                if mes.startswith('Duplicate entry'):
                    counts['exist'] += 1
                else:
                    counts['error'] += 1
            
            if ind % 100 == 0:
                service.save_status_string(counts)
                # print 'counts =', counts
            ind += 1

    print 'loaded', filename
    
    try:
        os.remove(filename)
        print 'removed', filename
    except:
        print 'NOT removed', filename

if __name__ == '__main__':
    service = get_service()

    dr = service.get_argument('dir', None)
    if not dr or not os.path.exists(dr):
        raise Exception('dir %s not exist' % dr)
    print 'dir =', dr

    print 'id = ', os.popen('id').read()

    files = service.get_argument('files', None)
    if files:
        files = files.split(',')
        print 'files =', files

    fs = []
    for f in os.listdir(dr):
        try:
            fn = os.path.join(dr, f)
        except:
            continue

        if not os.path.isfile(fn):
            continue

        if files != None and f not in files:
            continue
        fs.append(fn)
    print 'file count =', len(fs)

    counts = {
        'total': 0,
        'exist': 0,
        'success': 0,
        'error': 0,
    }
    ind = 0
    for f in fs:
        ind += 1
        counts['file'] = f
        counts['file_ind'] = '%d/%d' % (ind, len(fs))
        load_domains(f, service, counts)

    print counts

