#coding=utf8
import sys
sys.path.append('/var/www/mspanel/private/workers/')

from task_service import get_service, DataSet

from lib.domain import has_no_blocked_google
from multi import Worker,freeze_support
from grab import Grab

from django.db.models import Q
from django.utils import timezone

from main.models import Domain
from datetime import timedelta

def work(task):
    id, domain = task
    res = has_no_blocked_google(domain)
    print id, domain, res
    return id, res

if __name__ == '__main__':

    freeze_support()
    
    class MyWorker(Worker):
        
        def init(self, domains):
            self.domains = domains

        def push(self, results):
            for id, flag in results:
                d = Domain.objects.get(pk=id)
                d.action_activate(flag)

        def task(self):
            domain = self.domains.pop()
            if domain:
                return domain.id, domain.name

    domains = Domain.objects \
        .filter(
            Q(state=Domain.VALIDATED) | \
            Q(state=Domain.ACTIVATED, updated_at__lte=timezone.now() - timedelta(days=1)) # каждые сутки проверяем повторно
        ) \
        .order_by('updated_at')

    w = MyWorker(100, work, step=100, service=get_service(), domains=DataSet(domains))
    w.start()