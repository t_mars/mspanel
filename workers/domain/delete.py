#coding=utf8
import sys
sys.path.append('/var/www/mspanel/private/workers/')

from task_service import *

from django.db.models import Q
from django.utils import timezone

from main.models import Domain
from datetime import timedelta

from common import *


def isp(server, domains, service):
    api = server.init_api()
    deleted = 0
    errors = 0
    total = len(domains)
    for domain in domains:
        print domain.id, domain.name,
        
        # удаление алиаса
        if domain.alias_to:
            print 'alias',
            
            aliases = Domain.objects \
                .filter(virtual_server=server) \
                .filter(alias_to=domain.alias_to, state=Domain.ACTIVATED) \
                .distinct()
            kwargs = {
                'domain':domain.alias_to.name,
                'owner':server.manager_login,
                'admin':ADMIN_EMAIL,
                'ip':server.ip,
                'php':'phpmod',
                'docroot':DOCROOT,
                'alias': ' '.join([d.name for d in aliases])
            }
            res = api.www.add(**kwargs)
            api.domain.delete(domain.name)
            domain.action_delete(True)

            if res:
                print 'ok'
                deleted += 1
            else:
                print 'error', api.last_error
                errors += 1

        else:
            print 'domain',

            res = api.www.delete(domain.name)
            api.domain.delete(domain.name)
            
            domain.action_delete(True)

            if res:
                print 'ok'
                deleted += 1
            else:
                print 'error', api.last_error
                errors += 1

            # перебиваем все алиасы на другой основной
            aliases = Domain.objects \
                .filter(virtual_server=server) \
                .filter(alias_to=domain, state=Domain.ACTIVATED) \
                .order_by('-expiry_date') \
                .distinct()
            
            print 'aliases =',aliases.count()

            if len(aliases) > 0:
                domain, aliases = aliases[0], aliases[1:]
                kwargs = {
                    'domain':domain.name,
                    'owner':server.manager_login,
                    'admin':ADMIN_EMAIL,
                    'ip':server.ip,
                    'php':'phpmod',
                    'docroot':DOCROOT,
                    'alias': ' '.join([d.name for d in aliases])
                }
                res = api.www.add(**kwargs)
                
                domain.alias_to = None
                domain.save()
                for d in aliases:
                    print 'reset alias=%s domain=%s' % (d.name, domain.name)
                    d.alias_to = domain
                    d.save()
            

        service.save_status_string({'domain': domain.name, 'deleted': deleted, 'errors': errors, 'total': total})

def dreamhost(server, domains, service):
    api = server.init_api()
    for domain in domains:
        print domain.id, domain.name, 'domain',
        
        res = api.delete_domain(domain.name)
        domain.action_delete(res)
        
        if res:
            print 'ok'
        else:
            print 'error'

    api.close()

def isp5(server, domains, service):
    api = server.init_api()
    deleted_domains = []
    limit = 5
    from lib.mchost import MCHostClient
    mchost = MCHostClient('a151874', 'sm26Mu3A2n71')
    for domain in domains:
        print domain.id, domain.name, 'domain',
        
        res = api.delete_domain(domain.name)
        
        if res:
            deleted_domains.append(domain)
            print 'ok'
        else:
            print 'error'

        if len(deleted_domains) == limit:
            if mchost.delete_domains([d.name for d in deleted_domains]):
                for domain in deleted_domains:
                    domain.action_delete(True)
            deleted_domains = []

    if len(deleted_domains):
        if mchost.delete_domains([d.name for d in deleted_domains]):
            for domain in deleted_domains:
                domain.action_delete(True)

if __name__ == '__main__':
    service = get_service()

    domains = Domain.objects \
        .filter(virtual_server__isnull=False) \
        .filter(Q(state=Domain.NO_ACTIVATED) | Q(state=Domain.BANNED) | Q(state=Domain.NO_VALIDATED)) \
        .distinct()

    total = domains.count()
    print 'total=', total

    deleted = 0
    errors = 0
    for server, domains in group_domains_by_server(domains):
        
        if server.manager_type == VirtualServer.MT_ISP:
            isp(server, domains, service)

        elif server.manager_type == VirtualServer.MT_ISP5:
            isp5(server, domains, service)

        elif server.manager_type == VirtualServer.MT_DREAMHOST:
            dreamhost(server, domains, service)