#coding=utf8
import sys
sys.path.append('/var/www/mspanel/private/workers/')

from task_service import get_service, DataSet

from django.db.models import Q
from django.utils import timezone

from main.models import Domain
from datetime import timedelta

from common import *

if __name__ == '__main__':
    service = get_service()

    domains = Domain.objects \
        .filter(
            virtual_server__isnull=False, 
            state=Domain.NO_ADDED,
            updated_at__lte=timezone.now()-timedelta(days=1), # раз в сутки пытаемся добавить снова
            added_at__gte=timezone.now()-timedelta(days=3) # 3е суток
        ) \
        .distinct()

    print 'count=', domains.count()

    # добавляем
    for server, domains in group_domains_by_server(domains):
        add_domains(server, domains, service)