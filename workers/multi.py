#coding=utf8
# from multiprocessing import Pool,freeze_support
from multiprocessing.dummy import Pool,freeze_support
from functools import partial
import sys, os
import time

current_time = lambda: time.time()

class Worker:
    def __init__(self, size, work, step=-1, service=None, auto_status=True, *args, **kwargs):
        if 'kill' in sys.argv:
            sys.argv.remove('kill')
            pids = self.pids()
            
            if pids:
                cmd = 'kill -9 %s' % ' '.join(pids)
                print cmd
                os.popen(cmd).read()
            sys.exit(0)

        if 'count' in sys.argv:
            sys.argv.remove('count')
            pids = self.pids()

            print 'count=%d' % len(pids)
            sys.exit(0)
        
        print 'worker:initializing %d thread...' % size
        self.auto_status = auto_status
        self.pool = None
        self.size = size
        self.step = step
        self.work = work
        self.task_count = 0
        if service:
            self.service = service
        self.init(*args, **kwargs)
        self.time = current_time()
        self.tc = current_time()
        print 'worker:initialized.'

    def init(self):
        pass

    def finish(self):
        pass

    def task(self):
        raise Exception('not created Worker.task()')

    def push(self, results):
        pass
        # raise Exception('not created Worker.push()')

    def time_click(self):
        time = current_time()
        diff = time - self.tc
        self.tc = time

        return diff
    
    def pids(self):
        cmd = 'ps -ax | grep "python %s"' % ' '.join(sys.argv)
        print cmd
        res = os.popen(cmd).read().split('\n')
        res = [r.strip().split(' ')[0] for r in res if 'grep' not in r]
        res = [r for r in res if len(r)]
        pid = str(os.getpid())
        if pid in res:
            res.remove(pid)
        return res

    def start(self):
        print 'worker:started'
                
        if not self.pool:
            self.pool = Pool(self.size)

        self._start()
        
        # try:            
        #     self._start()
        # except Exception as e:
        #     self.pool.terminate()
        #     self.pool.join()
        #     print 'error',str(e)

    def _start(self):
        self.time = current_time()
        tasks = []
        ind = 0
        
        while True:
            task = self.task()

            if not task:
                # если есть задачи пушаем и внова идем по циклу
                # прсле пуша могут еще появится новые задания
                if tasks:
                    self._push(tasks)
                    ind = 0
                    tasks = []
                    continue
                else:
                    break
                
            tasks.append(task)
            self.task_count += 1
            ind += 1

            if ind != self.step:
                continue

            self._push(tasks)
            ind = 0
            tasks = []

        self.pool.close()
        self.pool.join()
        del self.pool
        self.finish()

    def _push(self, tasks):
        print 'worker:pushed %d' % self.task_count
        results = self.pool.map(self.work, tasks)
        self.push(results)
        
        time = current_time()
        time_diff = time - self.time
        print 'time=%2.2f' % time_diff
        self.time = time
        
        if hasattr(self, 'service') and self.auto_status:
            self.service.save_status_string({
                'pushed': self.task_count,
                'time': '%2.2f' % time_diff
            })
