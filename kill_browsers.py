import os

cmd = 'ps -ax | grep "Xvfb"'
print cmd

res = os.popen(cmd).read().split('\n')
pids = []
for r in res:
	ps = r.strip().split(' ')
	pid = ps[0]
	try:
		int(pid)
	except:
		continue
	pids.append(pid)

cmd = 'kill %s' % ' '.join(pids)
print cmd
os.popen(cmd)